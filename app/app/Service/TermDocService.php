<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Support\Facades\DB;

class TermDocService
{

    public static function search(array $searchParam): array
    {
        $termDocBuilder = db::table('terms_document')
            ->select(
                DB::raw("string_agg(dcnum::text,',') as id"),
                'itname as ntd_type',
                'dcnumber as ntd_number',
                'dcname as title',
                'dcgod as year',
                'dcprimech as description'
            )
            ->leftJoin("terms_indterm", "terms_document.itnum", "=", "terms_indterm.itnum")
            ->groupBy(['itname', 'dcnumber', 'dcname', 'dcgod', 'dcprimech'])
            ->orderByDesc('dcgod')
            ->orderBy('dcname');
        if (SearchHelper::isNotEmpty($searchParam['ntdType'])) {
            $termDocBuilder->where('itname', 'ilike', SearchHelper::searchString($searchParam['ntdType']));
        }
        if (SearchHelper::isNotEmpty($searchParam['ntdNumber'])) {
            $termDocBuilder->where('dcnumber', 'ilike', SearchHelper::searchString($searchParam['ntdNumber']));
        }
        if (SearchHelper::isNotEmpty($searchParam['title'])) {
            $termDocBuilder->where('dcname', 'ilike', SearchHelper::searchString($searchParam['title']));
        }
        if (SearchHelper::isNotEmpty($searchParam['year'])) {
            $termDocBuilder->where('dcgod', '=', $searchParam['year']);
        }
        $result = $termDocBuilder->get()->toArray();
        if (SearchHelper::isNotEmpty($searchParam['allSearchQuery'])) {
            $result = SearchHelper::allSearchFilter($result, $searchParam['allSearchQuery']);
        }
        return $result;
    }

    public static function getOne($id): array
    {
        $terms = db::table('terms_termin')
            ->distinct()
            ->select("trname       as title",
                "dttext       as definition")
            ->leftJoin("terms_defterm", "terms_termin.trnum", "=", "terms_defterm.trnum")
            ->leftJoin("terms_document", "terms_defterm.dtnum", "=", "terms_document.dtnum")
            ->leftJoin("terms_indterm", "terms_document.itnum", "=", "terms_indterm.itnum")
            ->whereIn("terms_document.dcnum", $id)
            ->get();
        $result = [];
        $index = 0;
        foreach ($terms as $term) {
            $result[] =
                [
                    'title' => 'Термин',
                    'info' => $term->title,
                    'blue' => true,
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => 'Определение',
                    'info' => $term->definition,
                    'index' => $index++,
                ];
        }
        $result[] =
            [
                'title' => "Подробная информация о документе",
                'info' => null,
                'bold' => true,
                'center' => true,
                'index' => $index++,
            ];
        $author = db::table('bibl_authorntdi')
            ->select('ntdi_num',
                db::raw("string_agg(authorntdi_author, '<br/>') as author"))
            ->groupBy(['ntdi_num']);
        $tematika = db::table('bibl_tematika')
            ->select('ntdi_num',
                db::raw("
                COALESCE(json_agg(json_build_object('number', tplanum, 'name', tname) order by tplanum),'[]') as bibl_tema"))
            ->leftJoin('bibl_tematikantdi', 'bibl_tematikantdi.tnum', '=', 'bibl_tematika.tnum')
            ->groupBy(['ntdi_num']);
        $terms_ext = DB::table("bibl_mainntdi")
            ->select(
                "bibl_mainntdi.ntdi_num                                            as id",
                "ntdi_title                                                        as title",
                db::raw("cast(upper(sindex_index || ' ' || sbelong_belong) as varchar(40)) as type_doc"),
                "sbelong_belong                                                    as belong",
                "ntdi_number                                                       as num_doc",
                "author",
                "ntdi_deistvuet                                                    as deistvuet",
                "ntdi_vvoddeistv                                                   as vvod",
                "ntdi_vpervie                                                      as vpervie",
                "ntdi_zamennast                                                    as zamennast",
                "ntdi_history                                                      as history_ntd",
                "ntdi_histfras                                                     as history_ras",
                "ntdi_pubyear                                                      as year_val",
                "ntdi_mestoizdan                                                   as mestoizdan",
                "ntdi_kolstr                                                       as count_page",
                "ntdi_kod                                                          as kod",
                "ntdi_izdatel                                                      as izdatel",
                "ntdi_ext                                                          as ftris",
                "ntdi_ctplanumik                                                   as num",
                "ntdi_ctdatazakik                                                  as data",
                "ntdi_textras                                                      as referat_type",
                "ntdi_primech                                                      as primech",
                "bibl_tema",
                "ntdi_tolerance                                                    as access",
                "ntdi_adrdergat                                                    as adrdergat")
            ->leftJoin("bibl_sindex", "bibl_mainntdi.sindex_num", "=", "bibl_sindex.sindex_num")
            ->leftJoin("bibl_sbelong", "bibl_mainntdi.sbelong_num", "=", "bibl_sbelong.sbelong_num")
            ->leftJoinSub($author, "author", "author.ntdi_num", "=", "bibl_mainntdi.ntdi_num")
            ->leftJoin("bibl_textsntdi", "bibl_mainntdi.ntdi_num", "=", "bibl_textsntdi.ntdi_num")
            ->leftJoinSub($tematika, "tematika", "tematika.ntdi_num", "=", "bibl_mainntdi.ntdi_num")
            ->leftJoin("terms_document", "bibl_mainntdi.ntdi_number", "=", "terms_document.dcnumber")
            ->whereIn("terms_document.dcnum", $id)
            ->first();
        if ($terms_ext) {
            $result[] =
                [
                    'title' => "Наименование",
                    'info' => $terms_ext->title,
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Тип документа",
                    'info' => $terms_ext->type_doc,
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Разработчик",
                    'info' => $terms_ext->author,
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Действует",
                    'info' => $terms_ext->deistvuet,
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Дата введения в действие",
                    'info' => SearchHelper::formatDate($terms_ext->vvod),
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Введен впервые",
                    'info' => SearchHelper::formatDate($terms_ext->vpervie),
                    'index' => $index++,
                ];
            if ($terms_ext->zamennast) {
                $result[] =
                    [
                        'title' => "Обозначение документа, замененного настоящим",
                        'info' => $terms_ext->zamennast,
                        'index' => $index++,
                    ];
            }
            $result[] =
                [
                    'title' => "Год издания",
                    'info' => $terms_ext->year_val,
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Место издания",
                    'info' => $terms_ext->mestoizdan,
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Количество страниц",
                    'info' => $terms_ext->count_page,
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Код",
                    'info' => $terms_ext->kod,
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Издательство",
                    'info' => $terms_ext->izdatel,
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Входит в ФТРиС",
                    'info' => $terms_ext->ftris == 10 ? "Да" : "Нет",
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Номер контракта",
                    'info' => $terms_ext->num,
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Дата контракта",
                    'info' => SearchHelper::formatDate($terms_ext->data),
                    'index' => $index++,
                ];
            $result[] =
                [
                    'title' => "Примечание",
                    'info' => $terms_ext->primech,
                    'index' => $index++,
                ];
            if ($terms_ext->adrdergat) {
                $result[] =
                    [
                        'title' => '<strong>Ссылка на документ</strong>',
                        'info' => SearchHelper::makeWebLink($terms_ext->adrdergat),
                        'index' => ++$index
                    ];
            }
            if ($terms_ext->bibl_tema) {
                $bibl_tema = implode('<br/>', array_map(function ($item) {
                    return $item->number . ' ' . $item->name;
                }, json_decode($terms_ext->bibl_tema)));
                $result[] =
                    [
                        'title' => "Классификация",
                        'info' => $bibl_tema,
                        'index' => $index++,
                    ];
            }
            $uuid = SearchHelper::getUuidFile('ois08/ntdi', $terms_ext->id . "." . trim($terms_ext->referat_type));
            if ($uuid) {
                $result[] = SearchHelper::makeDetail($index,
                    'Скачать документ',
                    SearchHelper::makeDownloadLink($uuid, $terms_ext->referat_type, $terms_ext->id . "." . trim($terms_ext->referat_type)));
            }
        }
        return $result;
    }
}
