<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class TenderService
{

    public static function search(array $searchParam): array
    {
        $tenderBuilder = db::table('ik_niokr_zaiavka')
            ->select(
                "ik_niokr_zaiavka.znum                   as id",
                "sname                          as budget_item",
                "zgod                           as year_plan",
                "ik_niokr_flskonkurs_.znumzakupki        as number_purchase",
                "ik_niokr_flskonkurs_.zdatarazmeshen     as date_purchase",
                "ik_niokr_flskonkurs_.fldatakonk         as date_review",
                "wtpobeda                       as winner",
                "zname                          as name",
                "onamekrat                      as organization",
                "ctplanum                       as number_contract",
                "ctdatazakluch                  as date_contract")
            ->leftJoin("ik_niokr_winnertorg", "ik_niokr_zaiavka.znum", "=", "ik_niokr_winnertorg.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoin("ik_niokr_flskonkurs_", "ik_niokr_zaiavka.flskonknum", "=", "ik_niokr_flskonkurs_.flskonknum")
            ->leftJoin("ik_niokr_organiz as winner", "ik_niokr_ispolnitel.onum", "=", "winner.onum")
            ->leftJoin("ik_niokr_contract", "ik_niokr_zaiavka.znum", "=", "ik_niokr_contract.znum")
            ->where("wtpobeda", "=", true)
            ->where("zstatus", "ilike", "вновь начинаемые")
            ->orderByDesc('year_plan')
            ->orderByDesc('date_contract');

        if (SearchHelper::isNotEmpty($searchParam["budgetItem"])) {
            $budgetItem = $searchParam["budgetItem"];
            $tenderBuilder->join('ik_niokr_statia', function (JoinClause $join) use ($budgetItem) {
                $join->on("ik_niokr_zaiavka.snum", "=", "ik_niokr_statia.snum")
                    ->where('ik_niokr_statia.snum', '=', $budgetItem);
            });
        } else {
            $tenderBuilder->leftJoin("ik_niokr_statia", "ik_niokr_zaiavka.snum", "=", "ik_niokr_statia.snum");
        }

        if (SearchHelper::isNotEmpty($searchParam['yearPlan'])) {
            $tenderBuilder->where("zgod", "=", $searchParam['yearPlan']);
        }
        if (SearchHelper::isNotEmpty($searchParam['numberPurchase'])) {
            $tenderBuilder->where('ik_niokr_flskonkurs_.znumzakupki', 'ilike', SearchHelper::searchString($searchParam['numberPurchase']));
        }
        if (SearchHelper::isNotEmpty($searchParam['numberContract'])) {
            $tenderBuilder->where('ctplanum', 'ilike', SearchHelper::searchString($searchParam['numberContract']));
        }
        if (SearchHelper::isNotEmpty($searchParam['datePurchaseFrom'])) {
            $tenderBuilder->where('ik_niokr_flskonkurs_.zdatarazmeshen', '>=', $searchParam['datePurchaseFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['datePurchaseTo'])) {
            $tenderBuilder->where('ik_niokr_flskonkurs_.zdatarazmeshen', '<=', $searchParam['datePurchaseTo']);
        }

        if (SearchHelper::isNotEmpty($searchParam['singleExecutor'])) {
            $tenderBuilder->where('ik_niokr_flskonkurs_.flonepostav', '=', true);
        }
        if (SearchHelper::isNotEmpty($searchParam['singleParticipant'])) {
            $tenderBuilder->where('ik_niokr_flskonkurs_.floneuchast', '=', true);
        }

//      numberLot: null,

        $result = [];
        foreach ($tenderBuilder->get() as $tender) {
            $result[] = [
                'id' => $tender->id,
                'budgetItem' => $tender->budget_item,
                'yearPlan' => $tender->year_plan,
                'numberPurchase' => $tender->number_purchase,
                'datePurchase' => SearchHelper::formatDate($tender->date_purchase),
                'dateReview' => SearchHelper::formatDate($tender->date_review),
                'name' => $tender->name,
                'organization' => $tender->organization,
                'numberContract' => $tender->number_contract,
                'dateContract' => SearchHelper::formatDate($tender->date_contract)
            ];
        }

        if (SearchHelper::isNotEmpty($searchParam['dateReviewFrom'])) {
            $search = $searchParam['dateReviewFrom'];
            return array_values(array_filter($result, function ($item) use ($search) {
                return strtotime($item['datePurchase']) >= strtotime($search);
            }));
        }
        if (SearchHelper::isNotEmpty($searchParam['dateReviewTo'])) {
            $search = $searchParam['dateReviewTo'];
            return array_values(array_filter($result, function ($item) use ($search) {
                return strtotime($item['datePurchase']) <= strtotime($search);
            }));
        }
        if (SearchHelper::isNotEmpty($searchParam['allSearchQuery'])) {
            $result = SearchHelper::allSearchFilter($result, $searchParam['allSearchQuery']);
        }
        return $result;
    }

    public static function getOne($id): array
    {
        $tender = db::table('ik_niokr_zaiavka')
            ->select("ik_niokr_zaiavka.znum               as id",
                "sname                               as budget_item",
                "ik_niokr_flskonkurs_.znumzakupki    as number_purchase",
                "zlotnum                             as num_date_lot",
                "ik_niokr_flskonkurs_.floneuchast    as single",
                "ik_niokr_flskonkurs_.flonepostav    as single_executor",
                "ik_niokr_actdoc.actype              as act_type",
                "ik_niokr_actdoc.acname              as act_name",
                "ik_niokr_actdoc.acras               as act_file_type",
                "zname                               as title",
                "ik_niokr_flskonkurs_.zdatarazmeshen as date_purchase",
                "ik_niokr_flskonkurs_.fldatakonk     as date_review",
                "ctplanum                            as contract_number",
                "ctdatazakluch                       as contract_date",
                "onamekrat                           as executor",
                "ik_niokr_contract.ctnum             as ctnum",
                "zpriceall                           as winner_price",
                db::raw("coalesce(
                    json_agg(
                        json_build_object(
                            'position', wtpismo,
                            'price', wtzvprice)), '[]')::text as tender"))
            ->leftJoin("ik_niokr_winnertorg", "ik_niokr_zaiavka.znum", "=", "ik_niokr_winnertorg.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoin("ik_niokr_flskonkurs_", "ik_niokr_zaiavka.flskonknum", "=", "ik_niokr_flskonkurs_.flskonknum")
            ->leftJoin("ik_niokr_organiz as winner", "ik_niokr_ispolnitel.onum", "=", "winner.onum")
            ->leftJoin("ik_niokr_statia", "ik_niokr_zaiavka.snum", "=", "ik_niokr_statia.snum")
            ->leftJoin("ik_niokr_contract", "ik_niokr_zaiavka.znum", "=", "ik_niokr_contract.znum")
            ->leftJoin("ik_niokr_actdoc", "ik_niokr_contract.ctnum", "=", "ik_niokr_actdoc.ctnum")
            ->leftJoin("ik_niokr_protokol", "ik_niokr_protokol.znum", "=", "ik_niokr_contract.znum")
            ->where("ik_niokr_zaiavka.znum", "=", $id)
            ->groupBy([
                "ik_niokr_zaiavka.znum",
                "sname",
                "ik_niokr_flskonkurs_.znumzakupki",
                "zlotnum",
                "ik_niokr_flskonkurs_.floneuchast",
                "ik_niokr_flskonkurs_.flonepostav",
                "ik_niokr_actdoc.actype",
                "ik_niokr_actdoc.acname",
                "ik_niokr_actdoc.acras",
                "zname",
                "ik_niokr_flskonkurs_.zdatarazmeshen",
                "ik_niokr_flskonkurs_.fldatakonk",
                "ctplanum",
                "ik_niokr_contract.ctnum",
                "ctdatazakluch",
                "onamekrat",
                "zpriceall"
            ])
            ->first();
        $index = 0;
        $result[] =
            [
                'title' => 'Статья бюджета',
                'info' => $tender->budget_item,
                'index' => $index,
            ];
        $result[] =
            [
                'title' => 'Номер закупки',
                'info' => $tender->number_purchase,
                'index' => ++$index,
            ];
        if ($tender->single) {
            $result[] =
                [
                    'title' => 'Конкурс с единственным участником',
                    'info' => 'Да',
                    'index' => ++$index,
                ];
        }
        if ($tender->single_executor) {
            $result[] =
                [
                    'title' => 'Конкурс с единственным поставщиком',
                    'info' => 'Да',
                    'index' => ++$index,
                ];
        }
        if ($tender->act_type) {
            $filename = $tender->ctnum . "_" . str_replace("/", "-", $tender->act_name) . "." . $tender->act_file_type;
            $actUuid = SearchHelper::getUuidFile('ACTDOC', $filename);
            if ($actUuid) {
                $type = match (trim($tender->act_type)) {
                    '0' => "Акт сдачи работ",
                    '1' => "Дополнительное соглашение",
                    '2' => "Протокол заседания НТС",
                    '3' => "Претензия",
                    '4' => "Решение ФАС",
                    default => "Документ"
                };
                $result[] =
                    [
                        'title' => $type,
                        'info' => SearchHelper::makeDownloadLink($actUuid, $tender->act_file_type),
                        'index' => ++$index,
                    ];
            }
        }
        $result[] =
            [
                'title' => 'Наименование контракта',
                'info' => $tender->title,
                'index' => ++$index,
                'bold' => true
            ];
        $result[] =
            [
                'title' => 'Дата размещения закупки',
                'info' => SearchHelper::formatDate($tender->date_purchase),
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Дата рассмотрения первых частей заявок',
                'info' => SearchHelper::formatDate($tender->date_review),
                'index' => ++$index,
            ];
        $docsUuid = SearchHelper::getUuidFile('KonkDok', $tender->id . '.doc%');
        if ($docsUuid) {
            $result[] =
                [
                    'title' => 'Конкурсная документация',
                    'info' => SearchHelper::makeDownloadLink($docsUuid, 'doc'),
                    'index' => ++$index,
                ];
        }
        $protocolUuid = SearchHelper::getUuidFile('KonkProt', $tender->id . '.doc%');
        if ($protocolUuid) {
            $result[] =
                [
                    'title' => 'Протоколы конкурса',
                    'info' => SearchHelper::makeDownloadLink($protocolUuid, 'doc'),
                    'index' => ++$index,
                ];
        }
        $result[] =
            [
                'title' => 'Номер контракта',
                'info' => $tender->contract_number,
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Дата заключения контракта',
                'info' => SearchHelper::formatDate($tender->contract_date),
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Победитель',
                'info' => $tender->executor,
                'index' => ++$index,
                "bold" => true
            ];
        $result[] =
            [
                'title' => 'Цена победителя',
                'info' => SearchHelper::moneyThousand($tender->winner_price),
                'index' => ++$index,
            ];
        if (!empty(json_decode($tender->tender))) {
            $i = 1;
            foreach (json_decode($tender->tender, true) as $tender_position) {
                $result[] =
                    [
                        'title' => 'Участник закупки №' . $i,
                        'info' => $tender_position['position'],
                        'index' => ++$index,
                    ];
                $result[] =
                    [
                        'title' => 'Цена №' . $i++,
                        'info' => SearchHelper::moneyThousand($tender_position['price']),
                        'index' => ++$index,
                    ];

            }
        }
        return $result;
    }
}
