<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class AssertionService
{

    public static function search(array $searchParam): array
    {
        $bdisTable = db::table('rntd_objectu')
            ->select("cdkontrnum",
                db::raw("coalesce(json_agg(json_build_object(
                                   'title', ounameobject,
                                   'number', ourospatsvidetnum,
                                   'date', ourospatsvidetdate,
                                   'type', ourospatsvidetkategor)), '[]')::text as bdis"))
            ->leftJoin("rntd_commondatekontr", "rntd_commondatekontr.cdnum", "=", "rntd_objectu.cdnum")
            ->groupBy("cdkontrnum");
        $ntdTable = db::table('bibl_mainntdi', 'm')
            ->select("ntdi_ctplanumik",
                "ntdi_ctdatazakik",
                db::raw("coalesce(json_agg(json_build_object('primech', ntdi_primech,
                      'datavvod', ntdi_vvoddeistv,
                      'dataapprov', ntdi_dateapproval,
                      'data', ntdi_ctdatazakik,
                      'num', ntdi_ctplanumik,
                      'title', ntdi_title,
                      'num_doc', cast(ntdi_number as VARCHAR(50)),
                      'ext', ntdi_textras,
                      'type_doc',
                      cast((sindex_index || ' ' || sbelong_belong) as varchar(50)),
                      'deistvuet', ntdi_deistvuet)), '[]')::text as ntd"))
            ->leftJoin("bibl_sbelong as b", "m.sbelong_num", "=", "b.sbelong_num")
            ->leftJoin("bibl_textsntdi as t", "m.ntdi_num", "=", "t.ntdi_num")
            ->leftJoin("bibl_sindex as s", "m.sindex_num", "=", "s.sindex_num")
            ->groupBy("ntdi_ctdatazakik", "ntdi_ctplanumik");
        $assertionBuilder = db::table('ik_niokr_contract')
            ->distinct()
            ->select(
                'ik_niokr_contract.ctnum as id',
                'zgod                    as year',
                'zname                   as title',
                'org_isp.oname           as executor',
                'ctplanum                as number',
                'ctdatazakluch           as date_start',
                'ctdatazaversh           as date_finish',
                'zpriceall               as price',
                'ctsostoianie            as stage',
                'bdis',
                'ntd',
                'npdiscuss',
                'npapproval',
                db::raw("coalesce(json_agg(treferat),'[]')::text as referat")
            )
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_contract.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoin("ik_niokr_zakazchik", "ik_niokr_zaiavka.znum", "=", "ik_niokr_zakazchik.znum")
            ->leftJoin("ik_niokr_statia", "ik_niokr_statia.snum", "ik_niokr_zaiavka.snum")
            ->leftJoin("ik_niokr_organiz as org_zak", "ik_niokr_zakazchik.onum", "=", "org_zak.onum")
            ->leftJoin("ik_niokr_organiz as org_isp", "ik_niokr_ispolnitel.onum", "org_isp.onum")
            ->leftJoin("ik_niokr_ntdproject", "ik_niokr_ntdproject.ctnum", "=", "ik_niokr_contract.ctnum")
            ->leftJoinSub($bdisTable, 'bdis_table', 'bdis_table.cdkontrnum', '=', 'ik_niokr_contract.ctplanum')
            ->leftJoinSub($ntdTable, 'ntd_table', function (JoinClause $join) {
                $join
                    ->on('ntd_table.ntdi_ctdatazakik', '=', 'ik_niokr_contract.ctdatazakluch')
                    ->on("ntd_table.ntdi_ctplanumik", "=", "ik_niokr_contract.ctplanum");
            })
            ->leftJoin("arxiv_tema", function (JoinClause $join) {
                $join
                    ->on('arxiv_tema.ctdatazakik', '=', 'ik_niokr_contract.ctdatazakluch')
                    ->on("arxiv_tema.ctplanumik", "=", "ik_niokr_contract.ctplanum");
            })
            ->where(function ($query) {
                $query->where('zkmain', '=', true)->orWhereNull('zkmain');
            })
            ->where(function ($query) {
                $query->where(function ($subQuery) {
                    $subQuery->where("zgod", "<", date("Y"))
                        ->whereIn("ctsostoianie", ['Завершен', 'Расторгнут']);
                })->orWhere("zgod", ">=", date("Y"));
            })
            ->where('nprealiz', '=', 'НТД')
            ->orderByDesc('zgod')
            ->orderBy('ik_niokr_contract.ctnum')
            ->groupBy("ik_niokr_contract.ctnum",
                "zgod",
                "zname",
                "org_isp.oname",
                "ctplanum",
                "ctdatazakluch",
                "ctdatazaversh",
                "zpriceall",
                "ctsostoianie",
                "bdis",
                "ntd",
                'npdiscuss',
                'npapproval');
        if (SearchHelper::isNotEmpty($searchParam['budgetItem'])) {
            $assertionBuilder->where('sname', 'ilike', SearchHelper::searchString($searchParam['budgetItem']));
        }
        if (SearchHelper::isNotEmpty($searchParam['yearFrom'])) {
            $assertionBuilder->where('zgod', '>=', $searchParam['yearFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['yearTo'])) {
            $assertionBuilder->where('zgod', '<=', $searchParam['yearTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['number'])) {
            $assertionBuilder->where('ctplanum', 'ilike', SearchHelper::searchString($searchParam['number']));
        }
        if (SearchHelper::isNotEmpty($searchParam['condition'])) {
            $assertionBuilder->where('ctsostoianie', 'ilike', SearchHelper::searchString($searchParam['condition']));
        }
        if (SearchHelper::isNotEmpty($searchParam['name'])) {
            $assertionBuilder->where('zname', 'ilike', SearchHelper::searchString($searchParam['name']));
        }
        if (SearchHelper::isNotEmpty($searchParam['executor'])) {
            $str = SearchHelper::searchString($searchParam['executor']);
            $assertionBuilder->where(function ($query) use ($str) {
                $query->where('org_isp.oname', 'ilike', $str)
                    ->orWhere('org_isp.onamekrat', 'ilike', $str);
            });
        }
        if (SearchHelper::isNotEmpty($searchParam['customer'])) {
            $str = SearchHelper::searchString($searchParam['customer']);
            $assertionBuilder->where(function ($query) use ($str) {
                $query->where('org_zak.oname', 'ilike', $str)
                    ->orWhere('org_zak.onamekrat', 'ilike', $str);
            });
        }
        if (SearchHelper::isNotEmpty($searchParam['dateStartFrom'])) {
            $assertionBuilder->where('ctdatazakluch', '>=', $searchParam['dateStartFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateStartTo'])) {
            $assertionBuilder->where('ctdatazakluch', '<=', $searchParam['dateStartTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateFinishFrom'])) {
            $assertionBuilder->where('ctdatazaversh', '>=', $searchParam['dateFinishFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateFinishTo'])) {
            $assertionBuilder->where('ctdatazaversh', '<=', $searchParam['dateFinishTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['status'])) {
            $assertionBuilder->where('zstatus', 'ilike', SearchHelper::searchString($searchParam['status']));
        }

        $aggregate['title']='Итого';
        $aggregate['price'] = 0.0;
        $result = [];
        foreach ($assertionBuilder->get() as $assertion) {
            $result[] = [
                'id' => $assertion->id,
                'year' => $assertion->year,
                'title' => $assertion->title,
                'executor' => $assertion->executor,
                'number' => $assertion->number,
                'date_start' => SearchHelper::formatDate($assertion->date_start),
                'date_finish' => SearchHelper::formatDate($assertion->date_finish),
                'price' => SearchHelper::moneyThousand($assertion->price),
                'stage' => $assertion->stage,
                "document" => implode('<hr/>', self::makeDocuments($assertion->bdis, $assertion->ntd, $assertion->referat))
                    . ($assertion->npdiscuss ? "</br><span class='text-primary'>Срок утверждения НТД " . $assertion->npdiscuss . " дней</span>" : "")
                    . ($assertion->npapproval ? "</br><span class='text-success'>Срок разработки НТД " . $assertion->npapproval . " дней</span>" : "")
            ];
            $aggregate['price'] += SearchHelper::numberCalc($assertion->price);
        }
        $aggregate['price'] = SearchHelper::moneyThousand($aggregate['price']);
        return ['rows'=>$result, 'aggregate'=>$aggregate];
    }

    private static function makeDocuments($bdis, $ntd, $referat)
    {
        $result = [];
        if ($bdis) {
            foreach (json_decode($bdis, true) as $row) {
                $result[] = $row["type"] . " " . $row["title"]
                    . '</br>№ ' . $row["number"] . " от " . SearchHelper::formatDate($row["date"]) . "</br></br>";
            }
        }
        if ($ntd) {
            foreach (json_decode($ntd, true) as $row) {
                $result[] = $row["type_doc"] . " " . $row["num_doc"] . "</br>" . $row["title"] . "</br>" . $row["primech"]
                    . "</br>Действует с " . SearchHelper::formatDate($row["datavvod"]);
            }
        }
        if (empty($result)) {
            $result[] = json_decode($referat)[0];
        }
        return $result;
    }

    public static function getOne($id)
    {
        $assertion = db::table('ik_niokr_contract')
            ->select('ik_niokr_contract.ctnum    as id',
                'zname             as title',
                'zlotnum           as num_date_lot',
                'org_isp.onamekrat as executor_short',
                'org_isp.oname     as executor',
                'ctplanum          as number',
                'ctdatazakluch     as date_start',
                'ctdatazaversh     as date_finish',
                'ctsostoianie      as stage',
                'zpriceall         as stoimost',
                'nptitle',
                'npindex',
                'ctrlzsp',
                db::raw("coalesce(json_agg(json_build_object(
                    'netype', netype,
                    'nedata', nedata,
                    'nedescript', nedescript,
                    'ndname', ndname,
                    'ndras', ndras,
                    'ndnum', ndnum) order by nedata), '[]')::text as project_ntd")
            )
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_contract.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoin("ik_niokr_zakazchik", "ik_niokr_zaiavka.znum", "=", "ik_niokr_zakazchik.znum")
            ->leftJoin("ik_niokr_statia", "ik_niokr_statia.snum", "=", "ik_niokr_zaiavka.snum")
            ->leftJoin("ik_niokr_dokumentats", "ik_niokr_zaiavka.znum", "=", "ik_niokr_dokumentats.znum")
            ->leftJoin("ik_niokr_econobosn", "ik_niokr_zaiavka.znum", "=", "ik_niokr_econobosn.znum")
            ->leftJoin("ik_niokr_protokol", "ik_niokr_zaiavka.znum", "=", "ik_niokr_protokol.znum")
            ->leftJoin("ik_niokr_podrazdel", "ik_niokr_zaiavka.prnum", "=", "ik_niokr_podrazdel.prnum")
            ->leftJoin("ik_niokr_razdel", "ik_niokr_podrazdel.rnum", "=", "ik_niokr_razdel.rnum")
            ->leftJoin("ik_niokr_organiz as org_zak", "ik_niokr_zakazchik.onum", "=", "org_zak.onum")
            ->leftJoin("ik_niokr_organiz as org_isp", "ik_niokr_ispolnitel.onum", "=", "org_isp.onum")
            ->leftJoin("ik_niokr_ntdproject", "ik_niokr_ntdproject.ctnum", "=", "ik_niokr_contract.ctnum")
            ->leftJoin("ik_niokr_ntdevents", "ik_niokr_ntdevents.npnum", "=", "ik_niokr_ntdproject.npnum")
            ->leftJoin("ik_niokr_ntddocum", "ik_niokr_ntddocum.nenum", "=", "ik_niokr_ntdevents.nenum")
            ->where('ik_niokr_contract.ctnum', '=', $id)
            ->groupBy('ik_niokr_contract.ctnum', 'zname', 'zlotnum', 'org_isp.onamekrat', 'org_isp.oname', 'ctplanum',
                'ctdatazakluch', 'ctdatazaversh', 'ctsostoianie', 'zpriceall', 'nptitle', 'npindex', 'ctrlzsp')
            ->first();
        $result = [];
        $index = 0;
        $result[] =
            [
                'title' => 'Наименование контракта',
                'info' => $assertion->title,
                'bold' => true,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Исполнитель (краткое наименование организации)',
                'info' => $assertion->executor_short,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Исполнитель (полное наименование организации)',
                'info' => $assertion->executor,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Номер контракта',
                'info' => $assertion->number,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Дата заключения контракта',
                'info' => SearchHelper::formatDate($assertion->date_start),
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Дата завершения контракта по КП',
                'info' => SearchHelper::formatDate($assertion->date_finish),
                'index' => ++$index
            ];
        $date_finish = db::table('ik_niokr_contract as a')
            ->select('scedata as sost_date')
            ->leftJoin("ik_niokr_conetapi as b", "a.ctnum", "=", "b.ctnum")
            ->leftJoin("ik_niokr_sostconetapi as s", "s.cenum", "=", "b.cenum")
            ->leftJoin("ik_niokr_claim as c", "s.cenum", "=", "c.cenum")
            ->leftJoin("ik_niokr_perechensostetapi as p", "p.psenum", "=", "s.psenum")
            ->where("a.ctnum", "=", $id)
            ->orderByDesc('sost_date')
            ->limit(1)
            ->first();
        $actual_finish = $date_finish->sost_date ?: $assertion->date_finish;
        if ($date_finish->sost_date) {
            $result[] =
                [
                    'title' => 'Дата фактического завершения ГК',
                    'info' => SearchHelper::formatDate($date_finish->sost_date),
                    'index' => ++$index
                ];
        }
        $result[] =
            [
                'title' => 'Стадия выполнения',
                'info' => $assertion->stage,
                'index' => ++$index
            ];
        $diff_date = null;
        if ($assertion->npindex) {
            $result[] =
                [
                    'title' => 'Проект НТД:',
                    'center' => true,
                    'info' => null,
                    'index' => ++$index
                ];
            $result[] =
                [
                    'title' => 'Вид документа Проекта НТД',
                    'info' => $assertion->npindex,
                    'index' => ++$index
                ];
            $result[] =
                [
                    'title' => 'Наименование Проекта НТД',
                    'info' => $assertion->nptitle,
                    'index' => ++$index
                ];
            $start_date = null;
            foreach (json_decode($assertion->project_ntd, true) as $projectNtd) {
                $start_date = $start_date ?: $projectNtd['nedata'];
                $finish_date = $projectNtd['nedata'];
                $result[] =
                    [
                        'title' => 'Состояние',
                        'info' => $projectNtd['netype'],
                        'index' => ++$index
                    ];
                $result[] =
                    [
                        'title' => 'Дата состояния',
                        'info' => SearchHelper::formatDate($projectNtd['nedata']),
                        'index' => ++$index
                    ];
                $path = 'ProjectNTD';
                $filename = $projectNtd['ndnum'] . '_' . $projectNtd['ndname'] . '.' . $projectNtd['ndras'];
                $uuid = SearchHelper::getUuidFile($path, $filename);
                if ($uuid) {
                    $result[] =
                        [
                            'title' => 'Документы НТД',
                            'info' => SearchHelper::makeDownloadLink($uuid, $projectNtd['ndras'], $filename),
                            'index' => ++$index
                        ];
                }
                $diff_date = \DateTime::createFromFormat("Y-m-d", $finish_date)
                    ->diff(\DateTime::createFromFormat("Y-m-d", $start_date))->format('%a');
            }
            if ($diff_date) {
                $result[] =
                    [
                        'title' => 'Срок утверждения НТД:',
                        'info' => $diff_date . '  дней',
                        'index' => ++$index
                    ];
            }
        }
        $ntds = self::getNtd($assertion->date_start, $assertion->number);
        if (!empty($ntds)) {
            foreach ($ntds as $ntd) {
                $result[] = [
                    'title' => $ntd['title'],
                    'info' => $ntd['info'],
                    'center' => isset($ntd['center']),
                    'index' => ++$index
                ];
            }
            $diff_date_develop = \DateTime::createFromFormat("Y-m-d", $actual_finish)
                ->diff(\DateTime::createFromFormat("Y-m-d", $assertion->date_start))->format('%a');
            $result[] = [
                'title' => 'Срок разработки НТД:',
                'info' => $diff_date_develop . ' дней',
                'index' => ++$index
            ];
            $result[] = [
                'title' => 'Сопоставление сроков разработки и сроков утверждения НТД:',
                'info' => null,
                'graph' => [
                    ['Тип', 'Срок'],
                    ['Сроки разработки: '.$diff_date_develop, (int)$diff_date_develop],
                    ['Сроки утверждения: '.$diff_date, (int)$diff_date]
                ],
                'index' => ++$index
            ];
        }
        return $result;
    }

    private static function getNtd($date, $number)
    {
        $ntdi = db::table('bibl_mainntdi', 'm')
            ->select(
                db::raw("'ntdi' as tbl"),
                'm.ntdi_num as id',
                'ntdi_title as title',
                db::raw('cast(ntdi_number as VARCHAR(50)) as num_doc'),
                'ntdi_textras as ext',
                db::raw("cast((sindex_index || ' ' || sbelong_belong) as varchar(50)) as type_doc")
            )
            ->leftJoin("bibl_sbelong as b", "m.sbelong_num", "=", "b.sbelong_num")
            ->leftJoin("bibl_textsntdi as t", "m.ntdi_num", "=", "t.ntdi_num")
            ->leftJoin("bibl_sindex as s", "m.sindex_num", "=", "s.sindex_num")
            ->where("m.ntdi_ctdatazakik", "=", $date)
            ->where("m.ntdi_ctplanumik", "=", $number);
        $ntd = db::table("bibl_mainntd", "m")
            ->select(
                db::raw("'ntdo' as tbl"),
                "m.ntd_num as id",
                "ntd_title as title",
                db::raw("cast('' as VARCHAR(50)) as num_doc"),
                "ntd_textras             as ext",
                "sgroup_group            as type_doc")
            ->leftJoin("bibl_textsntd as t", "m.ntd_num", "=", "t.ntd_num")
            ->leftJoin("bibl_sgroup as s", "m.sgroup_num", "=", "s.sgroup_num")
            ->where("m.ntd_ctdatazakik", "=", $date)
            ->where("m.ntd_ctplanumik", "=", $number);
        $rows = $ntdi->union($ntd)->get();
        $result = [];
        if ($rows->count() > 0) {
            $result[] =
                [
                    'title' => 'НТД:',
                    'center' => true,
                    'info' => null
                ];
            foreach ($rows as $row) {
                $path = 'ois08//' . $row->tbl;
                $filename = $row->id . $row->ext;
                $uuid = SearchHelper::getUuidFile($path, $filename);
                $result[] =
                    [
                        'title' => 'Вид реализации: ' . $row->type_doc,
                        'info' => $row->type_doc . '&nbsp;' . $row->num_doc . '&nbsp;' . $row->title .
                            ($uuid ? '<hr/>' . SearchHelper::makeDownloadLink($uuid, $row->ext, $filename) : '')
                    ];
            }
        }
        return $result;
    }
}
