<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Support\Facades\DB;

class ClassificationService
{

    public static function search(array $searchParam): array
    {
        $temaTable = db::table("arxiv_tema", "t")
            ->select('tplanum',
                db::raw("coalesce(json_agg(json_build_object(
                    'number', clplanum,
                    'title', clname)), '[]')::text as tema"))
            ->leftJoin("arxiv_classtema as ct", "t.tnum", "=", "ct.tnum")
            ->leftJoin("arxiv_classall as ca", "ct.clnum", "=", "ca.clnum")
            ->whereNotNull("clname")
            ->whereNotNull("clplanum")
            ->groupBy('tplanum');

        $classificationBuilder = db::table("ik_niokr_contract")
            ->select("ik_niokr_contract.ctnum                         as id",
                "zgod                                   as year",
                "sname                                  as budget_item",
                "zname                                  as title",
                "org_isp.onamekrat                      as executor",
                "ctplanum                               as number",
                "ceotchetnum                            as archive_number",
                "ctdatazakluch                          as date_start",
                "ctdatazaversh                          as date_finish",
                "ctsostoianie                           as status",
                "ctrlzpatent                            as patent",
                "ctrlzntd                               as ntd",
                "zpriceall                              as price",
                "ctrlzproectntd                         as proect_ntd",
                "ctrlzbd                                as bdis",
                "ctregregnum                            as cart",
                "ctxnum                                 as ctx_num",
                "ctxname                                as ctx_name",
                "pnnum                                  as prior_num",
                "pnname                                 as prior_name",
                'tema')
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_contract.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoin("ik_niokr_zakazchik", "ik_niokr_zaiavka.znum", "=", "ik_niokr_zakazchik.znum")
            ->leftJoin("ik_niokr_statia", "ik_niokr_statia.snum", "=", "ik_niokr_zaiavka.snum")
            ->leftJoin("ik_niokr_dokumentats", "ik_niokr_zaiavka.znum", "=", "ik_niokr_dokumentats.znum")
            ->leftJoin("ik_niokr_econobosn", "ik_niokr_zaiavka.znum", "=", "ik_niokr_econobosn.znum")
            ->leftJoin("ik_niokr_protokol", "ik_niokr_zaiavka.znum", "=", "ik_niokr_protokol.znum")
            ->leftJoin("ik_niokr_podrazdel", "ik_niokr_zaiavka.prnum", "=", "ik_niokr_podrazdel.prnum")
            ->leftJoin("ik_niokr_razdel", "ik_niokr_podrazdel.rnum", "=", "ik_niokr_razdel.rnum")
            ->leftJoin("ik_niokr_organiz as org_zak", "ik_niokr_zakazchik.onum", "=", "org_zak.onum")
            ->leftJoin("ik_niokr_organiz as org_isp", "ik_niokr_ispolnitel.onum", "=", "org_isp.onum")
            ->leftJoin("ik_niokr_conetapi", "ik_niokr_contract.ctnum", "=", "ik_niokr_conetapi.ctnum")
            ->where("ceplanum", "=", "1")
            ->where("ceotchetnum", "like", "Н%")
            ->where("ceotchetnum", "!=", 'Нет')
            ->where(function ($query) {
                $query->where('zkmain', '=', true)->orWhereNull('zkmain');
            })
            ->whereRaw("((zgod < date_part('year', CURRENT_DATE)
                            and (ctsostoianie ilike 'завершен' or ctsostoianie ilike 'расторгнут'))
                            or (zgod >= date_part('year', CURRENT_DATE)))")
            ->orderByDesc("zgod")
            ->orderByDesc("ctdatazakluch")
            ->orderByDesc("ctdatazaversh");

        if (SearchHelper::isNotEmpty($searchParam['year'])) {
            $classificationBuilder->where('zgod', '=', $searchParam['year']);
        }
        if (SearchHelper::isNotEmpty($searchParam['criticalTechnology'])) {
            if ($searchParam['criticalTechnology'] == 0) {
                $classificationBuilder->whereNull('ctxnum');
            } else {
                $classificationBuilder->where('ctxnum', '=', $searchParam['criticalTechnology']);
            }
        }
        if (SearchHelper::isNotEmpty($searchParam['direction'])) {
            $classificationBuilder->where('pnnum', '=', $searchParam['direction']);
        }
        if (SearchHelper::isNotEmpty($searchParam['priorityTechnology'])) {
            $temaTable->where('clplanum', '=', $searchParam['priorityTechnology']);
            $classificationBuilder->whereNotNull('tema');
        }
        if (SearchHelper::isNotEmpty($searchParam['directionDh'])) {
            $temaTable->where('clplanum', '=', $searchParam['directionDh']);
            $classificationBuilder->whereNotNull('tema');
        }
        $classificationBuilder->leftJoinSub($temaTable, "tema_table", "tema_table.tplanum", "=", "ik_niokr_conetapi.ceotchetnum");
//        dd($classificationBuilder->toRawSql());
        $result = [];
        $aggregate['title']="Итого";
        $aggregate['price_all'] = 0.0;
        foreach ($classificationBuilder->get() as $classification) {
            $types = [];
            if ($classification->patent) {
                $types[] = 'Патент';
            }
            if ($classification->ntd) {
                $types[] = 'НТД';
            }
            if ($classification->proect_ntd) {
                $types[] = 'Проект НТД';
            }
            if ($classification->bdis) {
                $types[] = 'БД/ИС';
            }
            $direction_dh = [];
            $priority_technology = [];
            if ($classification->tema) {
                foreach (json_decode($classification->tema, true) as $tema) {
                    if (str_starts_with($tema['number'], '1')) {
                        $direction_dh[] = $tema['number'] . ' ' . $tema['title'];
                    } else {
                        $priority_technology[] = $tema['number'] . ' ' . $tema['title'];
                    }
                }
            }
            $result[] = [
                'id' => $classification->id,
                'year' => $classification->year,
                'budget_item' => $classification->budget_item,
                'title' => $classification->title,
                'executor' => $classification->executor,
                'number' => $classification->number,
                'archive_number' => $classification->archive_number,
                'date_start' => $classification->date_start,
                'date_finish' => $classification->date_finish,
                'status' => $classification->status,
                'types' => implode('</br>', $types),
                'cart' => $classification->cart,
                'price_all' => $classification->price,
                'technology' => $classification->ctx_num . ' ' . $classification->ctx_name,
                'direction' => $classification->prior_num . ' ' . $classification->prior_name,
                'direction_dh' => implode('<br/>', $direction_dh),
                'priority_technology' => implode('<br/>', $priority_technology),
            ];
            $aggregate['price_all'] += SearchHelper::numberCalc($classification->price);
        }
        return ['rows'=>$result,'aggregate'=>$aggregate];
    }

}
