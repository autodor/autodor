<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class EventService
{

    public static function response()
    {
        $stoEvents = DB::table('bibl_mainntdi')
            ->select("bibl_mainntdi.ntdi_num as id",
                "ntdi_number as number",
                "ntdi_title as name",
                "bibl_authorntdi.authorntdi_author as organization",
                db::raw("to_char(bibl_textsptntdi.tsptntdidata,'dd.mm.yyyy') as date_start"),
                db::raw("to_char(bibl_textsptntdi.tsptntdidataend,'dd.mm.yyyy') as date_finish"))
            ->leftJoin("bibl_textsptntdi", "bibl_mainntdi.ntdi_num", "=", "bibl_textsptntdi.ntdi_num")
            ->leftJoin("bibl_authorntdi", "bibl_mainntdi.ntdi_num", "=", "bibl_authorntdi.ntdi_num")
            ->where("bibl_textsptntdi.tsptntdiimpt", "=", 'imp')
            ->where("bibl_textsptntdi.tsptntdidataend", "<", date('Y-m-d'))
            ->orderByDesc("bibl_textsptntdi.tsptntdidataend");


        $events = DB::table("ik_niokr_ntdproject")
            ->select(
                "ik_niokr_ntdproject.ctnum                as id",
                "npindex as events",
                "nptitle                         as title",
                "ik_niokr_organiz.onamekrat               as organization",
                "ik_niokr_contract.ctplanum               as number",
                db::raw("to_char(ik_niokr_contract.ctdatazakluch,'dd.mm.yyyy') as conclusion_date"),
                "ik_niokr_zaiavka.zname as name",
                db::raw("to_char(ik_niokr_sostconetapi.scedata,'dd.mm.yyyy') as date_payment"),
                db::raw("extract(day from now()  - (ik_niokr_sostconetapi.scedata + interval '6 month')) AS day_off"))
            ->leftJoin("ik_niokr_contract", "ik_niokr_ntdproject.ctnum", "=", "ik_niokr_contract.ctnum")
            ->leftJoin("ik_niokr_conetapi", "ik_niokr_contract.ctnum", "=", "ik_niokr_conetapi.ctnum")
            ->leftJoin("ik_niokr_sostconetapi", "ik_niokr_conetapi.cenum", "=", "ik_niokr_sostconetapi.cenum")
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_zaiavka.znum", "=", "ik_niokr_contract.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_ispolnitel.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_organiz", "ik_niokr_organiz.onum", "=", "ik_niokr_ispolnitel.onum")
            ->where("ik_niokr_sostconetapi.psenum", "=", '7')
            ->whereIn("npindex", ['ПНСТ', 'ГОСТ', 'ГОСТ Р', 'ОДМ'])
            ->where("ik_niokr_conetapi.ceend", "=", true)
            ->where("ik_niokr_sostconetapi.scedata", "<", date('Y-m-d'))
            ->where("ik_niokr_contract.ctrlzproectntd", "=", true)
            ->where("ik_niokr_ntdproject.nprealiz", "=", 'Проект')
            ->orderBy("npindex")
            ->orderByDesc("ik_niokr_sostconetapi.scedata");
        $contractFinish = db::table("ik_niokr_contract")
            ->select("ctnum as id",
                "ik_niokr_zaiavka.zgod as year",
                "ctplanum as number",
                db::raw("to_char(ctdatazakluch,'dd.mm.yyyy') as gk_date"),
                "ik_niokr_zaiavka.zname as title",
                "ik_niokr_organiz.onamekrat as organization",
                db::raw("to_char(ctdatazaversh,'dd.mm.yyyy') as conclusion_date"),
                db::raw("extract(day from now()  - (ctdatazaversh + interval '30 day')) AS day_off")
            )
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_zaiavka.znum", "=", "ik_niokr_contract.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_ispolnitel.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_organiz", "ik_niokr_organiz.onum", "=", "ik_niokr_ispolnitel.onum")
            ->where(db::raw("ctdatazaversh + 30"), "<", date('Y-m-d'))
            ->where("ik_niokr_zaiavka.snum", "=", '1')
            ->where("ik_niokr_zaiavka.zgod", "=", date('Y'))
            ->where("ctsostoianie", "ilike", 'выполняется')
            ->orderByDesc("ctdatazaversh");
        return [
            'events' => $events->get()->all(),
            'sto' => $stoEvents->get()->all(),
            'contractFinish' => $contractFinish->get()->all()
        ];
    }

    public static function count()
    {
        $stoEvents = DB::table('bibl_mainntdi')
            ->leftJoin("bibl_textsptntdi", "bibl_mainntdi.ntdi_num", "=", "bibl_textsptntdi.ntdi_num")
            ->leftJoin("bibl_authorntdi", "bibl_mainntdi.ntdi_num", "=", "bibl_authorntdi.ntdi_num")
            ->where("bibl_textsptntdi.tsptntdiimpt", "=", 'imp')
            ->where("bibl_textsptntdi.tsptntdidataend", "<", date('Y-m-d'))
            ->orderByDesc("bibl_textsptntdi.tsptntdidataend");

        $events = DB::table("ik_niokr_ntdproject")
            ->leftJoin("ik_niokr_contract", "ik_niokr_ntdproject.ctnum", "=", "ik_niokr_contract.ctnum")
            ->leftJoin("ik_niokr_conetapi", "ik_niokr_contract.ctnum", "=", "ik_niokr_conetapi.ctnum")
            ->leftJoin("ik_niokr_sostconetapi", "ik_niokr_conetapi.cenum", "=", "ik_niokr_sostconetapi.cenum")
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_zaiavka.znum", "=", "ik_niokr_contract.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_ispolnitel.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_organiz", "ik_niokr_organiz.onum", "=", "ik_niokr_ispolnitel.onum")
            ->where("ik_niokr_sostconetapi.psenum", "=", '7')
            ->whereIn("npindex", ['ПНСТ', 'ГОСТ', 'ГОСТ Р', 'ОДМ'])
            ->where("ik_niokr_conetapi.ceend", "=", true)
            ->where("ik_niokr_sostconetapi.scedata", "<", date('Y-m-d'))
            ->where("ik_niokr_contract.ctrlzproectntd", "=", true)
            ->where("ik_niokr_ntdproject.nprealiz", "=", 'Проект')
            ->orderBy("npindex")
            ->orderByDesc("ik_niokr_sostconetapi.scedata");
        $contractFinish = db::table("ik_niokr_contract")
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_zaiavka.znum", "=", "ik_niokr_contract.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_ispolnitel.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_organiz", "ik_niokr_organiz.onum", "=", "ik_niokr_ispolnitel.onum")
            ->where(db::raw("ctdatazaversh + 30"), "<", date('Y-m-d'))
            ->where("ik_niokr_zaiavka.snum", "=", '1')
            ->where("ik_niokr_zaiavka.zgod", "=", date('Y'))
            ->where("ctsostoianie", "ilike", 'выполняется')
            ->orderByDesc("ctdatazaversh");
        return [
            'all' => $events->count() +
                $stoEvents->count() +
                $contractFinish->count()
        ];
    }

    public static function getOne($id)
    {
        $event = db::table('bibl_mainntdi')
            ->distinct()
            ->select("bibl_mainntdi.ntdi_num                                             as id",
                "ntdi_title                                                                      as title",
                db::raw("cast(upper(sindex_index || ' ' || sbelong_belong) as varchar(40)) as type_doc"),
                "sbelong_belong                                                                  as belong",
                "ntdi_number                                                                     as number",
                "authorntdi_author                                                               as developer",
                "ntdi_deistvuet                                                                  as working",
                "ntdi_vvoddeistv                                                                 as introduction",
                "bibl_textsptntdi.tsptntdidata                                                   as date_start",
                "bibl_textsptntdi.tsptntdidataend                                                as date_finish",
                "ntdi_vpervie                                                                    as first",
                "ntdi_zamennast                                                                  as zamennast",
                "ntdi_pubyear                                                                    as year_published",
                "ntdi_mestoizdan                                                                 as place_published",
                "ntdi_kolstr                                                                     as count_page",
                "ntdi_kod                                                                        as code_published",
                "ntdi_izdatel                                                                    as publisher",
                "ntdi_ext                                                                        as ftris",
                "ntdi_ctplanumik                                                                 as number_contract",
                "ntdi_ctdatazakik                                                                as date_contract",
                "ntdi_textras                                                                    as file_type",
                "ntdi_primech                                                                    as description",
                "ntdi_tolerance",
                db::raw("COALESCE(string_agg(tplanum || ' '|| tname, '<br/>'), '')::text as topic"))
            ->leftJoin("bibl_sindex", "bibl_mainntdi.sindex_num", "=", "bibl_sindex.sindex_num")
            ->leftJoin("bibl_sbelong", "bibl_mainntdi.sbelong_num", "=", "bibl_sbelong.sbelong_num")
            ->leftJoin("bibl_authorntdi", "bibl_mainntdi.ntdi_num", "=", "bibl_authorntdi.ntdi_num")
            ->leftJoin("bibl_textsntdi", "bibl_mainntdi.ntdi_num", "=", "bibl_textsntdi.ntdi_num")
            ->leftJoin("bibl_tematikantdi", "bibl_mainntdi.ntdi_num", "=", "bibl_tematikantdi.ntdi_num")
            ->leftJoin("bibl_tematika", "bibl_tematikantdi.tnum", "=", "bibl_tematika.tnum")
            ->leftJoin('bibl_textsptntdi', function (JoinClause $join) {
                $join
                    ->whereNotNull("bibl_textsptntdi.tsptntdidata")
                    ->whereNotNull("bibl_textsptntdi.tsptntdidataend")
                    ->on("bibl_mainntdi.ntdi_num", "=", "bibl_textsptntdi.ntdi_num");
            })
            ->where("bibl_mainntdi.ntdi_num", "=", $id)
            ->groupBy(["bibl_mainntdi.ntdi_num",
                "ntdi_title",
                "sindex_index",
                "sbelong_belong",
                "ntdi_number",
                "authorntdi_author",
                "ntdi_deistvuet",
                "ntdi_vvoddeistv",
                "bibl_textsptntdi.tsptntdidata",
                "bibl_textsptntdi.tsptntdidataend",
                "ntdi_vpervie",
                "ntdi_zamennast",
                "ntdi_pubyear",
                "ntdi_mestoizdan",
                "ntdi_kolstr",
                "ntdi_kod",
                "ntdi_izdatel",
                "ntdi_ext",
                "ntdi_ctplanumik",
                "ntdi_ctdatazakik",
                "ntdi_textras",
                "ntdi_primech",
                "ntdi_tolerance"])
            ->first();
        $index = 0;
        $result = [];
        $result[] =
            [
                'title' => 'Наименование',
                'info' => $event->title,
                'index' => $index,
                'bold' => true
            ];
        $result[] =
            [
                'title' => 'Тип документа',
                'info' => $event->type_doc,
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Номер документа',
                'info' => $event->number,
                'index' => ++$index,
            ];
        if ($event->number_contract) {
            $result[] =
                [
                    'title' => 'Номер контракта',
                    'info' => $event->number_contract,
                    'index' => ++$index,
                ];
        }
        if ($event->date_contract) {
            $result[] =
                [
                    'title' => 'Дата контракта',
                    'info' => SearchHelper::formatDate($event->date_contract),
                    'index' => ++$index,
                ];
        }
        $result[] =
            [
                'title' => 'Согласование Росавтодором',
                'info' => $event->description,
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Разработчик',
                'info' => $event->developer,
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Действует',
                'info' => $event->working,
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Дата введения в действие',
                'info' => SearchHelper::formatDate($event->introduction),
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Дата согласования',
                'info' => SearchHelper::formatDate($event->date_start),
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Дата окончания согласования',
                'info' => SearchHelper::formatDate($event->date_finish),
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Введен впервые',
                'info' => $event->first ?: 'Нет',
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Год издания',
                'info' => $event->year_published,
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Место издания',
                'info' => $event->place_published,
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Количество страниц',
                'info' => $event->count_page,
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Код',
                'info' => $event->code_published,
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Издательство',
                'info' => $event->publisher,
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Входит в ФТРиС',
                'info' => $event->ftris == 10 ? 'Да' : 'Нет',
                'index' => ++$index,
            ];
        $result[] =
            [
                'title' => 'Тематика',
                'info' => $event->topic,
                'index' => ++$index,
            ];
        if ($event->file_type) {
            $filename = $id . '.' . $event->file_type;
            $docsUuid = SearchHelper::getUuidFile('ois08/NTDi', $filename);
            if ($docsUuid) {
                $result[] =
                    [
                        'title' => 'Скачать документ',
                        'info' => SearchHelper::makeDownloadLink($docsUuid, $event->file_type),
                        'index' => ++$index,
                    ];
            }
        }
        $filename = $id . '.' . $event->file_type;
        return array_merge($result, self::createListFile($id, $index));
    }

    private static function createListFile($id, $index): array
    {
        $files = db::table('files')
            ->where('path', '=', 'TsptNtdi')
            ->where(function ($query) use ($id) {
                $query->where('filename', 'ilike', '%_' . $id . '_imp_%')
                    ->orWhere('filename', 'ilike', '%_' . $id . '_clo_%');
            });
        $result = [];
        foreach ($files->get() as $file) {
            $filename = explode('_', $file->filename);
            if (!empty($filename) && count($filename) > 2) {
                $names = end($filename);
                $array = explode('.', $names);
                $type = match ($filename[2]) {
                    'imp' => 'Письмо о согласовании',
                    'clo' => 'Распоряжение об утверждении'
                };
                $result[] = [
                    'title' => $type,
                    'info' => $array[0] . '<hr/>' . SearchHelper::makeDownloadLink($file->uuid, end($array)),
                    'index' => ++$index,
                ];
            }
        }
        return $result;
    }
}
