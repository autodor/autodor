<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class EbNtdService
{
    public static function makeQuery(array $searchParam)
    {
        $ebNtdBuilder = DB::table('eb_ntd_view')
            ->select(
                "table_name",
                "id",
                "type_doc",
                "number",
                "topic_id",
                "title",
                "year_published",
                "developer",
                "ftris",
                "contract_number",
                "date",
                "date_entry",
                "date_approval");

        if (SearchHelper::isNotEmpty($searchParam['typeDoc'])) {
            $ebNtdBuilder->where('type_doc', 'ilike', SearchHelper::searchString($searchParam['typeDoc']));
        }
        if (SearchHelper::isNotEmpty($searchParam['nameDoc'])) {
            $ebNtdBuilder->where('title', 'ilike', SearchHelper::searchString($searchParam['nameDoc']));
        }
        if (SearchHelper::isNotEmpty($searchParam['numberNTD'])) {
            $ebNtdBuilder->where('number', 'ilike', SearchHelper::searchString($searchParam['numberNTD']));
        }
        if (SearchHelper::isNotEmpty($searchParam['developerNTD'])) {
            $ebNtdBuilder->where('developer', 'ilike', SearchHelper::searchString($searchParam['developerNTD']));
        }
        if (SearchHelper::isNotEmpty($searchParam['yearPublishedFrom'])) {
            $ebNtdBuilder->where('year_published', '>=', $searchParam['yearPublishedFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['yearPublishedTo'])) {
            $ebNtdBuilder->where('year_published', '<=', $searchParam['yearPublishedTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateEntryFrom'])) {
            $ebNtdBuilder->where('date_entry', '>=', $searchParam['dateEntryFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateEntryTo'])) {
            $ebNtdBuilder->where('date_entry', '<=', $searchParam['dateEntryTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateApprovalFrom'])) {
            $ebNtdBuilder->where('date_approval', '>=', $searchParam['dateApprovalFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateApprovalTo'])) {
            $ebNtdBuilder->where('date_approval', '<=', $searchParam['dateApprovalTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['entryFTRIS'])) {
            if ($searchParam['entryFTRIS'] === "true") {
                $ebNtdBuilder->where('ftris', '=', 10);
            } else {
                $ebNtdBuilder->where('ftris', '!=', 10);
            }
        }
        if (SearchHelper::isNotEmpty($searchParam['historyNTD'])) {
            if ($searchParam['historyNTD'] === "true") {
                $ebNtdBuilder->where('history', '=', true);
            } else {
                $ebNtdBuilder->where('history', '!=', true);
            }
        }
        if (SearchHelper::isNotEmpty($searchParam['developFDA'])) {
            if ($searchParam['developFDA'] === "true") {
                $ebNtdBuilder
                    ->whereNotNull('contract_number')
                    ->whereNot('contract_number', 'Инициативно');
            } else {
                $ebNtdBuilder
                    ->whereNull('contract_number')
                    ->orWhere('contract_number', '=', 'Инициативно');
            }
        }
        if (SearchHelper::isNotEmpty($searchParam['enabled'])) {
            if ($searchParam['enabled'] === "true") {
                $ebNtdBuilder
                    ->where('enable', '=', 'Да');
            } else {
                $ebNtdBuilder
                    ->where('enable', '=', 'Нет');
            }
        }
        return $ebNtdBuilder->get();
    }

    public static function search(array $searchParam)
    {
        $result = [];

        foreach (self::makeQuery($searchParam) as $ebNtd) {
            $result[] = [
                "id" => $ebNtd->table_name . '-' . $ebNtd->id,
                "type_doc" => $ebNtd->type_doc,
                "number" => $ebNtd->number,
                "title" => $ebNtd->title,
                "year_published" => $ebNtd->year_published,
                "developer" => $ebNtd->developer,
                "is_ftris" => $ebNtd->ftris == 10,
                "contract_number" => $ebNtd->contract_number,
                "topics" => $ebNtd->topic_id
            ];
        }
        if (SearchHelper::isNotEmpty($searchParam['qualifier'])) {
            $id = $searchParam['qualifier'];
            $result = array_values(array_filter($result, function ($item) use ($id) {
                return in_array($id, explode(', ', $item['topics']));
            }));
        }
        if (SearchHelper::isNotEmpty($searchParam['regulationTopic'])) {
            $id = $searchParam['regulationTopic'];
            $result = array_values(array_filter($result, function ($item) use ($id) {
                return in_array($id, explode(', ', $item['topics']));
            }));
        }
        if (SearchHelper::isNotEmpty($searchParam['functionTopic'])) {
            $id = $searchParam['functionTopic'];
            $result = array_values(array_filter($result, function ($item) use ($id) {
                return in_array($id, explode(', ', $item['topics']));
            }));
        }
        if (SearchHelper::isNotEmpty($searchParam['allSearchQuery'])) {
            $result = SearchHelper::allSearchFilter($result, $searchParam['allSearchQuery']);
        }

        return $result;
    }

    public static function excel(array $searchParam)
    {
        $result = [];

        foreach (self::makeQuery($searchParam) as $ebNtd) {
            $result[] = [
                "id" => $ebNtd->table_name . '-' . $ebNtd->id,
                "type_doc" => $ebNtd->type_doc,
                "number" => $ebNtd->number,
                "title" => $ebNtd->title,
                "year_published" => $ebNtd->year_published,
                "developer" => $ebNtd->developer,
                "is_ftris" => $ebNtd->ftris == 10 ? 'Да' : '',
                "contract_number" => $ebNtd->contract_number,
                "topics" => $ebNtd->topic_id
            ];
        }
        if (SearchHelper::isNotEmpty($searchParam['qualifier'])) {
            $id = $searchParam['qualifier'];
            $result = array_values(array_filter($result, function ($item) use ($id) {
                return in_array($id, explode(', ', $item['topics']));
            }));
        }
        if (SearchHelper::isNotEmpty($searchParam['regulationTopic'])) {
            $id = $searchParam['regulationTopic'];
            $result = array_values(array_filter($result, function ($item) use ($id) {
                return in_array($id, explode(', ', $item['topics']));
            }));
        }
        if (SearchHelper::isNotEmpty($searchParam['functionTopic'])) {
            $id = $searchParam['functionTopic'];
            $result = array_values(array_filter($result, function ($item) use ($id) {
                return in_array($id, explode(', ', $item['topics']));
            }));
        }
        if (SearchHelper::isNotEmpty($searchParam['allSearchQuery'])) {
            $result = SearchHelper::allSearchFilter($result, $searchParam['allSearchQuery']);
        }

        return $result;
    }

    public static function getOne(string $ids)
    {
        $FILE_DIR = env('FILE_DIR', '/app/blobdata/');
        list($table, $id) = explode('-', $ids);
        switch ($table) {
            case 'ntdi':
                $queryBuilder = DB::table("bibl_mainntdi")
                    ->select(
                        "bibl_mainntdi.ntdi_num as id",
                        "ntdi_title as title",
                        DB::raw("cast(upper(sindex_index || ' ' || sbelong_belong) as varchar(40)) as typeDoc"),
                        "sbelong_belong as belong",
                        "ntdi_number as num_oc",
                        "authorntdi_author as author",
                        "ntdi_deistvuet as active",
                        "ntdi_vvoddeistv as date_entry",
                        "ntdi_vpervie as first_entry",
                        "ntdi_zamennast as replaced_doc",
                        "ntdi_history as  history_ntd",
                        "ntdi_histfras as history_file",
                        "ntdi_pubyear as year_published",
                        "ntdi_mestoizdan as place_published",
                        "ntdi_kolstr as page_count",
                        "ntdi_kod as code",
                        "ntdi_izdatel as publisher",
                        "ntdi_ext as ftris",
                        "ntdi_ctplanumik as contract_number",
                        "ntdi_ctdatazakik as contract_date",
                        "ntdi_textras as referat_type",
                        "ntdi_primech as description",
                        "topic_id",
                        "ntdi_tolerance as access",
                        "ntdi_adrdergat as link_doc")
                    ->leftJoin("bibl_sindex", "bibl_mainntdi.sindex_num", "=", "bibl_sindex.sindex_num")
                    ->leftJoin("bibl_sbelong", "bibl_mainntdi.sbelong_num", "=", "bibl_sbelong.sbelong_num")
                    ->leftJoin("bibl_authorntdi", "bibl_mainntdi.ntdi_num", "=", "bibl_authorntdi.ntdi_num")
                    ->leftJoin("bibl_textsntdi", "bibl_mainntdi.ntdi_num", "=", "bibl_textsntdi.ntdi_num")
                    ->leftJoinSub(DB::table("bibl_tematikantdi")
                        ->select("ntdi_num",
                            DB::raw("string_agg(cast(tnum as varchar(50)), ', ') as topic_id")
                        )
                        ->groupBy("ntdi_num")
                        ->orderBy("ntdi_num"), "bt", function (JoinClause $join) {
                        $join->on("bibl_mainntdi.ntdi_num", "=", "bt.ntdi_num");
                    })
                    ->where("bibl_mainntdi.ntdi_num", "=", $id);
                break;
            case 'ntd':
                $queryBuilder = DB::table("bibl_mainntd")
                    ->select("bibl_mainntd.ntd_num as id",
                        "ntd_title            as title",
                        "sgroup_group         as type_doc",
                        "authorntd_author     as author",
                        "ntd_deistvuet        as active",
                        "ntd_vvoddeistv       as date_entry",
                        "ntd_vpervie          as first_entry",
                        "ntd_zamennast        as replaced_doc",
                        "ntd_pubyear          as year_val",
                        "ntd_mestoizdan       as year_published",
                        "ntd_kolstr           as page_count",
                        "ntd_ext              as ftris",
                        "ntd_ctplanumik       as contract_number",
                        "ntd_ctdatazakik      as contract_date",
                        "ntd_textras          as referat_type",
                        "ntd_primech          as description",
                        "topic_id",
                        "ntd_tolerance        as access")
                    ->leftJoin("bibl_sgroup", "bibl_mainntd.sgroup_num", "=", "bibl_sgroup.sgroup_num")
                    ->leftJoin("bibl_authorntd", "bibl_mainntd.ntd_num", "=", "bibl_authorntd.ntd_num")
                    ->leftJoin("bibl_textsntd", "bibl_mainntd.ntd_num", "=", "bibl_textsntd.ntd_num")
                    ->leftJoinSub(DB::table("bibl_tematikantd")
                        ->select("ntd_num",
                            DB::raw("string_agg(cast(tnum as varchar(50)), ', ') as topic_id")
                        )
                        ->groupBy("ntd_num")
                        ->orderBy("ntd_num"), "bt", function (JoinClause $join) {
                        $join->on("bibl_mainntd.ntd_num", "=", "bt.ntd_num");
                    })
                    ->where("bibl_mainntd.ntd_num", "=", $id);
                break;
            case 'book':
                $queryBuilder = DB::table("bibl_mainbook")
                    ->select("bibl_mainbook.book_num as id",
                        "book_title        as title",
                        "book_secondname   as type_doc",
                        "authorbook_author as author",
                        "book_pubyear      as year_published",
                        "book_mestoizdan   as place_published",
                        "book_kolstr       as page_count",
                        "book_ext          as ftris",
                        "book_ctplanumik   as contract_number",
                        "book_ctdatazakik  as contract_date",
                        "book_textras      as referat_type",
                        "book_primech      as description",
                        "topic_id",
                        DB::raw("null as history_ntd"),
                        "book_tolerance    as access")
                    ->leftJoin("bibl_authorbook", "bibl_mainbook.book_num", "=", "bibl_authorbook.book_num")
                    ->leftJoin("bibl_textsbook", "bibl_mainbook.book_num", "=", "bibl_textsbook.book_num")
                    ->leftJoinSub(DB::table("bibl_tematikabook")
                        ->select("book_num",
                            DB::raw("string_agg(cast(tnum as varchar(50)), ', ') as topic_id")
                        )
                        ->groupBy("book_num")
                        ->orderBy("book_num"), "bt", function (JoinClause $join) {
                        $join->on("bibl_mainbook.book_num", "=", "bt.book_num");
                    })
                    ->where("bibl_mainbook.book_num", "=", $id);
                break;
            case 'inform':
                $queryBuilder = DB::table("bibl_maininform")
                    ->select("bibl_maininform.inform_num as id",
                        "inform_title               as title",
                        "stype_type                 as type_doc",
                        "authorinform_author        as author",
                        "inform_pubyear             as year_published",
                        "inform_mestoizdan          as place_published",
                        "inform_kolstr              as page_count",
                        "inform_ext                 as ftris",
                        "inform_ctplanumik          as contract_number",
                        "inform_ctdatazakik         as contract_date",
                        "inform_textras             as referat_type",
                        "inform_primech             as description",
                        "topic_id",
                        "inform_tolerance           as access")
                    ->leftJoin("bibl_stype", "bibl_maininform.stype_num", "=", "bibl_stype.stype_num")
                    ->leftJoin("bibl_authorinform", "bibl_maininform.inform_num", "=", "bibl_authorinform.inform_num")
                    ->leftJoin("bibl_textsinform", "bibl_maininform.inform_num", "=", "bibl_textsinform.inform_num")
                    ->leftJoinSub(DB::table("bibl_tematikainform")
                        ->select("inform_num",
                            DB::raw("string_agg(cast(tnum as varchar(50)), ', ') as topic_id")
                        )
                        ->groupBy("inform_num")
                        ->orderBy("inform_num"), "bt", function (JoinClause $join) {
                        $join->on("bibl_maininform.inform_num", "=", "bt.inform_num");
                    })
                    ->where("bibl_maininform.inform_num", "=", $id);
                break;
        }
//        dd($queryBuilder->toRawSql());
        $ebntd = $queryBuilder->first();
        $index = 0;
        $result = [];
        $result[] =
            [
                'title' => 'Наименование',
                'info' => $ebntd->title,
                'bold' => true,
                'index' => ++$index
            ];
        if (!empty($ebntd->type_doc)) {
            $result[] =
                [
                    'title' => 'Тип документа',
                    'info' => $ebntd->type_doc,
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->num_doc)) {
            $result[] =
                [
                    'title' => 'Номер документа',
                    'info' => $ebntd->num_doc,
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->active)) {
            $result[] =
                [
                    'title' => 'Действует',
                    'info' => $ebntd->active,
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->date_entry)) {
            $result[] =
                [
                    'title' => 'Дата введения в действие',
                    'info' => $ebntd->date_entry,
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->first_entry)) {
            $result[] =
                [
                    'title' => 'Введен впервые',
                    'info' => $ebntd->first_entry,
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->replaced_doc)) {
            $result[] =
                [
                    'title' => 'Обозначение документа, замененного настоящим',
                    'info' => $ebntd->replaced_doc,
                    'index' => ++$index
                ];
        }
        if (isset($ebntd->history_ntd) && !empty($ebntd->history_ntd)) {
            if ($ebntd->history_ntd) {
                $file = "docs/HISTORY_NTD/" . $ebntd->id . "." . $ebntd->history_file;
                $result[] =
                    [
                        'title' => 'История НТД',
                        'info' => file_exists($FILE_DIR . $file)
                            ? "<img src='" . "docs/HISTORY_NTD/" . $file . "' width='100%' alt='История НТД'>"
                            : "<span class='text-secondary'>Нет данных</span></strong>",
                        'index' => ++$index
                    ];

            }
        }
        if (!empty($ebntd->year_published)) {
            $result[] =
                [
                    'title' => 'Год издания',
                    'info' => $ebntd->year_published,
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->place_published)) {
            $result[] =
                [
                    'title' => 'Место издания',
                    'info' => $ebntd->place_published,
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->page_count)) {
            $result[] =
                [
                    'title' => 'Количество страниц',
                    'info' => $ebntd->page_count,
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->code)) {
            $result[] =
                [
                    'title' => 'Код',
                    'info' => $ebntd->code,
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->publisher)) {
            $result[] =
                [
                    'title' => 'Издательство',
                    'info' => $ebntd->publisher,
                    'index' => ++$index
                ];
        }
        if ($ebntd->ftris != null) {
            $result[] =
                [
                    'title' => 'Входит в ФТРиС',
                    'info' => $ebntd->ftris == 10 ? 'Да' : 'Нет',
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->contract_number)) {
            $result[] =
                [
                    'title' => 'Разработка ФДА',
                    'info' => trim($ebntd->contract_number) != 'Инициативно' ? 'Да' : 'Нет',
                    'index' => ++$index
                ];
            $result[] =
                [
                    'title' => 'Номер контракта',
                    'info' => $ebntd->contract_number,
                    'index' => ++$index
                ];
        } else {
            $result[] =
                [
                    'title' => 'Разработка ФДА',
                    'info' => 'Нет',
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->contract_date)) {
            $result[] =
                [
                    'title' => 'Дата контракта',
                    'info' => date("d.m.Y", strtotime($ebntd->contract_date)),
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->description)) {
            $result[] =
                [
                    'title' => 'Примечание',
                    'info' => $ebntd->description,
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->topic_id)) {
            $topics = DB::table('bibl_tematika')
                ->select("tplanum",
                    "tname",
                    "ttip")
                ->whereIn("tnum", explode(", ", $ebntd->topic_id))
                ->orderBy("ttip")
                ->orderBy("tplanum");
            $qualified = [];
            $classification = [];
            foreach ($topics->get() as $topic) {
                if ($topic->ttip == 2) {
                    $qualified[] = $topic->tplanum . ' ' . $topic->tname;
                } else {
                    $classification[] = $topic->tplanum . ' ' . $topic->tname;
                }
            }
            if (!empty($qualified)) {
                $result[] =
                    [
                        'title' => 'Классификатор',
                        'info' => implode("<br>", $qualified),
                        'index' => ++$index
                    ];
            }
            if (!empty($classification)) {
                $result[] =
                    [
                        'title' => 'Тематика',
                        'info' => implode("<br>", $classification),
                        'index' => ++$index
                    ];
            }
        }
        if (!empty($ebntd->link_doc)) {
            $result[] =
                [
                    'title' => '<strong>Ссылка на документ</strong>',
                    'info' => SearchHelper::makeWebLink($ebntd->link_doc),
                    'index' => ++$index
                ];
        }
        if (!empty($ebntd->referat_type)) {
            $file = "docs/ois08/" . $table . "/" . $id . "." . trim($ebntd->referat_type);
            if (file_exists($FILE_DIR) . $file) {
                $result[] =
                    [
                        'title' => '<strong>Скачать документ</strong>',
                        'info' => "<a href='/docs/ois08/" . $file . "' target='_blanks'><img src='../img/"
                            . trim($ebntd->referat_type) . ".png' width='50'></a>",
                        'index' => ++$index
                    ];
            }
        }

        $listFile = glob($FILE_DIR . "TsptNtdi/*_{$id}_*");
        if (count($listFile) > 0) {
            $result[] = [
                'title' => "<strong>Приказы и распоряжения Росавтодора и Росстандарта</strong>",
                'info' => null,
                'index' => ++$index
            ];
        }
        foreach ($listFile as $filename) {
            $split = explode("_", $filename, 4);
            switch ($split[2]) {
                case "imp":
                    $title = "Письмо о согласовании";
                    break;
                case "clo":
                    $title = "Распоряжение об утверждении";
                    break;
            }
            $download = "<a href='" . str_replace($FILE_DIR, "/", $filename)
                . "'  target='_blanks'><img src='../img/" . substr(strrchr($filename, '.'), 1) . ".png' width='50'></a>";
            $result[] = [
                'title' => $title,
                'info' => $download,
                'index' => ++$index
            ];
        }

        return $result;
    }
}
