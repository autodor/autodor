<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Support\Facades\DB;

class ContractorService
{

    public static function search(array $searchParam): array
    {
        $step = db::table('ik_niokr_conetapi')
            ->select("ctnum",
                db::raw("COALESCE(
                    json_agg(
                        json_build_object(
                           'etap_num', ceplanum,
                           'stoimost_etapi', cesumwinner
                        ) order by ceplanum), '[]') as step")
            )
            ->groupBy('ctnum');
        $contract = db::table('ik_niokr_contract')
            ->select("onum",
                "ctplanum                                            as number",
                "zpriceall                                           as price_all",
                db::raw("json_agg(json_build_object(
                                       'year', zgod,
                                       'price', zpricethisyear,
                                       'step', step) order by zgod) as price_year"))
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_contract.znum", "=", "ik_niokr_zaiavka.znum")
            ->join("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoinSub($step, "conetapi", "ik_niokr_contract.ctnum", "=", "conetapi.ctnum")
            ->groupBy("ctplanum", "zpriceall", "onum");
        $contractorBuilder = db::table('ik_niokr_organiz')
            ->distinct()
            ->select("ik_niokr_organiz.onum as id",
                "onamekrat as executor",
                db::raw("json_agg(
                        json_build_object(
                                'number', number,
                                'price_all', price_all,
                                'price_year', price_year)
                    order by number)::text as contract_info"))
            ->joinSub($contract, "contract", "contract.onum", "=", "ik_niokr_organiz.onum")
            ->groupBy("ik_niokr_organiz.onum", "onamekrat")
            ->orderBy("ik_niokr_organiz.onum");
        $result = [];
        foreach ($contractorBuilder->get() as $contractor) {
            list($price_all, $price_year, $payment_year, $debt_year, $numbers) = self::makeInfo($contractor->contract_info);
            $result[] = [
                'id' => $contractor->id,
                'executor' => $contractor->executor,
                'price_all' => $price_all,
                'price_year' => $price_year,
                'payment_year' => $payment_year,
                'debt_year' => $debt_year,
                'numbers' => $numbers
            ];
        }
        return $result;
    }

    private static function makeInfo(string $contract_info): array
    {
        $price_all = 0;
        $price_year = [];
        $payment_year = null;
        $debt_year = null;
        $numbers = [];
        if ($contract_info) {
            $contracts = json_decode($contract_info, true);
            $priceYearArray = [];
            foreach ($contracts as $contract) {
                $price_all += (double)$contract['price_all'];
                $numbers[] = $contract['number'];
                foreach ($contract['price_year'] as $priceYear) {
                    $priceYearArray[$priceYear['year']] = $priceYearArray[$priceYear['year']] ?? 0 + (double)$priceYear['price'];
                }
            }
            ksort($priceYearArray, SORT_NUMERIC);
            foreach ($priceYearArray as $key => $value) {
                $price_year[] = $key . ': '. SearchHelper::moneyThousand($value);
            }
        }
        return [
            SearchHelper::moneyThousand($price_all),
            implode('<br/>', $price_year),
            $payment_year,
            $debt_year,
            implode('<br/>', $numbers)
        ];
    }
}
