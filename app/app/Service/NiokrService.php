<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Support\Facades\DB;

class NiokrService
{

    public static function makeQuery(array $searchParam)
    {
        $niokrBuilder = DB::table('arxiv_tema')
            ->select('arxiv_tema.tnum as id',
                'tname as title',
                'tgod as year_finish',
                'ctplanumik as number',
                'ctdatazakik as conclusion_date',
                'dname as developer',
                'tplanum as number_arh',
                'tsrpatent as patent',
                'tsrntd as ntd',
                'tsrproectntd as project_ntd',
                'tsrvnedren as integration',
                'tsrbd as bdis')
            ->leftJoin('arxiv_design', 'arxiv_tema.tnum', '=', 'arxiv_design.tnum')
            ->where('tzavershena', '=', 'true');

        if (SearchHelper::isNotEmpty($searchParam["yearFrom"])) {
            $niokrBuilder = $niokrBuilder->where("tgod", ">=", $searchParam["yearFrom"]);
        }
        if (SearchHelper::isNotEmpty($searchParam["yearTo"])) {
            $niokrBuilder = $niokrBuilder->where("tgod", "<=", $searchParam["yearTo"]);
        }
        if (SearchHelper::isNotEmpty($searchParam["number"])) {
            $niokrBuilder->where("ctplanumik", "ilike", "%" . $searchParam["number"] . "%");
        }
        if (SearchHelper::isNotEmpty($searchParam["numberArh"])) {
            $niokrBuilder->where("tplanum", "ilike", "%" . $searchParam["numberArh"] . "%");
        }
        if (SearchHelper::isNotEmpty($searchParam["name"])) {
            $niokrBuilder->where("tname", "ilike", "%" . $searchParam["name"] . "%");
        }
        if (SearchHelper::isNotEmpty($searchParam["executor"])) {
            $niokrBuilder->where("arxiv_design.dname", "ilike", "%" . $searchParam["executor"] . "%");
        }
        if (SearchHelper::isNotEmpty($searchParam["conclusionDateFrom"])) {
            $niokrBuilder->where("ctdatazakik", ">=",
                date("Y-m-d", strtotime($searchParam["conclusionDateFrom"])));
        }
        if (SearchHelper::isNotEmpty($searchParam["conclusionDateTo"])) {
            $niokrBuilder->where("ctdatazakik", "<=",
                date("Y-m-d", strtotime($searchParam["conclusionDateTo"])));
        }
        if (SearchHelper::isNotEmpty($searchParam["implementation"])) {
            switch (trim($searchParam["implementation"])) {
                case "ntd":
                    $niokrBuilder->where("tsrntd", "=", true);
                    break;
                case "project_ntd":
                    $niokrBuilder->where("tsrproectntd", "=", true);
                    break;
                case "bdis":
                    $niokrBuilder->where("tsrbd", "=", true);
                    break;
                case "patent":
                    $niokrBuilder->where("tsrpatent", "=", true);
                    break;
                case "integration":
                    $niokrBuilder->where("tsrvnedren", "=", true);
                    break;
            }
        }
        return $niokrBuilder->orderBy("tgod", 'desc')->get();
    }

    public static function excel(array $searchParam)
    {
        $result = [];
        foreach (self::makeQuery($searchParam) as $niokr) {
            $integration = [];
            if ($niokr->bdis) $integration[] = 'БД/ИС';
            if ($niokr->project_ntd) $integration[] = 'Проект НТД';
            if ($niokr->patent) $integration[] = 'Патент';
            if ($niokr->ntd) $integration[] = 'НТД';
            if ($niokr->integration) $integration[] = 'Внедрение';
            $result[] = [
                'id' => $niokr->id,
                'title' => $niokr->title,
                'year_finish' => $niokr->year_finish,
                'number' => $niokr->number,
                'conclusion_date' => date("d.m.Y", strtotime($niokr->conclusion_date)),
                'developer' => $niokr->developer,
                'number_arh' => explode('.', $niokr->number_arh)[0],
                'integration' => implode(', ', $integration)
            ];
        }
        if (isset($searchParam['allSearchQuery']) && SearchHelper::isNotEmpty($searchParam['allSearchQuery'])) {
            $result = SearchHelper::allSearchFilter($result, $searchParam['allSearchQuery']);
        }
        return $result;
    }

    public static function search(array $searchParam)
    {
        $result = [];
        $index = 0;
        foreach (self::makeQuery($searchParam) as $niokr) {
            $result[] = [
                'id' => $niokr->id,
                'title' => $niokr->title,
                'year_finish' => $niokr->year_finish,
                'number' => $niokr->number,
                'conclusion_date' => date("d.m.Y", strtotime($niokr->conclusion_date)),
                'developer' => $niokr->developer,
                'number_arh' => explode('.', $niokr->number_arh)[0],
                'bdis' => $niokr->bdis,
                'project_ntd' => $niokr->project_ntd,
                'patent' => $niokr->patent,
                'ntd' => $niokr->ntd,
                'integration' => $niokr->integration
            ];
        }
        if (isset($searchParam['allSearchQuery']) && SearchHelper::isNotEmpty($searchParam['allSearchQuery'])) {
            $result = SearchHelper::allSearchFilter($result, $searchParam['allSearchQuery']);
        }
        return $result;
    }

    public static function getOne($id): array
    {
        $niokr = DB::table("arxiv_tema")
            ->select(
                "arxiv_tema.tnum as id",
                "tname as title",
                "dname as developer",
                "tgod as year_finish",
                "ctplanumik as number",
                "ctdatazakik as conclusion_date",
                "tplanum as number_arh",
                "tsrpatent as patent",
                "tsrntd as ntd",
                "tsrproectntd as project_ntd",
                "tsrvnedren as integration",
                "tsrbd as bdis",
                "treferat as referat"
            )
            ->leftJoin('arxiv_design', 'arxiv_tema.tnum', '=', 'arxiv_design.tnum')
            ->where('tzavershena', '=', 'true')
            ->where('arxiv_tema.tnum', '=', $id)
            ->first();
        $index = 0;
        $result[] =
            [
                'title' => 'Наименование контракта',
                'info' => $niokr->title,
                'bold' => true,
                'index' => $index,
            ];
        if ($niokr->conclusion_date && $niokr->number) {
            $resultTmp = NiokrService::getNiokr($niokr->conclusion_date, $niokr->number);
            foreach ($resultTmp as $row) {
                $row['index'] = ++$index;
                $result[] = $row;
            }
        }
        $result[] =
            [
                'title' => 'Разработчик',
                'info' => $niokr->developer,
                'index' => ++$index,
            ];
        $result[] = [
            'title' => 'Год завершения',
            'info' => $niokr->year_finish,
            'index' => ++$index,
        ];
        $result[] = [
            'title' => 'Номер контракта',
            'info' => $niokr->number,
            'index' => ++$index,
        ];
        $result[] = [
            'title' => 'Дата контракта',
            'info' => SearchHelper::formatDate($niokr->conclusion_date),
            'index' => ++$index,
        ];
        $result[] = [
            'title' => 'Номер в архиве',
            'info' => explode('.', $niokr->number_arh)[0],
            'index' => ++$index,
        ];
        if ($niokr->ntd) {
            $resultTmp = NiokrService::getNTDI($niokr->conclusion_date, $niokr->number);
            foreach ($resultTmp as $row) {
                $row['index'] = ++$index;
                $result[] = $row;
            }
        }
        if ($niokr->bdis) {
            $resultTmp = NiokrService::getBDIS($niokr->conclusion_date, $niokr->number);
            foreach ($resultTmp as $row) {
                $row['index'] = ++$index;
                $result[] = $row;
            }
        }
        if ($niokr->patent) {
            $result[] = [
                'title' => 'Патент',
                'info' => 'есть',
                'index' => ++$index,
            ];
        }
        if ($niokr->project_ntd) {
            $result[] = [
                'title' => 'Проект НТД',
                'info' => 'есть',
                'index' => ++$index,
            ];
        }
        if ($niokr->integration) {
            $result[] = [
                'title' => 'Внедрение',
                'info' => 'есть',
                'index' => ++$index,
            ];
        }
        $result[] = [
            'title' => 'Реферат:',
            'info' => null,
            'center' => true,
            'index' => ++$index,
        ];
        $result[] = [
            'title' => SearchHelper::toSpoiler($niokr->referat),
            'info' => null,
            'index' => ++$index,
        ];
        return $result;
    }

    private static function getNTDI($date, $number)
    {
        $first = DB::table('bibl_mainntdi as m')
            ->selectRaw(
                "'ntdi' as tbl,
                m.ntdi_num as id,
                ntdi_title as title,
                cast(ntdi_number as VARCHAR(50)) as num_doc,
                ntdi_textras as ext,
                cast((sindex_index || ' ' || sbelong_belong) as varchar(50)) as type_doc")
            ->leftJoin("bibl_sbelong as b", "m.sbelong_num", "=", "b.sbelong_num")
            ->leftJoin("bibl_textsntdi as t", "m.ntdi_num", "=", "t.ntdi_num")
            ->leftJoin("bibl_sindex as s", "m.sindex_num", "=", "s.sindex_num")
            ->where("m.ntdi_ctdatazakik", "=", $date)
            ->where("m.ntdi_ctplanumik", "=", $number);
        $second = DB::table('bibl_mainntd as m')
            ->selectRaw(
                "'ntdo' as tbl,
                m.ntd_num as id,
                ntd_title as title,
                cast('' as VARCHAR(50)) as num_doc,
                ntd_textras as ext,
                sgroup_group as type_doc")
            ->leftJoin("bibl_textsntd as t", "m.ntd_num", "=", "t.ntd_num")
            ->leftJoin("bibl_sgroup as s", "m.sgroup_num", "=", "s.sgroup_num")
            ->where("m.ntd_ctdatazakik", "=", $date)
            ->where("m.ntd_ctplanumik", "=", $number);
        $first->union($second);
        $result[] = [
            'title' => 'Все реализации НТД по контракту:',
            'center' => true,
            'info' => null,
            'index' => 0,
        ];
        $FILE_DIR = env('FILE_DIR', '/app/blobdata/');
        foreach ($first->get() as $ntdi) {
            if (!empty($ntdi->title)) {
                $filePath = $ntdi->tbl . "/" . $ntdi->id . "." . trim($ntdi->ext);
                $result[] = [
                    'title' => 'Вид реализации: ' . $ntdi->type_doc,
                    'info' => $ntdi->type_doc . "&nbsp;" . $ntdi->num_doc . "&nbsp;" . $ntdi->title
                        . ($ntdi->id > 0 && file_exists($FILE_DIR . $filePath)
                            ? "<hr><a href='/docs/ois08/" . $filePath . "' target='_blanks'>"
                            . "<img src='../img/" . trim($ntdi->ext) . ".png' width='50'></a></td></tr>"
                            : ""),
                    'index' => 0
                ];
            }
        }
        return $result;
    }

    private static function getBDIS($date, $number)
    {
        $bdisRows = DB::table('rntd_commondatekontr')
            ->select(
                "ounameobject as title",
                "ourospatsvidetnum as svid_num",
                "ourospatsvidetdate as svid_data",
                "ourospatsvidetkategor as svid_vid")
            ->leftJoin("rntd_objectu", "rntd_commondatekontr.cdnum", "=", "rntd_objectu.cdnum")
            ->where("cddatezakluch", "=", $date)
            ->where("cdkontrnum", "=", $number);
        $result[] = [
            'title' => 'Все реализации РИД по контракту:',
            'center' => true,
            'info' => null,
            'index' => 0,
        ];
        foreach ($bdisRows->get() as $bdis) {
            if (!empty($bdis->title)) {
                $info = $bdis->title . " №&nbsp;" . $bdis->svid_num . " от " . date("d.m.Y", strtotime($bdis->svid_data));
                $bdisNaznach = DB::table("ofap_object")
                    ->select('onaznach as referat')
                    ->where("oname", "like", SearchHelper::specialToPercent($bdis->title));
                $extends = $bdisNaznach->first();
                if (!empty($extends->referat)) {
                    $info .= "<hr>" . SearchHelper::toSpoiler($extends->referat);
                }
                $result[] = [
                    'title' => $bdis->svid_vid,
                    'info' => $info,
                    'index' => 0,
                ];

            }
        }
        return $result;
    }

    private static function getNiokr($date, $number)
    {
        $niokr = DB::table('ik_niokr_contract as co')
            ->select("o1.oname as zakazchik",
                "za.zlotnum as num_date_lot")
            ->leftJoin("ik_niokr_zaiavka as za", "co.znum", "=", "za.znum")
            ->leftJoin("ik_niokr_zakazchik as zk", "za.znum", "=", "zk.znum")
            ->leftJoin("ik_niokr_organiz as o1", "zk.onum", "=", "o1.onum")
            ->where("co.ctdatazakluch", "=", $date)
            ->where("co.ctplanum", "=", $number)
            ->where("zk.zkmain", "=", true)
            ->first();
        $result[] = [
            'title' => "Заказчик",
            'info' => $niokr->zakazchik ?? "",
            'index' => 0,
        ];
        if (!empty($niokr->num_date_lot)) {
            $info = explode(" ", $niokr->num_date_lot);
            if (count($info) > 0) {
                $result[] = [
                    'title' => "Дата конкурса",
                    'info' => SearchHelper::formatDate($info[0]),
                    'index' => 0,
                ];
                $result[] = [
                    'title' => "Номер лота",
                    'info' => $info[1] . " " . $info[2],
                    'index' => 0,
                ];
            }
        }
        return $result;
    }
}
