<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Support\Facades\DB;

class TermService
{

    public static function search(array $searchParam): array
    {
        $tematiks = db::table('terms_tematikatermin')
            ->select('trnum',
                db::raw("
                COALESCE(json_agg(json_build_object('number', tmplanum,'name', tmname) order by tmplanum), '[]') as tematika"))
            ->leftJoin('terms_tematika', 'terms_tematika.tmnum', '=', 'terms_tematikatermin.tmnum')
            ->groupBy(['trnum']);

        $termBuilder = db::table('terms_termin')
            ->select(
                "terms_termin.trnum  as id",
                "terms_defterm.dtnum as defid",
                "trname              as terms",
                "dttext              as definition",
                "itname              as type",
                "dcnumber            as number",
                "dcname              as title",
                "dcgod               as year",
                "dtimportance        as priority",
                "tematika            as qualifiers",
                "dtprimech           as description")
            ->leftJoin("terms_defterm", "terms_termin.trnum", "=", "terms_defterm.trnum")
            ->leftJoin("terms_document", "terms_defterm.dtnum", "=", "terms_document.dtnum")
            ->leftJoin("terms_indterm", "terms_document.itnum", "=", "terms_indterm.itnum")
            ->leftJoinSub($tematiks, "t", "t.trnum", "=", "terms_termin.trnum")
            ->orderByRaw("lower(trname)")
            ->orderByDesc('dtimportance')
            ->orderBy('dcname');

        if (SearchHelper::isNotEmpty($searchParam['type'])) {
            if ($searchParam['type'] == 1) {
                $termBuilder->whereRaw("(dtimportance = 1 or dtimportance = 2)");
            } else {
                $termBuilder->whereRaw("dtimportance = 2");
            }
        }
        if (SearchHelper::isNotEmpty($searchParam['term'])) {
            $termBuilder->where('trname', 'ilike', SearchHelper::searchString($searchParam['term']));
        }
        if (SearchHelper::isNotEmpty($searchParam['definition'])) {
            $termBuilder->where('dttext', 'ilike', SearchHelper::searchString($searchParam['definition']));
        }
        if (SearchHelper::isNotEmpty($searchParam['typeDoc'])) {
            $termBuilder->where('itname', 'ilike', SearchHelper::searchString($searchParam['typeDoc']));
        }
        if (SearchHelper::isNotEmpty($searchParam['numberDoc'])) {
            $termBuilder->where('dcnumber', 'ilike', SearchHelper::searchString($searchParam['numberDoc']));
        }
        if (SearchHelper::isNotEmpty($searchParam['year'])) {
            $termBuilder->where('dcgod', '=', $searchParam['year']);
        }
        if (SearchHelper::isNotEmpty($searchParam['title'])) {
            $termBuilder->where('dcname', 'ilike', SearchHelper::searchString($searchParam['title']));
        }
        if (SearchHelper::isNotEmpty($searchParam['letter'])) {
            $termBuilder->where('trname', 'ilike', $searchParam['letter'] . '%');
        }

        $result = [];
        foreach ($termBuilder->get() as $term) {
            $result[] = [
                'id' => $term->defid,
                'terms' => $term->terms,
                'definition' => $term->definition,
                'type' => $term->type,
                'number' => $term->number,
                'title' => $term->title,
                'year' => $term->year,
                'priority' => $term->priority,
                'qualifiers' => $term->qualifiers ? json_decode($term->qualifiers) : null,
                'description' => $term->description,
            ];
        }
        if (SearchHelper::isNotEmpty($searchParam['qualifier'])) {
            $id = $searchParam['qualifier'];
            $result = array_values(array_filter($result, function ($item) use ($id) {
                if (!$item['qualifiers']) return false;
                foreach ($item['qualifiers'] as $qualifier) {
                    if ($qualifier->number == $id) return true;
                }
                return false;
            }));
        }

        if (SearchHelper::isNotEmpty($searchParam['query'])) {
            $result = SearchHelper::allSearchFilter($result, $searchParam['query']);
        }
        return $result;
    }

    public static function getOne($id): array
    {
        $tematiks = db::table('terms_tematikatermin')
            ->select('trnum',
                db::raw("
                COALESCE(json_agg(json_build_object('number', tmplanum,'name', tmname) order by tmplanum), '[]') as tematika"))
            ->leftJoin('terms_tematika', 'terms_tematika.tmnum', '=', 'terms_tematikatermin.tmnum')
            ->groupBy(['trnum']);

        $tender = db::table('terms_termin')
            ->select("terms_termin.trnum  as id",
                "terms_defterm.dtnum as defid",
                "trname              as name",
                "dttext              as definition",
                "itname              as ntd_type",
                "dcnumber            as ntd_num",
                "dcname              as ntd_name",
                "dcgod               as ntd_year",
                "dtimportance        as priority",
                "tematika            as qualifiers",
                "dtprimech           as note",
                "trdataentry         as date_entry",
                "trdataediting       as date_editing")
            ->leftJoin("terms_defterm", "terms_termin.trnum", "=", "terms_defterm.trnum")
            ->leftJoin("terms_document", "terms_defterm.dtnum", "=", "terms_document.dtnum")
            ->leftJoin("terms_indterm", "terms_document.itnum", "=", "terms_indterm.itnum")
            ->leftJoinSub($tematiks, "t", "t.trnum", "=", "terms_termin.trnum")
            ->where("terms_defterm.dtnum", "=", $id)
            ->first();
        $index = 0;
        $result[] =
            [
                'title' => 'Термин',
                'info' => $tender->name,
                'bold' => $tender->priority == 2 ? true : null,
                'index' => $index++,
            ];
        $result[] =
            [
                'title' => $tender->priority == 2 ? "Основное определение" : "Альтернативное определение",
                'info' => $tender->definition,
                'bold' => $tender->priority == 2 ? true : null,
                'index' => $index++,
            ];
        $result[] =
            [
                'title' => "Документ, где определен термин",
                'info' => $tender->ntd_type . " " . $tender->ntd_num . " " . $tender->ntd_name,
                'index' => $index++,
            ];
        if ($tender->qualifiers) {
            $qualifiers = implode('<br/>', array_map(function ($item) {
                return $item->number . ' ' . $item->name;
            }, json_decode($tender->qualifiers)));
            $result[] =
                [
                    'title' => "Классификация",
                    'info' => $qualifiers,
                    'index' => $index++,
                ];
        }
        $result[] =
            [
                'title' => "Примечание",
                'info' => $tender->note ?? ' ',
                'index' => $index++,
            ];
        $result[] =
            [
                'title' => "Подробная информация о документе",
                'info' => null,
                'bold' => true,
                'center' => true,
                'index' => $index++,
            ];
        if ($tender->ntd_num) {
            $author = db::table('bibl_authorntdi')
                ->select('ntdi_num',
                    db::raw("string_agg(authorntdi_author, '<br/>') as author"))
                ->groupBy(['ntdi_num']);
            $tematika = db::table('bibl_tematika')
                ->select('ntdi_num',
                    db::raw("
                COALESCE(json_agg(json_build_object('number', tplanum, 'name', tname) order by tplanum),'[]') as bibl_tema"))
                ->leftJoin('bibl_tematikantdi', 'bibl_tematikantdi.tnum', '=', 'bibl_tematika.tnum')
                ->groupBy(['ntdi_num']);
            $terms_ext = DB::table("bibl_mainntdi")
                ->select(
                    "bibl_mainntdi.ntdi_num                                            as id",
                    "ntdi_title                                                        as title",
                    db::raw("cast(upper(sindex_index || ' ' || sbelong_belong) as varchar(40)) as type_doc"),
                    "sbelong_belong                                                    as belong",
                    "ntdi_number                                                       as num_doc",
                    "author",
                    "ntdi_deistvuet                                                    as deistvuet",
                    "ntdi_vvoddeistv                                                   as vvod",
                    "ntdi_vpervie                                                      as vpervie",
                    "ntdi_zamennast                                                    as zamennast",
                    "ntdi_history                                                      as history_ntd",
                    "ntdi_histfras                                                     as history_ras",
                    "ntdi_pubyear                                                      as year_val",
                    "ntdi_mestoizdan                                                   as mestoizdan",
                    "ntdi_kolstr                                                       as count_page",
                    "ntdi_kod                                                          as kod",
                    "ntdi_izdatel                                                      as izdatel",
                    "ntdi_ext                                                          as ftris",
                    "ntdi_ctplanumik                                                   as num",
                    "ntdi_ctdatazakik                                                  as data",
                    "ntdi_textras                                                      as referat_type",
                    "ntdi_primech                                                      as primech",
                    "bibl_tema",
                    "ntdi_tolerance                                                    as access",
                    "ntdi_adrdergat                                                    as adrdergat")
                ->leftJoin("bibl_sindex", "bibl_mainntdi.sindex_num", "=", "bibl_sindex.sindex_num")
                ->leftJoin("bibl_sbelong", "bibl_mainntdi.sbelong_num", "=", "bibl_sbelong.sbelong_num")
                ->leftJoinSub($author, "author", "author.ntdi_num", "=", "bibl_mainntdi.ntdi_num")
                ->leftJoin("bibl_textsntdi", "bibl_mainntdi.ntdi_num", "=", "bibl_textsntdi.ntdi_num")
                ->leftJoinSub($tematika, "tematika", "tematika.ntdi_num", "=", "bibl_mainntdi.ntdi_num")
                ->where("bibl_mainntdi.ntdi_number", "=", $tender->ntd_num)
                ->first();
            if ($terms_ext) {
                $result[] =
                    [
                        'title' => "Наименование",
                        'info' => $terms_ext->title,
                        'bold' => $tender->priority == 2 ? true : null,
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Тип документа",
                        'info' => $terms_ext->type_doc,
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Разработчик",
                        'info' => $terms_ext->author,
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Действует",
                        'info' => $terms_ext->deistvuet,
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Дата введения в действие",
                        'info' => SearchHelper::formatDate($terms_ext->vvod),
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Введен впервые",
                        'info' => SearchHelper::formatDate($terms_ext->vpervie),
                        'index' => $index++,
                    ];
                if ($terms_ext->zamennast) {
                    $result[] =
                        [
                            'title' => "Обозначение документа, замененного настоящим",
                            'info' => $terms_ext->zamennast,
                            'index' => $index++,
                        ];
                }
                $result[] =
                    [
                        'title' => "Год издания",
                        'info' => $terms_ext->year_val,
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Место издания",
                        'info' => $terms_ext->mestoizdan,
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Количество страниц",
                        'info' => $terms_ext->count_page,
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Код",
                        'info' => $terms_ext->kod,
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Издательство",
                        'info' => $terms_ext->izdatel,
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Входит в ФТРиС",
                        'info' => $terms_ext->ftris == 10 ? "Да" : "Нет",
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Номер контракта",
                        'info' => $terms_ext->num,
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Дата контракта",
                        'info' => SearchHelper::formatDate($terms_ext->data),
                        'index' => $index++,
                    ];
                $result[] =
                    [
                        'title' => "Примечание",
                        'info' => $terms_ext->primech,
                        'index' => $index++,
                    ];
                if ($terms_ext->adrdergat) {
                    $result[] =
                        [
                            'title' => '<strong>Ссылка на документ</strong>',
                            'info' => SearchHelper::makeWebLink($terms_ext->adrdergat),
                            'index' => ++$index
                        ];
                }
                if ($terms_ext->bibl_tema) {
                    $bibl_tema = implode('<br/>', array_map(function ($item) {
                        return $item->number . ' ' . $item->name;
                    }, json_decode($terms_ext->bibl_tema)));
                    $result[] =
                        [
                            'title' => "Классификация",
                            'info' => $bibl_tema,
                            'index' => $index++,
                        ];
                }
                $uuid = SearchHelper::getUuidFile('ois08/ntdi', $terms_ext->id . "." . trim($terms_ext->referat_type));
                if ($uuid) {
                    $result[] = SearchHelper::makeDetail($index,
                        'Скачать документ',
                        SearchHelper::makeDownloadLink($uuid, $terms_ext->referat_type, $terms_ext->id . "." . trim($terms_ext->referat_type)));
                }
            }
        }
        return $result;
    }
}
