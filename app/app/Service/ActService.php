<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class ActService
{

    public static function search(array $searchParam): array
    {

        $ntdProjectBuilder = db::table('ik_niokr_ntdproject')
            ->select('ctnum',
                db::raw("coalesce(json_agg(json_build_object(
                    'title', nptitle,
                    'index', npindex,
                    'type', nprealiz)), '[]')::text as ntdproject"))
            ->where('nprealiz', 'ilike', 'Проект')
            ->groupBy('ctnum');
        $ntdiArray = db::table('bibl_mainntdi as m')
            ->select('ntdi_ctdatazakik as date',
                'ntdi_ctplanumik  as number',
                db::raw("coalesce(json_agg(json_build_object(
           'primech', ntdi_primech,
           'datavvod', ntdi_vvoddeistv,
           'dataapprov', ntdi_dateapproval,
           'title', ntdi_title,
           'num_doc', ntdi_number,
           'type_doc', (sindex_index || ' ' || sbelong_belong),
           'deistvuet', ntdi_deistvuet)), '[]')::text as ntdi_array"))
            ->leftJoin("bibl_sbelong as b", "m.sbelong_num", "=", "b.sbelong_num")
            ->leftJoin("bibl_textsntdi as t", "m.ntdi_num", "=", "t.ntdi_num")
            ->leftJoin("bibl_sindex as s", "m.sindex_num", "=", "s.sindex_num")
            ->whereNotNull("m.ntdi_ctdatazakik")
            ->whereNotNull("m.ntdi_ctplanumik")
            ->groupBy(["ntdi_ctdatazakik", "ntdi_ctplanumik"]);
        $sostBuilder = db::table("ik_niokr_sostconetapi")
            ->select("cenum",
                db::raw("COALESCE(
                json_agg(
                    json_build_object(
                        'doc', scepismo,
                        'date', scedata,
                        'status', psenum) order by psenum desc), '[]') as sost")
            )
            ->groupBy('cenum');
        $stageArray = db::table('ik_niokr_conetapi')
            ->select('ctnum',
                db::raw("COALESCE(json_agg(
                            json_build_object(
                                'etap_id', ik_niokr_claim.cenum,
                                'shtraf', clsumma,
                                'cldata', cldata,
                                'cldatepayment', cldatepayment,
                                'crdateexecut', crdateexecut,
                                'crdatecustom', crdatecustom,
                                'crcustomer', crcustomer,
                                'crsumma',crsumma,
                                'certificate_num',crnum,
                                'certificate_ras',crnum,
                                'crexecutor', crexecutor,
                                'etap_num', ceplanum,
                                'etap_name', cename,
                                'etap_date_end', cedataend,
                                'etap_summa', cekalendplansum,
                                'sost', sost) order by ceplanum)::text,'[]') as step"))
            ->leftJoinSub($sostBuilder, "s", "s.cenum", "=", "ik_niokr_conetapi.cenum")
            ->leftJoin("ik_niokr_claim", "ik_niokr_conetapi.cenum", "=", "ik_niokr_claim.cenum")
            ->leftJoin("ik_niokr_certificate", "ik_niokr_conetapi.cenum", "=", "ik_niokr_certificate.cenum")
            ->groupBy('ctnum');
        $actBuilder = db::table('ik_niokr_contract')
            ->select(
                'ik_niokr_contract.ctnum                  as id',
                'zgod                            as year_val',
                'sname                           as budget_item',
                'zname                           as title',
                'ctplanum                        as number',
                'ctdatazaversh                   as date_finish',
                'ctdatazakluch                   as date_start',
                'org_zak.onamekrat               as customer',
                'org_isp.onamekrat               as executor',
                'zpriceall                       as price_all',
                'zpricethisyear                  as price_year0',
                'zpricethisyplus1                as price_year1',
                'zpricethisyplus2                as price_year2',
                'ctsostoianie                    as stadia',
                'ctrlzpatent                     as patent',
                'ctrlzntd                        as ntd',
                'ctrlzproectntd                  as proect_ntd',
                'ctrlzbd                         as bdis',
                'ctregregnum                     as rk',
                'step',
                'ntdproject',
                "ntdi_array"
            )
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_contract.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoin("ik_niokr_zakazchik", "ik_niokr_zaiavka.znum", "=", "ik_niokr_zakazchik.znum")
            ->leftJoin("ik_niokr_statia", "ik_niokr_statia.snum", "ik_niokr_zaiavka.snum")
            ->leftJoin("ik_niokr_dokumentats", "ik_niokr_zaiavka.znum", "=", "ik_niokr_dokumentats.znum")
            ->leftJoin("ik_niokr_econobosn", "ik_niokr_zaiavka.znum", "=", "ik_niokr_econobosn.znum")
            ->leftJoin("ik_niokr_protokol", "ik_niokr_zaiavka.znum", "=", "ik_niokr_protokol.znum")
            ->leftJoin("ik_niokr_podrazdel", "ik_niokr_zaiavka.prnum", "=", "ik_niokr_podrazdel.prnum")
            ->leftJoin("ik_niokr_razdel", "ik_niokr_podrazdel.rnum", "=", "ik_niokr_razdel.rnum")
            ->leftJoin("ik_niokr_organiz as org_zak", "ik_niokr_zakazchik.onum", "=", "org_zak.onum")
            ->leftJoin("ik_niokr_organiz as org_isp", "ik_niokr_ispolnitel.onum", "org_isp.onum")
            ->leftJoinSub($stageArray, "stage", "stage.ctnum", "=", "ik_niokr_contract.ctnum")
            ->leftJoinSub($ntdProjectBuilder, "np", "np.ctnum", "=", "ik_niokr_contract.ctnum")
            ->leftJoinSub($ntdiArray, "ntdiArray", function (JoinClause $join) {
                $join
                    ->on('ntdiArray.date', '=', 'ctdatazakluch')
                    ->on("ntdiArray.number", "=", "ctplanum");
            })
            ->where(function ($query) {
                $query->where('zkmain', '=', true)->orWhereNull('zkmain');
            })
            ->orderByDesc('zgod')
            ->orderBy('ik_niokr_contract.ctnum');
        if (SearchHelper::isNotEmpty($searchParam['budgetItem'])) {
            $actBuilder->where('sname', 'ilike', SearchHelper::searchString($searchParam['budgetItem']));
        }
        if (SearchHelper::isNotEmpty($searchParam['yearFrom'])) {
            $actBuilder->where('zgod', '>=', $searchParam['yearFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['yearTo'])) {
            $actBuilder->where('zgod', '<=', $searchParam['yearTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['number'])) {
            $actBuilder->where('ctplanum', 'ilike', SearchHelper::searchString($searchParam['number']));
        }
        if (SearchHelper::isNotEmpty($searchParam['condition'])) {
            $actBuilder->where('ctsostoianie', 'ilike', SearchHelper::searchString($searchParam['condition']));
        }
        if (SearchHelper::isNotEmpty($searchParam['name'])) {
            $actBuilder->where('zname', 'ilike', SearchHelper::searchString($searchParam['name']));
        }
        if (SearchHelper::isNotEmpty($searchParam['executor'])) {
            $str = SearchHelper::searchString($searchParam['executor']);
            $actBuilder->where(function ($query) use ($str) {
                $query->where('org_isp.oname', 'ilike', $str)
                    ->orWhere('org_isp.onamekrat', 'ilike', $str);
            });
        }
        if (SearchHelper::isNotEmpty($searchParam['customer'])) {
            $str = SearchHelper::searchString($searchParam['customer']);
            $actBuilder->where(function ($query) use ($str) {
                $query->where('org_zak.oname', 'ilike', $str)
                    ->orWhere('org_zak.onamekrat', 'ilike', $str);
            });
        }
        if (SearchHelper::isNotEmpty($searchParam['dateStartFrom'])) {
            $actBuilder->where('ctdatazakluch', '>=', $searchParam['dateStartFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateStartTo'])) {
            $actBuilder->where('ctdatazakluch', '<=', $searchParam['dateStartTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateFinishFrom'])) {
            $actBuilder->where('ctdatazaversh', '>=', $searchParam['dateFinishFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateFinishTo'])) {
            $actBuilder->where('ctdatazaversh', '<=', $searchParam['dateFinishTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['status'])) {
            $actBuilder->where('zstatus', 'ilike', SearchHelper::searchString($searchParam['status']));
        }
////        claim: this.claim,

//        dd($actBuilder->toRawSql());
        $result = [];
        $aggregate['executor_and_customer'] = 'Итого';
        $aggregate['price_all'] = 0.0;
        $aggregate['claims'] = 0.0;
        foreach ($actBuilder->get() as $monitoring) {
            list($claim, $stage) = self::makeStage($monitoring->step);
            $result[] = [
                'id' => $monitoring->id,
                'title' => $monitoring->title . '<br/><br/>№ '
                    . $monitoring->number . ' от ' . SearchHelper::formatDate($monitoring->date_start),
                'working' => SearchHelper::formatDate($monitoring->date_start) . '<br/> - <br/>' . SearchHelper::formatDate($monitoring->date_finish),
                'executor_and_customer' => $monitoring->executor . '<br/> - <br/>' . $monitoring->customer,
                'price_all' => SearchHelper::number($monitoring->price_all),
                'claims' => $claim,
                'stage' => $stage,
                'realization' => self::makeRealization($monitoring->ntdi_array, $monitoring->ntdproject),
                'condition' => $monitoring->stadia == 'Завершен'
                    ? 'Завершен<br/><strong>' . SearchHelper::makeQuart($monitoring->date_finish) . '</strong>'
                    : $monitoring->stadia,
            ];
            $aggregate['price_all'] += SearchHelper::numberCalc($monitoring->price_all);
            $aggregate['claims'] += SearchHelper::numberCalc($claim);
        }
        if (SearchHelper::isNotEmpty($searchParam['claim'])) {
            $isClaim = strtolower($searchParam['claim']) == 'true';
            $result = array_values(array_filter($result, function ($item) use ($isClaim) {
                return (float)$item['claims'] > 0 ? $isClaim : !$isClaim;
            }));
        }
        $aggregate['price_all'] = SearchHelper::moneyThousand($aggregate['price_all']);
        $aggregate['claims'] = SearchHelper::moneyThousand($aggregate['claims']);
        return ['rows'=>$result,'aggregate'=>$aggregate];
    }

    private static function makeRealization($ntdi_array, $ntdproject_array): string
    {
        $result = [];
        if ($ntdi_array) {
            foreach (json_decode($ntdi_array, true) as $ntdi) {
                $result[] = $ntdi['type_doc'] . ' ' . $ntdi['num_doc'] . '<br/>' . $ntdi['title']
                    . ($ntdi['primech'] ?: '');
            }
        }
        if ($ntdproject_array) {
            foreach (json_decode($ntdproject_array, true) as $ntdproject) {
                $result[] = 'Проект ' . $ntdproject['index'] . '<br/>' . $ntdproject['title'];
            }
        }
        return implode('<hr/>', $result);
    }

    private static function makeStage($array): array
    {
        if (!$array) return [null, null, null];
        $claim = 0.0;
        $stage = [];
        foreach (json_decode($array, true) as $step) {
            $step['shtraf'] = str_replace(',', '.', $step['shtraf']);
            $claim += (float)$step['shtraf'];
            $isPayment = false;
            $context = [];
            if ($step['sost']) {
                foreach ($step['sost'] as $sost) {
                    $text = match ($sost['status']) {
                        1 => 'Сдан исполнителем: ',
                        6 => 'Направлен на оплату в ФЭУ: ',
                        7 => 'Оплачен: ',
                        default => '',
                    };
                    if ($text) $context[] = $text . SearchHelper::formatDate($sost['date']) . ' ' . $sost['doc'];
                    if ($sost['status'] == 7) {
                        $isPayment = true;
                    }
                }
            }
            $stage[] = '<strong>' . $step['etap_num'] . ' Этап:</strong>'
                . ' Дата сдачи исполнителем: ' . SearchHelper::formatDate($step['etap_date_end'])
                . '<br/>Стоимость этапа КП: <span class="text-primary">' . SearchHelper::moneyThousand($step['etap_summa']) . '</span>'
                . '<br/>Фактическая оплата:<br/><span class="text-primary">' . ($isPayment ? SearchHelper::moneyThousand($step['etap_summa'] ?: 0) : 0) . '</span><br/>'
                . implode('<br/>', $context)
                . '<br/><span class="text-danger fw-bold">Штраф: ' . SearchHelper::moneyThousand($step['shtraf']) . '</span>'
                . ($step['shtraf'] > 0
                    ? "<br/><span class='text-primary'>Дата подписания акта Исполнителем:</span> " . SearchHelper::formatDate($step["crdateexecut"])
                    . "<br/><span class='text-primary'>Подписавший акт от имени Исполнителя:</span> " . $step["crexecutor"]
                    . "<br/><span class='text-primary'>Дата подписания акта Заказчиком:</span> " . SearchHelper::formatDate($step["crdatecustom"])
                    . "<br/><span class='text-primary'>Подписавший акт от имени Заказчика:</span> " . $step["crcustomer"]
                    . "<br/><span class='text-primary'>Сумма денежных обязательств по акту:</span> " . SearchHelper::moneyThousand($step["crsumma"])
                    : '');

        }
        return [$claim, implode('<hr/>', $stage)];
    }
}
