<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Support\Facades\DB;

class AtsService
{
    private static function makeOrgBuilder()
    {
        $phones = db::table('ik_niokr_mentel')
            ->select('mnum',
                db::raw("json_agg(json_build_object(
                'tel',mtnumber, 'type',mtwhat)) as phones"))
            ->groupBy('mnum');
        $contacts = db::table('ik_niokr_men')
            ->select('onum',
                db::raw("json_agg(
               json_build_object(
                       'contact_job', mdol,
                       'contact_name', mname,
                       'contact_tel', phones,
                       'contact_email', memail)) as contact"))
            ->leftJoinSub($phones, 'phones_table', 'ik_niokr_men.mnum', '=', 'phones_table.mnum')
            ->groupBy('onum');

        return DB::table('ik_niokr_organiz')
            ->select(
                'ik_niokr_organiz.onum      as id',
                'onamekrat                  as short_name',
                'oname                      as full_name',
                'odopinform                 as type_business',
                'oadr                       as address',
                'owebadr                    as site',
                'oemail                     as email',
                'contact',
                'oblast                     as district',
                'goname                     as organization_group'
            )
            ->leftJoin("ik_niokr_grouporg", "ik_niokr_organiz.gonum", "=", "ik_niokr_grouporg.gonum")
            ->leftJoinSub($contacts, "contacts", "ik_niokr_organiz.onum", "=", "contacts.onum")
            ->orderByDesc('ik_niokr_organiz.onum');
    }

    public static function search(array $searchParam): array
    {
        $orgBuilder = self::makeOrgBuilder()
            ->where('ik_niokr_organiz.gonum', '!=', '2')
            ->where('oadrtel', '=', true);
        if (SearchHelper::isNotEmpty($searchParam['organizationName'])) {
            $orgBuilder->where('onamekrat', 'ilike', SearchHelper::searchString($searchParam['organizationName']))
                ->orWhere('oname', 'ilike', SearchHelper::searchString($searchParam['organizationName']));
        }
        if (SearchHelper::isNotEmpty($searchParam['typeBusiness'])) {
            $orgBuilder->where('odopinform', 'ilike', SearchHelper::searchString($searchParam['typeBusiness']));
        }
        if (SearchHelper::isNotEmpty($searchParam['organizationAddress'])) {
            $orgBuilder->where('oadr', 'ilike', SearchHelper::searchString($searchParam['organizationAddress']));
        }
        if (SearchHelper::isNotEmpty($searchParam['organizationSite'])) {
            $orgBuilder->where('owebadr', 'ilike', SearchHelper::searchString($searchParam['organizationSite']));
        }
        if (SearchHelper::isNotEmpty($searchParam['organizationEmail'])) {
            $orgBuilder->where('oemail', 'ilike', SearchHelper::searchString($searchParam['organizationEmail']));
        }
        if (SearchHelper::isNotEmpty($searchParam['district'])) {
            $orgBuilder->where('oblast', 'ilike', SearchHelper::searchString($searchParam['district']));
        }
        if (SearchHelper::isNotEmpty($searchParam["organizationGroup"])) {
            $orgBuilder->where('goname', 'ilike', SearchHelper::searchString($searchParam['organizationGroup']));
        }

        $result = [];
        foreach ($orgBuilder->get() as $org) {
            $result[] = [
                'id' => $org->id,
                'name' => $org->short_name . '<hr/>' . $org->full_name,
                'type_business' => $org->type_business ?? "",
                'address' => $org->address,
                'site' => SearchHelper::makeUrl($org->site),
                'email' => SearchHelper::makeEmail($org->email),
                'person' => self::getFirstPerson($org->contact),
                'district' => $org->district,
                'group' => $org->organization_group
            ];
        }
        if (SearchHelper::isNotEmpty($searchParam['allSearchQuery'])) {
            $result = SearchHelper::allSearchFilter($result, $searchParam['allSearchQuery']);
        }
        return $result;
    }

    private static function getFirstPerson($contactsJson): string
    {
        $contacts = json_decode($contactsJson, true);
        if (empty($contacts)) return '';
        return
            $contacts[0]['contact_name'] . '<hr/>'
            . 'Должность: ' . $contacts[0]['contact_job'] . '<br/>'
            . 'Телефон: ' . (empty($contacts[0]['contact_tel']) ? '' : $contacts[0]['contact_tel'][0]['tel']) . '<br/>'
            . 'Электронная&nbsp;почта: ' . SearchHelper::makeEmail($contacts[0]['contact_email']);
    }

    public static function getOne($id): array
    {
        $org = self::makeOrgBuilder()
            ->where('ik_niokr_organiz.onum', '=', $id)->first();
        $result = [];
        $index = 0;
        $result[] = SearchHelper::makeDetailBold(++$index, 'Сокращенное наименование организации', $org->short_name ?? "");
        $result[] = SearchHelper::makeDetailBold(++$index, 'Наименование организации', $org->full_name ?? "");
        $result[] = SearchHelper::makeDetail(++$index, 'Административная единица', $org->district);
        $result[] = SearchHelper::makeDetail(++$index, 'Группа организаций', $org->organization_group);
        $result[] = SearchHelper::makeDetail(++$index, 'Вид деятельности', SearchHelper::toSpoiler($org->type_business));
        $result[] = SearchHelper::makeDetail(++$index, 'Почтовый адрес', SearchHelper::makeAddressLink($org->address));
        $result[] = SearchHelper::makeDetail(++$index, 'Сайт', SearchHelper::makeUrl($org->site));
        $result[] = SearchHelper::makeDetail(++$index, 'Электронная почта организации', SearchHelper::makeEmail($org->email));
        if ($org->contact) {
            $result[] = SearchHelper::makeDetailCenter(++$index, 'Контакты:', null);
            foreach (json_decode($org->contact, true) as $contact) {
                $result[] = SearchHelper::makeDetail(++$index, 'Контактное лицо', $contact['contact_name']);
                $result[] = SearchHelper::makeDetail(++$index, 'Должность', $contact['contact_job']);
                $result[] = SearchHelper::makeDetail(++$index, 'Электронная почта контактного лица', SearchHelper::makeEmail($contact['contact_email']));
                if ($contact['contact_tel']) {
                    foreach ($contact['contact_tel'] as $phone) {
                        $type = match ($phone['type']) {
                                'tel' => 'Телефон',
                                'fax' => 'Факс',
                                't&f' => 'Телефон и факс'
                            } . ' контактного лица';
                        $result[] = SearchHelper::makeDetail(++$index, $type, $phone['tel']);
                    }
                }
            }
        }
        return $result;
    }

}
