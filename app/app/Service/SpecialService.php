<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class SpecialService
{
    public static function search(array $searchParam)
    {
        $specialBuilder = DB::table('ik_niokr_contract')
            ->select(
                "ik_niokr_contract.ctnum as id",
                "zgod                            as year_plan",
                "sname                           as budget_item",
                "zname                           as title",
                "org_isp.onamekrat               as executor",
                "ctplanum                        as number_contract",
                "ctdatazakluch                   as date_start",
                "ctdatazaversh                   as data_finish",
                "ctsostoianie                    as condition",
                "zpriceall                       as price",
                "ctrlzpatent                     as patent",
                "ctrlzntd                        as ntd",
                "ctrlzproectntd                  as proect_ntd",
                "ctrlzbd                         as bdis",
                "ctxnum                          as technology_code",
                "ctxname                         as technology_name",
                "pnnum                           as direction_code",
                "pnname                          as direction_name",
                "ctregregnum                     as registration_card",
                "ctfdainvnum                     as inventory_fda",
                "ctinvcart                       as inventory_card",
                "ctrnfi                          as number_rnfi",
                "ctnumcartri                     as number_card",
                "ctnumzaprri                     as number_request",
                "ctsistnumri                     as system_number")
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_contract.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoin("ik_niokr_zakazchik", "ik_niokr_zaiavka.znum", "=", "ik_niokr_zakazchik.znum")
            ->leftJoin("ik_niokr_statia", "ik_niokr_statia.snum", "=", "ik_niokr_zaiavka.snum")
            ->leftJoin("ik_niokr_dokumentats", "ik_niokr_zaiavka.znum", "=", "ik_niokr_dokumentats.znum")
            ->leftJoin("ik_niokr_econobosn", "ik_niokr_zaiavka.znum", "=", "ik_niokr_econobosn.znum")
            ->leftJoin("ik_niokr_protokol", "ik_niokr_zaiavka.znum", "=", "ik_niokr_protokol.znum")
            ->leftJoin("ik_niokr_podrazdel", "ik_niokr_zaiavka.prnum", "=", "ik_niokr_podrazdel.prnum")
            ->leftJoin("ik_niokr_razdel", "ik_niokr_podrazdel.rnum", "=", "ik_niokr_razdel.rnum")
            ->leftJoin("ik_niokr_organiz as org_zak", "ik_niokr_zakazchik.onum", "=", "org_zak.onum")
            ->leftJoin("ik_niokr_organiz as org_isp", "ik_niokr_ispolnitel.onum", "=", "org_isp.onum")
            ->leftJoin("ik_niokr_actdoc", "ik_niokr_contract.ctnum", "=", "ik_niokr_actdoc.ctnum")
            ->distinct()
            ->where(function ($query) {
                $query->where("zkmain", true)->orWhereNull("zkmain");
            })
            ->where(function ($query) {
                $query->where(function ($subQuery) {
                    $subQuery->where("zgod", "<", date("Y"))
                        ->whereIn("ctsostoianie", ['Завершен', 'Расторгнут']);
                })->orWhere("zgod", ">=", date("Y"));
            })
            ->orderByDesc("zgod");
//        dd($specialBuilder->toRawSql());


//        technology: this.technology,
//        direction: this.direction,
        if (SearchHelper::isNotEmpty($searchParam['technology'])) {
            if (SearchHelper::isNotEmpty($searchParam['technology']) != '-1') {
                $specialBuilder->where('ctxnum', '=', $searchParam['technology']);
            } else {
                $specialBuilder->whereNull('ctxnum');
            }
        }
        if (SearchHelper::isNotEmpty($searchParam['direction'])) {
            if (SearchHelper::isNotEmpty($searchParam['direction']) != '-1') {
                $specialBuilder->where('pnnum', '=', $searchParam['direction']);
            } else {
                $specialBuilder->whereNull('pnnum');
            }
        }
        if (SearchHelper::isNotEmpty($searchParam['executor'])) {
            $specialBuilder->where('org_isp.onamekrat', 'ilike', SearchHelper::searchString($searchParam['executor']));
        }
        if (SearchHelper::isNotEmpty($searchParam['customer'])) {
            $specialBuilder->where('org_zak.onamekrat', 'ilike', SearchHelper::searchString($searchParam['customer']));
        }

        if (SearchHelper::isNotEmpty($searchParam['budgetItem'])) {
            $specialBuilder->where('ik_niokr_zaiavka.snum', '=', $searchParam['budgetItem']);
        }
        if (SearchHelper::isNotEmpty($searchParam['condition'])) {
            $specialBuilder->where('ctsostoianie', '=', $searchParam['condition']);
        }

        if (SearchHelper::isNotEmpty($searchParam['status'])) {
            $specialBuilder->where('ik_niokr_zaiavka.zstatus', '=', $searchParam['status']);
        }
        if (SearchHelper::isNotEmpty($searchParam['registrationCart'])) {
            if ($searchParam['registrationCart'] == 'true') {
                $specialBuilder->whereNotNull('ctregregnum');
            } else {
                $specialBuilder->whereNull('ctregregnum');
            }
        }
        if (SearchHelper::isNotEmpty($searchParam['yearPlan'])) {
            $specialBuilder->where('zgod', '=', $searchParam['yearPlan']);
        }
        if (SearchHelper::isNotEmpty($searchParam['number'])) {
            $specialBuilder->where('ctplanum', 'ilike', SearchHelper::searchString($searchParam['number']));
        }
        if (SearchHelper::isNotEmpty($searchParam['name'])) {
            $specialBuilder->where('zname', 'ilike', SearchHelper::searchString($searchParam['name']));
        }
        if (SearchHelper::isNotEmpty($searchParam['additional'])) {
            $specialBuilder->where('ik_niokr_actdoc.actype', '=', '1');
        }
        if (SearchHelper::isNotEmpty($searchParam['claim'])) {
            $specialBuilder->where('ik_niokr_actdoc.actype', '=', '3');
        }
        if (SearchHelper::isNotEmpty($searchParam['implementation'])) {
            switch ($searchParam['implementation']) {
                case 'patent':
                    $specialBuilder->where('ctrlzpatent', '=', 'true');
                    break;
                case 'ntd':
                    $specialBuilder->where('ctrlzntd', '=', 'true');
                    break;
                case 'projectNtd':
                    $specialBuilder->where('ctrlzproectntd', '=', 'true');
                    break;
                case 'bdis':
                    $specialBuilder->where('ctrlzbd', '=', 'true');
                    break;
            }
        }

        if (SearchHelper::isNotEmpty($searchParam['dateStartFrom'])) {
            $specialBuilder->where('ctdatazakluch', '>=', $searchParam['dateStartFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateStartTo'])) {
            $specialBuilder->where('ctdatazakluch', '<=', $searchParam['dateStartTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateFinishFrom'])) {
            $specialBuilder->where('ctdatazaversh', '>=', $searchParam['dateFinishFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateFinishTo'])) {
            $specialBuilder->where('ctdatazaversh', '<=', $searchParam['dateFinishTo']);
        }

//        dd($specialBuilder->toRawSql());
        $result = [];

        foreach ($specialBuilder->get() as $special) {
            $result[] = [
                "id" => $special->id,
                "yearPlan" => $special->year_plan,
                "budgetItem" => $special->budget_item,
                "title" => $special->title,
                "executor" => $special->executor,
                "numberContract" => $special->number_contract,
                "dateStart" => SearchHelper::formatDate($special->date_start),
                "dataFinish" => SearchHelper::formatDate($special->data_finish),
                "condition" => $special->condition,
                "price" => number_format(str_replace(',', '.', $special->price) * 1000, 2, ',', '\'') . '₽',
                "implementation" => [
                    $special->patent ? 'Патент' : null,
                    $special->ntd ? 'НТД' : null,
                    $special->proect_ntd ? 'Проект НТД' : null,
                    $special->bdis ? 'БД/ИС' : null
                ],
                "technology" => trim($special->technology_code . ' ' . $special->technology_name),
                "direction" => trim($special->direction_code . ' ' . $special->direction_name),
                "card" => [
                    $special->registration_card ? 'Номер РК: ' . $special->registration_card : null,
                    $special->inventory_fda ? 'Инвентарный номер ФДА: ' . $special->inventory_fda : null,
                    $special->inventory_card ? 'Номер Инвентарной карточки: ' . $special->inventory_card : null,
                    $special->number_rnfi ? 'Номер РНФИ: ' . $special->number_rnfi : null,
                    $special->number_card ? 'Номер карты (записи) ЕСУГИ: ' . $special->number_card : null,
                    $special->number_request ? 'Номер запроса ЕСУГИ: ' . $special->number_request : null,
                    $special->system_number ? 'Системный номер ЕСУГИ: ' . $special->system_number : null
                ]
            ];
        }
        if (SearchHelper::isNotEmpty($searchParam['allSearchQuery'])) {
            $result = SearchHelper::allSearchFilter($result, $searchParam['allSearchQuery']);
        }

        return $result;
    }

    public static function getOne(string $id)
    {
        $docsPath = 'KonkDok';
        $protocolPath = 'KonkProt';
        $ntdiNums = DB::table('bibl_mainntdi')
            ->select(
                "ntdi_ctdatazakik",
                "ntdi_ctplanumik",
                DB::raw("json_agg(ntdi_num)::text AS ntdi_nums")
            )
            ->groupBy(["ntdi_ctdatazakik", "ntdi_ctplanumik"]);
        $stageDb = DB::table('ik_niokr_conetapi')
            ->select('ctnum',
                db::raw("COALESCE(
                                   json_agg(
                                           json_build_object('step', ceplanum,
                                                             'name', cename,
                                               'date',cedataend,
                                               'payment', cesumwinner)), '[]')::text as stage")
            )
            ->orderBy('ctnum')
            ->groupBy(['ctnum']);
        $ikrbs = DB::table('ik_niokr_conetapi')
            ->select('ctnum',
                db::raw("COALESCE(
               json_agg(
                       json_build_object('step', ceplanum,
                                         'ikrbs_id', cenum,
                                         'ikrbs_number', cereginvnum,
                                         'ikrbs_date', ceregdata,
                                         'ikrbs_file_type', ceregcartras)), '[]')::text as ikrbs")
            )
            ->whereNotNull('cereginvnum')
            ->orderBy('ctnum')
            ->groupBy(['ctnum']);

        $classification = DB::table('arxiv_tema as t')
            ->select("tplanum",
                "t.ctdatazakik",
                "tsrproectntd",
                "treferat",
                'ctplanumik',
                "tsrpatent",
                "tsrntd",
                "tsrbd",
                db::raw("string_agg(clplanum || ' ' || clname, '<br/>') as classification")
            )
            ->leftJoin("arxiv_classtema as ct", "t.tnum", "=", "ct.tnum")
            ->leftJoin("arxiv_classall as ca", "ct.clnum", "=", "ca.clnum")
            ->where("t.tzavershena", "=", true)
            ->groupBy(["tplanum", "t.ctdatazakik", 'ctplanumik', "tsrproectntd", "tsrpatent", "tsrntd", "tsrbd", "treferat"]);
        $ntdProject = db::table('ik_niokr_ntdproject')
            ->select('ctnum',
                db::raw("COALESCE(json_agg(
               json_build_object('type_doc', npindex,
                                 'title', nptitle,
                                 'status', netype,
                                 'date', nedata,
                                 'remark', nedescript,
                                 'filename', ndname,
                                 'file_type', ndras,
                                 'number', ndnum
                   )), '[]')::text as ntdproject"))
            ->leftJoin("ik_niokr_ntdevents", "ik_niokr_ntdevents.npnum", "=", "ik_niokr_ntdproject.npnum")
            ->leftJoin("ik_niokr_ntddocum", "ik_niokr_ntddocum.nenum", "=", "ik_niokr_ntdevents.nenum")
            ->groupBy("ctnum");
        $ntd = db::table('bibl_mainntdi as m')
            ->select("m.ntdi_ctdatazakik as date_ntd",
                "m.ntdi_ctplanumik as number_ntd",
                db::raw("COALESCE(json_agg(
                        json_build_object(
                                'tbl', 'ntdi',
                                'id', m.ntdi_num,
                                'title', ntdi_title,
                                'number', ntdi_number,
                                'filetype', ntdi_textras,
                                'type_doc', sindex_index || ' ' || sbelong_belong
                            )), '[]')::text as ntd"))
            ->leftJoin("bibl_sbelong as b", "m.sbelong_num", "=", "b.sbelong_num")
            ->leftJoin("bibl_textsntdi as t", "m.ntdi_num", "=", "t.ntdi_num")
            ->leftJoin("bibl_sindex as s", "m.sindex_num", "=", "s.sindex_num")
            ->groupBy(["m.ntdi_ctdatazakik", "m.ntdi_ctplanumik"]);
        $ntdi = db::table('bibl_mainntd as m')
            ->select("m.ntd_ctdatazakik as date_ntd",
                "m.ntd_ctplanumik as number_ntd",
                db::raw("COALESCE(json_agg(
                        json_build_object(
                                'tbl', 'ntdo',
                                'id', m.ntd_num,
                                'title', ntd_title,
                                'number', null,
                                'filetype', ntd_textras,
                                'type_doc', sgroup_group
                            )), '[]')::text as ntd"))
            ->leftJoin("bibl_textsntd as t", "m.ntd_num", "=", "t.ntd_num")
            ->leftJoin("bibl_sgroup as s", "m.sgroup_num", "=", "s.sgroup_num")
            ->groupBy(["m.ntd_ctdatazakik", "m.ntd_ctplanumik"]);
        $ntd->union($ntdi);
        $bdis = db::table('rntd_commondatekontr')
            ->select(
                "cddatezakluch",
                "cdkontrnum",
                db::raw("COALESCE(json_agg(
                    json_build_object(
                        'title', ounameobject,
                   'number', ourospatsvidetnum,
                   'date', ourospatsvidetdate,
                   'type', ourospatsvidetkategor,
                   'context', onaznach)), '[]')::text as bdis"))
            ->leftJoin("rntd_objectu", "rntd_commondatekontr.cdnum", "=", "rntd_objectu.cdnum")
            ->leftJoin("ofap_object", "ofap_object.oname", "like", "rntd_objectu.ounameobject")
            ->groupBy(["cddatezakluch", "cdkontrnum"]);
        $specialBuilder = DB::table('ik_niokr_contract')
            ->select(
                "ik_niokr_contract.ctnum    as id",
                "ik_niokr_zaiavka.znum              as znum",
                "zname                              as title",
                "zgod                               as year_plan",
                "sname                              as budget_item",
                "zlotnum                            as lot",
                "docs.uuid                          as docs",
                "protocol.uuid                      as protocol",
                "org_zak.oname                      as customer",
                "org_isp.onamekrat                  as executor_short",
                "org_isp.oname                      as executor_full",
                "ctplanum                           as number",
                "ctdatazakluch                      as date_start",
                "ctdatazaversh                      as date_finish",
                "eras                               as file_type",
                "ctsostoianie                       as condition",
                "zpriceall                          as price",
                "ctregregnum                        as rk",
                "ctregdata                          as rk_date",
                "ctregcartras                       as rk_file_type",
                "ctrlztext                          as rk_text",
                "ctpractispol                       as practical_use",
                "ctxnum                             as technology_code",
                "ctxname                            as technology_name",
                "pnnum                              as direction_code",
                "pnname                             as direction_name",
                "ik_niokr_vnedrenie.vrvnedren       as vnedren",
                "ctrlzproectntd                     as proect_ntd",
                "ctindevelopt                       as history_ntd",
                "cthistfras                         as history_ntd_ras",
                "ctfdainvnum",
                "ctinvcart",
                "ctrnfi",
                "ctnumcartri",
                "ctnumzaprri",
                "ctsistnumri",
                "ikrbs",
                "tsrproectntd",
                "treferat",
                "ctrlzsp",
                "ntdproject",
                "ntd",
                "bdis",
                "tplanum                            as number_archive",
                db::raw("coalesce(
           json_agg(
               json_build_object('type', actype,
                   'name', acname,
                   'doctype',acras)), '[]') as acts"),
                db::raw("COALESCE(ntdi_nums,'[]') as ntdi_nums"),
                'stage',
                'classification'
            )
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_contract.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoin("ik_niokr_zakazchik", "ik_niokr_zaiavka.znum", "=", "ik_niokr_zakazchik.znum")
            ->leftJoin("ik_niokr_statia", "ik_niokr_statia.snum", "=", "ik_niokr_zaiavka.snum")
            ->leftJoin("ik_niokr_dokumentats", "ik_niokr_zaiavka.znum", "=", "ik_niokr_dokumentats.znum")
            ->leftJoin("ik_niokr_econobosn", "ik_niokr_zaiavka.znum", "=", "ik_niokr_econobosn.znum")
            ->leftJoin("ik_niokr_protokol", "ik_niokr_zaiavka.znum", "=", "ik_niokr_protokol.znum")
            ->leftJoin("ik_niokr_podrazdel", "ik_niokr_zaiavka.prnum", "=", "ik_niokr_podrazdel.prnum")
            ->leftJoin("ik_niokr_razdel", "ik_niokr_podrazdel.rnum", "=", "ik_niokr_razdel.rnum")
            ->leftJoin("ik_niokr_organiz as org_zak", "ik_niokr_zakazchik.onum", "=", "org_zak.onum")
            ->leftJoin("ik_niokr_organiz as org_isp", "ik_niokr_ispolnitel.onum", "=", "org_isp.onum")
            ->leftJoin("ik_niokr_vnedrenie", "ik_niokr_vnedrenie.ctnum", "=", "ik_niokr_contract.ctnum")
            ->leftJoin("ik_niokr_actdoc", "ik_niokr_contract.ctnum", "=", "ik_niokr_actdoc.ctnum")
            ->leftJoinSub($ntdiNums, "ntdiNums", function (JoinClause $join) {
                $join
                    ->on('ntdi_ctdatazakik', '=', 'ctdatazakluch')
                    ->on("ntdi_ctplanumik", "=", "ctplanum");
            })
            ->leftJoinSub($ntd, "ntd", function (JoinClause $join) {
                $join
                    ->on('date_ntd', '=', 'ctdatazakluch')
                    ->on("number_ntd", "=", "ctplanum");
            })
            ->leftJoin('files as docs', function (JoinClause $join) use ($docsPath) {
                $join
                    ->where("docs.path", "=", $docsPath)
                    ->on("docs.filename", "=", db::raw("(ik_niokr_ZAIAVKA.ZNUM::text || '.doc'::text)"));
            })
            ->leftJoin('files as protocol', function (JoinClause $join) use ($protocolPath) {
                $join
                    ->where("protocol.path", "=", $protocolPath)
                    ->on("protocol.filename", "=", db::raw("(ik_niokr_ZAIAVKA.ZNUM::text || '.doc'::text)"));
            })
            ->leftJoinSub($stageDb, 'payment', 'payment.ctnum', '=', 'ik_niokr_contract.ctnum')
            ->leftJoinSub($ikrbs, 'ikrbs', 'ikrbs.ctnum', '=', 'ik_niokr_contract.ctnum')
            ->leftJoinSub($ntdProject, 'ntdProject', 'ntdProject.ctnum', '=', 'ik_niokr_contract.ctnum')
            ->leftJoinSub($classification, "c", function (JoinClause $join) use ($protocolPath) {
                $join
                    ->on("c.ctdatazakik", "=", "ik_niokr_contract.ctdatazakluch")
                    ->on("c.ctplanumik", "=", "ik_niokr_contract.ctplanum");
            })
            ->leftJoinSub($bdis, "b", function (JoinClause $join) use ($protocolPath) {
                $join
                    ->on("b.cddatezakluch", "=", "ik_niokr_contract.ctdatazakluch")
                    ->on("b.cdkontrnum", "=", "ik_niokr_contract.ctplanum");
            })
            ->where("ik_niokr_contract.ctnum", "=", $id)
            ->groupby([
                "ik_niokr_contract.ctnum", "ik_niokr_zaiavka.znum", "title", "year_plan", "budget_item", "lot", "docs",
                "protocol", "executor_short", "executor_full", "number", "date_start", "date_finish", "file_type",
                "condition", "price", "rk", "rk_date", "rk_file_type", "rk_text", "practical_use", "technology_code",
                "technology_name", "direction_code", "direction_name", "vnedren", "proect_ntd", "history_ntd",
                "history_ntd_ras", "ctfdainvnum", "ctinvcart", "ctrnfi", "ctnumcartri", "ctnumzaprri", "ctsistnumri",
                "ntdi_nums", 'stage', "classification", "tplanum", "ikrbs", "org_zak.oname", "tsrproectntd",
                "tsrpatent", "tsrntd", "tsrbd", "ctrlzsp", "treferat", "ntdproject", "ntd", "bdis"]);
//        dd($specialBuilder->toRawSql());
        $special = $specialBuilder->first();
        $result[] = [
            'title' => 'Общая информация',
            'info' => [
                SearchHelper::makeTr('Наименование контракта', $special->title),
                SearchHelper::makeTr('Год плана', $special->year_plan),
                SearchHelper::makeTr('Статья бюджета', $special->budget_item),
                SearchHelper::makeTr('Заказчик', $special->customer),
                SearchHelper::makeTr('Номер контракта', $special->number),
            ]
        ];
        if ($special->lot != null) {
            $lotInfo = explode(' ', $special->lot, 2);
            $tender = [
                'title' => 'Конкурс',
                'info' => [
                    SearchHelper::makeTr('Дата конкурса', SearchHelper::formatDate($lotInfo[0])),
                    SearchHelper::makeTr('Номер лота', $lotInfo[1]),
                ]
            ];
            if ($special->docs !== null) {
                $value = "<a href='/api/download/?file={$special->docs}' target='_blanks'><img src='/img/doc.png' width='50'></a>";
                $tender['info'][] = SearchHelper::makeTr('Конкурсная документация', $value);
            }
            if ($special->docs !== null) {
                $value = "<a href='/api/download/?file={$special->docs}' target='_blanks'><img src='/img/doc.png' width='50'></a>";
                $tender['info'][] = SearchHelper::makeTr('Протоколы конкурса', $value);
            }
            $result[] = $tender;
        }
        $props = [
            'title' => 'Реквизиты контракта',
            'info' => [
                SearchHelper::makeTr('Исполнитель (краткое наименование организации)', $special->executor_short),
                SearchHelper::makeTr('Исполнитель (полное наименование организации)', $special->executor_full),
                SearchHelper::makeTr('Дата заключения контракта', SearchHelper::formatDate($special->date_start)),
                SearchHelper::makeTr('Дата завершения контракта', SearchHelper::formatDate($special->date_finish)),
                SearchHelper::makeTr('Стадия выполнения', $special->condition),
            ]
        ];

        if ($special->file_type != null) {
            $path = 'econobos/' . date("Y", strtotime($special->date_start));
            $fileContract = str_replace("/", "_", $special->number) . " " . SearchHelper::formatDate($special->date_start) . "." . $special->file_type;
            $uuid = SearchHelper::getUuidFile($path, $fileContract);
            if ($uuid != null) {
                $props['info'][] =
                    SearchHelper::makeTr('Текст контракта', SearchHelper::makeDownloadLink($uuid, $special->file_type));
            }
        }
        $docs = self::makePropsDocs($special->id, $special->acts);
        if ($docs !== null) {
            $props['info'] = array_merge($props['info'], $docs);
        }
        $result[] = $props;
        $result[] = [
            'title' => 'Этапы контракта',
            'info' => [
                self::makeStage($id)
            ]
        ];
        $result[] = [
            'title' => 'Оплата работ',
            'info' => self::makePayment($special->price, $special->stage)
        ];
        $result[] = [
            'title' => 'Классификация',
            'info' => [
                SearchHelper::makeTr('Классификатор', $special->classification ?: '-')
            ]
        ];
        $report = [
            'title' => 'Отчетные материалы',
            'info' => self::makeReport($special->number, $special->date_start)
        ];
        if ($special->number_archive != null) {
            $referat = [
                SearchHelper::makeTr('Номер в архиве', explode('.', $special->number_archive, 1)[0]),
                SearchHelper::makeTr('Аннотация:', null, true),
                SearchHelper::makeTr($special->treferat, null),
                SearchHelper::makeTr('Практическое использование результатов НИОКР:', null, true),
                SearchHelper::makeTr($special->practical_use, null),
                SearchHelper::makeTr('Электронный вид отчета:', null, true),
            ];
            $report['info'] = array_merge($referat, $report['info']);
        }
        $result[] = $report;
        $rk = [
            'title' => 'Регистрационные карты ЦИТИС',
            'info' => []
        ];
        if ($special->rk !== null) {
            $rkInfo = [];
            $rkInfo[] = 'Номер карты: ' . $special->rk;
            if ($special->rk_date && strtotime($special->rk_date) > 0) {
                $rkInfo[] = 'Дата создания карты: ' . SearchHelper::formatDate($special->rk_date);
            }
            if ($special->rk_file_type) {
                $filename = $special->id . '_' . $special->rk . '.' . $special->rk_file_type;
                $rkFileUuid = SearchHelper::getUuidFile('RK', $filename);
                if ($rkFileUuid !== null) {
                    $rkInfo[] = "Скачать файл карты: " . SearchHelper::makeDownloadLink($rkFileUuid, $special->rk_file_type);
                }
            }
            $rk['info'][] = SearchHelper::makeTr('Карта РК', implode('<hr/>', $rkInfo));
        }
        if (!empty(json_decode($special->ikrbs, true))) {
            foreach (json_decode($special->ikrbs, true) as $rkIkrbs) {
                $rkInfo = [];
                $rkInfo[] = 'Номер карты: ' . $rkIkrbs['ikrbs_number'];
                if ($rkIkrbs['ikrbs_date'] && strtotime($rkIkrbs['ikrbs_date']) > 0) {
                    $rkInfo[] = 'Дата создания карты: ' . SearchHelper::formatDate($rkIkrbs['ikrbs_date']);
                }
                if ($rkIkrbs['ikrbs_file_type']) {
                    $filename = $rkIkrbs["ikrbs_id"] . "_" . $rkIkrbs["ikrbs_number"] . "." . $rkIkrbs['ikrbs_file_type'];
                    $rkFileUuid = SearchHelper::getUuidFile('IKRBS', $filename);
                    if ($rkFileUuid !== null) {
                        $rkInfo[] = "Скачать файл карты: " . SearchHelper::makeDownloadLink($rkFileUuid, $rkIkrbs['ikrbs_file_type']);
                    }
                }
                $rk['info'][] = SearchHelper::makeTr('Карта ИКРБС (' . $rkIkrbs['step'] . ' этап)', implode('<hr/>', $rkInfo));
            }
        }
        $result[] = $rk;
        $technology = [
            'title' => 'Критические технологии, приоритетные направления',
            'info' => []
        ];
        if ($special->technology_code !== null) {
            $technology['info'][] = SearchHelper::makeTr('Номер КТ', $special->technology_code);
        }
        if ($special->technology_name !== null) {
            $technology['info'][] = SearchHelper::makeTr('Наименование КТ', $special->technology_name);
        }
        if ($special->direction_code !== null) {
            $technology['info'][] = SearchHelper::makeTr('Номер приоритетного направления', $special->direction_code);
        }
        if ($special->direction_name !== null) {
            $technology['info'][] = SearchHelper::makeTr('Наименование приоритетного направления', $special->direction_name);
        }
        $result[] = $technology;
        $contractExecute = [
            'title' => 'Реализация контракта',
            'info' => []
        ];
        if (!empty(json_decode($special->ntd))) {
            $info[] = SearchHelper::makeTr('Все реализации НТД по контракту:', null, true);
            $strWhere = [];
            foreach (json_decode($special->ntd, true) as $ntd) {
                $filename = $ntd['id'] . trim($ntd['filetype']);
                $uuid = SearchHelper::getUuidFile('ois08/' . $ntd['tbl'], $filename);
                $info[] = SearchHelper::makeTr('Вид реализации: ' . $ntd['type_doc'],
                    "{$ntd['type_doc']} {$ntd['number']} {$ntd['title']}"
                    . ($uuid ? "<hr/>" . SearchHelper::makeDownloadLink($uuid, $ntd['filetype']) : ''));
                $strWhere[] = "filename ilike '%_{$ntd['id']}_clo_%'";
            }
            $files = db::table('files')
                ->where('path', '=', 'TsptNtdi')
                ->whereRaw('(' . implode(' or ', $strWhere) . ')')
                ->get();
            foreach ($files as $file) {
                $filename_ = explode('_', $file->filename);
                $filename = explode('.', end($filename_));
                $info[] = SearchHelper::makeTr('Распоряжение об утверждении',
                    end($filename_) . '<hr/>' . SearchHelper::makeDownloadLink($file->uuid, end($filename)));
            }
            $contractExecute['info'] = $info;
        }
        if (!empty(json_decode($special->bdis, true))) {
            $bdisInfo = [];
            foreach (json_decode($special->bdis, true) as $bdis) {
                if ($bdis['title']) {
                    $context = $bdis['title'] . ' №&nbsp;' . $bdis['number'] . ' от ' . SearchHelper::formatDate($bdis['date'])
                        . ($bdis['context'] ? '<hr/>' . $bdis['context'] : '');
                    $bdisInfo[] = SearchHelper::makeTr($bdis['type'], $context);
                }
            }
            if (!empty($bdisInfo)) {
                $info[] = SearchHelper::makeTr('Все реализации РИД по контракту:', null, true);
                $info = array_merge($info, $bdisInfo);
                $path = 'svidet/' . date("Y", strtotime($special->date_start)) . '/'
                    . str_replace("-", " ", str_replace("/", "_", $special->number) . ' '
                        . SearchHelper::formatDate($special->date_start));
                $files = db::table('files')->where('path', '=', $path);
                $listFile = [];
                foreach ($files->get() as $file) {
                    $fileType = explode('.', $file->filename);
                    $listFile[] = SearchHelper::makeDownloadLink($file->uuid, end($fileType));
                }
                if (!empty($listFile)) {
                    $info[] = SearchHelper::makeTr('Охранные документы РИД Роспатента', implode('', $listFile));
                }
                $contractExecute['info'] = $info;
            }
        }
        if (empty(json_decode($special->ntd)) && !empty(json_decode($special->ntdproject))) {
            $info[] = SearchHelper::makeTr($special->ctrlzsp ? 'НТД СП:' : 'Проект НТД:', null, true);
            foreach (json_decode($special->ntdproject, true) as $ntdproject) {
                $info[] = SearchHelper::makeTr(
                    'Вид документа ' . ($special->ctrlzsp ? 'НТД СП' : 'Проекта НТД'),
                    $ntdproject['type_doc']
                );
                $info[] = SearchHelper::makeTr(
                    'Наименование ' . ($special->ctrlzsp ? 'НТД СП' : 'Проекта НТД'),
                    $ntdproject['title']
                );
                $info[] = SearchHelper::makeTr('Состояние', $ntdproject['status']);
                if ($ntdproject['remark']) {
                    $info[] = SearchHelper::makeTr('Дополнительная информация', $ntdproject['remark']);
                }
                $filename = $ntdproject['number'] . '_' . $ntdproject['filename'] . '.' . $ntdproject['file_type'];
                $uuid = SearchHelper::getUuidFile('ProjectNTD', $filename);
                if ($uuid) {
                    $info[] = SearchHelper::makeTr('Документы НТД',
                        SearchHelper::makeDownloadLink($uuid, $ntdproject['file_type']));
                }
            }
            $contractExecute['info'] = $info;
        }
        $result[] = $contractExecute;
        $history = [];
        if ($special->history_ntd) {
            $uuid = SearchHelper::getUuidFile('HISTORY_NIOKR', $special->id . '.jpg');
            $history[] = SearchHelper::makeTr('История НТД',
                $uuid ? SearchHelper::makeDownloadLink($uuid, 'jpg') : 'Нет данных');
        }
        $result[] = [
            'title' => 'История НТД',
            'info' => $history
        ];
        $fda = [
            'title' => 'Постановка на баланс (ФДА)',
            'info' => []
        ];
        if ($special->ctfdainvnum) {
            $fda['info'][] = SearchHelper::makeTr('Инвентарный номер ФДА', $special->ctfdainvnum);
        }
        if ($special->ctinvcart) {
            $fda['info'][] = SearchHelper::makeTr('Номер Инвентарной карточки', $special->ctinvcart);
        }
        $result[] = $fda;
        $fpma = [
            'title' => 'Постановка на баланс (Росимущество)',
            'info' => []
        ];
        if ($special->ctrnfi) {
            $fpma['info'][] = SearchHelper::makeTr('Номер РНФИ', $special->ctrnfi);
        }
        if ($special->ctnumcartri) {
            $fpma['info'][] = SearchHelper::makeTr('Номер карты (записи) ЕСУГИ', $special->ctnumcartri);
        }
        if ($special->ctnumzaprri) {
            $fpma['info'][] = SearchHelper::makeTr('Номер запроса ЕСУГИ', $special->ctnumzaprri);
        }
        if ($special->ctsistnumri) {
            $fpma['info'][] = SearchHelper::makeTr('Системный номер ЕСУГИ', $special->ctsistnumri);
        }
        $result[] = $fpma;
        return $result;
    }

    private static function makePropsDocs(string $id, string $propsJson): ?array
    {
        $props = json_decode($propsJson, true);
        if (empty($props)) return null;
        $docs = [];
        foreach ($props as $prop) {
            $title = match (trim($prop['type'])) {
                '0' => "Акт сдачи работ",
                '1' => "Дополнительное соглашение",
                '2' => "Протокол заседания НТС",
                '3' => "Претензия",
                '4' => "Решение ФАС",
                default => "Неизвестный тип документа",
            };
            $filename = $id . "_" . str_replace("/", "-", $prop['name']) . "." . $prop['doctype'];
            $uuid = SearchHelper::getUuidFile('ACTDOC', $filename);
            if ($uuid !== null) {
                $value = SearchHelper::makeDownloadLink($uuid, $prop['doctype']) . "<br/>{$prop['name']}</a>";
                $docs[] = SearchHelper::makeTr($title, $value);
            }
        }
        return $docs;
    }

    private static function makeStage($id)
    {
        $stageQuery = db::table('ik_niokr_conetapi')
            ->select(
                "ik_niokr_conetapi.ceplanum as step",
                "cename      as name",
                "cedataend   as date",
                "cesumwinner as payment",
                "psename     as status",
                "crdateexecut as executor_date_sign",
                "crexecutor as executor_sign",
                "crdatecustom as customer_date_sign",
                "crcustomer as customer_sign",
                "crsumma as price",
                "s.cenum as id",
                "crnum        as certificate_number",
                "crfileactras as file_type")
            ->leftJoin(db::raw("(
            select
              distinct on(1) cenum, psenum, scedata
            from ik_niokr_sostconetapi
            order by cenum, scedata desc) as s"), "ik_niokr_conetapi.cenum", "=", "s.cenum")
            ->leftJoin("ik_niokr_perechensostetapi", "ik_niokr_perechensostetapi.psenum", "=", "s.psenum")
            ->leftJoin("ik_niokr_certificate", "ik_niokr_certificate.cenum", "=", "ik_niokr_conetapi.cenum")
            ->where("ctnum", "=", $id)
            ->orderBy('ceplanum');
        $result = [];
        foreach ($stageQuery->get() as $stage) {
            $info = "<strong>{$stage->step} Этап:</strong> {$stage->name}<br/>"
                . "Дата сдачи Исполнителем: " . SearchHelper::formatDate($stage->date) . "<br/>"
                . ($stage->status == null ? '' : "Состояние: <span class='text-success'>$stage->status</span><br/>")
                . "Сумма: <span class='text-primary'>" . SearchHelper::moneyThousand($stage->payment) . "</span><br/>"
                . ($stage->executor_date_sign == null ? '' : "Дата подписания акта Исполнителем: " . SearchHelper::formatDate($stage->executor_date_sign) . "<br/>")
                . ($stage->executor_sign == null ? '' : "Подписавший акт от имени Исполнителя: {$stage->executor_sign}<br/>")
                . ($stage->customer_date_sign == null ? '' : "Дата подписания акта Заказчиком: " . SearchHelper::formatDate($stage->customer_date_sign) . "<br/>")
                . ($stage->customer_sign == null ? '' : "Подписавший акт от имени Заказчика: {$stage->customer_sign}<br/>")
                . ($stage->price == null ? '' : "Сумма денежных обязательств по акту: " . SearchHelper::moneyThousand($stage->price) . "<br/>");
            if ($stage->certificate_number !== null) {
                $filename = $stage->id . "_" . $stage->certificate_number . "." . $stage->file_type;
                $uuid = SearchHelper::getUuidFile('CERTIFICATE', $filename);
                $info .= SearchHelper::makeDownloadLink($uuid, $stage->file_type);
            }
            $result[] = $info;
        }
        return SearchHelper::makeTr('Этапы', implode('<hr/>', $result));
    }

    private static function makePayment(string $price, ?string $stageJson): array
    {
        $totalAmount = floatval(str_replace(',', '.', $price)) * 1000;
        $stage = $stageJson ? json_decode($stageJson, true) : [];
        $expense = 0;
        $compare = array_column($stage, 'step');
        array_multisort($compare, SORT_ASC, $stage);
        $steps = [];
        foreach ($stage as $step) {
            $payment = str_replace(',', '.', $step['payment']) * 1000;
            $steps[] = $step['step'] . ' Этап: ' . SearchHelper::money($step['payment']);
            $expense += $payment;
        }
        $result[] = SearchHelper::makeTr('Стоимость всего', SearchHelper::money($totalAmount));
        $result[] = SearchHelper::makeTr('Стоимость всего',
            implode('<br/>', $steps) . '<hr/> Итого: ' .
            SearchHelper::money($expense));
        $result[] = SearchHelper::makeTr('Остаток', SearchHelper::money($totalAmount - $expense));
        return $result;
    }

    private static function makeReport(string $number, string $date): array
    {
        $path = "fulltext/" . date("Y", strtotime($date)) . "/"
            . str_replace("/", "_", $number) . " " . SearchHelper::formatDate($date) . "/";
        $files = db::table('files')->where('path', 'ilike', $path . '%');
        $result = [];
        $stage = [];
        foreach ($files->get() as $file) {
            $step = substr(strrchr(str_replace($path, '', $file->path), '.'), 1);
            $img = substr(strrchr(str_replace($path, '', $file->filename), '.'), 1);
            $stage['Этап №' . $step][] = SearchHelper::makeDownloadLink($file->uuid, $img);
        }
        foreach ($stage as $key => $value) {
            $result[] = SearchHelper::makeTr($key, implode('', $value));
        }
        return $result;
    }
}
