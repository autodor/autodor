<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Support\Facades\DB;

class EbPatentService
{

    public static function search(array $searchParam): array
    {
        $ebPatentBuilder = DB::table('bibl_mainpatent')
            ->select(
                "bibl_mainpatent.mpnum              as id",
                "mpizobreten                   as type",
                "mpnumpat                      as number",
                "mpname                        as title",
                "mpdatapubl                    as date_published",
                "ptrplanum                     as mpk",
                "ptprplanum                    as mpk_sub")
            ->leftJoin("bibl_ptpodrazdel", "bibl_mainpatent.ptprnum", "=", "bibl_ptpodrazdel.ptprnum")
            ->leftJoin("bibl_ptrazdel", "bibl_ptpodrazdel.ptrnum", "=", "bibl_ptrazdel.ptrnum")
            ->whereNotNull("mpdatapubl")
            ->orderByDesc("mpdatapubl")
            ->orderBy("bibl_mainpatent.mpnum");

        if (SearchHelper::isNotEmpty($searchParam['developFDA'])) {
            if (filter_var($searchParam['developFDA'], FILTER_VALIDATE_BOOLEAN)) {
                $ebPatentBuilder->where('mpgroupowner', 'ilike', SearchHelper::searchString('ФЕДЕРАЛЬНОЕ ДОРОЖНОЕ АГЕНТСТВО'));
            } else {
                $ebPatentBuilder->where('mpgroupowner', 'not ilike', SearchHelper::searchString('ФЕДЕРАЛЬНОЕ ДОРОЖНОЕ АГЕНТСТВО'));
            }
        }
        if (SearchHelper::isNotEmpty($searchParam['typePatent'])) {
            $ebPatentBuilder->where('mpizobreten', '=', $searchParam['typePatent']);
        }
        if (SearchHelper::isNotEmpty($searchParam['datePublished'])) {
            $ebPatentBuilder->where('mpdatapubl', '=', $searchParam['datePublished']);
        }
        if (SearchHelper::isNotEmpty($searchParam['yearPublished'])) {
            $ebPatentBuilder->whereRaw("date_part('year',mpdatapubl)=" . $searchParam['yearPublished']);
        }
        if (SearchHelper::isNotEmpty($searchParam['mpkChapter'])) {
            $ebPatentBuilder->where("ptrplanum", '=', $searchParam['mpkChapter']);
        }
        if (SearchHelper::isNotEmpty($searchParam['mpkSubsection'])) {
            $ebPatentBuilder->where("ptprplanum", '=', $searchParam['mpkSubsection']);
        }
        if (SearchHelper::isNotEmpty($searchParam['numberPatent'])) {
            $ebPatentBuilder->where('mpnumpat', 'ilike', SearchHelper::searchString($searchParam['numberPatent']));
        }
        if (SearchHelper::isNotEmpty($searchParam['namePatent'])) {
            $ebPatentBuilder->where('mpname', 'ilike', SearchHelper::searchString($searchParam['namePatent']));
        }
//        dd($ebPatentBuilder->toRawSql());
        $result = [];
        foreach ($ebPatentBuilder->get() as $ebPatent) {
            $result[] = [
                'id' => $ebPatent->id,
                'type' => $ebPatent->type ? 'Изобретение' : 'Полезная модель',
                'number' => $ebPatent->number,
                'title' => $ebPatent->title,
                'date_published' => date("d.m.Y", strtotime($ebPatent->date_published)),
                'year_published' => date("Y", strtotime($ebPatent->date_published)),
                'mpk' => $ebPatent->mpk,
                'mpk_sub' => $ebPatent->mpk_sub,
            ];
        }
        if (SearchHelper::isNotEmpty($searchParam['allSearchQuery'])) {
            $result = SearchHelper::allSearchFilter($result, $searchParam['allSearchQuery']);
        }
        return $result;
    }

    public static function getOne($id): array
    {
        $patent = DB::table('bibl_mainpatent')
            ->select(
                'bibl_mainpatent.mpnum as id',
                "mpname as title",
                'mpizobreten      as type',
                'ptrplanum        as mpk_id',
                'ptrname          as mpk_name',
                'ptprplanum       as mpk_sub_id',
                'ptprname         as mpk_sub_name',
                'mpgroupowner     as owner',
                'mpnumpat         as number',
                'mpnumzaiavk      as request_number',
                'mpdatazaiavk     as request_date',
                'mpmestopubl      as place_published',
                'mpdatapubl       as date_published',
                'mpauthor         as author',
                'mpzaiavit        as gov_contract',
                'mptextras        as docs',
                'mpprimech        as description'
            )
            ->leftJoin("bibl_ptpodrazdel", "bibl_mainpatent.ptprnum", "=", "bibl_ptpodrazdel.ptprnum")
            ->leftJoin("bibl_ptrazdel", "bibl_ptpodrazdel.ptrnum", "=", "bibl_ptrazdel.ptrnum")
            ->where('bibl_mainpatent.mpnum', '=', $id)->first();
        $result = [];
        $index = 0;
        $result[] = SearchHelper::makeDetailBold(++$index, 'Наименование патента', $patent->title);
        $result[] = SearchHelper::makeDetail(++$index, 'Вид патента', $patent->type ? 'Изобретение' : 'Полезная модель');
        $result[] = SearchHelper::makeDetail(++$index, 'Раздел МПК', $patent->mpk_id . ' ' . $patent->mpk_name);
        $result[] = SearchHelper::makeDetail(++$index, 'Подраздел МПК', $patent->mpk_sub_id . ' ' . $patent->mpk_sub_name);
        if ($patent->gov_contract) {
            $result[] = SearchHelper::makeDetail(++$index, 'Разработка ФДА', 'Да');
            $result[] = SearchHelper::makeDetail(++$index, 'Дата и номер гос. контракта', $patent->gov_contract);
        }
        $result[] = SearchHelper::makeDetail(++$index, 'Номер заявки', $patent->request_number);
        $result[] = SearchHelper::makeDetail(++$index, 'Дата заявки', SearchHelper::formatDate($patent->request_date));
        $result[] = SearchHelper::makeDetail(++$index, 'Место публикации', $patent->place_published);
        $result[] = SearchHelper::makeDetail(++$index, 'Дата публикации', SearchHelper::formatDate($patent->date_published));
        $result[] = SearchHelper::makeDetail(++$index, 'Авторы', $patent->author);
        $result[] = SearchHelper::makeDetail(++$index, 'Патентообладатели', $patent->owner);
        $uuid = SearchHelper::getUuidFile('ois08/patent', $patent->id . "." . trim($patent->docs));
        if ($uuid) {
            $result[] = SearchHelper::makeDetail(++$index, 'Описание ' . ($patent->type ? 'изобретения' : 'полезной модели'),
                SearchHelper::makeDownloadLink($uuid, $patent->docs, $patent->id . "." . trim($patent->docs)));
        }
        $result[] = SearchHelper::makeDetail(++$index, 'Примечание', $patent->description);
        $resultOut = [];
        foreach($result as $item){
            if($item['info'] !== null) {
                $resultOut[] = $item;
            }
        }
//        $result = array_filter($result, function ($item) {
//            return $item['info'] !== null;
//        });
        return $resultOut;
    }
}
