<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class ProjectService
{

    public static function search(array $searchParam): array
    {
        $year = SearchHelper::isNotEmpty($searchParam['year']) ? $searchParam['year'] : date('Y');
        $projectBuilder = db::table('ik_niokr_contract')
            ->distinct()
            ->select("ik_niokr_contract.ctnum          as id",
                "zname                            as title",
                "zgod                             as year",
                "ctplanum                         as number",
                "ctdatazakluch                    as date",
                "org_isp.onamekrat                as executor",
                "zpriceall                        as price",
                "ctsostoianie                     as stage",
                db::raw("
                    coalesce(json_agg(ik_niokr_ntdproject.npindex || ' ' || ik_niokr_ntdproject.nptitle),'[]')::text as realization"
                )
            )
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_contract.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoin("ik_niokr_zakazchik", "ik_niokr_zaiavka.znum", "=", "ik_niokr_zakazchik.znum")
            ->leftJoin("ik_niokr_statia", "ik_niokr_statia.snum", "=", "ik_niokr_zaiavka.snum")
            ->leftJoin("ik_niokr_dokumentats", "ik_niokr_zaiavka.znum", "=", "ik_niokr_dokumentats.znum")
            ->leftJoin("ik_niokr_econobosn", "ik_niokr_zaiavka.znum", "=", "ik_niokr_econobosn.znum")
            ->leftJoin("ik_niokr_protokol", "ik_niokr_zaiavka.znum", "=", "ik_niokr_protokol.znum")
            ->leftJoin("ik_niokr_podrazdel", "ik_niokr_zaiavka.prnum", "=", "ik_niokr_podrazdel.prnum")
            ->leftJoin("ik_niokr_razdel", "ik_niokr_podrazdel.rnum", "=", "ik_niokr_razdel.rnum")
            ->leftJoin("ik_niokr_organiz as org_zak", "ik_niokr_zakazchik.onum", "=", "org_zak.onum")
            ->leftJoin("ik_niokr_organiz as org_isp", "ik_niokr_ispolnitel.onum", "=", "org_isp.onum")
            ->leftJoin("ik_niokr_ntdproject", "ik_niokr_ntdproject.ctnum", "=", "ik_niokr_contract.ctnum")
            ->leftJoin("ik_niokr_ntdevents", "ik_niokr_ntdevents.npnum", "=", "ik_niokr_ntdproject.npnum")
            ->groupBy("ik_niokr_contract.ctnum", "zname", "zgod", "ctplanum", "ctdatazakluch", "org_isp.onamekrat",
                "zpriceall", "ctsostoianie")
            ->whereRaw("((zkmain = 'y' OR zkmain is null)
                and ((zgod < ".$year."
                   and (ctsostoianie = 'Завершен' or ctsostoianie = 'Расторгнут'))
                    or (zgod >= ".$year.")) and ctrlzproectntd = 'y')")
            ->orderByDesc("ik_niokr_contract.ctnum");
        if (SearchHelper::isNotEmpty($searchParam['year'])) {
            $projectBuilder->where('zgod', '=', $searchParam['year']);
        }
        if (SearchHelper::isNotEmpty($searchParam['number'])) {
            $projectBuilder->where('ctplanum', 'ilike', SearchHelper::searchString($searchParam['number']));
        }
        if (SearchHelper::isNotEmpty($searchParam['title'])) {
            $projectBuilder->where('zname', 'ilike', SearchHelper::searchString($searchParam['title']));
        }
        if (SearchHelper::isNotEmpty($searchParam['eventProject'])) {
            $projectBuilder->where('ik_niokr_ntdevents.netype', 'ilike', SearchHelper::searchString($searchParam['eventProject']));
        }
        if (SearchHelper::isNotEmpty($searchParam['budgetItem'])) {
            $projectBuilder->where('ik_niokr_ntdproject.npindex', 'ilike', SearchHelper::searchString($searchParam['budgetItem']));
        }
        if (SearchHelper::isNotEmpty($searchParam['executor'])) {
            $str = SearchHelper::searchString($searchParam['executor']);
            $projectBuilder->where(function ($query) use ($str) {
                $query->where('org_isp.oname', 'ilike', $str)
                    ->orWhere('org_isp.onamekrat', 'ilike', $str);
            });
        }
        if (SearchHelper::isNotEmpty($searchParam['customer'])) {
            $str = SearchHelper::searchString($searchParam['customer']);
            $projectBuilder->where(function ($query) use ($str) {
                $query->where('org_zak.oname', 'ilike', $str)
                    ->orWhere('org_zak.onamekrat', 'ilike', $str);
            });
        }
        if (SearchHelper::isNotEmpty($searchParam['dateStartFrom'])) {
            $projectBuilder->where('ctdatazakluch', '>=', $searchParam['dateStartFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateStartTo'])) {
            $projectBuilder->where('ctdatazakluch', '<=', $searchParam['dateStartTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateFinishFrom'])) {
            $projectBuilder->where('ctdatazaversh', '>=', $searchParam['dateFinishFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateFinishTo'])) {
            $projectBuilder->where('ctdatazaversh', '<=', $searchParam['dateFinishTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['condition'])) {
            $projectBuilder->where('ctsostoianie', 'ilike', SearchHelper::searchString($searchParam['condition']));
        }

//        dd($projectBuilder->toRawSql());


        $result = [];
        $aggregate['title']='Итого';
        $aggregate['price'] = 0.0;
        foreach ($projectBuilder->get() as $project) {
            $result[] = [
                "id" => $project->id,
                "title" => $project->title,
                "year" => $project->year,
                "number" => $project->number,
                "date" => SearchHelper::formatDate($project->date),
                "executor" => $project->executor,
                "price" => SearchHelper::moneyThousand($project->price),
                "stage" => $project->stage,
                "realization" => $project->realization ? implode('<hr/>', json_decode($project->realization)) : ""
            ];
            $aggregate['price'] += SearchHelper::numberCalc($project->price);
        }
        $aggregate['price'] = SearchHelper::moneyThousand($aggregate['price']);
        return ['rows'=>$result, 'aggregate'=>$aggregate];
    }

    public static function getOne(int $id)
    {
        $archiveBuilder = db::table('arxiv_tema')
            ->select('ctdatazakik', 'ctplanumik',
                db::raw("COALESCE(json_agg(json_build_object(
                    'archive_number', tplanum,
                    'context', treferat,
                    'pro_ntd', tsrproectntd))::text, '[]') as archive")
            )->where('tzavershena', '=', true)
            ->groupBy('ctplanumik', 'ctdatazakik');

        $projectBuilder = db::table("ik_niokr_contract")
            ->select("ik_niokr_contract.ctnum as id",
                "zname                   as title",
                "zgod                    as year",
                "sname                   as budget_item",
                "zlotnum                 as number_date_lot",
                "ik_niokr_zaiavka.znum   as docs",
                "ik_niokr_zaiavka.znum   as protokol",
                "org_isp.onamekrat       as executor_short",
                "org_isp.oname           as executor",
                "org_zak.oname           as customer",
                "ctplanum                as number",
                "ctdatazakluch           as date",
                "ctdatazaversh           as date_finish",
                "eras                    as text_con",
                "ctsostoianie            as stage",
                "zpriceall               as price",
                "archive")
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_contract.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoin("ik_niokr_zakazchik", "ik_niokr_zaiavka.znum", "=", "ik_niokr_zakazchik.znum")
            ->leftJoin("ik_niokr_statia", "ik_niokr_statia.snum", "=", "ik_niokr_zaiavka.snum")
            ->leftJoin("ik_niokr_dokumentats", "ik_niokr_zaiavka.znum", "=", "ik_niokr_dokumentats.znum")
            ->leftJoin("ik_niokr_econobosn", "ik_niokr_zaiavka.znum", "=", "ik_niokr_econobosn.znum")
            ->leftJoin("ik_niokr_protokol", "ik_niokr_zaiavka.znum", "=", "ik_niokr_protokol.znum")
            ->leftJoin("ik_niokr_podrazdel", "ik_niokr_zaiavka.prnum", "=", "ik_niokr_podrazdel.prnum")
            ->leftJoin("ik_niokr_razdel", "ik_niokr_podrazdel.rnum", "=", "ik_niokr_razdel.rnum")
            ->leftJoin("ik_niokr_organiz as org_zak", "ik_niokr_zakazchik.onum", "=", "org_zak.onum")
            ->leftJoin("ik_niokr_organiz as org_isp", "ik_niokr_ispolnitel.onum", "=", "org_isp.onum")
            ->leftJoinSub($archiveBuilder, 'a', function (JoinClause $join) {
                $join
                    ->on('a.ctdatazakik', '=', 'ctdatazakluch')
                    ->on("a.ctplanumik", "=", "ctplanum");
            })
            ->where("ik_niokr_contract.ctnum", "=", $id)
            ->first();
//        dd($projectBuilder->toRawSql());
        $result = [];
        $index = 0;
        $result[] =
            [
                'title' => 'Наименование контракта',
                'info' => $projectBuilder->title,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Год плана',
                'info' => $projectBuilder->year,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Статья бюджета',
                'info' => $projectBuilder->budget_item,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Заказчик',
                'info' => $projectBuilder->customer,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Исполнитель (краткое наименование организации)',
                'info' => $projectBuilder->executor_short,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Исполнитель (полное наименование организации)',
                'info' => $projectBuilder->executor,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Номер контракта',
                'info' => $projectBuilder->number,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Дата заключения контракта',
                'info' => SearchHelper::formatDate($projectBuilder->date),
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Дата завершения контракта по КП',
                'info' => SearchHelper::formatDate($projectBuilder->date_finish),
                'index' => ++$index
            ];
        if ($projectBuilder->text_con) {
            $path = 'econobos/' . date("Y", strtotime($projectBuilder->date));
            $filename = str_replace("/", "_", $projectBuilder->number)
                . ' ' . SearchHelper::formatDate($projectBuilder->date) . '.' . $projectBuilder->text_con;
            $uuid = SearchHelper::getUuidFile($path, $filename);
            if ($uuid) {
                $result[] =
                    [
                        'title' => 'Текст контракта',
                        'info' => SearchHelper::makeDownloadLink($uuid, $projectBuilder->text_con, $filename),
                        'index' => ++$index
                    ];
            }
        }
        $result[] =
            [
                'title' => 'Стадия выполнения',
                'info' => $projectBuilder->stage,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Стоимость всего',
                'info' => SearchHelper::moneyThousand($projectBuilder->price),
                'index' => ++$index
            ];
        if ($projectBuilder->archive) {
            $archive = json_decode($projectBuilder->archive, true)[0];
            $archiveNumber = explode('.', $archive['archive_number'])[0];
            $result[] =
                [
                    'title' => 'Номер в архиве',
                    'info' => $archiveNumber,
                    'index' => ++$index
                ];
            $result[] =
                [
                    'title' => 'Реферат:',
                    'info' => null,
                    'index' => ++$index
                ];
            $result[] =
                [
                    'title' => SearchHelper::toSpoiler($archive['context']),
                    'info' => null,
                    'index' => ++$index
                ];
            $path = 'fulltext/' . date("Y", strtotime($projectBuilder->date))
                . '/' . str_replace("/", "_", $projectBuilder->number)
                . ' ' . SearchHelper::formatDate($projectBuilder->date) . '/%';
            $files = db::table('files')
                ->select('path',
                    db::raw("COALESCE(json_agg(json_build_object(
                                    'uuid', uuid,
                                    'file', filename))::text, '[]') as files"))
                ->where('path', 'ilike', $path)->groupBy('path')->get();

            if ($files->count() > 0) {
                $result[] =
                    [
                        'title' => 'Электронный вид отчета:',
                        'info' => null,
                        'index' => ++$index
                    ];
                foreach ($files as $file) {
                    $listFile = [];
                    foreach (json_decode($file->files, true) as $filename) {
                        $type = explode('.', $filename['file']);
                        $listFile[] = SearchHelper::makeDownloadLink($filename['uuid'], end($type), $filename['file']);
                    }
                    $stage = explode('.', $file->path);
                    $result[] =
                        [
                            'title' => 'Этап №' . end($stage),
                            'info' => implode('', $listFile),
                            'index' => ++$index
                        ];
                }
            }
        }
        $ntds = self::getNtd($projectBuilder->date, $projectBuilder->number);
        if (!empty($ntds)) {
            foreach ($ntds as $ntd) {
                $result[] = [
                    'title' => $ntd['title'],
                    'info' => $ntd['info'],
                    'index' => ++$index
                ];
            }
        } else {
            $projectNtds = self::getProjectNtd($projectBuilder->id);
            if (!empty($projectNtds)) {
                foreach ($projectNtds as $projectNtd) {
                    $result[] = [
                        'title' => $projectNtd['title'],
                        'info' => $projectNtd['info'],
                        'index' => ++$index
                    ];
                }
            }
        }
        $bdiss = self::getBdis($projectBuilder->date, $projectBuilder->number);
        if (!empty($bdiss)) {
            foreach ($bdiss as $bdis) {
                $result[] = [
                    'title' => $bdis['title'],
                    'info' => $bdis['info'],
                    'index' => ++$index
                ];
            }
        }
        return $result;
    }

    private static function getNtd($date, $number)
    {
        $ntdi = db::table('bibl_mainntdi', 'm')
            ->select(
                db::raw("'ntdi' as tbl"),
                'm.ntdi_num as id',
                'ntdi_title as title',
                db::raw('cast(ntdi_number as VARCHAR(50)) as num_doc'),
                'ntdi_textras as ext',
                db::raw("cast((sindex_index || ' ' || sbelong_belong) as varchar(50)) as type_doc")
            )
            ->leftJoin("bibl_sbelong as b", "m.sbelong_num", "=", "b.sbelong_num")
            ->leftJoin("bibl_textsntdi as t", "m.ntdi_num", "=", "t.ntdi_num")
            ->leftJoin("bibl_sindex as s", "m.sindex_num", "=", "s.sindex_num")
            ->where("m.ntdi_ctdatazakik", "=", $date)
            ->where("m.ntdi_ctplanumik", "=", $number);
        $ntd = db::table("bibl_mainntd", "m")
            ->select(
                db::raw("'ntdo' as tbl"),
                "m.ntd_num as id",
                "ntd_title as title",
                db::raw("cast('' as VARCHAR(50)) as num_doc"),
                "ntd_textras             as ext",
                "sgroup_group            as type_doc")
            ->leftJoin("bibl_textsntd as t", "m.ntd_num", "=", "t.ntd_num")
            ->leftJoin("bibl_sgroup as s", "m.sgroup_num", "=", "s.sgroup_num")
            ->where("m.ntd_ctdatazakik", "=", $date)
            ->where("m.ntd_ctplanumik", "=", $number);
        $rows = $ntdi->union($ntd)->get();
        $result = [];
        if ($rows->count() > 0) {
            $result[] =
                [
                    'title' => 'НТД:',
                    'info' => null
                ];
            foreach ($rows as $row) {
                $path = 'ois08//' . $row->tbl;
                $filename = $row->id . $row->ext;
                $uuid = SearchHelper::getUuidFile($path, $filename);
                $result[] =
                    [
                        'title' => 'Вид реализации: ' . $row->type_doc,
                        'info' => $row->type_doc . '&nbsp;' . $row->num_doc . '&nbsp;' . $row->title .
                            ($uuid ? '<hr/>' . SearchHelper::makeDownloadLink($uuid, $row->ext, $filename) : '')
                    ];
            }
        }
        return $result;
    }

    private static function getBdis($date, $number)
    {
        $rows = db::table("rntd_commondatekontr")
            ->select("ounameobject          as title",
                "ourospatsvidetnum     as svid_num",
                "ourospatsvidetdate    as svid_data",
                "ourospatsvidetkategor as svid_vid",
                "onaznach as referat")
            ->leftJoin("rntd_objectu", "rntd_commondatekontr.cdnum", "=", "rntd_objectu.cdnum")
            ->leftJoin("ofap_object", "ofap_object.oname", "like", "rntd_objectu.ounameobject")
            ->where("cddatezakluch", "=", $date)
            ->where("cdkontrnum", "=", $number)->get();
        $result = [];
        if ($rows->count() > 0) {
            $result[] = [
                'title' => 'Все реализации РИД по контракту:',
                'info' => null
            ];
            foreach ($rows as $row) {
                $result[] = [
                    'title' => $row->svid_vid ?: '',
                    'info' => $row->title . " №&nbsp;" . $row->svid_num . " от " . SearchHelper::formatDate($row->svid_data) .
                        '<hr/>' . $row->referat
                ];
            }
        }
        return $result;
    }

    private static function getProjectNtd($id)
    {
        $rows = db::table("ik_niokr_contract", "co")
            ->select("ctrlzsp",
                "npindex",
                "nptitle",
                "netype",
                "nedata",
                "nedescript",
                "ndname",
                "ndras",
                "ndnum")
            ->leftJoin("ik_niokr_ntdproject", "ik_niokr_ntdproject.ctnum", "=", "co.ctnum")
            ->leftJoin("ik_niokr_ntdevents", "ik_niokr_ntdevents.npnum", "=", "ik_niokr_ntdproject.npnum")
            ->leftJoin("ik_niokr_ntddocum", "ik_niokr_ntddocum.nenum", "=", "ik_niokr_ntdevents.nenum")
            ->where("co.ctnum", "=", $id)
            ->orderBy("nptitle")
            ->get();
        $result = [];
        if ($rows->count() > 0) {
            $result[] = [
                'title' => 'Проект НТД:',
                'info' => null
            ];
            foreach ($rows as $row) {
                $result[] = [
                    'title' => "Вид документа Проекта НТД",
                    'info' => $row->npindex
                ];
                $result[] = [
                    'title' => "Наименование Проекта НТД",
                    'info' => $row->nptitle
                ];
                if ($row->netype) {
                    $result[] = [
                        'title' => "Состояние",
                        'info' => $row->netype
                    ];
                }
                if ($row->nedata) {
                    $result[] = [
                        'title' => "Дата состояния",
                        'info' => SearchHelper::formatDate($row->nedata)
                    ];
                }
                $filename = $row->ndnum . '_' . $row->ndname . '.' . $row->ndras;
                $uuid = SearchHelper::getUuidFile('ProjectNTD', $filename);
                if ($uuid) {
                    $result[] = [
                        'title' => "Документы НТД",
                        'info' => SearchHelper::makeDownloadLink($uuid, $row->ndras, $filename)
                    ];
                }
            }
        }
        return $result;
    }
}
