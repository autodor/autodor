<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Support\Facades\DB;

class FormService
{

    public static function search($data)
    {
        return match ($data['type']) {
            'form5-io' => self::form5Search($data['yearFrom'], 3),
            'form5-is' => self::form5Search($data['yearFrom'], 6),
            'form-executor' => self::formExecutor($data['yearFrom']),
            'report-ed-176' => self::formReportEd176($data['yearFrom']),
            'form-gk' => self::formGk($data['yearTo'], $data['yearFrom']),
            default => [],
        };
    }

    private static function form5Search($year, $snum): array
    {
        $summBuilder = db::table('ik_niokr_conetapi')
            ->select(db::raw("COALESCE(json_agg(json_build_object('pay',cesumwinner)),
                               '[]')::text as year_pay")
                , 'ctnum')
            ->join('ik_niokr_sostconetapi', 'ik_niokr_conetapi.cenum', '=', 'ik_niokr_sostconetapi.cenum')
            ->where('ik_niokr_sostconetapi.psenum', '=', '7')
            ->where(db::raw('extract(year from scedata)'), '=', $year)
            ->groupBy('ctnum');
        $formBuilder = db::table('ik_niokr_contract')
            ->select('ik_niokr_contract.ctnum         as id',
                'zname                           as title',
                'ctplanum                        as num',
                'ctdatazakluch                   as date_start',
                'ctdatazaversh                   as date_finish',
                'org_isp.oname                   as design',
                'zgod                            as year_val',
                'zlotnum                         as num_date_lot',
                'zpriceall                       as stoimost',
                'zpricethisyear                  as stoimost_this_year',
                'year_pay',
                'ctsostoianie                    as stadia',
                'stuslugy',
                db::raw("string_agg(strasxdy, ',') as type"),
                'ik_niokr_zaiavka.prnum')
            ->leftJoin('ik_niokr_zaiavka', 'ik_niokr_contract.znum', '=', 'ik_niokr_zaiavka.znum')
            ->leftJoin('ik_niokr_ispolnitel', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_ispolnitel.znum')
            ->leftJoin('ik_niokr_zakazchik', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_zakazchik.znum')
            ->leftJoin('ik_niokr_statia', 'ik_niokr_statia.snum', '=', 'ik_niokr_zaiavka.snum')
            ->leftJoin('ik_niokr_dokumentats', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_dokumentats.znum')
            ->leftJoin('ik_niokr_econobosn', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_econobosn.znum')
            ->leftJoin('ik_niokr_protokol', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_protokol.znum')
            ->leftJoin('ik_niokr_podrazdel', 'ik_niokr_zaiavka.prnum', '=', 'ik_niokr_podrazdel.prnum')
            ->leftJoin('ik_niokr_razdel', 'ik_niokr_podrazdel.rnum', '=', 'ik_niokr_razdel.rnum')
            ->leftJoin('ik_niokr_organiz as org_zak', 'ik_niokr_zakazchik.onum', '=', 'org_zak.onum')
            ->leftJoin('ik_niokr_organiz as org_isp', 'ik_niokr_ispolnitel.onum', '=', 'org_isp.onum')
            ->leftJoinSub($summBuilder, 'summ', 'summ.ctnum', '=', 'ik_niokr_contract.ctnum')
            ->leftJoin('ik_niokr_stoimost', 'ik_niokr_stoimost.znum', '=', 'ik_niokr_zaiavka.znum')
            ->where('zgod', '=', $year)
            ->where('ik_niokr_zaiavka.snum', '=', $snum)
            ->where(function ($query) {
                $query->where('zkmain', '=', true)->orWhereNull('zkmain');
            })
            ->groupBy(["ik_niokr_contract.ctnum", "zname", "ctplanum", "ctdatazakluch", "ctdatazaversh",
                "org_isp.oname", "zgod", "zlotnum", "zpriceall", "zpricethisyear", "year_pay", "ctsostoianie",
                "stuslugy", "ik_niokr_zaiavka.prnum"])
            ->orderBy('ik_niokr_contract.ctnum');
        $result = [];
        $index = 0;
        $aggregate['title'] = 'Итого';
        $aggregate['all_pay'] = 0.0;
        $aggregate['plan_pay'] = 0.0;
        $aggregate['fact_pay'] = 0.0;

        foreach ($formBuilder->get() as $item) {
            $result[] = [
                'index' => ++$index,
                'id' => $item->id,
                'title' => 'Выполнение работ по теме:<br/>' . $item->title . "<br/> Исполнитель: " . $item->design,
                'code' => implode('<br/>', array_unique(explode(',', $item->type))),
                'type' => $item->stuslugy,
                'period' => date("Y", strtotime($item->date_start)) . '-' . date("Y", strtotime($item->date_finish)),
                'date' => $item->num_date_lot != null ? SearchHelper::formatDate(explode('№', $item->num_date_lot)[0]) : "",
                'all_pay' => $item->stoimost,
                'plan_pay' => $item->stoimost_this_year,
                'fact_pay' => self::calc($item->year_pay),
                'status' => 'Контракт ' . $item->stadia
            ];
            $aggregate['all_pay'] += SearchHelper::numberCalc($item->stoimost);
            $aggregate['plan_pay'] += SearchHelper::numberCalc($item->stoimost_this_year);
            $aggregate['fact_pay'] += self::calc($item->year_pay);
        }
        return ['rows' => $result, 'aggregate' => $aggregate];
    }

    private static function formExecutor($year): array
    {
        $formBuilder = db::table('ik_niokr_contract')
            ->select("ik_niokr_ispolnitel.onum        as id",
                db::raw("count(ik_niokr_ispolnitel.onum) as count"),
                "onamekrat as org",
                "zgod",
                db::raw("coalesce(json_agg(json_build_object(
                               'now', zpricethisyear,
                               'first', zpricethisyplus1,
                               'second', zpricethisyplus2
                       ) ), '[]')::text          as year_pay")
            )
            ->leftJoin('ik_niokr_zaiavka', 'ik_niokr_contract.znum', '=', 'ik_niokr_zaiavka.znum')
            ->leftJoin('ik_niokr_ispolnitel', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_ispolnitel.znum')
            ->leftJoin('ik_niokr_organiz', 'ik_niokr_ispolnitel.onum', '=', 'ik_niokr_organiz.onum')
            ->leftJoin('ik_niokr_statia', 'ik_niokr_statia.snum', '=', 'ik_niokr_zaiavka.snum')
            ->where('ik_niokr_zaiavka.snum', '=', '1')
            ->where('zgod', '=', $year)
            ->groupBy(["ik_niokr_ispolnitel.onum", "onamekrat", "zgod"])
            ->orderBy("onamekrat")->orderBy("id")->orderBy("zgod");
        $result = [];
        $index = 0;
        $aggregate['title'] = 'Итого';
        $aggregate['count'] = 0;
        $aggregate['all'] = 0.0;
        $aggregate['now'] = 0.0;
        $aggregate['first'] = 0.0;
        $aggregate['second'] = 0.0;
        foreach ($formBuilder->get() as $item) {
            list($now, $first, $second) = self::calcFromYears($item->year_pay);
            $all = $now + $first + $second;
            $result[] = [
                'index' => ++$index,
                'id' => $item->id,
                'org' => $item->org,
                'count' => $item->count,
                'all' => sprintf("%01.3f", $all),
                'now' => sprintf("%01.3f", $now),
                'first' => sprintf("%01.3f", $first),
                'second' => sprintf("%01.3f", $second),
                'coef' => sprintf("%01.3f", $all / (double)$item->count),
            ];
            $aggregate['all'] += $all;
            $aggregate['count'] += $item->count;
            $aggregate['now'] += $now;
            $aggregate['first'] += $first;
            $aggregate['second'] += $second;
        }
        $aggregate['now'] = sprintf("%01.3f", $aggregate['now']);
        $aggregate['first'] = sprintf("%01.3f", $aggregate['first']);
        $aggregate['second'] = sprintf("%01.3f", $aggregate['second']);
        $aggregate['coef'] = sprintf("%01.3f", $aggregate['all'] / (double)$aggregate['count']);
        return ['rows' => $result, 'aggregate' => $aggregate];
    }

    private static function calc($str): float
    {
        if (!$str) return 0;
        $items = json_decode($str, true);
        /* @var double $result */
        $result = 0.0;
        foreach ($items as $item) {
            $strItem = str_replace(',', '.', $item['pay']);
            $result += (double)$strItem;
        }
        return $result;
    }

    private static function calcFromYears($str): array
    {
        $now = 0.0;
        $first = 0.0;
        $second = 0.0;
        if ($str != null) {
            foreach (json_decode($str, true) as $item) {
                $now += (double)str_replace(',', '.', $item['now']);
                $first += (double)str_replace(',', '.', $item['first']);
                $second += (double)str_replace(',', '.', $item['second']);
            }
        }
        return [$now, $first, $second];
    }

    private static function formReportEd176($year): array
    {
        $claims = DB::table('ik_niokr_conetapi')
            ->distinct()
            ->select(
                'ctnum',
                db::raw("coalesce(
                                  json_agg( DISTINCT
                                            jsonb_build_object(
                                                  'cldata', cldata,
                                                  'clnumber', clnumber,
                                                  'psename', psename
                                          )), '[]')::text as claim")
            )
            ->join('ik_niokr_sostconetapi', 'ik_niokr_conetapi.cenum', '=', 'ik_niokr_sostconetapi.cenum')
            ->join('ik_niokr_perechensostetapi', 'ik_niokr_sostconetapi.psenum', '=', 'ik_niokr_perechensostetapi.psenum')
            ->join('ik_niokr_claim', 'ik_niokr_conetapi.cenum', '=', 'ik_niokr_claim.cenum')
            ->groupBy(["ctnum"])
            ->where('psename', 'ilike', 'Претензия')
            ->orWhere('psename', 'ilike', 'Мотивированный отказ');
        $form = DB::table('ik_niokr_zaiavka')
            ->select(
                'ik_niokr_contract.ctnum      as id',
                'zname                        as title',
                'ik_niokr_zaiavka.zeditnum    as num_edit',
                'ik_niokr_zaiavka.zstandartrf as stand_rf',
                'ctplanum                     as num',
                'ctdatazakluch                as date_start',
                'ctdatazaversh                as date_finish',
                'org_isp.onamekrat            as design_short',
                'org_zak.onamekrat            as consumer_short',
                'zgod                         as year_val',
                'zlotnum                      as num_date_lot',
                'zpriceall                    as price',
                'zpricethisyear               as price_this_year',
                'zpricethisyplus1             as price_this_year_plus1',
                'zpricethisyplus2             as price_this_year_plus2',
                'ctsostoianie                 as stage',
                'zsrokbegin',
                'zsrokend',
                'ik_niokr_zaiavka.prnum',
                'claim')
            ->leftJoin('ik_niokr_contract', 'ik_niokr_contract.znum', '=', 'ik_niokr_zaiavka.znum')
            ->leftJoin('ik_niokr_ispolnitel', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_ispolnitel.znum')
            ->leftJoin('ik_niokr_zakazchik', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_zakazchik.znum')
            ->leftJoin('ik_niokr_statia', 'ik_niokr_statia.snum', '=', 'ik_niokr_zaiavka.snum')
            ->leftJoin('ik_niokr_dokumentats', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_dokumentats.znum')
            ->leftJoin('ik_niokr_econobosn', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_econobosn.znum')
            ->leftJoin('ik_niokr_protokol', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_protokol.znum')
            ->leftJoin('ik_niokr_podrazdel', 'ik_niokr_zaiavka.prnum', '=', 'ik_niokr_podrazdel.prnum')
            ->leftJoin('ik_niokr_razdel', 'ik_niokr_podrazdel.rnum', '=', 'ik_niokr_razdel.rnum')
            ->leftJoin('ik_niokr_organiz as org_zak', 'ik_niokr_zakazchik.onum', '=', 'org_zak.onum')
            ->leftJoin('ik_niokr_organiz as org_isp', 'ik_niokr_ispolnitel.onum', '=', 'org_isp.onum')
            ->leftJoinSub($claims, "claims", "ik_niokr_contract.ctnum", "=", "claims.ctnum")
            ->whereRaw("(zgod = " . $year . ") and (sname ilike 'ниокр') and (zkmain = 'y' or zkmain is null)")
            ->orWhereRaw("sname ilike 'ниокр' and (zkmain = 'y' or zkmain is null) and zgod = " . $year . " and ik_niokr_zaiavka.zeditnum = '1'");
//        $form->ddRawSql();
        $resultProgram = [];
        $resultNotProgram = [];
        $aggregateNotProgram['count'] = 0;
        $aggregateNotProgram['price'] = 0.0;
        $aggregateNotProgram['price_this_year'] = 0.0;
        $aggregateNotProgram['price_this_year_plus1'] = 0.0;
        $aggregateNotProgram['price_this_year_plus2'] = 0.0;
        $aggregateProgram['count'] = 0;
        $aggregateProgram['price'] = 0.0;
        $aggregateProgram['price_this_year'] = 0.0;
        $aggregateProgram['price_this_year_plus1'] = 0.0;
        $aggregateProgram['price_this_year_plus2'] = 0.0;
        foreach ($form->get() as $item) {
            $result = [
                'id' => $item->id,
                'title' => $item->title,
                'contract' => $item->num . '<br/>от<br/>' . SearchHelper::formatDate($item->date_start),
                'design' => $item->design_short,
                'consumer' => $item->consumer_short,
                'date_start' => SearchHelper::formatDateYear($item->date_start),
                'date_finish' => SearchHelper::formatDateYear($item->date_finish),
                'price' => SearchHelper::moneyThousand($item->price),
                'price_this_year' => SearchHelper::moneyThousand($item->price_this_year),
                'price_this_year_plus1' => SearchHelper::moneyThousand($item->price_this_year_plus1),
                'price_this_year_plus2' => SearchHelper::moneyThousand($item->price_this_year_plus2),
                'description' => self::formatDescription($item->stage, $item->claim),
            ];
            if (!$item->stand_rf && $item->num_edit == 2) {
                $aggregateNotProgram['count']++;
                $aggregateNotProgram['price'] += SearchHelper::numberCalc($item->price);
                $aggregateNotProgram['price_this_year'] += SearchHelper::numberCalc($item->price_this_year);
                $aggregateNotProgram['price_this_year_plus1'] += SearchHelper::numberCalc($item->price_this_year_plus1);
                $aggregateNotProgram['price_this_year_plus2'] += SearchHelper::numberCalc($item->price_this_year_plus2);
                $resultNotProgram[] = array_merge(
                    ['index' => count($resultNotProgram) + 1],
                    $result
                );
            }
            if ($item->stand_rf && $item->num_edit == 2) {
                $aggregateProgram['count']++;
                $aggregateProgram['price'] += SearchHelper::numberCalc($item->price);
                $aggregateProgram['price_this_year'] += SearchHelper::numberCalc($item->price_this_year);
                $aggregateProgram['price_this_year_plus1'] += SearchHelper::numberCalc($item->price_this_year_plus1);
                $aggregateProgram['price_this_year_plus2'] += SearchHelper::numberCalc($item->price_this_year_plus2);
                $resultProgram[] = array_merge(
                    ['index' => count($resultProgram) + 1],
                    $result
                );
            }
        }
        $programTitle = [];
        $programTitle[] = [
            'id' => null,
            'title' => 'Государственная программа Российской Федерации "Развитие транспортной системы"',
            'contract' => null,
            'design' => null,
            'consumer' => null,
            'date_start' => null,
            'date_finish' => null,
            'price' => null,
            'price_this_year' => null,
            'price_this_year_plus1' => null,
            'price_this_year_plus2' => null,
            'description' => null,
            'fontBold'=>true
        ];
        $programTitle[] = [
            'id' => null,
            'title' => 'Бюджетные ассигнования',
            'contract' => null,
            'design' => null,
            'consumer' => null,
            'date_start' => null,
            'date_finish' => null,
            'price' => SearchHelper::moneyThousand($aggregateNotProgram['price'] + $aggregateProgram['price']),
            'price_this_year' => SearchHelper::moneyThousand($aggregateNotProgram['price_this_year'] + $aggregateProgram['price_this_year']),
            'price_this_year_plus1' => SearchHelper::moneyThousand($aggregateNotProgram['price_this_year_plus1'] + $aggregateProgram['price_this_year_plus1']),
            'price_this_year_plus2' => SearchHelper::moneyThousand($aggregateNotProgram['price_this_year_plus2'] + $aggregateProgram['price_this_year_plus2']),
            'description' => null,
            'fontBold'=>true
        ];
        $programTitle[] = [
            'id' => null,
            'title' => 'Бюджетные Контрактные обязательства, всего',
            'contract' => $aggregateNotProgram['count'] + $aggregateProgram['count'],
            'design' => null,
            'consumer' => null,
            'date_start' => null,
            'date_finish' => null,
            'price' => SearchHelper::moneyThousand($aggregateNotProgram['price'] + $aggregateProgram['price']),
            'price_this_year' => SearchHelper::moneyThousand($aggregateNotProgram['price_this_year'] + $aggregateProgram['price_this_year']),
            'price_this_year_plus1' => SearchHelper::moneyThousand($aggregateNotProgram['price_this_year_plus1'] + $aggregateProgram['price_this_year_plus1']),
            'price_this_year_plus2' => SearchHelper::moneyThousand($aggregateNotProgram['price_this_year_plus2'] + $aggregateProgram['price_this_year_plus2']),
            'description' => null,
            'fontBold'=>true
        ];
        $programTitle[] = [
            'id' => null,
            'title' => 'в том числе:',
            'contract' => null,
            'design' => null,
            'consumer' => null,
            'date_start' => null,
            'date_finish' => null,
            'price' => null,
            'price_this_year' => null,
            'price_this_year_plus1' => null,
            'price_this_year_plus2' => null,
            'description' => null,
        ];
        $programTitle[] = [
            'id' => null,
            'title' => 'по Программе стандартизации и Графику обновления стандартов',
            'contract' => $aggregateProgram['count'],
            'design' => null,
            'consumer' => null,
            'date_start' => null,
            'date_finish' => null,
            'price' => SearchHelper::moneyThousand($aggregateProgram['price']),
            'price_this_year' => SearchHelper::moneyThousand($aggregateProgram['price_this_year']),
            'price_this_year_plus1' => SearchHelper::moneyThousand($aggregateProgram['price_this_year_plus1']),
            'price_this_year_plus2' => SearchHelper::moneyThousand($aggregateProgram['price_this_year_plus2']),
            'description' => null,
            'fontBold'=>true
        ];
        $notProgramTitle[] = [
            'id' => null,
            'title' => 'Вне Программы стандартизации и Графика обновления стандартов',
            'contract' => $aggregateNotProgram['count'],
            'design' => null,
            'consumer' => null,
            'date_start' => null,
            'date_finish' => null,
            'price' => SearchHelper::moneyThousand($aggregateNotProgram['price']),
            'price_this_year' => SearchHelper::moneyThousand($aggregateNotProgram['price_this_year']),
            'price_this_year_plus1' => SearchHelper::moneyThousand($aggregateNotProgram['price_this_year_plus1']),
            'price_this_year_plus2' => SearchHelper::moneyThousand($aggregateNotProgram['price_this_year_plus2']),
            'description' => null,
            'fontBold'=>true
        ];
        return ['rows' => array_merge($programTitle, $resultProgram,$notProgramTitle, $resultNotProgram), 'aggregate' => null];
    }

    private static function formatDescription($stage, $claim): string
    {
        $result = [];
        if (mb_strtolower($stage) == 'выполняется') {
            $result[] = $claim ? "Контракт выполняется" : "Контракт выполняется в плановом порядке";
        } else $result[] = "Контракт " . $stage;
        if ($claim) {
            $claimArray = json_decode($claim, true);
            $result[] = "<hr/>";
            foreach ($claimArray as $item) {
                $result[] = "<b>Срыв сроков</b>(" . mb_strtolower($item['psename'])
                    . ($item['cldata'] ? '<br/>от ' . SearchHelper::formatDate($item['cldata']) : '')
                    . ($item['clnumber'] ? '<br/>№ ' . $item['clnumber'] : '');
            }

        }
        return implode('<br/>', $result);
    }

    private static function formGk($yearTo, $yearFrom): array
    {
        $bdisText = DB::table('rntd_objectu')
            ->select('cdkontrnum',
                DB::raw("       coalesce(
               json_agg(distinct
                        jsonb_build_object(
                                'gk_num', cdkontrnum,
                                'primech', ourospatprimechan,
                                'title', ounameobject,
                                'svid_num', ourospatsvidetnum,
                                'svid_data', ourospatsvidetdate,
                                'svid_vid', ourospatsvidetkategor
                        )), '[]')::text as bdis_text")
            )
            ->leftJoin('rntd_commondatekontr', 'rntd_commondatekontr.cdnum', '=', 'rntd_objectu.cdnum')
            ->groupBy('cdkontrnum');

        $projectntdText = DB::table('ik_niokr_contract')
            ->select('ik_niokr_contract.ctnum',
                DB::raw("coalesce(
               json_agg(distinct
                        jsonb_build_object(
                                'title', nptitle,
                                'index', npindex,
                                'realiz', nprealiz
                        )), '[]')::text as projectntd_text"))
            ->leftJoin('ik_niokr_ntdproject', 'ik_niokr_ntdproject.ctnum', '=', 'ik_niokr_contract.ctnum')
            ->where('ik_niokr_ntdproject.ctnum', '>', 0)
            ->where(function ($query) {
                $query->where('nprealiz', 'ilike', 'проект')->orWhere('nprealiz', 'ilike', 'нтд сп');
            })
            ->groupBy('ik_niokr_contract.ctnum')
            ->orderBy('ik_niokr_contract.ctnum');

        $ntditext = DB::table('bibl_mainntdi as m')
            ->select("ntdi_ctdatazakik                 as data",
                "ntdi_ctplanumik                  as num",
                db::raw("coalesce(
               json_agg(distinct
                        jsonb_build_object(
                                'title', ntdi_title,
                                'type_doc', sindex_index || ' ' || sbelong_belong,
                                'num_doc', ntdi_number,
                                'primech', ntdi_primech,
                                'datavvod', ntdi_vvoddeistv
                        )), '[]')::text as ntd_text"))
            ->leftJoin('bibl_sbelong as b', 'm.sbelong_num', '=', 'b.sbelong_num')
            ->leftJoin('bibl_textsntdi as t', 'm.ntdi_num', '=', 't.ntdi_num')
            ->leftJoin('bibl_sindex as s', 'm.sindex_num', '=', 's.sindex_num')
            ->whereNotNull('m.ntdi_ctdatazakik')
            ->whereNotNull('ntdi_ctplanumik')
            ->groupBy('ntdi_ctdatazakik', 'ntdi_ctplanumik');

        $form = DB::table("ik_niokr_contract")
            ->distinct()
            ->select("ik_niokr_contract.ctnum                  as id",
                "zname                           as title",
                "ctplanum                        as number",
                "ctdatazakluch                   as date_start",
                "ctdatazaversh                   as date_finish",
                "org_isp.oname                   as design",
                "zgod                            as year",
                "zlotnum                         as num_date_lot",
                "zpriceall                       as price",
                "ik_niokr_ntdproject.nprealiz             as realization",
                "ctrlzpatent                     as patent",
                "ctrlzntd                        as ntd",
                "ctrlzproectntd                  as proect_ntd",
                "ctrlzbd                         as bdis",
                "ik_niokr_zaiavka.prnum",
                "ik_niokr_conetapi.psenum",
                "bdis_text",
                "projectntd_text",
                "ntd_text"
            )
            ->leftJoin('ik_niokr_zaiavka', 'ik_niokr_contract.znum', '=', 'ik_niokr_zaiavka.znum')
            ->leftJoin('ik_niokr_ispolnitel', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_ispolnitel.znum')
            ->leftJoin('ik_niokr_zakazchik', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_zakazchik.znum')
            ->leftJoin('ik_niokr_statia', 'ik_niokr_statia.snum', '=', 'ik_niokr_zaiavka.snum')
            ->leftJoin('ik_niokr_dokumentats', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_dokumentats.znum')
            ->leftJoin('ik_niokr_econobosn', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_econobosn.znum')
            ->leftJoin('ik_niokr_protokol', 'ik_niokr_zaiavka.znum', '=', 'ik_niokr_protokol.znum')
            ->leftJoin('ik_niokr_podrazdel', 'ik_niokr_zaiavka.prnum', '=', 'ik_niokr_podrazdel.prnum')
            ->leftJoin('ik_niokr_razdel', 'ik_niokr_podrazdel.rnum', '=', 'ik_niokr_razdel.rnum')
            ->leftJoin('ik_niokr_organiz as org_zak', 'ik_niokr_zakazchik.onum', '=', 'org_zak.onum')
            ->leftJoin('ik_niokr_organiz as org_isp', 'ik_niokr_ispolnitel.onum', '=', 'org_isp.onum')
            ->leftJoin('ik_niokr_ntdproject', 'ik_niokr_ntdproject.ctnum', '=', 'ik_niokr_contract.ctnum')
            ->leftJoin('ik_niokr_conetapi', 'ik_niokr_contract.ctnum', '=', 'ik_niokr_conetapi.ctnum')
            ->leftJoin('ik_niokr_sostconetapi', 'ik_niokr_conetapi.cenum', '=', 'ik_niokr_sostconetapi.cenum')
            ->leftJoinSub($bdisText, "bdisText", "bdisText.cdkontrnum", "=", "ik_niokr_contract.ctplanum")
            ->leftJoinSub($projectntdText, "projectntdText", "projectntdText.ctnum", "=", "ik_niokr_contract.ctnum")
            ->leftJoinSub($ntditext, "ntdiText", function ($join) {
                $join->on("ntdiText.num", "=", "ik_niokr_contract.ctplanum");
                $join->on("ntdiText.data", "=", "ik_niokr_contract.ctdatazakluch");
            })
            ->where('ctsostoianie', 'ilike', 'завершен')
            ->where(function ($query) use ($yearTo, $yearFrom) {
                $query->where('zgod', '>=', $yearFrom)
                    ->where('zgod', '<=', $yearTo)
                    ->where('ik_niokr_zaiavka.snum', '=', '1')
                    ->where(function ($query) {
                        $query->where("zkmain", '=', true)
                            ->orWhereNull("zkmain");
                    });
            })
            ->orderBy('ik_niokr_contract.ctnum');
        $result = [];
        $index = 0;
        $aggregate['title'] = 'Итого';
        $aggregate['price'] = 0.0;
        foreach ($form->get() as $item) {
            $realization = [];
            if ($item->ntd) $realization[] = "НТД";
            if ($item->proect_ntd) $realization[] = "Проект&nbsp;НТД";
            if ($item->bdis) $realization[] = "БД/ИС";
            if ($item->patent) $realization[] = "Патент";
            if (empty($realization)) $realization[] = "Научно-технический отчёт";
            $docs = array_merge(self::bdisText($item->bdis_text),
                self::projectntdText($item->projectntd_text),
                self::ntdiText($item->ntd_text));
            if (empty($docs)) $docs = self::referat($item->date_start, $item->number);
            $result[] = [
                'id' => $item->id,
                'index' => ++$index,
                'year' => $item->year,
                'title' => $item->title,
                'design' => $item->design,
                'number' => $item->number,
                'date_start' => SearchHelper::formatDate($item->date_start),
                'date_finish' => SearchHelper::formatDate($item->date_finish),
                'price' => SearchHelper::moneyThousand($item->price),
                'realization' => implode('<br/>', $realization),
                'documents' => $docs,
            ];
            $aggregate['price'] += SearchHelper::numberCalc($item->price);
        }
        $aggregate['price'] = SearchHelper::moneyThousand($aggregate['price']);
        return ['rows' => $result, 'aggregate' => $aggregate];
    }

    private static function bdisText($bdisText): array
    {
        $result = [];
        if ($bdisText) {
            foreach (json_decode($bdisText, true) as $item) {
                $result[] = $item['svid_vid'] . ' ' . $item['title'] . '<br/>№ ' . $item['svid_num']
                    . ' от ' . SearchHelper::formatDate($item['svid_data']);
            }
        }
        return $result;
    }

    private static function projectntdText($projectntdText): array
    {
        $result = [];
        if ($projectntdText) {
            foreach (json_decode($projectntdText, true) as $item) {
                $result[] = $item['index'] . ' ' . $item['title'] .
                    ($item['realiz'] == 'НТД СП' ? ' (для служебного пользования)' : '');
            }
        }
        return $result;
    }

    private static function ntdiText($ntdiText): array
    {
        $result = [];
        if ($ntdiText) {
            foreach (json_decode($ntdiText, true) as $item) {
                $result[] = $item['type_doc'] . ' ' . $item['num_doc'] . '<br/>' . $item['title']
                    . '<br/>' . $item['primech'] . '</br>Действует с '
                    . SearchHelper::formatDate($item['datavvod']) . '<br/>';
            }
        }
        return $result;
    }

    private static function referat($data, $number)
    {
        $ref = DB::table("arxiv_tema")
            ->select("treferat")
            ->where("ctdatazakik", "=", $data)
            ->where("ctplanumik", "=", $number)
            ->where("tzavershena", "=", true)
            ->first();
        return $ref->treferat;
    }
}

