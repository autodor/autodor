<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class RealizationService
{

    public static function search(array $searchParam): array
    {
        $bdisArray = db::table('rntd_objectu')
            ->select('cdkontrnum',
                db::raw("coalesce(json_agg(json_build_object(
               'primech', ourospatprimechan,
               'title', ounameobject,
               'svid_num', ourospatsvidetnum,
               'svid_data', ourospatsvidetdate,
               'svid_vid', ourospatsvidetkategor
           )), '[]')::text as bdis_array"))
            ->leftJoin('rntd_commondatekontr', 'rntd_commondatekontr.cdnum', '=', 'rntd_objectu.cdnum')
            ->groupBy(['cdkontrnum']);

        $ntdiArray = db::table('bibl_mainntdi as m')
            ->select('ntdi_ctdatazakik as date',
                'ntdi_ctplanumik  as number',
                db::raw("coalesce(json_agg(json_build_object(
           'primech', ntdi_primech,
           'datavvod', ntdi_vvoddeistv,
           'dataapprov', ntdi_dateapproval,
           'title', ntdi_title,
           'num_doc', ntdi_number,
           'type_doc', (sindex_index || ' ' || sbelong_belong),
           'deistvuet', ntdi_deistvuet)), '[]')::text as ntdi_array"))
            ->leftJoin("bibl_sbelong as b", "m.sbelong_num", "=", "b.sbelong_num")
            ->leftJoin("bibl_textsntdi as t", "m.ntdi_num", "=", "t.ntdi_num")
            ->leftJoin("bibl_sindex as s", "m.sindex_num", "=", "s.sindex_num")
            ->whereNotNull("m.ntdi_ctdatazakik")
            ->whereNotNull("m.ntdi_ctplanumik")
            ->groupBy(["ntdi_ctdatazakik", "ntdi_ctplanumik"]);

        $articleArray = db::table('arxiv_tema')
            ->select(
                'ctdatazakik',
                'ctplanumik',
                db::raw("string_agg(treferat, '<hr/>') as article_array")
            )
            ->where('tzavershena', '=', true)
            ->groupBy(['ctdatazakik', 'ctplanumik']);

        $realizationBuilder = DB::table('ik_niokr_contract')
            ->distinct()
            ->select(
                "ik_niokr_contract.ctnum as id",
                "zname as title",
                "ctplanum as number",
                "ctdatazakluch as date_sign",
                "ctdatazaversh as date_finish",
                "org_isp.oname as executor",
                "zgod as year",
                "zlotnum as num_date_lot",
                "zpriceall as price",
                "ik_niokr_ntdproject.nprealiz as nprealiz",
                "ctsostoianie as status",
                "ctrlzpatent as patent",
                "ctrlzntd as ntd",
                "ctrlzproectntd as proect_ntd",
                "ctrlzbd as bdis",
                "ik_niokr_zaiavka.prnum",
                "bdis_array",
                "ntdi_array",
                "article_array",
                db::raw("coalesce(json_agg(json_build_object(
                                'title', nptitle,
                                'index', npindex,
                                'type', nprealiz)), '[]')::text as ntdproject")
            )
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_contract.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoin("ik_niokr_zakazchik", "ik_niokr_zaiavka.znum", "=", "ik_niokr_zakazchik.znum")
            ->leftJoin("ik_niokr_statia", "ik_niokr_statia.snum", "=", "ik_niokr_zaiavka.snum")
            ->leftJoin("ik_niokr_dokumentats", "ik_niokr_zaiavka.znum", "=", "ik_niokr_dokumentats.znum")
            ->leftJoin("ik_niokr_econobosn", "ik_niokr_zaiavka.znum", "=", "ik_niokr_econobosn.znum")
            ->leftJoin("ik_niokr_protokol", "ik_niokr_zaiavka.znum", "=", "ik_niokr_protokol.znum")
            ->leftJoin("ik_niokr_podrazdel", "ik_niokr_zaiavka.prnum", "=", "ik_niokr_podrazdel.prnum")
            ->leftJoin("ik_niokr_razdel", "ik_niokr_podrazdel.rnum", "=", "ik_niokr_razdel.rnum")
            ->leftJoin("ik_niokr_organiz as org_zak", "ik_niokr_zakazchik.onum", "=", "org_zak.onum")
            ->leftJoin("ik_niokr_organiz as org_isp", "ik_niokr_ispolnitel.onum", "=", "org_isp.onum")
            ->leftJoin("ik_niokr_ntdproject", "ik_niokr_ntdproject.ctnum", "=", "ik_niokr_contract.ctnum")
            ->leftJoinSub($bdisArray, "bdisArray", 'bdisArray.cdkontrnum', '=', 'ctplanum')
            ->leftJoinSub($ntdiArray, "ntdiArray", function (JoinClause $join) {
                $join
                    ->on('ntdiArray.date', '=', 'ctdatazakluch')
                    ->on("ntdiArray.number", "=", "ctplanum");
            })
            ->leftJoinSub($articleArray, 'article', function (JoinClause $join) {
                $join
                    ->on('article.ctdatazakik', '=', 'ctdatazakluch')
                    ->on("article.ctplanumik", "=", "ctplanum");
            })
            ->where(function ($query) {
                $query->where('zkmain', '=', true)->orWhereNull('zkmain');
            })
            ->whereRaw("((zgod < date_part('year', CURRENT_DATE)
                            and (ctsostoianie ilike 'завершен' or ctsostoianie ilike 'расторгнут'))
                            or (zgod >= date_part('year', CURRENT_DATE)))")
            ->orderBy("ik_niokr_contract.ctnum")
            ->groupBy(['ik_niokr_contract.ctnum', 'zname', 'ctplanum', 'ctdatazakluch', 'ctdatazaversh', 'org_isp.oname',
                'zgod', 'zlotnum', 'zpriceall', 'ik_niokr_ntdproject.nprealiz', 'ctsostoianie', 'ctrlzpatent',
                'ctrlzntd', 'ctrlzproectntd', 'ctrlzbd', 'ik_niokr_zaiavka.prnum', 'bdis_array', 'ntdi_array', "article_array"]);
        if (SearchHelper::isNotEmpty($searchParam['budgetItem'])) {
            $realizationBuilder->where('sname', 'ilike', SearchHelper::searchString($searchParam['budgetItem']));
        }
        if (SearchHelper::isNotEmpty($searchParam['yearPlanTo'])) {
            $realizationBuilder->where('zgod', '<=', $searchParam['yearPlanTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['yearPlanFrom'])) {
            $realizationBuilder->where('zgod', '>=', $searchParam['yearPlanFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['status'])) {
            $realizationBuilder->where('zstatus', 'ilike', SearchHelper::searchString($searchParam['status']));
        }
        if (SearchHelper::isNotEmpty($searchParam['typeNTD'])) {
            if ($searchParam['typeNTD'] != 'ПРОЕКТ НТД') {
                $realizationBuilder->where('nprealiz', 'ilike', 'НТД');
                $realizationBuilder->where('npindex', '=', $searchParam['typeNTD']);
            } else {
                $realizationBuilder->where('nprealiz', 'ilike', 'Проект');
            }

        }
        if (SearchHelper::isNotEmpty($searchParam['number'])) {
            $realizationBuilder->where('ctplanum', 'ilike', SearchHelper::searchString($searchParam['number']));
        }
        if (SearchHelper::isNotEmpty($searchParam['condition'])) {
            $realizationBuilder->where('ctsostoianie', '=', $searchParam['condition']);
        }
        if (SearchHelper::isNotEmpty($searchParam['name'])) {
            $realizationBuilder->where('zname', 'ilike', SearchHelper::searchString($searchParam['name']));
        }
        if (SearchHelper::isNotEmpty($searchParam['executor'])) {
            $realizationBuilder->where('org_isp.oname', 'ilike', SearchHelper::searchString($searchParam['executor']));
        }
        if (SearchHelper::isNotEmpty($searchParam['customer'])) {
            $realizationBuilder->where('org_zak.oname', 'ilike', SearchHelper::searchString($searchParam['customer']));
        }

        if (SearchHelper::isNotEmpty($searchParam['dateStartFrom'])) {
            $realizationBuilder->where('ctdatazakluch', '>=', $searchParam['dateStartFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateStartTo'])) {
            $realizationBuilder->where('ctdatazakluch', '<=', $searchParam['dateStartTo']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateFinishFrom'])) {
            $realizationBuilder->where('ctdatazaversh', '>=', $searchParam['dateFinishFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateFinishTo'])) {
            $realizationBuilder->where('ctdatazaversh', '<=', $searchParam['dateFinishTo']);
        }

        $result = [];
        $aggregate['price'] = 0.0;
        foreach ($realizationBuilder->get() as $realization) {
            $result[] = [
                'id' => $realization->id,
                'year' => $realization->year,
                'title' => $realization->title,
                'executor' => $realization->executor,
                'number' => $realization->number,
                'date_sign' => SearchHelper::formatDate($realization->date_sign),
                'date_finish' => SearchHelper::formatDate($realization->date_finish),
                'price' => SearchHelper::number($realization->price),
                'status' => $realization->status,
                'context' => implode('<br/><br/>', array_merge(
                    self::makeBdis($realization->bdis_array),
                    self::makentdproject($realization->ntdproject),
                    self::makeNtdi($realization->ntdi_array))) ?: $realization->article_array
            ];
            $aggregate['price'] += SearchHelper::numberCalc($realization->price);
        }
        return ['rows' => $result, 'aggregate' => $aggregate];
    }

    private static function makeBdis($bdisArray)
    {
        if (!$bdisArray) return [];
        $result = [];
        foreach (json_decode($bdisArray, true) as $bdis) {
            $result[] = $bdis['svid_vid'] . ' ' . $bdis['title'] . '<br/>' . $bdis['svid_num'] . ' от ' . SearchHelper::formatDate($bdis['svid_data']);
        }
        return $result;
    }

    private static function makentdproject($ntdprojectArray)
    {
        if (!$ntdprojectArray) return [];
        $result = [];
        foreach (json_decode($ntdprojectArray, true) as $ntdproject) {

            $result[] = $ntdproject['index'] . ' ' . $ntdproject['title'] .
                ($ntdproject['type'] == 'НТД СП' ? ' (для служебного пользования)' : '');
        }
        return $result;
    }

    private static function makeNtdi($ntdiArray)
    {
        if (!$ntdiArray) return [];
        $result = [];
        foreach (json_decode($ntdiArray, true) as $ntdi) {
            $result[] = $ntdi['type_doc'] . ' ' . $ntdi['num_doc']
                . '<br/>' . $ntdi['title']
                . '<br/>' . $ntdi['primech']
                . '<br/>Действует с ' . SearchHelper::formatDate($ntdi['datavvod'])
                . '<br/>Действует: ' . $ntdi['deistvuet'];
        }
        return $result;
    }
}
