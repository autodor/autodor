<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Support\Facades\DB;

class BalanceService
{

    public static function search(array $searchParam): array
    {
        $balanceBuilder = db::table('rntd_objectu')
            ->select(
                "rntd_objectu.ounum                 as id",
                "ounameobject                  as name",
                "cdkontrnum                    as number",
                "cddatezakluch                 as date_start",
                "inamekratk                    as executor",
                "ptbalananum                   as inventory_number",
                "ptbalanastoim                 as price",
                "ptbalanapostav                as state")
            ->leftJoin("rntd_commondatekontr", "rntd_objectu.cdnum", "=", "rntd_commondatekontr.cdnum")
            ->leftJoin("rntd_zakazchik", "rntd_zakazchik.cdnum", "=", "rntd_commondatekontr.cdnum")
            ->leftJoin("rntd_ispolnitel", "rntd_ispolnitel.cdnum", "=", "rntd_commondatekontr.cdnum")
            ->leftJoin("rntd_patent", "rntd_patent.ounum", "=", "rntd_objectu.ounum")
            ->orderBy('rntd_patent.ounum');

//      this.dateFrom = null
//      this.dateTo = null
//      this.state = null
//
//        if (SearchHelper::isNotEmpty($searchParam['yearPlan'])) {
//            $balanceBuilder->where("zgod", "=", $searchParam['yearPlan']);
//        }
        if (SearchHelper::isNotEmpty($searchParam['inventoryNumber'])) {
            $balanceBuilder->where('ptbalananum', 'ilike', SearchHelper::searchString($searchParam['inventoryNumber']));
        }
        if (SearchHelper::isNotEmpty($searchParam['number'])) {
            $balanceBuilder->where('cdkontrnum', 'ilike', SearchHelper::searchString($searchParam['number']));
        }
        if (SearchHelper::isNotEmpty($searchParam['name'])) {
            $balanceBuilder->where('ounameobject', 'ilike', SearchHelper::searchString($searchParam['name']));
        }
        if (SearchHelper::isNotEmpty($searchParam['executor'])) {
            $balanceBuilder->where('inamekratk', 'ilike', SearchHelper::searchString($searchParam['executor']));
        }
        if (SearchHelper::isNotEmpty($searchParam['state'])) {
            if ($searchParam['state'] == "true") {
                $balanceBuilder->where('ptbalanapostav', '=', true);
            } else {
                $balanceBuilder->where(function ($query) {
                    $query->where('ptbalanapostav', '=', false)
                        ->orWhereNull('ptbalanapostav');
                });
            }
        }
        if (SearchHelper::isNotEmpty($searchParam['dateFrom'])) {
            $balanceBuilder->where('cddatezakluch', '>=', $searchParam['datePurchaseFrom']);
        }
        if (SearchHelper::isNotEmpty($searchParam['dateTo'])) {
            $balanceBuilder->where('cddatezakluch', '<=', $searchParam['datePurchaseTo']);
        }

        $result = [];
        foreach ($balanceBuilder->get() as $balance) {
            $result[] = [
                'id' => $balance->id,
                'name' => $balance->name,
                'number' => $balance->number,
                'executor' => $balance->executor,
                'date_start' => SearchHelper::formatDate($balance->date_start),
                'inventory_number' => $balance->inventory_number,
                'price' => SearchHelper::moneyThousand($balance->price),
                'state' => $balance->state ? 'Да' : 'Нет'
            ];
        }
        return $result;
    }

    public static function getOne($id): array
    {
        $balance = db::table('rntd_objectu')
            ->select(
                "ounameobject   as name",
                "ptbalananum    as inventory_number",
                "inamekratk     as executor_short",
                "inamepoln      as executor",
                "zknamekratk    as customer_short",
                "zknamepoln     as customer",
                "cdkontrnum     as number",
                "cddatezakluch  as date_start",
                "ptbalanastoim  as price",
                "ptbalanapostav as state",
                "onaznach",
                "ourospatsvidetnum     as svid_num",
                "ourospatsvidetdate    as svid_data",
                "ourospatsvidetkategor as svid_vid")
            ->leftJoin("rntd_commondatekontr", "rntd_objectu.cdnum", "=", "rntd_commondatekontr.cdnum")
            ->leftJoin("rntd_zakazchik", "rntd_zakazchik.cdnum", "=", "rntd_commondatekontr.cdnum")
            ->leftJoin("rntd_ispolnitel", "rntd_ispolnitel.cdnum", "=", "rntd_commondatekontr.cdnum")
            ->leftJoin("rntd_patent", "rntd_patent.ounum", "=", "rntd_objectu.ounum")
            ->leftJoin("ofap_object", "ofap_object.oname", "ilike", "ounameobject")
            ->orderBy('rntd_patent.ounum')
            ->first();
        $index = 0;
        $result[] =
            [
                'title' => 'Наименование объекта',
                'info' => $balance->name,
                'bold' => true,
                'index' => $index,
            ];
        $result[] =
            [
                'title' => 'Исполнитель (краткое наименование организации)',
                'info' => $balance->executor_short,
                'index' => $index,
            ];
        $result[] =
            [
                'title' => 'Исполнитель (полное наименование организации)',
                'info' => $balance->executor,
                'index' => $index,
            ];
        $result[] =
            [
                'title' => 'Заказчик (краткое наименование организации)',
                'info' => $balance->customer_short,
                'index' => $index,
            ];
        $result[] =
            [
                'title' => 'Заказчик (полное наименование организации)',
                'info' => $balance->customer,
                'index' => $index,
            ];
        $result[] =
            [
                'title' => 'Номер контракта',
                'info' => $balance->number,
                'index' => $index,
            ];
        $result[] =
            [
                'title' => 'Дата заключения контракта',
                'info' => SearchHelper::formatDate($balance->date_start),
                'index' => $index,
            ];
        $result[] =
            [
                'title' => 'Все реализации РИД по контракту:',
                'info' => null,
                'center' => true,
                'index' => $index,
            ];
        $result[] =
            [
                'title' => $balance->svid_vid,
                'info' => $balance->name . " №&nbsp;" . $balance->svid_num . " от " . $balance->svid_data
                    . ($balance->onaznach ? '<hr/>' . $balance->onaznach : ''),
                'index' => $index,
            ];
        $result[] =
            [
                'title' => 'Постановка на баланс:',
                'info' => null,
                'center' => true,
                'index' => $index,
            ];
        $result[] =
            [
                'title' => 'На балансе',
                'info' => $balance->state ? 'Да' : 'Нет',
                'index' => $index,
            ];
        $result[] =
            [
                'title' => 'Балансовая стоимость',
                'info' => SearchHelper::moneyThousand($balance->price),
                'index' => $index,
            ];
        $result[] =
            [
                'title' => 'Инвентарный номер',
                'info' => $balance->number,
                'index' => $index,
            ];
        return $result;
    }
}
