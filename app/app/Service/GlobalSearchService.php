<?php

namespace App\Service;

use App\Http\Resources\SearchHelper;
use Illuminate\Support\Facades\DB;

class GlobalSearchService
{

    public static function search(array $searchParam)
    {
        $result = [];
        $niokrs = DB::table("ik_niokr_contract")
            ->select(
                'ik_niokr_contract.ctnum         as id',
                'ctplanum                        as number',
                "ctdatazakluch                   as conclusion_date",
                'zname                           as title'
            )
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_contract.znum", "=", "ik_niokr_zaiavka.znum")
            ->whereRaw("ik_niokr_zaiavka.snum = '7' or (ik_niokr_zaiavka.snum = '3' and ik_niokr_zaiavka.prnum = '39')");
        foreach ($niokrs->get() as $niokr) {
            $result[] = [
                "id" => ['id' => $niokr->id, 'table' => 'contract'],
                "table_name" => 'НИОКТР',
                "title" =>
                    trim($niokr->number .
                        ($niokr->conclusion_date !== null
                            ? " " . date("d.m.Y", strtotime($niokr->conclusion_date)) . " "
                            : " ")
                        . $niokr->title)
            ];
        }
        $niokrs = DB::table("arxiv_tema")
            ->select(
                'tnum             as id',
                'ctplanumik                  as number',
                'ctdatazakik                 as conclusion_date',
                'tname                       as title'
            )
            ->where("tzavershena", "=", true)
            ->orderBy("tnum");
        foreach ($niokrs->get() as $niokr) {
            $result[] = [
                "id" => ['id' => $niokr->id, 'table' => 'niokr'],
                "table_name" => 'НИОКТР',
                "title" =>
                    trim($niokr->number .
                        ($niokr->conclusion_date !== null
                            ? " " . SearchHelper::formatDate($niokr->conclusion_date) . " "
                            : " ")
                        . $niokr->title)
            ];
        }
        $patents = db::table("bibl_mainpatent")
            ->distinct()
            ->select(
                "mpnum         as id",
                "mpnumpat      as number",
                "mpdatapubl    as conclusion_date",
                "mpname        as title")
            ->orderByDesc("mpnum");
        foreach ($patents->get() as $patent) {
            $result[] = [
                "id" => ['id' => $patent->id, 'table' => 'patent'],
                "table_name" => 'ЭБ&nbsp;Патенты',
                "title" => 'Патент: ' . trim($patent->number .
                        ($patent->conclusion_date !== null
                            ? " от " . SearchHelper::formatDate($patent->conclusion_date) . " "
                            : " ")
                        . $patent->title)
            ];
        }
        $ntds = DB::table('eb_ntd_view')
            ->select(
                "table_name",
                "id",
                "type_doc",
                "number",
                "topic_id",
                "title",
                "year_published",
                "developer",
                "ftris",
                "contract_number",
                "date as contract_date",
                "date_entry",
                "date_approval");

        foreach ($ntds->get() as $ntd) {
            $result[] = [
                "id" => ['id' => $ntd->table_name . '-' . $ntd->id, 'table' => 'ntd'],
                "table_name" => 'ЭБ&nbsp;НТД',
                "title" =>
                    $ntd->type_doc . " " . $ntd->contract_number .
                    ($ntd->contract_date !== null
                        ? " " . date("d.m.Y", strtotime($ntd->contract_date)) . " "
                        : " ")
                    . $ntd->title
            ];
        }
        if (SearchHelper::isNotEmpty($searchParam['allSearchQuery'])) {
            $result = SearchHelper::allSearchFilter($result, $searchParam['allSearchQuery']);
        }

        return $result;
    }

    public static function getContractInfo($id)
    {
        $contract = DB::table("ik_niokr_contract")
            ->select(
                'ik_niokr_contract.ctnum         as id',
                'ctplanum                        as number',
                'org_isp.oname  as design',
                'zlotnum        as num_date_lot',
                "ctdatazakluch                   as conclusion_date",
                'zname                           as title'
            )
            ->leftJoin("ik_niokr_zaiavka", "ik_niokr_contract.znum", "=", "ik_niokr_zaiavka.znum")
            ->leftJoin("ik_niokr_ispolnitel", "ik_niokr_zaiavka.znum", "=", "ik_niokr_ispolnitel.znum")
            ->leftJoin("ik_niokr_organiz as org_isp", "ik_niokr_ispolnitel.onum", "=", "org_isp.onum")
            ->whereRaw("ik_niokr_zaiavka.snum = '7' or (ik_niokr_zaiavka.snum = '3' and ik_niokr_zaiavka.prnum = '39')")
            ->where("ik_niokr_contract.ctnum", "=", $id)
            ->first();
        $result = [];
        $index = 0;
        $result[] =
            [
                'title' => 'Наименование контракта',
                'info' => $contract->title,
                'bold' => true,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Исполнитель (полное наименование организации)',
                'info' => $contract->design ?? "",
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Номер и дата лота',
                'info' => $contract->num_date_lot,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Номер контракта',
                'info' => $contract->number,
                'index' => ++$index
            ];
        $result[] =
            [
                'title' => 'Дата заключения контракта',
                'info' => SearchHelper::formatDate($contract->conclusion_date),
                'index' => ++$index
            ];
        return $result;
    }
}
