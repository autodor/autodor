<?php

namespace App\Exports;

use App\Http\Controllers\Public\RegistryController;
use App\Service\NiokrService;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\Calculation\DateTimeExcel\Date;

class NiokrExport implements FromCollection, Responsable, WithHeadings
{

    use Exportable;

    private $fileName = 'НИОКР.xlsx';
    private $writerType = Excel::XLSX;
    private $search;

    /**
     * @param $search
     */
    public function __construct($search)
    {
        $this->search = $search;
    }

    public function headings(): array
    {
        return [
            '№',
            'Наименование',
            'Год завершения',
            'Номер',
            'Дата',
            'Разработчик',
            'Номер в Архиве',
            'Реализации',
        ];
    }

    public function collection()
    {
        return collect(NiokrService::excel($this->search));
    }
}
