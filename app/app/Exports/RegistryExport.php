<?php

namespace App\Exports;

use App\Http\Controllers\Public\RegistryController;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Excel;

class RegistryExport implements FromCollection, Responsable, WithHeadings
{

    use Exportable;

    private $fileName = 'РЕЕСТР РИД.xlsx';
    private $writerType = Excel::XLSX;

    public function headings(): array
    {
        return [
            '№',
            'Номер в ОР РИД',
            'Наименование РИД, номер и дата ГК',
            'Государственная регистрация'
        ];
    }

    public function collection()
    {
        return RegistryController::data();
    }
}
