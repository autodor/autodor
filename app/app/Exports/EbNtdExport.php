<?php

namespace App\Exports;

use App\Service\EbNtdService;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Excel;

class EbNtdExport implements FromCollection, Responsable, WithHeadings
{

    use Exportable;

    private $fileName = 'ЭБ НТД.xlsx';
    private $writerType = Excel::XLSX;
    private $search;

    /**
     * @param $search
     */
    public function __construct($search)
    {
        $this->search = $search;
    }

    public function headings(): array
    {
        return [
            '№',
            'Вид документа',
            'Номер документа',
            'Наименование',
            'Год издания',
            'Разработчик',
            'Входит в ФТРиС',
            'Разработано для Росавтодора',
            'Классификатор стандартов и отраслевых документов',
            'Тематика по уровню регулирования',
            'Тематика по функциональной ориентации и объекту нормирования',
        ];
    }

    public function collection()
    {
        return collect(EbNtdService::excel($this->search));
    }
}
