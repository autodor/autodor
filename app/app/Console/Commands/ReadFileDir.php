<?php

namespace App\Console\Commands;

use App\Models\FileModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ReadFileDir extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:read-file-dir';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload paths to db';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        FileModel::where('path', '!=', 'uuid')->delete();
        $directories = Storage::disk('upload')->allDirectories();
        foreach ($directories as $directory) {
            if ($directory !== 'uuid') {
                $count = 0;
                $files = Storage::disk('upload')->files($directory);
                foreach ($files as $file) {
                    $uuid = Str::uuid();
                    FileModel::create([
                            'uuid' => $uuid,
                            'path' => $directory,
                            'filename' => basename($file)
                        ]
                    );
                    $count++;
                }
                echo $directory . ' добавлено файлов: ' . $count . PHP_EOL;
            }
        }
    }
}
