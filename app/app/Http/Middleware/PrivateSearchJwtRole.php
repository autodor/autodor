<?php

namespace App\Http\Middleware;

use App\enums\UserRole;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PrivateSearchJwtRole
{

    const USER_ROLE = [UserRole::Admin, UserRole::User, UserRole::Operator, UserRole::FKU];

    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (auth()->user() === null || auth()->user()->role === null) {
            return response()->json(['error' => 'не авторизован'], 401);
        }
        if (!in_array(auth()->user()->role, self::USER_ROLE)) {
            return response()->json(['error' => 'Permission disable'], 403);
        }
        return $next($request);
    }
}
