<?php

namespace App\Http\Controllers\Public;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Models\EbPatentModel;
use App\Models\RegistryModel;
use App\Service\EbPatentService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class EbPatentSearchController extends Controller
{
    public function index(Request $data)
    {
        $result = EbPatentService::search($data->all());
        return response()->json($result);

    }

    public function getOne(int $id)
    {
        return response()->json(EbPatentService::getOne($id));
    }

    public function getExcel()
    {
        return Excel::download(new RegistryExport, 'РЕЕСТР РИД.xlsx');
    }
}
