<?php

namespace App\Http\Controllers\Public;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Service\EbNtdService;
use App\Service\EbPatentService;
use App\Service\GlobalSearchService;
use App\Service\NiokrService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class GlobalSearchController extends Controller
{
    public function index(Request $data)
    {
        $result = GlobalSearchService::search($data->all());
        return response()->json($result);
    }

    public function getOne(Request $data)
    {
        switch ($data['table']) {
            case 'contract':
                return response()->json(GlobalSearchService::getContractInfo($data['id']));
            case 'niokr':
                return response()->json(NiokrService::getOne($data['id']));
            case 'ntd':
                return response()->json(EbNtdService::getOne($data['id']));
            case 'patent':
                return response()->json(EbPatentService::getOne($data['id']));
        }
    }

    public function getExcel()
    {
        return Excel::download(new RegistryExport, 'РЕЕСТР РИД.xlsx');
    }
}
