<?php

namespace App\Http\Controllers\Public;

use App\Exports\EbNtdExport;
use App\Http\Controllers\Controller;
use App\Service\EbNtdService;
use Illuminate\Http\Request;

class EbNtdSearchController extends Controller
{
    public function index(Request $data)
    {
        $result = EbNtdService::search($data->all());
        return response()->json($result);
    }

    public function getOne(string $id)
    {
        $result = EbNtdService::getOne($id);
        return response()->json($result);
    }

    public function getExcel(Request $data)
    {
        new EbNtdExport($data->all());
    }
}
