<?php

namespace App\Http\Controllers\Public;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Http\Resources\RegistryResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class RegistryController extends Controller
{
    public static function data()
    {
        return db::table('rntd_objectu')
            ->select('rntd_objectu.ounum                 as id',
                'oufdaplanum                   as key',
                'ounameobject                  as name',
                'cdkontrnum                    as contractNumber',
                'cddatezakluch                 as contractDate',
                'ourospatsvidetkategor         as patentName',
                'ourospatsvidetnum             as patentNumber',
                'ourospatsvidetdate            as patentDate',
                'ouregnum                      as certificateNumber',
                'oudatereg                     as certificateDate')
            ->leftJoin("rntd_commondatekontr", "rntd_objectu.cdnum", "=", "rntd_commondatekontr.cdnum")
            ->whereNotNull('oufdaplanum')
            ->orderBy('oufdaplanum')
            ->get();
    }

    public function index(): JsonResource
    {
        $registries = self::data();
        return RegistryResource::collection($registries);
    }

    public function getOne(int $id)
    {
        $registry = db::table('rntd_objectu')
            ->select("rntd_objectu.ounum         as id",
                "oufdaplanum           as key",
                "ounameobject          as name",
                "cdnamework            as contractName",
                "cdkontrnum            as contractNumber",
                "cddatezakluch         as contractDate",
                "ourospatsvidetkategor as patentName",
                "ourospatsvidetnum     as patentNumber",
                "ourospatsvidetdate    as patentDate",
                "ouregnum              as certificateNumber",
                "oudatereg             as certificateDate",
                "ouopisan              as description")
            ->leftJoin("rntd_commondatekontr", "rntd_objectu.cdnum", "=", "rntd_commondatekontr.cdnum")
            ->where("rntd_objectu.ounum", "=", $id)
            ->first();
        return response(
            [
                'id' => $registry->id,
                'key' => $registry->key,
                'name' => $registry->name,
                'contractName' => $registry->contractName,
                'contractNumber' => $registry->contractNumber,
                'contractDate' => date("d.m.Y", strtotime($registry->contractDate)),
                'patentName' => $registry->patentName,
                'patentNumber' => $registry->patentNumber,
                'patentDate' => date("d.m.Y", strtotime($registry->patentDate)),
                'certificateNumber' => $registry->certificateNumber,
                'certificateDate' => date("d.m.Y", strtotime($registry->certificateDate)),
                'description' => $registry->description
            ]
        );
    }

    public function getExcel()
    {
        return new RegistryExport;
    }
}
