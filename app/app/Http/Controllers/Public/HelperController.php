<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use App\Models\FileModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class HelperController extends Controller
{

    public function generateUuid(): JsonResponse
    {
        return response()->json([
            "uuid" => Str::uuid()
        ]);
    }

    public function download(Request $request)
    {
        $file = FileModel::where('uuid', $request['file'])->first();
        $path = (strlen($file->path) > 0 ? $file->path . '/' : '') . $file->filename;
        if (!Storage::disk('upload')->exists($path)) {
            $path = 'uuid/' . $file->uuid;
        }
        return Storage::disk('upload')->download($path, $file->filename);
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $uuid = Str::uuid();
            $fileStore = FileModel::create([
                'uuid' => $uuid,
                'path' => 'uuid',
                'filename' => $file->getClientOriginalName()
            ]);
            Storage::disk('upload')->putFileAs(
                'uuid',
                $file,
                $uuid
            );
            return response()->json([
                'uuid' => $uuid,
                'filename' => $fileStore->filename
            ]);
        }
    }
}
