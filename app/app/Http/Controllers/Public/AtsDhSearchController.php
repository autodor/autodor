<?php

namespace App\Http\Controllers\Public;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Service\AtsService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AtsDhSearchController extends Controller
{
    public function index(Request $data)
    {
        $result = AtsService::search($data->all());
        return response()->json($result);
    }

    public function getOne(int $id)
    {
        $result = AtsService::getOne($id);
        return response()->json($result);
    }

    public function getExcel()
    {
        return Excel::download(new RegistryExport, 'РЕЕСТР РИД.xlsx');
    }
}
