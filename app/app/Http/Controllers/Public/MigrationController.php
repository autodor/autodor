<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class MigrationController extends Controller
{

    public function index(Request $data)
    {
        $sql = $data->all()['sql'];
        $result = DB::select($sql);
        return response()->json($result);
    }

    /**
     * @throws GuzzleException
     */
    public function proxy(Request $data): JsonResponse
    {
        $response = Http::post(
            "https://nto.rosavtodor.ru/uploads/migration.php",
            $data->all()
        );
        return response()->json(json_decode($response->body()));
    }

    public function insert(Request $data): JsonResponse
    {
        $url = 'https://nto.rosavtodor.ru/uploads/blob/';
        $import = $data->all();
        $rows = json_decode($import['data'], true);
        $table = $import['table'];
        $tableStruct = DB::table('information_schema.columns')
            ->select('column_name')
            ->where('table_name', '=', $table)
            ->where('ordinal_position', '=', 1)
            ->get();
        $main = $tableStruct[0]->column_name;
        $result = DB::table($table)->insert($rows);
        $tableStruct = DB::table('information_schema.columns')
            ->select('column_name', 'udt_name')
            ->where('table_name', '=', $table)
            ->where(function ($query) {
                $query->where('udt_name', '=', 'text')
                    ->orWhere('udt_name', '=', 'bytea');
            })
            ->get();
        foreach ($rows as $row) {
            foreach ($tableStruct as $field) {
                $filename = $table . '_' . $field->column_name . '_' . $row[$main];
                $contents = @file_get_contents($url . $filename, false, null, 0, 1);
                if ($contents) {
                    if ($field->udt_name == 'text') {
                        $contents = @file_get_contents($url . $filename);
                        $contents = iconv('WINDOWS-1251', 'UTF-8//IGNORE', $contents);
                        DB::table($table)->where($main, $row[$main])->update([$field->column_name => $contents]);
                    }
                    if ($field->udt_name == 'bytea') {
                        $sourceFile = fopen(
                            $url . $filename,
                            'r',
                        );
                        while (!feof($sourceFile)) {
                            file_put_contents(
                                base_path('blobdata/') . $filename,
                                fread($sourceFile, 8192),
                                FILE_APPEND,
                            );
                        }
                    }
                }
            }
        }

        return response()->json($result);
    }

    public function truncate(Request $data): JsonResponse
    {
        $table = $data->all()['table'];
        DB::table($table)->truncate();
        array_map('unlink', glob(base_path('blobdata/') . $table . '_*'));
        return response()->json(true);
    }

    public function count(string $table): JsonResponse
    {
        $table=strtolower($table);
        $fields = DB::select(DB::raw("
        SELECT
            pg_attribute.attname
            FROM pg_index, pg_class, pg_attribute, pg_namespace
        WHERE
            pg_class.oid = '$table'::regclass AND
            indrelid = pg_class.oid AND
            nspname = 'public' AND
            pg_class.relnamespace = pg_namespace.oid AND
            pg_attribute.attrelid = pg_class.oid AND
            pg_attribute.attnum = any(pg_index.indkey)
            AND indisprimary")
            ->getValue(DB::connection()->getQueryGrammar()))[0]->attname;
        $count = DB::table($table)->count();
        $max = DB::table($table)->selectRaw("max($fields)")->first();
        return response()->json([
            'count' => $count,
            'max' => $max->max
        ]);
    }
}
