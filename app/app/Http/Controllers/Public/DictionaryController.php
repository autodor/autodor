<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class DictionaryController extends Controller
{
    public function getDictionaries(): JsonResponse
    {
        $topics = array_map(function ($item) {
            return [
                "id" => $item->tnum,
                "code" => $item->tplanum,
                "name" => $item->tname,
                "type" => $item->ttip,
            ];
        }, DB::table('bibl_tematika')->get()->toArray());
        $mpkSubsection = array_map(function ($item) {
            return [
                'id' => $item->ptprplanum,
                'name' => $item->ptprname
            ];
        }, DB::table('bibl_ptpodrazdel')->get()->toArray());
        $mpkChapter = array_map(function ($item) {
            return [
                'id' => $item->ptrplanum,
                'name' => $item->ptrname
            ];
        }, DB::table('bibl_ptrazdel')->get()->toArray());
        $budgetItem = array_map(function ($item) {
            return [
                'id' => $item->snum,
                'name' => $item->sname
            ];
        }, DB::table('ik_niokr_statia')->get()->toArray());
        $result = [
            'topics' => $topics,
            'mpkSubsection' => $mpkSubsection,
            'mpkChapter' => $mpkChapter,
            'budgetItem' => $budgetItem,
        ];
        return response()->json($result);
    }

}
