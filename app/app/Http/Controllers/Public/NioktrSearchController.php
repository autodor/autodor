<?php

namespace App\Http\Controllers\Public;

use App\Exports\NiokrExport;
use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Models\NiokrModel;
use App\Service\NiokrService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class NioktrSearchController extends Controller
{
    public function index(Request $data)
    {
        $result = NiokrService::search($data->all());
        return response()->json($result);
    }

    public function getOne(int $id)
    {
        $result = NiokrService::getOne($id);
        return response()->json($result);
    }

    public function getExcel(Request $data)
    {
        return new NiokrExport($data->all());
    }
}
