<?php

namespace App\Http\Controllers\Private;

use App\Http\Controllers\Controller;
use App\Http\Resources\SearchHelper;
use App\Models\NtdModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class NtdController extends Controller
{

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'uuid' => 'required',
            'dateStart' => 'nullable|date',
            'dateFinish' => 'nullable|date',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $offerCaFda = NtdModel::updateOrCreate(
            ['uuid' => $request->uuid],
            array_merge($request->all(),
                ['user_id' => auth()->user()->id]
            )
        );
        return response()->json($offerCaFda);
    }

    public function listByUser()
    {
        $list = NtdModel::where('user_id', auth()->user()->id);
        $result = [];
        foreach ($list->get() as $item) {
            $result[] = [
                'uuid' => $item->uuid,
                'typeNtd' => $item->typeNtd,
                'numberNtd' => $item->numberNtd,
                'titleNtd' => $item->titleNtd,
                'titleFku' => $item->titleFku,
                'numberPurchase' => $item->numberPurchase,
                'numberContract' => $item->numberContract,
                'dateStart' => SearchHelper::formatDate($item->dateStart),
                'dateFinish' => SearchHelper::formatDate($item->dateFinish),
                'developer' => $item->developer,
                'title' => $item->title,
                'place' => $item->place,
                'type' => $item->type
            ];
        }
        return $result;
    }

    public function load($uuid)
    {
        $ntdModel = NtdModel::where('uuid', $uuid)->first();
        return response()->json($ntdModel);
    }

    public function search(Request $request)
    {
        $search = DB::table('ntd');
        if ($request['uuid'] !== null) {
            $search = $search->where('uuid', 'like', '%' . $request['uuid'] . '%');
        }
        if ($request['typeNtd'] !== null) {
            $search = $search->where('typeNtd', $request['typeNtd']);
        }
        if ($request['numberNtd'] !== null) {
            $search = $search->where('numberNtd', 'like', '%' . $request['numberNtd'] . '%');
        }
        if ($request['titleNtd'] !== null) {
            $search = $search->where('titleNtd', 'like', '%' . $request['titleNtd'] . '%');
        }
        if ($request['titleFku'] !== null) {
            $search = $search->where('titleFku', 'like', '%' . $request['titleFku'] . '%');
        }
        if ($request['numberPurchase'] !== null) {
            $search = $search->where('number', 'like', '%' . $request['numberPurchase'] . '%');
        }
        if ($request['numberContract'] !== null) {
            $search = $search->where('number', 'like', '%' . $request['numberContract'] . '%');
        }
        if ($request['dateStartFrom'] !== null) {
            $search = $search->where('dateStart', '>=', $request['dateStartFrom']);
        }
        if ($request['dateStartTo'] !== null) {
            $search = $search->where('dateStart', '<=', $request['dateStartTo']);
        }
        if ($request['dateFinishFrom'] !== null) {
            $search = $search->where('dateFinish', '>=', $request['dateFinishFrom']);
        }
        if ($request['dateFinishTo'] !== null) {
            $search = $search->where('dateFinishTo', '<=', $request['dateFinish']);
        }
        if ($request['developer'] !== null) {
            $search = $search->where('developer', 'like', '%' . $request['developer'] . '%');
        }
        if ($request['title'] !== null) {
            $search = $search->where('title', 'like', '%' . $request['title'] . '%');
        }
        if ($request['place'] !== null) {
            $search = $search->where('place', 'like', '%' . $request['place'] . '%');
        }
        if ($request['type'] !== null) {
            $search = $search->where('type', 'like', '%' . $request['type'] . '%');
        }
        $result = [];
        foreach ($search->get() as $item) {
            $result[] = [
                'uuid' => $item->uuid,
                'typeNtd' => $item->typeNtd,
                'numberNtd' => $item->numberNtd,
                'titleNtd' => $item->titleNtd,
                'titleFku' => $item->titleFku,
                'numberPurchase' => $item->numberPurchase,
                'numberContract' => $item->numberContract,
                'dateStart' => SearchHelper::formatDate($item->dateStart),
                'dateFinish' => SearchHelper::formatDate($item->dateFinish),
                'developer' => $item->developer,
                'title' => $item->title,
                'place' => $item->place,
                'type' => $item->type
            ];
        }
        return response()->json($result);
    }

    public function detail($uuid)
    {
        $ntdModel = NtdModel::where('uuid', $uuid)->first();
        $index = 0;
        $result = [];
        $result[] = SearchHelper::makeDetailBold($index++, 'Уникальный номер заявки:', null);
        $result[] = SearchHelper::makeDetailBold($index++, $uuid, null);
        $result[] = SearchHelper::makeDetail($index++, "Вид НТД", $ntdModel->typeNtd ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Номер НТД", $ntdModel->numberNtd ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Наименование НТД", $ntdModel->titleNtd ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Наименование ФКУ", $ntdModel->titleFku ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Номер закупки", $ntdModel->numberPurchase ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Регистрационный номер контракта", $ntdModel->numberContract ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Дата заключения контракта", SearchHelper::formatDate($ntdModel->dateStart) ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Дата завершения работ", SearchHelper::formatDate($ntdModel->dateFinish) ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Наименование организации-исполнителя", $ntdModel->developer ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Наименование работ", $ntdModel->title ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Место проведения работ", $ntdModel->place ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Вид работ", $ntdModel->type ?: "-");
        return response()->json($result);
    }
}
