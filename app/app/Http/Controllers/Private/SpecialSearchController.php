<?php

namespace App\Http\Controllers\Private;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Models\RegistryModel;
use App\Service\SpecialService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SpecialSearchController extends Controller
{
    public function index(Request $data)
    {
        $result = SpecialService::search($data->all());
        return response()->json($result);
    }

    public function getOne(int $id)
    {
        $result = SpecialService::getOne($id);
        return response()->json($result);
    }

    public function getExcel()
    {
        return Excel::download(new RegistryExport, 'РЕЕСТР РИД.xlsx');
    }
}
