<?php

namespace App\Http\Controllers\Private;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Service\TenderService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TenderSearchController extends Controller
{
    public function index(Request $data)
    {
        $result = TenderService::search($data->all());
        return response()->json($result);
    }

    public function getOne(int $id)
    {
        return response()->json(TenderService::getOne($id));
    }

    public function getExcel()
    {
        return Excel::download(new RegistryExport, 'РЕЕСТР РИД.xlsx');
    }
}
