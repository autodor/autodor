<?php

namespace App\Http\Controllers\Private;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Models\RegistryModel;
use App\Service\ProjectService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProjectSearchController extends Controller
{
    public function index(Request $data)
    {
        $result = ProjectService::search($data->all());
        return response()->json($result);
    }

    public function getOne(int $id)
    {
        $result = ProjectService::getOne($id);
        return response()->json($result);
    }

    public function getExcel()
    {
        return Excel::download(new RegistryExport, 'РЕЕСТР РИД.xlsx');
    }
}
