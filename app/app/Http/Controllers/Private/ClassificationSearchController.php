<?php

namespace App\Http\Controllers\Private;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Service\ClassificationService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ClassificationSearchController extends Controller
{
    public function index(Request $data)
    {
        $result = ClassificationService::search($data->all());
        return response()->json($result);
    }

    public function getExcel()
    {
        return Excel::download(new RegistryExport, 'РЕЕСТР РИД.xlsx');
    }
}
