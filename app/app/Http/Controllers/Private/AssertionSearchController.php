<?php

namespace App\Http\Controllers\Private;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Models\RegistryModel;
use App\Service\AssertionService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AssertionSearchController extends Controller
{
    public function index(Request $data)
    {
        $result = AssertionService::search($data->all());
        return response()->json($result);
    }

    public function getOne(int $id)
    {
        $result = AssertionService::getOne($id);
        return response()->json($result);
    }

    public function getExcel()
    {
        return Excel::download(new RegistryExport, 'РЕЕСТР РИД.xlsx');
    }
}
