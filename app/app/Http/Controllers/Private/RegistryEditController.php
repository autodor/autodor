<?php

namespace App\Http\Controllers\Private;

use App\Http\Controllers\Controller;
use App\Models\RegistryModel;
use Illuminate\Http\Request;
use Validator;

class RegistryEditController extends Controller
{
    public function delete(string $id)
    {
        $validator = Validator::make(['id' => $id], [
            'id' => 'required|integer']);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $registry = RegistryModel::find($id);
        if ($registry === null) {
            return response()->json([
                'message' => 'Record not found',
            ], 404);
        }
        $registry->delete();
        return response()->json([
            'message' => 'Record successfully delete',
        ], 204);
    }

    public function insert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'key' => 'required|string',
            'name' => 'required|string',
            'contractName' => 'required|string',
            'contractNumber' => 'required|string',
            'contractDate' => 'required|date',
            'patentName' => 'required|string',
            'patentNumber' => 'required|string',
            'patentDate' => 'required|date',
            'certificateNumber' => 'string|nullable',
            'certificateDate' => 'date|nullable',
            'description' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $registry = RegistryModel::create($validator->validated());
        return response(
            [
                'id' => $registry->id,
                'key' => $registry->key,
                'name' => $registry->name,
                'contractName' => $registry->contractName,
                'contractNumber' => $registry->contractNumber,
                'contractDate' => date("d.m.Y", strtotime($registry->contractDate)),
                'patentName' => $registry->patentName,
                'patentNumber' => $registry->patentNumber,
                'patentDate' => date("d.m.Y", strtotime($registry->patentDate)),
                'certificateNumber' => $registry->certificateNumber,
                'certificateDate' => date("d.m.Y", strtotime($registry->certificateDate)),
                'description' => $registry->description
            ]
        );
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'key' => 'required|string',
            'name' => 'required|string',
            'contractName' => 'required|string',
            'contractNumber' => 'required|string',
            'contractDate' => 'required|date',
            'patentName' => 'required|string',
            'patentNumber' => 'required|string',
            'patentDate' => 'required|date',
            'certificateNumber' => 'string|nullable',
            'certificateDate' => 'date|nullable',
            'description' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $registry = RegistryModel::findOrFail($request->id);
        $registry->id = $request->id;
        $registry->key = $request->key;
        $registry->name = $request->name;
        $registry->contractName = $request->contractName;
        $registry->contractNumber = $request->contractNumber;
        $registry->contractDate = date("Y-m-d", strtotime($request->contractDate));
        $registry->patentName = $request->patentName;
        $registry->patentNumber = $request->patentNumber;
        $registry->patentDate = date("Y-m-d", strtotime($request->patentDate));
        $registry->certificateNumber = $request->certificateNumber;
        $registry->certificateDate = date("Y-m-d", strtotime($request->certificateDate));
        $registry->description = $request->description;
        $registry->updated_at = now();
        $registry->save();
        return response(
            [
                'id' => $registry->id,
                'key' => $registry->key,
                'name' => $registry->name,
                'contractName' => $registry->contractName,
                'contractNumber' => $registry->contractNumber,
                'contractDate' => date("d.m.Y", strtotime($registry->contractDate)),
                'patentName' => $registry->patentName,
                'patentNumber' => $registry->patentNumber,
                'patentDate' => date("d.m.Y", strtotime($registry->patentDate)),
                'certificateNumber' => $registry->certificateNumber,
                'certificateDate' => date("d.m.Y", strtotime($registry->certificateDate)),
                'description' => $registry->description
            ]
        );
    }
}
