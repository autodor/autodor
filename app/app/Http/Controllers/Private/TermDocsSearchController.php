<?php

namespace App\Http\Controllers\Private;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Service\TermDocService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TermDocsSearchController extends Controller
{
    public function index(Request $data)
    {
        $result = TermDocService::search($data->all());
        return response()->json($result);
    }

    public function getOne(string $idStr)
    {
        $id = array_map(function ($item) {
            return (int)$item;
        }, explode(',', $idStr));
        return response()->json(TermDocService::getOne($id));
    }

    public function getExcel()
    {
        return Excel::download(new RegistryExport, 'РЕЕСТР РИД.xlsx');
    }
}
