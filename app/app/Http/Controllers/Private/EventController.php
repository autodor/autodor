<?php

namespace App\Http\Controllers\Private;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Models\EventModel;
use App\Models\StoEventModel;
use App\Service\EventService;
use Maatwebsite\Excel\Facades\Excel;

class EventController extends Controller
{
    public function index()
    {
        return response()->json(EventService::response());
    }

    public function count()
    {
        return response()->json(EventService::count());
    }

    public function getOne($id)
    {
        return response()->json(EventService::getOne($id));
    }


    public function getExcel()
    {
        return Excel::download(new RegistryExport, 'РЕЕСТР РИД.xlsx');
    }
}
