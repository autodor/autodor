<?php

namespace App\Http\Controllers\Private;

use App\Http\Controllers\Controller;
use App\Http\Resources\SearchHelper;
use App\Models\OfferModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class OfferController extends Controller
{

    public function save(string $type, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'uuid' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        if ($type != 'all') {
            $request['type'] = $type;
        }
        $offerCaFda = OfferModel::updateOrCreate(
            ['uuid' => $request->uuid],
            array_merge($request->all(),
                ['user_id' => auth()->user()->id]
            )
        );
        return response()->json($offerCaFda);
    }

    public function listByUser(string $type)
    {
        $list = OfferModel::where('user_id', auth()->user()->id);
        if ($type !== 'all') {
            $list = $list->where('type', $type);
        }
        $result = [];
        foreach ($list->get() as $item) {
            $result[] = [
                'uuid' => $item->uuid,
                'type' => $item->type,
                'year' => $item->year,
                'approved' => $item->approved,
                'title' => $item->title,
                'finishWork' => $item->finishWork,
                'scopeApplication' => $item->scopeApplication,
                'integrationWork' => $item->integrationWork,
                'conditionWork' => $item->conditionWork,
                'subject' => $item->subject,
                'specialConditions' => $item->specialConditions,
                'placeWork' => $item->placeWork,
                'deadlineWork' => $item->deadlineWork,
                'niokrDirection' => $item->niokrDirection
            ];
        }
        return $result;
    }

    public function load($type, $uuid)
    {
        $offerCaFdaModel = OfferModel::where('uuid', $uuid)
            ->where('user_id', auth()->user()->id)
            ->first();
        return response()->json(
            array_merge($offerCaFdaModel->toArray(),
                [
                    'initDataFileLoad' => SearchHelper::getFileInfo($offerCaFdaModel->initDataFile),
                    'conditionFileLoad' => SearchHelper::getFileInfo($offerCaFdaModel->conditionFile),
                    'contentWorkFileLoad' => SearchHelper::getFileInfo($offerCaFdaModel->contentWorkFile),
                    'requirementFileLoad' => SearchHelper::getFileInfo($offerCaFdaModel->requirementFile),
                    'listReviewerFileLoad' => SearchHelper::getFileInfo($offerCaFdaModel->listReviewerFile),
                    'planWorkFileLoad' => SearchHelper::getFileInfo($offerCaFdaModel->planWorkFile),
                    'suggestionFileLoad' => SearchHelper::getFileInfo($offerCaFdaModel->suggestionFile),
                    'offersFileLoad' => SearchHelper::getFileInfo($offerCaFdaModel->offersFile),
                ]
            )
        );
    }

    public function search(Request $request)
    {
        $search = DB::table('offer');
        if ($request['year'] !== null) {
            $search = $search->where('year', $request['year']);
        }
        if ($request['type'] !== null) {
            $search = $search->where('type', $request['type']);
        }
        if ($request['title'] !== null) {
            $search = $search->where('title', 'like', '%' . $request['title'] . '%');
        }
        if ($request['description'] !== null) {
            $search = $search->where('targetWork', 'like', '%' . $request['description'] . '%')
                ->orWhere('infoWork', 'like', '%' . $request['description'] . '%');
        }
        if ($request['niokrDirection'] !== null) {
            $search = $search->where('niokrDirection', $request['niokrDirection']);
        }
        if ($request['customer'] !== null) {
            $search = $search->where('customer', $request['customer']);
        }
        if ($request['approved'] !== null) {
            $search = $search->where('approved', $request['approved']);
        }
        $result = [];
        foreach ($search->get() as $item) {
            $result[] = [
                'uuid' => $item->uuid,
                'type' => $item->type,
                'year' => $item->year,
                'approved' => $item->approved,
                'title' => $item->title,
                'targetWork' => $item->targetWork,
                'infoWork' => $item->infoWork,
                'niokrDirection' => $item->niokrDirection,
                'customer' => $item->customer
            ];
        }
        return response()->json($result);
    }

    public function detail($uuid)
    {
        $offerCaFdaModel = OfferModel::where('uuid', $uuid)->first();
        $index = 0;
        $result = [];
        $result[] = SearchHelper::makeDetailBold($index++, 'Уникальный номер заявки:', null);
        $result[] = SearchHelper::makeDetailBold($index++, $uuid, null);
        $result[] = SearchHelper::makeDetail($index++, "Тип формы", $offerCaFdaModel->type == 'ca-fda' ? 'ЦА ФДА' : 'Организация');
        $result[] = SearchHelper::makeDetail($index++, "Год плана НИОКР", $offerCaFdaModel->year ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Получено одобрение", $offerCaFdaModel->approved ? 'Да' : 'Нет');
        $result[] = SearchHelper::makeDetail($index++, "Наименование темы(исследований, работ)", $offerCaFdaModel->title ?: "-");
        $result[] = SearchHelper::makeDetailBold($index++, "Контактные данные руководителя организации:", null);
        $result[] = SearchHelper::makeDetail($index++, "Ф.И.О.", $offerCaFdaModel->lastName . " " . $offerCaFdaModel->firstName . " " . $offerCaFdaModel->middleName);
        $result[] = SearchHelper::makeDetail($index++, "Должность", $offerCaFdaModel->position ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Наименование структурного подразделения", $offerCaFdaModel->organization ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Телефонный номер", $offerCaFdaModel->phoneNumber ?: "-");
        $result[] = SearchHelper::makeDetailBold($index++, "Данные о вносимом предложении:", null);
        $result[] = SearchHelper::makeDetail($index++, "Функциональный заказчик(и)", $offerCaFdaModel->customer ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Основание для разработки", $offerCaFdaModel->basis ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Статус работы", $offerCaFdaModel->workStatus ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Источник финансирования", $offerCaFdaModel->sourceFinance ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Исходные данные",
            SearchHelper::downloadFile($offerCaFdaModel->initDataFile));
        $result[] = SearchHelper::makeDetail($index++, "Состояние проблемы (вопроса)",
            SearchHelper::downloadFile($offerCaFdaModel->conditionFile));
        $result[] = SearchHelper::makeDetail($index++, "Цель работы", $offerCaFdaModel->targetWork ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Отличия, новизна и преимущества", $offerCaFdaModel->infoWork ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Содержание работы",
            SearchHelper::downloadFile($offerCaFdaModel->contentWorkFile));
        $result[] = SearchHelper::makeDetail($index++, "Конечный результат работы", $offerCaFdaModel->finishWork ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Область применения", $offerCaFdaModel->scopeApplication ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Внедрение результатов работы", $offerCaFdaModel->integrationWork ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Права сторон", $offerCaFdaModel->rights ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Требования к представляемой продукции",
            SearchHelper::downloadFile($offerCaFdaModel->requirementFile));
        $result[] = SearchHelper::makeDetail($index++, "Условия приемки работ", $offerCaFdaModel->conditionWork ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Сроки выполнения работ", $offerCaFdaModel->deadlineWork ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Предмет стандартизации", $offerCaFdaModel->subject ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Перечень рецензентов",
            SearchHelper::downloadFile($offerCaFdaModel->listReviewerFile));
        $result[] = SearchHelper::makeDetail($index++, "Особые условия", $offerCaFdaModel->specialConditions ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Место выполнения работ", $offerCaFdaModel->placeWork ?: "-");
        $result[] = SearchHelper::makeDetail($index++, "Направление стратегии развития инновационной " .
            "деятельности в области дорожного хозяйства", $offerCaFdaModel->niokrDirection ?: "Не назначена");
        $result[] = SearchHelper::makeDetail($index++, "Календарный план работ",
            SearchHelper::downloadFile($offerCaFdaModel->planWorkFile));
        return response()->json($result);
    }
}
