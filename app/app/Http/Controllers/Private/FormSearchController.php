<?php

namespace App\Http\Controllers\Private;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Models\RegistryModel;
use App\Service\FormService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class FormSearchController extends Controller
{
    public function index(Request $data)
    {
        return FormService::search($data);
    }

    public function getExcel()
    {
        return Excel::download(new RegistryExport, 'РЕЕСТР РИД.xlsx');
    }
}
