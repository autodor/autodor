<?php

namespace App\Http\Controllers\Private;

use App\Exports\RegistryExport;
use App\Http\Controllers\Controller;
use App\Models\RegistryModel;
use App\Service\BalanceService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BalanceSearchController extends Controller
{
    public function index(Request $data)
    {
        $result = BalanceService::search($data->all());
        return response()->json($result);
    }

    public function getOne(int $id)
    {
        return response()->json(BalanceService::getOne($id));
    }

    public function getExcel()
    {
        return Excel::download(new RegistryExport, 'РЕЕСТР РИД.xlsx');
    }
}
