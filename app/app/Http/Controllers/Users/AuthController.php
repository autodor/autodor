<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\UserModel;
use Illuminate\Http\Request;
use Validator;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'refresh', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'login' => 'required|string',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (!$token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'wrong password or login'], 401);
        }
        return $this->createNewToken($token);
    }

    /**
     * Update password for User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'password' => 'required|string|confirmed|min:6'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = UserModel::find($request->id);
        if ($user === null) {
            return response()->json([
                'message' => 'User not found',
            ], 404);
        }
        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json([
            'message' => "For user $user->name password reset",
        ], 200);
    }

    /**
     * Update info the User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'user.id' => 'required|integer',
            'user.login' => 'required|string|between:2,100',
            'user.first_name' => 'required|string|between:2,100',
            'user.last_name' => 'required|string|between:2,100'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = UserModel::find($request->user['id']);
        if ($user === null) {
            return response()->json([
                'message' => 'User not found',
            ], 404);
        }
        $user->login = $request->user['login'];
        $user->role = $request->user['role'];
        $user->email = $request->user['email'];
        $user->first_name = $request->user['first_name'];
        $user->middle_name = $request->user['middle_name'];
        $user->last_name = $request->user['last_name'];
        $user->position = $request->user['position'];
        $user->organization = $request->user['organization'];
        $user->phone = $request->user['phone'];
        $user->fax = $request->user['fax'];
        $user->save();
        return response()->json([
            'message' => 'User successfully updated',
            'user' => $user
        ], 200);
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'user.login' => 'required|string|between:2,100',
            'user.first_name' => 'required|string|between:2,100',
            'user.last_name' => 'required|string|between:2,100',
            'password' => 'required|string|confirmed|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = UserModel::create(array_merge(
            $request->all()['user'],
            ['password' => bcrypt($request->password)]
        ));
        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }

    public function delete(string $id)
    {
        $user = UserModel::find($id);
        if ($user === null) {
            return response()->json([
                'message' => 'User not found',
            ], 404);
        }
        $user->delete();
        return response()->json([
            'message' => 'User successfully delete',
        ], 204);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile()
    {
        return response()->json(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token)
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    public function allUser()
    {
        $users = UserModel::all();
        return UserResource::collection($users);
    }
}
