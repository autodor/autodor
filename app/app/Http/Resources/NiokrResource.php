<?php

namespace App\Http\Resources;

use App\Models\NiokrModel;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class NiokrResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'year_finish' => $this->year_finish,
            'number' => $this->number,
            'conclusion_date' => $this->id,
            'developer' => $this->developer,
            'number_arh' => $this->number_arh,
            'bdis' => $this->bdis,
            'project_ntd' => $this->project_ntd,
            'patent' => $this->patent,
            'ntd' => $this->ntd
        ];
    }
}
