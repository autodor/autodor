<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RegistryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'key' => $this->key,
            'name' => $this->name,
            'contractNumber' => $this->contractNumber,
            'contractDate' => date("d.m.Y", strtotime($this->contractDate)),
            'patentName' => $this->patentName,
            'patentNumber' => $this->patentNumber,
            'patentDate' =>  date("d.m.Y", strtotime($this->patentDate)),
            'certificateNumber' => $this->certificateNumber,
            'certificateDate' => date("d.m.Y", strtotime($this->certificateDate))
        ];
    }
}
