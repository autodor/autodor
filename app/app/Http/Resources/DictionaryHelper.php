<?php

namespace App\Http\Resources;

class DictionaryHelper
{
    public static function getDictionary($collection): array
    {
        return array_map(function ($item) {
            return [
                "id" => $item['id'],
                "name" => $item['name']
            ];
        }, $collection);
    }
}
