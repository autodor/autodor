<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\DB;

class SearchHelper
{
    public static function isNotEmpty(?string $var): bool
    {
        return $var != null && $var != "";
    }

    public static function searchString(string $str): string
    {
        return '%' . $str . '%';
    }

    public static function allSearchFilter(array $array, string $search): array
    {
        return array_values(array_filter($array, function ($item) use ($search) {
            foreach ($item as $key => $value) {
                if (($key != "id") && (mb_strripos($value, $search) !== false)) return true;
            }
            return false;
        }));
    }

    public static function toSpoiler($str): string
    { // создает спойлер для модального окна из длинных сообщений

        return '#spoiler' . $str;
    }

    public static function specialToPercent($val): string
    { // заменяет пробелы и спец.символы на %
        $val_out = str_replace(" ", "%", $val);
        $val_out = str_replace("'", "%", $val_out);
        $val_out = str_replace('"', '%', $val_out);
        $val_out = str_replace("&", "%", $val_out);
        $val_out = str_replace("/", "%", $val_out);
        $val_out = str_replace("\\", "%", $val_out);
        $val_out = str_replace("-", "%", $val_out);
        return $val_out;
    }

    public static function makeTr($title, $value, $center = false)
    {
        return [
            'title' => $title,
            'value' => $value,
            'center' => $center
        ];
    }

    public static function formatDate(?string $str): string
    {
        if ($str == null) return "";
        $str = str_replace('.', '-', $str);
        return date("d.m.Y", strtotime($str));
    }

    public static function formatDateYear(?string $str): string
    {
        if ($str == null) return "";
        $str = str_replace('.', '-', $str);
        return date("Y", strtotime($str));
    }

    public static function getUuidFile($path, $filename)
    {
        $file = db::table('files')
            ->select('uuid')
            ->where('path', 'ilike', $path)
            ->where('filename', 'ilike', $filename)
            ->first();
        return $file?->uuid;
    }

    public static function moneyThousand(?string $str): string
    {
        if (!$str) $str = 0;
        return self::money(str_replace(',', '.', $str) * 1000);
    }


    public static function money(string $str): string
    {
        return number_format(str_replace(',', '.', $str), 2, ',', '\'') . '&nbsp;₽';
    }

    public static function number(?string $str): string
    {
        if (!$str) $str = 0;
        return number_format(str_replace(',', '.', $str), 2, ',', ' ');
    }

    public static function numberCalc(?string $str): float
    {
        if (!$str) $str = 0.0;
        $str = str_replace(' ', '', $str);
        return (float)str_replace(',', '.', $str);
    }

    public static function makeDownloadLink($uuid, $type, $filename = '')
    {
        return "<a title='{$filename}' href='/api/helper/download/?file={$uuid}' target='_blanks'><img src='/img/{$type}.png' width='50'></a>";
    }

    public static function makeQuart($str)
    {
        if (!$str) return '';
        return ceil(date("m", strtotime($str)) / 3) . ' кв. ' . date("Y", strtotime($str)) . 'г.';
    }

    public static function makeDetailCenter($index, $title, $info)
    {
        return SearchHelper::makeDetail($index, $title, $info, null, true);
    }

    public static function makeDetailBold($index, $title, $info)
    {
        return SearchHelper::makeDetail($index, $title, $info, true);
    }

    public static function makeDetailBoldAndCenter($index, $title, $info)
    {
        return SearchHelper::makeDetail($index, $title, $info, true, true);
    }

    public static function makeDetail($index, $title, $info, $bold = null, $center = null)
    {
        return [
            'title' => $title,
            'info' => $info,
            'bold' => $bold,
            'center' => $center,
            'index' => $index
        ];
    }

    public static function makeEmail(?string $contact_email): ?string
    {
        if ($contact_email !== null) {
            return "<a href='mailto:{$contact_email}'>{$contact_email}</a>";
        } else return '';
    }

    public static function makeUrl(?string $url): ?string
    {
        if ($url !== null) {
            if (str_contains('http://', $url) || str_contains('https://', $url)) {
                return "<a href='{$url}' target='_blank'>{$url}</a>";
            } else {
                return "<a href='http://{$url}' target='_blank'>{$url}</a>";
            }
        } else return '';
    }

    public static function makeAddressLink(?string $address): string
    {
        if ($address) {
            return "<a href='https://yandex.ru/maps/?mode=search&text={$address}' target='_blank'>{$address}</a>";
        }
        return '';
    }

    public static function makeWebLink(?string $link): string
    {
        if ($link) {
            return preg_replace('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?%=&_/-]+!',
                "<a href=\"\\0\" target='_blank'><img src='../img/www.png' width='50'></a>",
                $link);
        }
        return '';
    }

    public static function getFileInfo(?string $uuid): ?array
    {
        if ($uuid == null) return null;
        $file = DB::table('files')->select('filename')->where('uuid', $uuid)->first();
        return $file ? [
            'uuid' => $uuid,
            'file' => $file->filename
        ] : null;
    }

    public static function downloadFile($uuid): string
    {
        $file = SearchHelper::getFileInfo($uuid);
        return $file ? SearchHelper::makeDownloadLink($file['uuid'], 'pdf', $file['file']) : 'Документ не загружен';
    }
}
