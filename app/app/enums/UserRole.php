<?php

namespace App\enums;

use JsonSerializable;

enum UserRole implements JsonSerializable
{
    case User;
    case Admin;
    case Operator;
    case UNTIiIT;
    case Central;
    case FKU;

    public function jsonSerialize(): mixed
    {
        return match($this) {
            UserRole::User => "User",
            UserRole::Admin => "Admin",
            UserRole::Operator => "Operator",
            UserRole::UNTIiIT => "UNTIiIT",
            UserRole::Central => "Central",
            UserRole::FKU => "FKU",
        };
    }
}
