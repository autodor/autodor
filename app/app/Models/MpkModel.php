<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MpkModel extends Model
{
    use HasFactory;

    protected $keyType = 'string';

    protected $table = "mpk_chapter";

}
