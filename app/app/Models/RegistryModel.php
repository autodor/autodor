<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegistryModel extends Model
{
    use HasFactory;

    protected $table = "registries";

    protected $fillable = [
        'key',
        'name',
        'contractName',
        'contractNumber',
        'contractDate',
        'patentName',
        'patentNumber',
        'patentDate',
        'certificateNumber',
        'certificateDate',
        'description'
    ];
}
