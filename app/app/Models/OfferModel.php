<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OfferModel extends Model
{
    use HasFactory;

    protected $primaryKey = 'uuid';

    public $incrementing = false;

    protected $keyType = 'UUID';

    protected $fillable = [
        'uuid',
        'type',
        'user_id',
        'approved',
        'organization',
        'lastName',
        'firstName',
        'middleName',
        'position',
        'phoneNumber',
        'year',
        'title',
        'customer',
        'basis',
        'workStatus',
        'sourceFinance',
        'initDataFile',
        'conditionFile',
        'targetWork',
        'infoWork',
        'contentWorkFile',
        'finishWork',
        'scopeApplication',
        'integrationWork',
        'rights',
        'requirementFile',
        'conditionWork',
        'deadlineWork',
        'subject',
        'listReviewerFile',
        'specialConditions',
        'placeWork',
        'planWorkFile',
        'offersFile',
        'niokrDirection'
    ];

    protected $table = "offer";

    public function user(): BelongsTo
    {
        return $this->belongsTo(UserModel::class);
    }
}
