<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class NtdModel extends Model
{
    use HasFactory;

    protected $primaryKey = 'uuid';

    public $incrementing = false;

    protected $keyType = 'UUID';

    protected $fillable = [
        'uuid',
        'user_id',
        'typeNtd',
        'numberNtd',
        'titleNtd',
        'titleFku',
        'numberPurchase',
        'numberContract',
        'dateStart',
        'dateFinish',
        'developer',
        'title',
        'place',
        'type'
    ];

    protected $table = "ntd";

    public function user(): BelongsTo
    {
        return $this->belongsTo(UserModel::class);
    }
}
