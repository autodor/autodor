<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FunctionTopicModel extends Model
{
    use HasFactory;

    protected $table = "function_topic";

}
