<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EbNtdModel extends Model
{
    use HasFactory;

    protected $table = "eb_ntd";

    protected $detailArray = [
        'detail' => 'array'
    ];
}
