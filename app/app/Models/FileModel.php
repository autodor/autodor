<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileModel extends Model
{
    use HasFactory;
    protected $fillable = [
        'path',
        'uuid',
        'filename'
    ];
    protected $table = "files";
}
