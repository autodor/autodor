<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NiokrModel extends Model
{
    use HasFactory;

    protected $table = "niokrs";

    public function organization()
    {
        return $this->belongsTo(OrganizationModel::class, "developer");
    }
}
