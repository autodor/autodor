<?php

use App\Http\Controllers\Private\ActSearchController;
use App\Http\Controllers\Private\AssertionSearchController;
use App\Http\Controllers\Private\BalanceSearchController;
use App\Http\Controllers\Private\ClassificationSearchController;
use App\Http\Controllers\Private\ContractorSearchController;
use App\Http\Controllers\Private\EventController;
use App\Http\Controllers\Private\FormSearchController;
use App\Http\Controllers\Private\MonitoringSearchController;
use App\Http\Controllers\Private\NtdController;
use App\Http\Controllers\Private\OfferController;
use App\Http\Controllers\Private\ProjectSearchController;
use App\Http\Controllers\Private\RealizationSearchController;
use App\Http\Controllers\Private\RegistryEditController;
use App\Http\Controllers\Private\SpecialSearchController;
use App\Http\Controllers\Private\TenderSearchController;
use App\Http\Controllers\Private\TermDocsSearchController;
use App\Http\Controllers\Private\TermSearchController;
use App\Http\Controllers\Public\AtsDhSearchController;
use App\Http\Controllers\Public\DictionaryController;
use App\Http\Controllers\Public\EbNtdSearchController;
use App\Http\Controllers\Public\EbPatentSearchController;
use App\Http\Controllers\Public\GlobalSearchController;
use App\Http\Controllers\Public\HelperController;
use App\Http\Controllers\Public\MigrationController;
use App\Http\Controllers\Public\NioktrSearchController;
use App\Http\Controllers\Public\RegistryController;
use App\Http\Controllers\Users\AuthController;
use App\Http\Resources\DictionaryHelper;
use App\Models\DistrictModel;
use App\Models\FunctionTopicModel;
use App\Models\MpkModel;
use App\Models\OrgGroupModel;
use App\Models\PositionModel;
use App\Models\QualifierModel;
use App\Models\RegulationTopicModel;
use App\Models\TypeDocModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/me', [AuthController::class, 'userProfile']);
});

Route::group(
    [
        'middleware' => 'admin',
        'prefix' => 'admin'
    ], function () {
    Route::post('/update', [AuthController::class, 'update']);
    Route::post('/reset-password', [AuthController::class, 'resetPassword']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::delete('/delete/{id}', [AuthController::class, 'delete']);
    Route::get('/users', [AuthController::class, 'allUser']);
});

Route::group(
    [
        'middleware' => 'admin',
        'prefix' => 'edit'
    ], function () {
    Route::post('/registries', [RegistryEditController::class, 'update']);
    Route::put('/registries', [RegistryEditController::class, 'insert']);
    Route::delete('/registries/{id}', [RegistryEditController::class, 'delete']);
});

Route::group(
    [
        'middleware' => 'admin',
        'prefix' => 'migration'
    ], function () {
    Route::post('read', [MigrationController::class, 'index'])->middleware('throttle:6000,1');
    Route::post('insert', [MigrationController::class, 'insert'])->middleware('throttle:6000,1');
    Route::post('truncate', [MigrationController::class, 'truncate'])->middleware('throttle:6000,1');
    Route::post('proxy', [MigrationController::class, 'proxy'])->middleware('throttle:6000,1');
    Route::get('count/{table}', [MigrationController::class, 'count'])->middleware('throttle:6000,1');
});

Route::prefix('helper')->group(function () {
    Route::get('get-uuid', [HelperController::class, 'generateUuid']);
    Route::get('download', [HelperController::class, 'download']);
    Route::post('upload', [HelperController::class, 'upload']);
});

Route::prefix('public-search')->group(function () {
    Route::post('global', [GlobalSearchController::class, 'index']);
    Route::post('global/detail', [GlobalSearchController::class, 'getOne']);
    Route::post('niokr', [NioktrSearchController::class, 'index']);
    Route::get('niokr/{id}', [NioktrSearchController::class, 'getOne']);
    Route::post('niokr/excel', [NioktrSearchController::class, 'getExcel']);
    Route::post('eb-ntd', [EbNtdSearchController::class, 'index']);
    Route::get('eb-ntd/{id}', [EbNtdSearchController::class, 'getOne']);
    Route::post('eb-ntd/excel', [EbNtdSearchController::class, 'getExcel']);
    Route::post('eb-patents', [EbPatentSearchController::class, 'index']);
    Route::get('eb-patents/{id}', [EbPatentSearchController::class, 'getOne']);
    Route::post('ats-dh', [AtsDhSearchController::class, 'index']);
    Route::get('ats-dh/{id}', [AtsDhSearchController::class, 'getOne']);
});


Route::group(
    [
        'middleware' => 'private.search',
        'prefix' => 'private-search'
    ], function () {
    Route::post('special', [SpecialSearchController::class, 'index']);
    Route::get('special/{id}', [SpecialSearchController::class, 'getOne']);
    Route::post('tender', [TenderSearchController::class, 'index']);
    Route::get('tender/{id}', [TenderSearchController::class, 'getOne']);
    Route::get('events', [EventController::class, 'index']);
    Route::get('events/{id}', [EventController::class, 'getOne']);
    Route::post('term', [TermSearchController::class, 'index']);
    Route::get('term/{id}', [TermSearchController::class, 'getOne']);
    Route::post('term-docs', [TermDocsSearchController::class, 'index']);
    Route::get('term-docs/{id}', [TermDocsSearchController::class, 'getOne']);
});

Route::group(
    [
        'middleware' => 'private.search',
        'prefix' => 'offer'
    ], function () {
    Route::post('/search', [OfferController::class, 'search']);
    Route::get('/{uuid}/detail', [OfferController::class, 'detail']);
    Route::post('/{type}/save', [OfferController::class, 'save']);
    Route::get('/{type}/list', [OfferController::class, 'listByUser']);
    Route::get('/{type}/load/{uuid}', [OfferController::class, 'load']);
});

Route::group(
    [
        'middleware' => 'private.search',
        'prefix' => 'ntd'
    ], function () {
    Route::post('/search', [NtdController::class, 'search']);
    Route::get('/detail/{uuid}/', [NtdController::class, 'detail']);
    Route::post('/save', [NtdController::class, 'save']);
    Route::get('/list', [NtdController::class, 'listByUser']);
    Route::get('/load/{uuid}/', [NtdController::class, 'load']);
});

Route::group(
    [
        'middleware' => 'private.search',
        'prefix' => 'private-search/information'
    ], function () {
    Route::post('monitoring', [MonitoringSearchController::class, 'index']);
    Route::post('realization', [RealizationSearchController::class, 'index']);
    Route::post('contractor', [ContractorSearchController::class, 'index']);
    Route::post('form', [FormSearchController::class, 'index']);
    Route::post('classification', [ClassificationSearchController::class, 'index']);
    Route::post('act', [ActSearchController::class, 'index']);
    Route::post('project', [ProjectSearchController::class, 'index']);
    Route::get('project/{id}', [ProjectSearchController::class, 'getOne']);
    Route::get('assertion/{id}', [AssertionSearchController::class, 'getOne']);
    Route::post('assertion', [AssertionSearchController::class, 'index']);
    Route::post('balance', [BalanceSearchController::class, 'index']);
    Route::get('balance/{id}', [BalanceSearchController::class, 'getOne']);
});

Route::group(
    [
        'middleware' => 'private.search',
        'prefix' => 'event'
    ], function () {
    Route::get('count', [EventController::class, 'count']);
});

Route::prefix('public')->group(function () {
    Route::get('/registries', [RegistryController::class, 'index']);
    Route::get('/registries/excel', [RegistryController::class, 'getExcel']);
    Route::get('/registries/{id}', [RegistryController::class, 'getOne']);
});


Route::post("dictionary", [DictionaryController::class, 'getDictionaries']);

Route::prefix('dictionary')->group(function () {
    Route::get('/qualifiers', function () {
        return DictionaryHelper::getDictionary(QualifierModel::orderBy('id')->get()->toArray());
    });
    Route::get('/functions', function () {
        return DictionaryHelper::getDictionary(FunctionTopicModel::orderBy('id')->get()->toArray());
    });
    Route::get('/regulations', function () {
        return DictionaryHelper::getDictionary(RegulationTopicModel::orderBy('id')->get()->toArray());
    });
    Route::get('/type-doc', function () {
        return DictionaryHelper::getDictionary(TypeDocModel::orderBy('id')->get()->toArray());
    });
    Route::get('/mpk-chapter', function () {
        return DictionaryHelper::getDictionary(MpkModel::whereNull('parent_id')->orderBy('id')->get()->toArray());
    });
    Route::get('/mpk-subsection', function () {
        return DictionaryHelper::getDictionary(MpkModel::whereNotNull('parent_id')->orderBy('id')->get()->toArray());
    });
    Route::get('/district', function () {
        return DictionaryHelper::getDictionary(DistrictModel::orderBy('id')->get()->toArray());
    });
    Route::get('/organization-groups', function () {
        return DictionaryHelper::getDictionary(OrgGroupModel::orderBy('id')->get()->toArray());
    });
    Route::get('/positions', function () {
        return DictionaryHelper::getDictionary(PositionModel::orderBy('name')->get()->toArray());
    });
    Route::get('/topics', function () {
        return array_map(function ($item) {
            return [
                "id" => $item->tnum,
                "code" => $item->tplanum,
                "name" => $item->tname,
                "type" => $item->ttip,
            ];
        }, DB::table('bibl_tematika')->get()->toArray());
    });
});
