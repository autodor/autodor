<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('eb_patent', function (Blueprint $table) {
            $table->id();
            $table->boolean("type")->default(false);
            $table->string("number", 20)->nullable();
            $table->string("referal", 100);
            $table->string("title", 1000)->nullable();
            $table->date("date_published")->nullable();
            $table->string("place_published", 255)->nullable();
            $table->string("mpk_id",20);
            $table->foreign('mpk_id')->references('id')->on('mpk_chapter');
            $table->string('gov_contract',255)->nullable();
            $table->string("description", 500)->nullable();
            $table->string("request_number", 50)->nullable();
            $table->date("request_date")->nullable();
            $table->string("author", 1000)->nullable();
            $table->string("owner", 1000)->nullable();
            $table->string("docs", 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('eb_patent');
    }
};
