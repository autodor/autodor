<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('eb_ntd', function (Blueprint $table) {
            $table->id();
            $table->string("referal",100);
            $table->unsignedBigInteger('type_doc_id');
            $table->foreign('type_doc_id')->references('id')->on('type_doc');
            $table->string("number",40);
            $table->string("title",300);
            $table->smallInteger("year_published");
            $table->unsignedBigInteger('developer')->nullable();
            $table->foreign('developer')->references('id')->on('organizations');
            $table->boolean("is_ftris");
            $table->string("contract_number",30)->nullable();
            $table->date("contract_date")->nullable();
            $table->boolean("is_active")->default(false)->nullable();
            $table->boolean("is_history")->default(false)->nullable();
            $table->date("date_entry")->nullable();
            $table->date("date_approval")->nullable();
            $table->json('detail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('eb_ntd');
    }
};
