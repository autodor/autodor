<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ofap_anketaofap', function (Blueprint $table) {
            $table->unsignedInteger('aonum')->autoIncrement();
            $table->string('aocontrnum', 20)->nullable();
            $table->date('aodatazakcontr')->nullable();
            $table->string('aonamecontr', 600)->nullable();
            $table->string('aodesignnamep', 400)->nullable();
            $table->string('aodesignnamek', 400)->nullable();
            $table->string('aodesignadr', 400)->nullable();
            $table->string('aodesigntel', 400)->nullable();
            $table->string('aodesignelpost', 400)->nullable();
            $table->string('aonamesistem', 400)->nullable();
            $table->string('aogosregnum', 400)->nullable();
            $table->string('aonaznach', 6000)->nullable();
            $table->string('aosodergan', 6000)->nullable();
            $table->string('aoviddok', 400)->nullable();
            $table->string('aovsostav', 400)->nullable();
            $table->string('aosizestrok', 100)->nullable();
            $table->string('aosizemb', 100)->nullable();
            $table->string('aocountrekv', 100)->nullable();
            $table->string('aogod', 4)->nullable();
            $table->string('aoretrosp', 800)->nullable();
            $table->string('aoperiodobn', 100)->nullable();
            $table->string('aovedenbd', 400)->nullable();
            $table->string('aocomp', 400)->nullable();
            $table->string('aoos', 100)->nullable();
            $table->string('aosubd', 100)->nullable();
            $table->string('aoprogsreda', 400)->nullable();
            $table->string('aoudaldostup', 400)->nullable();
            $table->string('aopostavka', 400)->nullable();
            $table->string('aocountusers', 400)->nullable();
            $table->string('aoogranich', 400)->nullable();
            $table->string('aoadrispolz', 2000)->nullable();
            $table->string('aoadministr', 400)->nullable();
            $table->date('aodatavvoda')->nullable();
            $table->string('aowhosewrite', 100)->nullable();
            $table->string('aoobrabotano', 10)->nullable();
        });
        Schema::create('ofap_contrchilde', function (Blueprint $table) {
            $table->unsignedInteger('ccnum')->autoIncrement();
            $table->integer('onum')->nullable();
            $table->string('ccplanum', 20)->nullable();
            $table->date('ccdatazakluch')->nullable();
            $table->string('ccprim', 100)->nullable();
        });
        Schema::create('ofap_designer', function (Blueprint $table) {
            $table->unsignedInteger('dnum')->autoIncrement();
            $table->integer('onum')->nullable();
            $table->string('dname', 400)->nullable();
            $table->string('dadr', 300)->nullable();
        });
        Schema::create('ofap_mesto', function (Blueprint $table) {
            $table->unsignedInteger('mnum')->autoIncrement();
            $table->integer('onum')->nullable();
            $table->string('mname', 400)->nullable();
            $table->string('madr', 300)->nullable();
        });
        Schema::create('ofap_object', function (Blueprint $table) {
            $table->unsignedInteger('onum')->autoIncrement();
            $table->integer('prnum')->nullable();
            $table->string('oname', 400)->nullable();
            $table->string('oregistr', 200)->nullable();
            $table->smallInteger('ogod')->nullable();
            $table->text('onaznach')->nullable();
            $table->string('oobnovlen', 200)->nullable();
            $table->string('otechparam', 400)->nullable();
            $table->integer('oplanum')->nullable();
            $table->string('oarxivplanum', 30)->nullable();
            $table->integer('export_id')->nullable();
            $table->integer('export_state')->nullable();
            $table->string('octplanum', 20)->nullable();
            $table->string('ozname', 600)->nullable();
            $table->date('octdatazakluch')->nullable();
            $table->string('ofield1', 20)->nullable();
        });
        Schema::create('ofap_podrazdel', function (Blueprint $table) {
            $table->unsignedInteger('prnum')->autoIncrement();
            $table->integer('rnum')->nullable();
            $table->string('prname', 300)->nullable();
            $table->integer('prplanum')->nullable();
        });
        Schema::create('ofap_pravoobl', function (Blueprint $table) {
            $table->unsignedInteger('pnum')->autoIncrement();
            $table->integer('onum')->nullable();
            $table->string('pname', 400)->nullable();
            $table->string('padr', 300)->nullable();
        });
        Schema::create('ofap_razdel', function (Blueprint $table) {
            $table->unsignedInteger('rnum')->autoIncrement();
            $table->string('rname', 100)->nullable();
            $table->integer('rplanum')->nullable();
        });
        Schema::create('ofap_reginfsist', function (Blueprint $table) {
            $table->unsignedInteger('onum')->autoIncrement();
            $table->text('ristext')->nullable();
            $table->date('risdatareg')->nullable();
            $table->string('rispaspnum', 100)->nullable();
        });
        Schema::create('ofap_registrzaiav', function (Blueprint $table) {
            $table->unsignedInteger('onum')->autoIncrement();
            $table->string('rznazvpoln', 400)->nullable();
            $table->string('rznazvsokr', 400)->nullable();
            $table->string('rzvedomstvo', 400)->nullable();
            $table->string('rzopf', 400)->nullable();
            $table->string('rzadrindex', 400)->nullable();
            $table->string('rzadr', 400)->nullable();
            $table->string('rztel0', 400)->nullable();
            $table->string('rztel1', 400)->nullable();
            $table->string('rztel2', 400)->nullable();
            $table->string('rzfax0', 400)->nullable();
            $table->string('rzfax1', 400)->nullable();
            $table->string('rzmail', 400)->nullable();
            $table->string('rzdopsved', 400)->nullable();
            $table->string('rzadmin', 400)->nullable();
            $table->string('rzadmintel', 400)->nullable();
            $table->string('rzpredstf', 400)->nullable();
            $table->string('rzpredsti', 400)->nullable();
            $table->string('rzpredsto', 400)->nullable();
            $table->string('rzpredstdol', 400)->nullable();
            $table->string('rznaimenovbd', 400)->nullable();
            $table->string('rznaznachen', 1000)->nullable();
            $table->string('rzsoder', 1000)->nullable();
            $table->string('rzistoch', 1000)->nullable();
            $table->string('rzsostav', 400)->nullable();
            $table->string('rzobembdzap', 400)->nullable();
            $table->string('rzobembdmb', 400)->nullable();
            $table->string('rzrekvizit', 400)->nullable();
            $table->string('rzlangv', 400)->nullable();
            $table->string('rzgod', 400)->nullable();
            $table->string('rzretrosp', 400)->nullable();
            $table->string('rzobnovlen', 3)->nullable();
            $table->string('rztobnovlen', 400)->nullable();
            $table->string('rzvedenie', 3)->nullable();
            $table->string('rzcomp', 400)->nullable();
            $table->string('rzoc', 400)->nullable();
            $table->string('rzsubd', 400)->nullable();
            $table->string('rzudalendostup', 3)->nullable();
            $table->string('rznameseti', 400)->nullable();
            $table->string('rzadrseti', 400)->nullable();
            $table->string('rzrazovoe', 3)->nullable();
            $table->string('rzfragment', 3)->nullable();
            $table->string('rzpostavka', 3)->nullable();
            $table->string('rzdrugoe', 400)->nullable();
            $table->string('rzformat', 400)->nullable();
            $table->string('rznositel', 400)->nullable();
            $table->string('rzkolpolzov', 400)->nullable();
            $table->string('rzkolzapros', 400)->nullable();
            $table->string('rzogranichen', 3)->nullable();
            $table->string('rzkommerch', 3)->nullable();
            $table->string('rzsledstv', 3)->nullable();
            $table->string('rzslugebn', 3)->nullable();
            $table->string('rzprof', 3)->nullable();
            $table->string('rzpatent', 3)->nullable();
            $table->string('rzpersdan', 3)->nullable();
            $table->string('rzdopsveden', 1000)->nullable();
            $table->string('rzfedbud', 3)->nullable();
            $table->string('rzgosfond', 3)->nullable();
            $table->string('rzbudgsubekt', 3)->nullable();
            $table->string('rzsobstvsr', 3)->nullable();
            $table->string('rzmunbudg', 3)->nullable();
            $table->string('rzinieurl', 3)->nullable();
            $table->string('rznumregistr', 200)->nullable();
            $table->date('rzdateregistr')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ofap_anketaofap');
        Schema::dropIfExists('ofap_contrchilde');
        Schema::dropIfExists('ofap_designer');
        Schema::dropIfExists('ofap_mesto');
        Schema::dropIfExists('ofap_object');
        Schema::dropIfExists('ofap_podrazdel');
        Schema::dropIfExists('ofap_pravoobl');
        Schema::dropIfExists('ofap_razdel');
        Schema::dropIfExists('ofap_reginfsist');
        Schema::dropIfExists('ofap_registrzaiav');
    }
};
