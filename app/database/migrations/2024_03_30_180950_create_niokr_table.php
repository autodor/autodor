<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('niokrs', function (Blueprint $table) {
            $table->id();
            $table->string("referal",100);
            $table->string("title", 600);
            $table->integer("year_finish");
            $table->string("number",20);
            $table->date("conclusion_date")->nullable();
            $table->unsignedBigInteger('developer')->nullable();
            $table->foreign('developer')->references('id')->on('organizations');
            $table->string("number_arh",30);
            $table->boolean("bdis")->default(false);
            $table->boolean("project_ntd")->default(false);
            $table->boolean("patent")->default(false);
            $table->boolean("ntd")->default(false);
            $table->string("number_lot", 10)->nullable();
            $table->text("description")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('niokr');
    }
};
