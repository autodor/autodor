<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('arxiv_tema', function (Blueprint $table) {
            $table->index(['ctplanumik','ctdatazakik'],'index_date_and_num');
            $table->index('tplanum','index_arxiv_tema_tplanum');
        });
        Schema::table('ik_niokr_zaiavka', function (Blueprint $table) {
            $table->index('zgod','index_ik_niokr_zgod');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('arxiv_tema', function (Blueprint $table) {
            $table->dropIndex('index_date_and_num');
            $table->dropIndex('index_arxiv_tema_tplanum');
        });
        Schema::table('ik_niokr_zaiavka', function (Blueprint $table) {
            $table->dropIndex('index_ik_niokr_zgod');
        });
    }
};
