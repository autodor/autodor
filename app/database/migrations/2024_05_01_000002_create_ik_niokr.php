<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ik_niokr_actdoc', function (Blueprint $table) {
            $table->unsignedInteger('acnum')->autoIncrement();
            $table->integer('ctnum')->nullable();
            $table->string('acname', 300)->nullable();
            $table->string('actype', 1)->nullable();
            $table->string('acras', 5)->nullable();
            $table->text('acdoc')->nullable();
        });
        Schema::create('ik_niokr_avans', function (Blueprint $table) {
            $table->unsignedInteger('avnum')->autoIncrement();
            $table->integer('ctnum')->nullable();
            $table->integer('avgod')->nullable();
            $table->string('avsum', 20)->nullable();
        });
        Schema::create('ik_niokr_certificate', function (Blueprint $table) {
            $table->unsignedInteger('crnum')->autoIncrement();
            $table->integer('cenum')->nullable();
            $table->date('crdateexecut')->nullable();
            $table->string('crexecutor', 160)->nullable();
            $table->date('crdatecustom')->nullable();
            $table->string('crcustomer', 160)->nullable();
            $table->string('crsumma', 30)->nullable();
            $table->text('crfileact')->nullable();
            $table->string('crfileactras', 5)->nullable();
        });
        Schema::create('ik_niokr_claim', function (Blueprint $table) {
            $table->unsignedInteger('clnum')->autoIncrement();
            $table->integer('cenum')->nullable();
            $table->date('cldata')->nullable();
            $table->date('cldatepayment')->nullable();
            $table->string('clorderpayment', 12)->nullable();
            $table->text('clfileclaim')->nullable();
            $table->string('clfileras', 5)->nullable();
            $table->text('clfilepayment')->nullable();
            $table->string('clfilepayras', 5)->nullable();
            $table->string('clnumber', 12)->nullable();
            $table->string('clsumma', 10)->nullable();
        });
        Schema::create('ik_niokr_condob', function (Blueprint $table) {
            $table->unsignedInteger('ctdobnum')->autoIncrement();
            $table->integer('ctnum')->nullable();
            $table->string('ctdobplanum', 20)->nullable();
            $table->date('ctdobdata')->nullable();
        });
        Schema::create('ik_niokr_conetapi', function (Blueprint $table) {
            $table->unsignedInteger('cenum')->autoIncrement();
            $table->integer('ctnum')->nullable();
            $table->string('ceplanum', 20)->nullable();
            $table->boolean('ceend')->nullable();
            $table->date('cedatastart')->nullable();
            $table->date('cedataend')->nullable();
            $table->string('cesumwinner', 20)->nullable();
            $table->string('ceotchetnum', 20)->nullable();
            $table->boolean('cereferat')->nullable();
            $table->boolean('ceelectronotchet')->nullable();
            $table->integer('ceslugebn')->nullable();
            $table->string('cekalendplansum', 20)->nullable();
            $table->string('cevozvratavanssum', 20)->nullable();
            $table->string('cename', 3000)->nullable();
            $table->string('cesrivsroka', 70)->nullable();
            $table->integer('wwhosenum')->nullable();
            $table->date('cewhosedate')->nullable();
            $table->integer('psenum')->nullable();
            $table->date('celastscedata')->nullable();
            $table->string('cereginvnum', 30)->nullable();
            $table->string('ceregnumik', 30)->nullable();
            $table->date('ceregdata')->nullable();
            $table->text('ceregcart')->nullable();
            $table->string('ceprimechan', 1000)->nullable();
            $table->string('ceregcartras', 4)->nullable();
        });
        Schema::create('ik_niokr_contract', function (Blueprint $table) {
            $table->unsignedInteger('ctnum')->autoIncrement();
            $table->integer('znum')->nullable();
            $table->string('ctplanum', 20)->nullable();
            $table->date('ctdatazakluch')->nullable();
            $table->date('ctdatazaversh')->nullable();
            $table->boolean('ctrlzntd')->nullable();
            $table->boolean('ctrlzsvid')->nullable();
            $table->boolean('ctrlzbd')->nullable();
            $table->boolean('ctrlzpatent')->nullable();
            $table->integer('ctslugebn')->nullable();
            $table->string('ctsostoianie', 15)->nullable();
            $table->string('ctpodsostoianie', 300)->nullable();
            $table->string('ctregnumcart', 30)->nullable();
            $table->string('ctregregnum', 30)->nullable();
            $table->date('ctregdata')->nullable();
            $table->text('ctregcart')->nullable();
            $table->string('ctrlztext', 1400)->nullable();
            $table->integer('ctxnum')->nullable();
            $table->string('ctxname', 500)->nullable();
            $table->string('ctrezultut', 2000)->nullable();
            $table->integer('pnnum')->nullable();
            $table->string('pnname', 500)->nullable();
            $table->integer('export_id')->nullable();
            $table->integer('export_state')->nullable();
            $table->boolean('ctrlzproectntd')->nullable();
            $table->string('ctpractispol', 2000)->nullable();
            $table->string('ctregcartras', 4)->nullable();
            $table->boolean('ctrlzsp')->nullable();
            $table->boolean('ctrlzrid')->nullable();
            $table->boolean('ctindevelopt')->nullable();
            $table->string('ctrnfi', 15)->nullable();
            $table->string('ctsistnumri', 15)->nullable();
            $table->string('ctnumkazn', 25)->nullable();
            $table->string('ctnumzaprri', 15)->nullable();
            $table->string('ctnumcartri', 25)->nullable();
            $table->text('cthistf')->nullable();
            $table->string('cthistfras', 5)->nullable();
            $table->string('ctinvcart', 15)->nullable();
            $table->string('ctfdainvnum', 25)->nullable();
            $table->date('ctfdainvdate')->nullable();
            $table->date('ctrnfidate')->nullable();
        });
        Schema::create('ik_niokr_crittex', function (Blueprint $table) {
            $table->unsignedInteger('ctxnum')->autoIncrement();
            $table->string('ctxname', 500)->nullable();
        });
        Schema::create('ik_niokr_dokumentats', function (Blueprint $table) {
            $table->unsignedInteger('znum')->autoIncrement();
            $table->text('ddokument')->nullable();
            $table->string('dras', 5)->nullable();
        });
        Schema::create('ik_niokr_econobosn', function (Blueprint $table) {
            $table->unsignedInteger('znum')->autoIncrement();
            $table->text('eeconomobos')->nullable();
            $table->string('eras', 3)->nullable();
        });
        Schema::create('ik_niokr_flskonkdok', function (Blueprint $table) {
            $table->unsignedInteger('fkdnum')->autoIncrement();
            $table->integer('flskonknum')->nullable();
            $table->text('fkddokument')->nullable();
            $table->string('fkdras', 5)->nullable();
            $table->string('fkdname', 30)->nullable();
        });
        Schema::create('ik_niokr_flskonkurs_', function (Blueprint $table) {
            $table->unsignedInteger('flskonknum')->autoIncrement();
            $table->string('znumzakupki', 25)->nullable();
            $table->date('zdatarazmeshen')->nullable();
            $table->string('fldatakonk', 10)->nullable();
            $table->boolean('flonepostav')->nullable();
            $table->boolean('floneuchast')->nullable();
        });
        Schema::create('ik_niokr_grouporg', function (Blueprint $table) {
            $table->unsignedInteger('gonum')->autoIncrement();
            $table->string('goname', 300)->nullable();
        });
        Schema::create('ik_niokr_histntd', function (Blueprint $table) {
            $table->unsignedInteger('hsnum')->autoIncrement();
            $table->integer('ctnum')->nullable();
            $table->integer('ntdi_num')->nullable();
        });
        Schema::create('ik_niokr_ispolnitel', function (Blueprint $table) {
            $table->unsignedInteger('inum')->autoIncrement();
            $table->integer('znum')->nullable();
            $table->integer('onum')->nullable();
            $table->date('idatepismo')->nullable();
            $table->string('ipismo', 50)->nullable();
        });
        Schema::create('ik_niokr_listotvet', function (Blueprint $table) {
            $table->unsignedInteger('lonum')->autoIncrement();
            $table->integer('lznum')->nullable();
            $table->date('lodata')->nullable();
            $table->integer('onum')->nullable();
            $table->string('lotext', 600)->nullable();
        });
        Schema::create('ik_niokr_listzamech', function (Blueprint $table) {
            $table->unsignedInteger('lznum')->autoIncrement();
            $table->integer('znum')->nullable();
            $table->date('lzdata')->nullable();
            $table->string('lzrechenzent', 150)->nullable();
            $table->string('lzprimechan', 600)->nullable();
            $table->string('lztext', 1500)->nullable();
            $table->integer('lzslugebn')->nullable();
        });
        Schema::create('ik_niokr_men', function (Blueprint $table) {
            $table->unsignedInteger('mnum')->autoIncrement();
            $table->integer('onum')->nullable();
            $table->string('mname', 150)->nullable();
            $table->string('mdol', 150)->nullable();
            $table->string('mzvan', 200)->nullable();
            $table->string('memail', 100)->nullable();
            $table->string('mdopinform', 700)->nullable();
            $table->string('mkomnata', 15)->nullable();
            $table->string('motdel', 300)->nullable();
        });
        Schema::create('ik_niokr_mentel', function (Blueprint $table) {
            $table->unsignedInteger('mtnum')->autoIncrement();
            $table->integer('mnum')->nullable();
            $table->string('mtnumber', 100)->nullable();
            $table->string('mtwhat', 3)->nullable();
        });
        Schema::create('ik_niokr_ntddocum', function (Blueprint $table) {
            $table->unsignedInteger('ndnum')->autoIncrement();
            $table->integer('nenum')->nullable();
            $table->text('nddokument')->nullable();
            $table->string('ndname', 300)->nullable();
            $table->string('ndras', 5)->nullable();
            $table->string('ndclarif', 30)->nullable();
        });
        Schema::create('ik_niokr_ntdevents', function (Blueprint $table) {
            $table->unsignedInteger('nenum')->autoIncrement();
            $table->integer('npnum')->nullable();
            $table->date('nedata')->nullable();
            $table->string('netype', 20)->nullable();
            $table->string('nedescript', 1000)->nullable();
            $table->string('neletter', 50)->nullable();
            $table->date('neenddiscus')->nullable();
        });
        Schema::create('ik_niokr_ntdproject', function (Blueprint $table) {
            $table->unsignedInteger('npnum')->autoIncrement();
            $table->integer('ctnum')->nullable();
            $table->string('npindex', 10)->nullable();
            $table->string('nptitle', 300)->nullable();
            $table->string('npnotice', 1000)->nullable();
            $table->string('nprealiz', 10)->nullable();
            $table->integer('npdiscuss')->nullable();
            $table->integer('npapproval')->nullable();
        });
        Schema::create('ik_niokr_organiz', function (Blueprint $table) {
            $table->unsignedInteger('onum')->autoIncrement();
            $table->string('onamekrat', 150)->nullable();
            $table->string('oadr', 200)->nullable();
            $table->string('oname', 250)->nullable();
            $table->integer('gonum')->nullable();
            $table->string('oemail', 100)->nullable();
            $table->string('owebadr', 100)->nullable();
            $table->string('odopinform', 1400)->nullable();
            $table->string('oblast', 100)->nullable();
            $table->boolean('oadrtel')->nullable();
            $table->integer('export_id')->nullable();
            $table->string('oinn_kpp', 25)->nullable();
            $table->string('ookpo', 17)->nullable();
            $table->boolean('oedit')->nullable();
            $table->string('oadrurid', 200)->nullable();
        });
        Schema::create('ik_niokr_orgtel', function (Blueprint $table) {
            $table->unsignedInteger('otnum')->autoIncrement();
            $table->integer('onum')->nullable();
            $table->string('otnumber', 100)->nullable();
            $table->string('otwhat', 3)->nullable();
        });
        Schema::create('ik_niokr_orgvnedren', function (Blueprint $table) {
            $table->unsignedInteger('ovnum')->autoIncrement();
            $table->integer('wvnum')->nullable();
            $table->integer('onum')->nullable();
        });
        Schema::create('ik_niokr_perechensostetapi', function (Blueprint $table) {
            $table->unsignedInteger('psenum')->autoIncrement();
            $table->string('psename', 300)->nullable();
        });
        Schema::create('ik_niokr_podrazdel', function (Blueprint $table) {
            $table->unsignedInteger('prnum')->autoIncrement();
            $table->integer('rnum')->nullable();
            $table->string('prname', 300)->nullable();
            $table->integer('prplanum')->nullable();
        });
        Schema::create('ik_niokr_priornaprav', function (Blueprint $table) {
            $table->unsignedInteger('pnnum')->autoIncrement();
            $table->string('pnname', 500)->nullable();
        });
        Schema::create('ik_niokr_prnkonkurs', function (Blueprint $table) {
            $table->unsignedInteger('pknum')->autoIncrement();
            $table->string('sname', 300)->nullable();
            $table->date('pkdatekonkurs')->nullable();
            $table->string('pknumlot', 50)->nullable();
            $table->string('zname', 600)->nullable();
            $table->string('oname', 250)->nullable();
            $table->string('wtzvprice', 50)->nullable();
            $table->string('wtpobeda', 5)->nullable();
            $table->integer('znum')->nullable();
            $table->string('znumzakupki', 25)->nullable();
            $table->date('zdatarazmeshen')->nullable();
        });
        Schema::create('ik_niokr_protokol', function (Blueprint $table) {
            $table->unsignedInteger('znum')->autoIncrement();
            $table->text('pprotokol')->nullable();
            $table->string('pras', 5)->nullable();
        });
        Schema::create('ik_niokr_razdel', function (Blueprint $table) {
            $table->unsignedInteger('rnum')->autoIncrement();
            $table->string('rname', 300)->nullable();
            $table->integer('rplanum')->nullable();
        });
        Schema::create('ik_niokr_sostconetapi', function (Blueprint $table) {
            $table->unsignedInteger('scenum')->autoIncrement();
            $table->integer('cenum')->nullable();
            $table->date('scedata')->nullable();
            $table->string('scepismo', 200)->nullable();
            $table->integer('psenum')->nullable();
            $table->integer('wwhosenum')->nullable();
            $table->date('scewhosedate')->nullable();
        });
        Schema::create('ik_niokr_statia', function (Blueprint $table) {
            $table->unsignedInteger('snum')->autoIncrement();
            $table->string('sname', 300)->nullable();
            $table->string('splanum', 10)->nullable();
        });
        Schema::create('ik_niokr_stoimetap', function (Blueprint $table) {
            $table->unsignedInteger('stenum')->autoIncrement();
            $table->integer('cenum')->nullable();
            $table->string('sterasxdy', 10)->nullable();
            $table->string('steuslugy', 10)->nullable();
            $table->string('stesumma', 30)->nullable();
            $table->string('steoplata', 30)->nullable();
        });
        Schema::create('ik_niokr_stoimost', function (Blueprint $table) {
            $table->unsignedInteger('stnum')->autoIncrement();
            $table->integer('znum')->nullable();
            $table->string('strasxdy', 10)->nullable();
            $table->string('stuslugy', 10)->nullable();
            $table->string('stsumma', 30)->nullable();
            $table->string('stwhatgod', 5)->nullable();
        });
        Schema::create('ik_niokr_subcontract', function (Blueprint $table) {
            $table->unsignedInteger('scnum')->autoIncrement();
            $table->integer('cenum')->nullable();
            $table->string('scplanum', 20)->nullable();
            $table->date('scdatazakluch')->nullable();
            $table->date('scdatazaversh')->nullable();
            $table->string('scsostoianie', 15)->nullable();
            $table->string('scpodsostoianie', 300)->nullable();
            $table->text('sctexzadan')->nullable();
            $table->string('scnameispoln', 250)->nullable();
            $table->string('scadrispoln', 200)->nullable();
            $table->string('scname', 600)->nullable();
            $table->string('scpriceall', 30)->nullable();
            $table->string('scpricethisyear', 30)->nullable();
            $table->string('scpricethisyplus1', 30)->nullable();
            $table->string('scpricethisyplus2', 30)->nullable();
            $table->string('scpricethisyplus3', 30)->nullable();
        });
        Schema::create('ik_niokr_subetapi', function (Blueprint $table) {
            $table->unsignedInteger('senum')->autoIncrement();
            $table->integer('scnum')->nullable();
            $table->string('seplanum', 20)->nullable();
            $table->boolean('seend')->nullable();
            $table->date('sedatastart')->nullable();
            $table->date('sedataend')->nullable();
            $table->string('sesumwinner', 20)->nullable();
            $table->string('seotchetnum', 20)->nullable();
            $table->boolean('sereferat')->nullable();
            $table->boolean('seelectronotchet')->nullable();
            $table->string('seprimechan', 400)->nullable();
            $table->string('sekalendplansum', 20)->nullable();
            $table->string('sevozvratavanssum', 20)->nullable();
            $table->string('sename', 3000)->nullable();
            $table->integer('psenum')->nullable();
            $table->string('psedata', 20)->nullable();
        });
        Schema::create('ik_niokr_sudalen', function (Blueprint $table) {
            $table->unsignedInteger('sunum')->autoIncrement();
            $table->integer('znum')->nullable();
            $table->date('sudata')->nullable();
            $table->integer('onum')->nullable();
            $table->string('suprichina', 600)->nullable();
        });
        Schema::create('ik_niokr_svosstan', function (Blueprint $table) {
            $table->unsignedInteger('svnum')->autoIncrement();
            $table->integer('znum')->nullable();
            $table->date('svdata')->nullable();
            $table->integer('onum')->nullable();
            $table->string('svprichina', 600)->nullable();
        });
        Schema::create('ik_niokr_vnedrenie', function (Blueprint $table) {
            $table->unsignedInteger('vrnum')->autoIncrement();
            $table->integer('ctnum')->nullable();
            $table->string('vrindexnumber', 50)->nullable();
            $table->string('vrtitle', 300)->nullable();
            $table->boolean('vrvnedren')->nullable();
        });
        Schema::create('ik_niokr_vnespred', function (Blueprint $table) {
            $table->unsignedInteger('vnum')->autoIncrement();
            $table->integer('znum')->nullable();
            $table->integer('onum')->nullable();
            $table->date('vdatepismo')->nullable();
            $table->string('vpismo', 50)->nullable();
        });
        Schema::create('ik_niokr_whosewrite', function (Blueprint $table) {
            $table->unsignedInteger('wwhosenum')->autoIncrement();
            $table->string('wwhosenumber', 20)->nullable();
            $table->string('wwho', 20)->nullable();
        });
        Schema::create('ik_niokr_winnersubpodr', function (Blueprint $table) {
            $table->unsignedInteger('wsnum')->autoIncrement();
            $table->integer('wtnum')->nullable();
            $table->integer('onum')->nullable();
        });
        Schema::create('ik_niokr_winnertorg', function (Blueprint $table) {
            $table->unsignedInteger('wtnum')->autoIncrement();
            $table->integer('znum')->nullable();
            $table->integer('onum')->nullable();
            $table->boolean('wtpobeda')->nullable();
            $table->date('wtdatepismo')->nullable();
            $table->string('wtpismo', 50)->nullable();
            $table->integer('wtslugebn')->nullable();
            $table->double('wtzvprice')->nullable();
        });
        Schema::create('ik_niokr_workvnedren', function (Blueprint $table) {
            $table->unsignedInteger('wvnum')->autoIncrement();
            $table->integer('vrnum')->nullable();
            $table->string('wvvid', 300)->nullable();
            $table->string('wvdescript', 1500)->nullable();
            $table->integer('wvgod')->nullable();
        });
        Schema::create('ik_niokr_zaiavka', function (Blueprint $table) {
            $table->unsignedInteger('znum')->autoIncrement();
            $table->integer('snum')->nullable();
            $table->integer('prnum')->nullable();
            $table->integer('zgod')->nullable();
            $table->string('zlotnum', 20)->nullable();
            $table->string('zname', 600)->nullable();
            $table->string('ztarget', 1500)->nullable();
            $table->date('zsrokbegin')->nullable();
            $table->date('zsrokend')->nullable();
            $table->integer('zsrokkolmounth')->nullable();
            $table->string('zpriceall', 30)->nullable();
            $table->string('zpricethisyear', 30)->nullable();
            $table->string('zpricethisyplus1', 30)->nullable();
            $table->string('zpricethisyplus2', 30)->nullable();
            $table->string('zpricethisyplus3', 30)->nullable();
            $table->string('zpricerasch', 30)->nullable();
            $table->string('zeffecto', 1500)->nullable();
            $table->boolean('znotinplan')->nullable();
            $table->string('zeditnum', 100)->nullable();
            $table->string('zstatus', 50)->nullable();
            $table->integer('zplanum')->nullable();
            $table->string('znumprotokol', 50)->nullable();
            $table->date('zdataprotocol')->nullable();
            $table->date('zdatarazmeshen')->nullable();
            $table->integer('export_id')->nullable();
            $table->integer('export_state')->nullable();
            $table->string('znumzakupki', 25)->nullable();
            $table->integer('flskonknum')->nullable();
            $table->boolean('zstandartbkd')->nullable();
            $table->boolean('zstandartrf')->nullable();
            $table->string('zpricethisyminus1', 30)->nullable();
            $table->string('zpricethisyminus2', 30)->nullable();
            $table->string('zpricethisyminus3', 30)->nullable();
        });
        Schema::create('ik_niokr_zakazchik', function (Blueprint $table) {
            $table->unsignedInteger('zknum')->autoIncrement();
            $table->boolean('zkmain')->nullable();
            $table->integer('znum')->nullable();
            $table->integer('onum')->nullable();
            $table->date('zkdatepismo')->nullable();
            $table->string('zkpismo', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ik_niokr_actdoc');
        Schema::dropIfExists('ik_niokr_avans');
        Schema::dropIfExists('ik_niokr_certificate');
        Schema::dropIfExists('ik_niokr_claim');
        Schema::dropIfExists('ik_niokr_condob');
        Schema::dropIfExists('ik_niokr_conetapi');
        Schema::dropIfExists('ik_niokr_contract');
        Schema::dropIfExists('ik_niokr_crittex');
        Schema::dropIfExists('ik_niokr_dokumentats');
        Schema::dropIfExists('ik_niokr_econobosn');
        Schema::dropIfExists('ik_niokr_flskonkdok');
        Schema::dropIfExists('ik_niokr_flskonkurs_');
        Schema::dropIfExists('ik_niokr_grouporg');
        Schema::dropIfExists('ik_niokr_histntd');
        Schema::dropIfExists('ik_niokr_ispolnitel');
        Schema::dropIfExists('ik_niokr_listotvet');
        Schema::dropIfExists('ik_niokr_listzamech');
        Schema::dropIfExists('ik_niokr_men');
        Schema::dropIfExists('ik_niokr_mentel');
        Schema::dropIfExists('ik_niokr_ntddocum');
        Schema::dropIfExists('ik_niokr_ntdevents');
        Schema::dropIfExists('ik_niokr_ntdproject');
        Schema::dropIfExists('ik_niokr_organiz');
        Schema::dropIfExists('ik_niokr_orgtel');
        Schema::dropIfExists('ik_niokr_orgvnedren');
        Schema::dropIfExists('ik_niokr_perechensostetapi');
        Schema::dropIfExists('ik_niokr_podrazdel');
        Schema::dropIfExists('ik_niokr_priornaprav');
        Schema::dropIfExists('ik_niokr_prnkonkurs');
        Schema::dropIfExists('ik_niokr_protokol');
        Schema::dropIfExists('ik_niokr_razdel');
        Schema::dropIfExists('ik_niokr_sostconetapi');
        Schema::dropIfExists('ik_niokr_statia');
        Schema::dropIfExists('ik_niokr_stoimetap');
        Schema::dropIfExists('ik_niokr_stoimost');
        Schema::dropIfExists('ik_niokr_subcontract');
        Schema::dropIfExists('ik_niokr_subetapi');
        Schema::dropIfExists('ik_niokr_sudalen');
        Schema::dropIfExists('ik_niokr_svosstan');
        Schema::dropIfExists('ik_niokr_vnedrenie');
        Schema::dropIfExists('ik_niokr_vnespred');
        Schema::dropIfExists('ik_niokr_whosewrite');
        Schema::dropIfExists('ik_niokr_winnersubpodr');
        Schema::dropIfExists('ik_niokr_winnertorg');
        Schema::dropIfExists('ik_niokr_workvnedren');
        Schema::dropIfExists('ik_niokr_zaiavka');
        Schema::dropIfExists('ik_niokr_zakazchik');
    }
};
