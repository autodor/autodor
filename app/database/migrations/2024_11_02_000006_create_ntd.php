<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ntd', function (Blueprint $table) {
            $table->uuid()->unique()->primary();
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('typeNtd')->nullable();
            $table->string('numberNtd')->nullable();
            $table->string('titleNtd')->nullable();
            $table->string('titleFku')->nullable();
            $table->string('numberPurchase')->nullable();
            $table->string('numberContract')->nullable();
            $table->date('dateStart')->nullable();
            $table->date('dateFinish')->nullable();
            $table->string('developer')->nullable();
            $table->string('title')->nullable();
            $table->string('place')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ntd');
    }
};
