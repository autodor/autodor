<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rntd_carts', function (Blueprint $table) {
            $table->unsignedInteger('canum')->autoIncrement();
            $table->integer('ounum')->nullable();
            $table->binary('catext')->nullable();
            $table->string('caras', 7)->nullable();
            $table->string('cacartsname', 10)->nullable();
            $table->string('cainternet', 30)->nullable();
            $table->string('caregnum', 50)->nullable();
            $table->date('cadatareg')->nullable();
        });
        Schema::create('rntd_classifrasx', function (Blueprint $table) {
            $table->unsignedInteger('crnum')->autoIncrement();
            $table->string('crname', 300)->nullable();
            $table->string('crplanum', 30)->nullable();
        });
        Schema::create('rntd_commondatekontr', function (Blueprint $table) {
            $table->unsignedInteger('cdnum')->autoIncrement();
            $table->string('cdkontrnum', 30)->nullable();
            $table->string('cdcity', 30)->nullable();
            $table->date('cddatezakluch')->nullable();
            $table->date('cddatestart')->nullable();
            $table->date('cddatefinish')->nullable();
            $table->string('cdshifrwork', 200)->nullable();
            $table->integer('pranum')->nullable();
            $table->integer('vdnum')->nullable();
            $table->integer('ownum')->nullable();
            $table->integer('vwnum')->nullable();
            $table->integer('frnum')->nullable();
            $table->integer('pnum')->nullable();
            $table->string('cdinternetnum', 30)->nullable();
            $table->string('cdregnum', 50)->nullable();
            $table->date('cddatereg')->nullable();
            $table->string('cdplanumik_niokr', 30)->nullable();
            $table->integer('prnum')->nullable();
            $table->string('cdreestrnum', 30)->nullable();
            $table->date('cdreestrdate')->nullable();
            $table->string('cdnamework', 1000)->nullable();
            $table->string('cdarxnum', 30)->nullable();
        });
        Schema::create('rntd_dopzvkaregform', function (Blueprint $table) {
            $table->unsignedInteger('dzrfnum')->autoIncrement();
            $table->integer('zrf1num')->nullable();
            $table->string('dzrfdokprav', 400)->nullable();
            $table->string('dzrfpravarf', 400)->nullable();
            $table->string('dzrfvidrezul', 400)->nullable();
            $table->string('dzrfdokoxran', 400)->nullable();
            $table->string('dzrfpravousdok', 400)->nullable();
            $table->string('dzrfdoktaina', 400)->nullable();
            $table->string('dzrfnamedeyt', 400)->nullable();
            $table->string('dzrfviddeyt', 400)->nullable();
            $table->string('dzrfxodoform', 400)->nullable();
            $table->string('dzrfdokou', 400)->nullable();
            $table->string('dzrfreznooxran', 400)->nullable();
            $table->string('dzrfavtorname', 1000)->nullable();
            $table->string('dzrfavtordata', 1000)->nullable();
            $table->string('dzrfavtorsobst', 1000)->nullable();
            $table->string('dzrfavtorrabota', 1000)->nullable();
            $table->string('dzrfavtorvoznagr', 1000)->nullable();
            $table->string('dzrfrekzaiavki', 400)->nullable();
            $table->string('dzrfrekpatent', 400)->nullable();
            $table->string('dzrfrektaina', 400)->nullable();
            $table->string('dzrfrekotkaz', 400)->nullable();
            $table->string('dzrfrekprekr', 400)->nullable();
            $table->string('dzrfviddogov', 400)->nullable();
            $table->string('dzrfnameou', 400)->nullable();
            $table->string('dzrfsrokdog', 400)->nullable();
            $table->string('dzrfterrdog', 400)->nullable();
            $table->string('dzrfpreddog', 400)->nullable();
            $table->string('dzrfregnum', 400)->nullable();
            $table->string('dzrfperechdok', 400)->nullable();
            $table->string('dzrflrname', 400)->nullable();
            $table->string('dzrflrsname', 400)->nullable();
            $table->string('dzrflradr', 400)->nullable();
            $table->string('dzrflrokpo', 400)->nullable();
            $table->string('dzrflrinn', 400)->nullable();
            $table->string('dzrfltname', 400)->nullable();
            $table->string('dzrfltsname', 400)->nullable();
            $table->string('dzrfltadr', 400)->nullable();
            $table->string('dzrfltokpo', 400)->nullable();
            $table->string('dzrfltinn', 400)->nullable();
            $table->string('dzrfdokproizv', 400)->nullable();
            $table->string('dzrfnameprod', 400)->nullable();
            $table->date('dzrfdatevvoda')->nullable();
        });
        Schema::create('rntd_finans', function (Blueprint $table) {
            $table->unsignedInteger('fnum')->autoIncrement();
            $table->integer('cdnum')->nullable();
            $table->integer('prbknum')->nullable();
            $table->integer('srnum')->nullable();
            $table->integer('vrnum')->nullable();
            $table->integer('crnum')->nullable();
            $table->integer('rbknum')->nullable();
            $table->string('fsum', 30)->nullable();
        });
        Schema::create('rntd_form1', function (Blueprint $table) {
            $table->unsignedInteger('ounum')->autoIncrement();
            $table->binary('f1text')->nullable();
        });
        Schema::create('rntd_form1niokr', function (Blueprint $table) {
            $table->unsignedInteger('cdnum')->autoIncrement();
            $table->binary('f1niokrtext')->nullable();
            $table->string('f1niokrnumb', 30)->nullable();
            $table->string('f1niokrdate', 30)->nullable();
        });
        Schema::create('rntd_form2', function (Blueprint $table) {
            $table->unsignedInteger('f2num')->autoIncrement();
            $table->integer('cdnum')->nullable();
            $table->binary('f2text')->nullable();
            $table->string('f2ras', 7)->nullable();
            $table->string('f2formname', 30)->nullable();
            $table->string('f2numb', 30)->nullable();
            $table->string('f2date', 30)->nullable();
        });
        Schema::create('rntd_formrezult', function (Blueprint $table) {
            $table->unsignedInteger('frnum')->autoIncrement();
            $table->string('frname', 300)->nullable();
        });
        Schema::create('rntd_ispolnitel', function (Blueprint $table) {
            $table->unsignedInteger('inum')->autoIncrement();
            $table->integer('cdnum')->nullable();
            $table->string('iokpo', 30)->nullable();
            $table->string('iinn', 30)->nullable();
            $table->string('inamepoln', 400)->nullable();
            $table->string('inamekratk', 400)->nullable();
            $table->string('iadr', 400)->nullable();
        });
        Schema::create('rntd_objectu', function (Blueprint $table) {
            $table->unsignedInteger('ounum')->autoIncrement();
            $table->integer('cdnum')->nullable();
            $table->string('ounameobject', 400)->nullable();
            $table->string('ouupolnomoch', 400)->nullable();
            $table->string('oudolupolnomoch', 400)->nullable();
            $table->string('oupravooblosnovan', 400)->nullable();
            $table->string('ouorgnamepravoobl', 400)->nullable();
            $table->integer('ponum')->nullable();
            $table->string('ouinternetnum', 30)->nullable();
            $table->string('ouregnum', 50)->nullable();
            $table->date('oudatereg')->nullable();
            $table->string('ourospatzaiavknum', 50)->nullable();
            $table->date('ourospatzaiavkdate')->nullable();
            $table->string('ourospatsvidetnum', 50)->nullable();
            $table->string('ourospatsvidetkategor', 300)->nullable();
            $table->date('ourospatsvidetdate')->nullable();
            $table->string('ourospatprimechan', 400)->nullable();
            $table->binary('ousvidreg')->nullable();
            $table->binary('ourospatsvid')->nullable();
            $table->string('oufdaplanum', 50)->nullable();
            $table->string('ousvdregprimechan', 400)->nullable();
            $table->integer('export_id')->nullable();
            $table->integer('export_state')->nullable();
            $table->date('oufdadateregrid')->nullable();
            $table->string('oufdadeleterid', 3)->nullable();
            $table->text('ouopisan')->nullable();
            $table->string('ourospatsvidras', 7)->nullable();
        });
        Schema::create('rntd_osnovaniework', function (Blueprint $table) {
            $table->unsignedInteger('ownum')->autoIncrement();
            $table->string('owname', 300)->nullable();
        });
        Schema::create('rntd_patent', function (Blueprint $table) {
            $table->unsignedInteger('ptnum')->autoIncrement();
            $table->integer('ounum')->nullable();
            $table->string('ptrospatzaiavknum', 50)->nullable();
            $table->date('ptrospatzaiavkdate')->nullable();
            $table->binary('ptrospatzaiavk')->nullable();
            $table->string('ptrospatzaiavkras', 7)->nullable();
            $table->string('ptsptdoc1name', 50)->nullable();
            $table->binary('ptsptdoc1')->nullable();
            $table->string('ptsptdoc1ras', 7)->nullable();
            $table->string('ptsptdoc2name', 50)->nullable();
            $table->binary('ptsptdoc2')->nullable();
            $table->string('ptsptdoc2ras', 7)->nullable();
            $table->string('ptsptdoc3name', 50)->nullable();
            $table->binary('ptsptdoc3')->nullable();
            $table->string('ptsptdoc3ras', 7)->nullable();
            $table->string('ptsptdoc4name', 50)->nullable();
            $table->binary('ptsptdoc4')->nullable();
            $table->string('ptsptdoc4ras', 7)->nullable();
            $table->string('ptsptdoc5name', 50)->nullable();
            $table->binary('ptsptdoc5')->nullable();
            $table->string('ptsptdoc5ras', 7)->nullable();
            $table->string('ptsptdoc6name', 50)->nullable();
            $table->binary('ptsptdoc6')->nullable();
            $table->string('ptsptdoc6ras', 7)->nullable();
            $table->string('ptrospatsvidetnum', 50)->nullable();
            $table->string('ptrospatsvidetkategor', 300)->nullable();
            $table->date('ptrospatsvidetdate')->nullable();
            $table->string('ptrospatprimechan', 400)->nullable();
            $table->binary('ptrospatsvid')->nullable();
            $table->string('ptbalansnaname1', 50)->nullable();
            $table->binary('ptbalansnadoc1')->nullable();
            $table->string('ptbalansnaras1', 7)->nullable();
            $table->string('ptbalansnaname2', 50)->nullable();
            $table->binary('ptbalansnadoc2')->nullable();
            $table->string('ptbalansnaras2', 7)->nullable();
            $table->string('ptbalansnaname3', 50)->nullable();
            $table->binary('ptbalansnadoc3')->nullable();
            $table->string('ptbalansnaras3', 7)->nullable();
            $table->string('ptbalansnaname4', 50)->nullable();
            $table->binary('ptbalansnadoc4')->nullable();
            $table->string('ptbalansnaras4', 7)->nullable();
            $table->string('ptbalansnaname5', 50)->nullable();
            $table->binary('ptbalansnadoc5')->nullable();
            $table->string('ptbalansnaras5', 7)->nullable();
            $table->string('ptbalansnaname6', 50)->nullable();
            $table->binary('ptbalansnadoc6')->nullable();
            $table->string('ptbalansnaras6', 7)->nullable();
            $table->boolean('ptbalanapostav')->nullable();
            $table->date('ptbalanadate')->nullable();
            $table->string('ptbalananum', 50)->nullable();
            $table->string('ptbalanastoim', 20)->nullable();
            $table->string('ptrospatsvidras', 7)->nullable();
        });
        Schema::create('rntd_podrazdel', function (Blueprint $table) {
            $table->unsignedInteger('prnum')->autoIncrement();
            $table->integer('rnum')->nullable();
            $table->string('prname', 300)->nullable();
            $table->integer('prplanum')->nullable();
        });
        Schema::create('rntd_podrazdelaim', function (Blueprint $table) {
            $table->unsignedInteger('pranum')->autoIncrement();
            $table->integer('ranum')->nullable();
            $table->string('praname', 300)->nullable();
            $table->string('praplanum', 30)->nullable();
        });
        Schema::create('rntd_podrazdelbk', function (Blueprint $table) {
            $table->unsignedInteger('prbknum')->autoIncrement();
            $table->string('prbkname', 300)->nullable();
            $table->string('prbkplanum', 30)->nullable();
        });
        Schema::create('rntd_podrazdelobjectperpr', function (Blueprint $table) {
            $table->unsignedInteger('proppnum')->autoIncrement();
            $table->integer('roppnum')->nullable();
            $table->string('proppname', 300)->nullable();
            $table->string('proppplanum', 30)->nullable();
        });
        Schema::create('rntd_podrazdelobjectprim', function (Blueprint $table) {
            $table->unsignedInteger('propnum')->autoIncrement();
            $table->integer('ropnum')->nullable();
            $table->string('propname', 300)->nullable();
            $table->string('propplanum', 30)->nullable();
        });
        Schema::create('rntd_podrazdelobjectsoversh', function (Blueprint $table) {
            $table->unsignedInteger('prosnum')->autoIncrement();
            $table->integer('rosnum')->nullable();
            $table->string('prosname', 300)->nullable();
            $table->string('prosplanum', 30)->nullable();
        });
        Schema::create('rntd_prava', function (Blueprint $table) {
            $table->unsignedInteger('pnum')->autoIncrement();
            $table->string('pname', 300)->nullable();
        });
        Schema::create('rntd_pravaobject', function (Blueprint $table) {
            $table->unsignedInteger('ponum')->autoIncrement();
            $table->string('ponamerf', 300)->nullable();
            $table->string('ponameispoln', 300)->nullable();
        });
        Schema::create('rntd_razdel', function (Blueprint $table) {
            $table->unsignedInteger('rnum')->autoIncrement();
            $table->string('rname', 300)->nullable();
            $table->string('rplanum', 30)->nullable();
            $table->integer('rgod')->nullable();
        });
        Schema::create('rntd_razdelaim', function (Blueprint $table) {
            $table->unsignedInteger('ranum')->autoIncrement();
            $table->string('raname', 300)->nullable();
            $table->string('raplanum', 30)->nullable();
        });
        Schema::create('rntd_razdelbk', function (Blueprint $table) {
            $table->unsignedInteger('rbknum')->autoIncrement();
            $table->string('rbkname', 300)->nullable();
            $table->string('rbkplanum', 30)->nullable();
        });
        Schema::create('rntd_razdelobjectperpr', function (Blueprint $table) {
            $table->unsignedInteger('roppnum')->autoIncrement();
            $table->string('roppname', 300)->nullable();
            $table->string('roppplanum', 30)->nullable();
        });
        Schema::create('rntd_razdelobjectprim', function (Blueprint $table) {
            $table->unsignedInteger('ropnum')->autoIncrement();
            $table->string('ropname', 300)->nullable();
            $table->string('ropplanum', 30)->nullable();
        });
        Schema::create('rntd_razdelobjectsoversh', function (Blueprint $table) {
            $table->unsignedInteger('rosnum')->autoIncrement();
            $table->string('rosname', 300)->nullable();
            $table->string('rosplanum', 30)->nullable();
        });
        Schema::create('rntd_rosavtodor', function (Blueprint $table) {
            $table->unsignedInteger('radnum')->autoIncrement();
            $table->string('radnamekrat', 300)->nullable();
        });
        Schema::create('rntd_statiarasx', function (Blueprint $table) {
            $table->unsignedInteger('srnum')->autoIncrement();
            $table->string('srname', 300)->nullable();
            $table->string('srplanum', 30)->nullable();
        });
        Schema::create('rntd_submenpravoobl', function (Blueprint $table) {
            $table->unsignedInteger('smpnum')->autoIncrement();
            $table->integer('ounum')->nullable();
            $table->string('smpname', 400)->nullable();
        });
        Schema::create('rntd_suborgpravoobl', function (Blueprint $table) {
            $table->unsignedInteger('sopnum')->autoIncrement();
            $table->integer('ounum')->nullable();
            $table->string('sopname', 400)->nullable();
        });
        Schema::create('rntd_subpodr', function (Blueprint $table) {
            $table->unsignedInteger('snum')->autoIncrement();
            $table->integer('inum')->nullable();
            $table->string('sokpo', 30)->nullable();
            $table->string('sinn', 30)->nullable();
            $table->string('snamepoln', 400)->nullable();
            $table->string('snamekratk', 400)->nullable();
            $table->string('sadr', 400)->nullable();
        });
        Schema::create('rntd_username', function (Blueprint $table) {
            $table->unsignedInteger('unnum')->autoIncrement();
            $table->string('unlogin', 30)->nullable();
            $table->string('unispolnnamepoln', 400)->nullable();
            $table->string('unpassword', 30)->nullable();
        });
        Schema::create('rntd_viddoc', function (Blueprint $table) {
            $table->unsignedInteger('vdnum')->autoIncrement();
            $table->string('vdname', 300)->nullable();
        });
        Schema::create('rntd_vidrasx', function (Blueprint $table) {
            $table->unsignedInteger('vrnum')->autoIncrement();
            $table->string('vrname', 300)->nullable();
            $table->string('vrplanum', 30)->nullable();
        });
        Schema::create('rntd_vidwork', function (Blueprint $table) {
            $table->unsignedInteger('vwnum')->autoIncrement();
            $table->string('vwname', 300)->nullable();
            $table->string('vwpadegname', 300)->nullable();
        });
        Schema::create('rntd_zaiavkaregform1', function (Blueprint $table) {
            $table->unsignedInteger('zrf1num')->autoIncrement();
            $table->integer('cdnum')->nullable();
            $table->string('zrf1name', 1000)->nullable();
            $table->string('zrf1objectprim', 100)->nullable();
            $table->string('zrf1lifecikl', 100)->nullable();
            $table->string('zrf1soversh', 100)->nullable();
            $table->string('zrf1formrez', 100)->nullable();
            $table->string('zrf1perpr', 100)->nullable();
            $table->string('zrf1kluch', 1000)->nullable();
            $table->string('zrf1index', 1000)->nullable();
            $table->string('zrf1vidwork', 1000)->nullable();
            $table->string('zrf1namework', 1000)->nullable();
            $table->string('zrf1shifrwork', 1000)->nullable();
            $table->string('zrf1celevprog', 1000)->nullable();
            $table->string('zrf1patent', 1000)->nullable();
            $table->string('zrf1perechdok', 1000)->nullable();
            $table->string('zrf1datestend', 1000)->nullable();
            $table->string('zrf1dergatel', 1000)->nullable();
            $table->date('zrf1datevvoda')->nullable();
            $table->string('zrf1uprisp', 1000)->nullable();
            $table->string('zrf1uprsub', 1000)->nullable();
            $table->integer('zrf1unikalnum')->nullable();
            $table->string('zrf1opisan', 2000)->nullable();
            $table->string('zrf1oblprim', 1000)->nullable();
            $table->string('zrf1textrez', 300)->nullable();
            $table->string('zrf1objprimtxt', 1000)->nullable();
            $table->string('zrf1sovertxt', 1000)->nullable();
            $table->string('zrf1perprimtxt', 1000)->nullable();
        });
        Schema::create('rntd_zakazchik', function (Blueprint $table) {
            $table->unsignedInteger('zknum')->autoIncrement();
            $table->integer('cdnum')->nullable();
            $table->string('zkokogu', 30)->nullable();
            $table->string('zkinn', 30)->nullable();
            $table->string('zknamepoln', 400)->nullable();
            $table->string('zknamekratk', 400)->nullable();
            $table->string('zkadr', 400)->nullable();
            $table->string('zkupolnomoch', 400)->nullable();
            $table->string('zkdolupolnomoch', 400)->nullable();
            $table->date('zkdatepodachizaiav')->nullable();
            $table->string('zkpodrazdelen', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rntd_carts');
        Schema::dropIfExists('rntd_classifrasx');
        Schema::dropIfExists('rntd_commondatekontr');
        Schema::dropIfExists('rntd_dopzvkaregform');
        Schema::dropIfExists('rntd_finans');
        Schema::dropIfExists('rntd_form1');
        Schema::dropIfExists('rntd_form1niokr');
        Schema::dropIfExists('rntd_form2');
        Schema::dropIfExists('rntd_formrezult');
        Schema::dropIfExists('rntd_ispolnitel');
        Schema::dropIfExists('rntd_objectu');
        Schema::dropIfExists('rntd_osnovaniework');
        Schema::dropIfExists('rntd_patent');
        Schema::dropIfExists('rntd_podrazdel');
        Schema::dropIfExists('rntd_podrazdelaim');
        Schema::dropIfExists('rntd_podrazdelbk');
        Schema::dropIfExists('rntd_podrazdelobjectperpr');
        Schema::dropIfExists('rntd_podrazdelobjectprim');
        Schema::dropIfExists('rntd_podrazdelobjectsoversh');
        Schema::dropIfExists('rntd_prava');
        Schema::dropIfExists('rntd_pravaobject');
        Schema::dropIfExists('rntd_razdel');
        Schema::dropIfExists('rntd_razdelaim');
        Schema::dropIfExists('rntd_razdelbk');
        Schema::dropIfExists('rntd_razdelobjectperpr');
        Schema::dropIfExists('rntd_razdelobjectprim');
        Schema::dropIfExists('rntd_razdelobjectsoversh');
        Schema::dropIfExists('rntd_rosavtodor');
        Schema::dropIfExists('rntd_statiarasx');
        Schema::dropIfExists('rntd_submenpravoobl');
        Schema::dropIfExists('rntd_suborgpravoobl');
        Schema::dropIfExists('rntd_subpodr');
        Schema::dropIfExists('rntd_username');
        Schema::dropIfExists('rntd_viddoc');
        Schema::dropIfExists('rntd_vidrasx');
        Schema::dropIfExists('rntd_vidwork');
        Schema::dropIfExists('rntd_zaiavkaregform1');
        Schema::dropIfExists('rntd_zakazchik');
    }
};
