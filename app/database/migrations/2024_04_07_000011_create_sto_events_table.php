<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sto_events', function (Blueprint $table) {
            $table->id();
            $table->string("referal", 100);
            $table->string('number',50);
            $table->string('name', 500)->nullable();
            $table->string('organization', 200)->nullable();
            $table->date('date_start')->nullable();
            $table->date('date_finish')->nullable();
            $table->json('detail')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sto_events');
    }
};
