<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mpk_chapter', function (Blueprint $table) {
            $table->string("id", 15)->primary();
            $table->string("parent_id", 15)->nullable();
            $table->string("name", 1000);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mpk_chapter');
    }
};
