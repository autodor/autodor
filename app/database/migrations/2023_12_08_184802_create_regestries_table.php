<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('registries', function (Blueprint $table) {
            $table->id();
            $table->string("key");
            $table->string("name");
            $table->string("contractName", 1000);
            $table->string("contractNumber");
            $table->date("contractDate");
            $table->string("patentName");
            $table->string("patentNumber");
            $table->date("patentDate");
            $table->string("certificateNumber")->nullable();
            $table->date("certificateDate")->nullable();
            $table->mediumText("description");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('registries');
    }
};
