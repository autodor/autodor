<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tender', function (Blueprint $table) {
            $table->id();
            $table->string("referal", 100);
            $table->bigInteger('budget_item_id');
            $table->string('number_purchase', 50)->nullable();
            $table->date('date_purchase')->nullable();
            $table->date('date_review')->nullable();
            $table->string('name', 1000)->nullable();
            $table->bigInteger('organization_id')->nullable();
            $table->string('number_contract',200)->nullable();
            $table->date('date_contract')->nullable();
            $table->boolean('single_executor')->nullable()->default(false);
            $table->boolean('single_participant')->nullable()->default(false);
            $table->integer('count_lots')->nullable()->default(0);
            $table->json('detail');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tender');
    }
};
