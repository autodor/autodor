<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('eb_ntd_to_qualifier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('eb_ntd_id');
            $table->unsignedBigInteger('qualifier_id');
            $table->foreign('eb_ntd_id')->references('id')->on('eb_ntd')->onDelete('cascade');
            $table->foreign('qualifier_id')->references('id')->on('qualifier')->onDelete('cascade');
            $table->timestamps();
            $table->index('eb_ntd_id','eb_ntd_to_qualifier_for_eb_ntd');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('eb_ntd_to_qualifier');
    }
};
