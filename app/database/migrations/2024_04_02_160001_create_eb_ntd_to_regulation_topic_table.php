<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('eb_ntd_to_regulation_topic', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('eb_ntd_id');
            $table->unsignedBigInteger('regulation_topic_id');
            $table->foreign('eb_ntd_id')->references('id')->on('eb_ntd')->onDelete('cascade');
            $table->foreign('regulation_topic_id')->references('id')->on('regulation_topic')->onDelete('cascade');
            $table->timestamps();
            $table->index('eb_ntd_id','eb_ntd_to_regulation_topic_for_eb_ntd');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('eb_ntd_to_regulation_topic');
    }
};
