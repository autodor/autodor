<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('arxiv_arxorg', function (Blueprint $table) {
            $table->unsignedInteger('onum')->autoIncrement();
            $table->string('oadr', 200)->nullable();
            $table->string('onamepoln', 250)->nullable();
        });
        Schema::create('arxiv_chif', function (Blueprint $table) {
            $table->unsignedInteger('cnum')->autoIncrement();
            $table->unsignedInteger('tnum')->nullable();
            $table->string('cname', 20)->nullable();
            $table->string('czvanie', 80)->nullable();
        });
        Schema::create('arxiv_classall', function (Blueprint $table) {
            $table->unsignedInteger('clnum')->autoIncrement();
            $table->string('clname', 200)->nullable();
            $table->string('clplanum', 20)->nullable();
        });
        Schema::create('arxiv_classtema', function (Blueprint $table) {
            $table->unsignedInteger('cltnum')->autoIncrement();
            $table->unsignedInteger('clnum')->nullable();
            $table->unsignedInteger('tnum')->nullable();
        });
        Schema::create('arxiv_design', function (Blueprint $table) {
            $table->unsignedInteger('dnum')->autoIncrement();
            $table->unsignedInteger('tnum')->nullable();
            $table->string('dadr', 200)->nullable();
            $table->string('dname', 250)->nullable();
        });
        Schema::create('arxiv_kluch', function (Blueprint $table) {
            $table->unsignedInteger('knum')->autoIncrement();
            $table->unsignedInteger('tnum')->nullable();
            $table->string('kname', 100)->nullable();
        });
        Schema::create('arxiv_listpereddok', function (Blueprint $table) {
            $table->unsignedInteger('lpdnum')->autoIncrement();
            $table->unsignedInteger('tnum')->nullable();
            $table->string('lpdname', 500)->nullable();
            $table->date('lpddata')->nullable();
            $table->string('lpdwhere', 5)->nullable();
            $table->string('lpdprimech', 300)->nullable();
        });
        Schema::create('arxiv_owner', function (Blueprint $table) {
            $table->unsignedInteger('onum')->autoIncrement();
            $table->unsignedInteger('tnum')->nullable();
            $table->string('oname', 250)->nullable();
        });
        for ($i = 1991; $i < 2025; $i++) {
            Schema::create('arxiv_pltxt' . $i, function (Blueprint $table) {
                $table->unsignedInteger('polnnum')->autoIncrement();
                $table->unsignedInteger('tnum')->nullable();
                $table->text('polndocum')->nullable();
                $table->string('polnras', 5)->nullable();
                $table->string('polnname', 600)->nullable();
            });
        }
        Schema::create('arxiv_rubrikator', function (Blueprint $table) {
            $table->unsignedInteger('rubnummkvi')->autoIncrement();
            $table->string('rubnamemkvi', 200)->nullable();
            $table->integer('rubparentmkvi')->nullable();
            $table->string('rubplanummkvi', 30)->nullable();
        });
        Schema::create('arxiv_tema', function (Blueprint $table) {
            $table->unsignedInteger('tnum')->autoIncrement();
            $table->unsignedInteger('tmnum')->nullable();
            $table->smallInteger('tgod')->nullable();
            $table->text('treferat')->nullable();
            $table->boolean('tsrpatent')->nullable();
            $table->boolean('tsrntd')->nullable();
            $table->boolean('tsrvnedren')->nullable();
            $table->boolean('tsrbd')->nullable();
            $table->boolean('tzavershena')->nullable();
            $table->boolean('tfile')->nullable();
            $table->string('tplanum', 30)->nullable();
            $table->string('ctplanumik', 20)->nullable();
            $table->date('ctdatazakik')->nullable();
            $table->string('tprimechan', 300)->nullable();
            $table->date('tdatavvoda')->nullable();
            $table->string('tname', 600)->nullable();
            $table->boolean('tsortweb')->nullable();
            $table->unsignedInteger('export_id')->nullable();
            $table->unsignedInteger('export_state')->nullable();
            $table->boolean('tsrproectntd')->nullable();
            $table->string('tnumstorage', 10)->nullable();
            $table->string('tvidplan', 20)->nullable();
        });
        Schema::create('arxiv_tematika', function (Blueprint $table) {
            $table->unsignedInteger('tmnum')->autoIncrement();
            $table->string('tmname', 500)->nullable();
            $table->unsignedInteger('tmplanum')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {

        Schema::dropIfExists('arxiv_arxorg');
        Schema::dropIfExists('arxiv_chif');
        Schema::dropIfExists('arxiv_classall');
        Schema::dropIfExists('arxiv_classtema');
        Schema::dropIfExists('arxiv_design');
        Schema::dropIfExists('arxiv_kluch');
        Schema::dropIfExists('arxiv_listpereddok');
        Schema::dropIfExists('arxiv_owner');
        for ($i = 1991; $i < 2025; $i++) {
            Schema::dropIfExists('arxiv_pltxt' . $i);
        }
        Schema::dropIfExists('arxiv_rubrikator');
        Schema::dropIfExists('arxiv_tema');
        Schema::dropIfExists('arxiv_tematika');
    }
};
