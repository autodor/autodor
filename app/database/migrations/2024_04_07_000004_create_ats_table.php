<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ats', function (Blueprint $table) {
            $table->id();
            $table->string("referal", 100);
            $table->string("name", 500)->nullable();
            $table->unsignedBigInteger('district_id')->nullable();
            $table->unsignedBigInteger('group_id')->nullable();
            $table->string("short_name", 250)->nullable();
            $table->string("type_business", 1500)->nullable();
            $table->string("address", 500)->nullable();
            $table->string("email", 100)->nullable();
            $table->string("site", 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ats');
    }
};
