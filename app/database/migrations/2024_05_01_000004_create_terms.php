<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('terms_categdoc', function (Blueprint $table) {
            $table->unsignedInteger('cdnum')->autoIncrement();
            $table->string('cdname', 10)->nullable();
        });
        Schema::create('terms_defterm', function (Blueprint $table) {
            $table->unsignedInteger('dtnum')->autoIncrement();
            $table->integer('trnum')->nullable();
            $table->smallInteger('dtimportance')->nullable();
            $table->string('dtprimech', 250)->nullable();
            $table->date('dtdataentry')->nullable();
            $table->date('dtdataediting')->nullable();
            $table->string('dttext', 2000)->nullable();
        });
        Schema::create('terms_document', function (Blueprint $table) {
            $table->unsignedInteger('dcnum')->autoIncrement();
            $table->integer('dtnum')->nullable();
            $table->string('dcname', 300)->nullable();
            $table->string('dcnumber', 40)->nullable();
            $table->integer('dcgod')->nullable();
            $table->string('dcprimech', 250)->nullable();
            $table->string('dccategdoc', 10)->nullable();
            $table->integer('itnum')->nullable();
        });
        Schema::create('terms_dopfile', function (Blueprint $table) {
            $table->unsignedInteger('dfnum')->autoIncrement();
            $table->integer('trnum')->nullable();
            $table->string('dfname', 100)->nullable();
            $table->text('dffile')->nullable();
            $table->string('dffileras', 5)->nullable();
            $table->string('dfprimech', 250)->nullable();
        });
        Schema::create('terms_indterm', function (Blueprint $table) {
            $table->unsignedInteger('itnum')->autoIncrement();
            $table->string('itname', 10)->nullable();
        });
        Schema::create('terms_tematika', function (Blueprint $table) {
            $table->unsignedInteger('tmnum')->autoIncrement();
            $table->string('tmname', 250)->nullable();
            $table->string('tmplanum', 20)->nullable();
        });
        Schema::create('terms_tematikatermin', function (Blueprint $table) {
            $table->unsignedInteger('ttnum')->autoIncrement();
            $table->integer('tmnum')->nullable();
            $table->integer('trnum')->nullable();
        });
        Schema::create('terms_termin', function (Blueprint $table) {
            $table->unsignedInteger('trnum')->autoIncrement();
            $table->string('trname', 250)->nullable();
            $table->string('trprimech', 250)->nullable();
            $table->date('trdataentry')->nullable();
            $table->date('trdataediting')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('terms_categdoc');
        Schema::dropIfExists('terms_defterm');
        Schema::dropIfExists('terms_document');
        Schema::dropIfExists('terms_dopfile');
        Schema::dropIfExists('terms_indterm');
        Schema::dropIfExists('terms_tematika');
        Schema::dropIfExists('terms_tematikatermin');
        Schema::dropIfExists('terms_termin');
    }
};
