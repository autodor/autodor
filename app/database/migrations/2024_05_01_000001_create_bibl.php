<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bibl_authorbook', function (Blueprint $table) {
            $table->unsignedInteger('authorbook_pnum')->autoIncrement();
            $table->integer('book_num')->nullable();
            $table->string('authorbook_author', 120)->nullable();
        });
        Schema::create('bibl_authorinform', function (Blueprint $table) {
            $table->unsignedInteger('authorinform_pnum')->autoIncrement();
            $table->integer('inform_num')->nullable();
            $table->string('authorinform_author', 120)->nullable();
        });
        Schema::create('bibl_authorntd', function (Blueprint $table) {
            $table->unsignedInteger('authorntd_pnum')->autoIncrement();
            $table->integer('ntd_num')->nullable();
            $table->string('authorntd_author', 120)->nullable();
        });
        Schema::create('bibl_authorntdi', function (Blueprint $table) {
            $table->unsignedInteger('authorntdi_pnum')->autoIncrement();
            $table->integer('ntdi_num')->nullable();
            $table->string('authorntdi_author', 500)->nullable();
        });
        Schema::create('bibl_keywordbook', function (Blueprint $table) {
            $table->unsignedInteger('keywordbook_pnum')->autoIncrement();
            $table->integer('book_num')->nullable();
            $table->string('keywordbook_keyword', 255)->nullable();
        });
        Schema::create('bibl_keywordinform', function (Blueprint $table) {
            $table->unsignedInteger('keywordinform_pnum')->autoIncrement();
            $table->integer('inform_num')->nullable();
            $table->string('keywordinform_keyword', 255)->nullable();
        });
        Schema::create('bibl_keywordntd', function (Blueprint $table) {
            $table->unsignedInteger('keywordntd_pnum')->autoIncrement();
            $table->integer('ntd_num')->nullable();
            $table->string('keywordntd_keyword', 255)->nullable();
        });
        Schema::create('bibl_keywordntdi', function (Blueprint $table) {
            $table->unsignedInteger('keywordntdi_pnum')->autoIncrement();
            $table->integer('ntdi_num')->nullable();
            $table->string('keywordntdi_keyword', 255)->nullable();
        });
        Schema::create('bibl_mainbook', function (Blueprint $table) {
            $table->unsignedInteger('book_num')->autoIncrement();
            $table->string('book_title', 300)->nullable();
            $table->smallInteger('book_pubyear')->nullable();
            $table->string('book_secondname', 50)->nullable();
            $table->text('book_spravka')->nullable();
            $table->smallInteger('book_ext')->nullable();
            $table->date('book_lastedited')->nullable();
            $table->string('book_tplanum', 20)->nullable();
            $table->string('book_ctplanumik', 30)->nullable();
            $table->date('book_ctdatazakik')->nullable();
            $table->string('book_povtor', 500)->nullable();
            $table->string('book_izdatel', 500)->nullable();
            $table->string('book_mestoizdan', 500)->nullable();
            $table->string('book_kolstr', 50)->nullable();
            $table->string('book_primech', 1000)->nullable();
            $table->date('book_datavvoda')->nullable();
            $table->integer('export_id')->nullable();
            $table->integer('export_state')->nullable();
            $table->boolean('book_tolerance')->nullable();
            $table->string('book_invnum', 500)->nullable();
        });
        Schema::create('bibl_maininform', function (Blueprint $table) {
            $table->unsignedInteger('inform_num')->autoIncrement();
            $table->string('inform_title', 300)->nullable();
            $table->smallInteger('inform_pubyear')->nullable();
            $table->integer('stype_num')->nullable();
            $table->text('inform_spravka')->nullable();
            $table->smallInteger('inform_ext')->nullable();
            $table->date('inform_lastedited')->nullable();
            $table->string('inform_tplanum', 20)->nullable();
            $table->string('inform_ctplanumik', 30)->nullable();
            $table->date('inform_ctdatazakik')->nullable();
            $table->string('inform_seria', 50)->nullable();
            $table->string('inform_vipusknum', 50)->nullable();
            $table->string('inform_mestoizdan', 500)->nullable();
            $table->string('inform_kolstr', 50)->nullable();
            $table->string('inform_invnum', 500)->nullable();
            $table->string('inform_primech', 1000)->nullable();
            $table->date('inform_datavvoda')->nullable();
            $table->integer('export_id')->nullable();
            $table->integer('export_state')->nullable();
            $table->boolean('inform_tolerance')->nullable();
        });
        Schema::create('bibl_mainntd', function (Blueprint $table) {
            $table->unsignedInteger('ntd_num')->autoIncrement();
            $table->string('ntd_title', 300)->nullable();
            $table->smallInteger('ntd_pubyear')->nullable();
            $table->integer('sgroup_num')->nullable();
            $table->text('ntd_spravka')->nullable();
            $table->smallInteger('ntd_ext')->nullable();
            $table->date('ntd_lastedited')->nullable();
            $table->string('ntd_tplanum', 20)->nullable();
            $table->string('ntd_ctplanumik', 30)->nullable();
            $table->date('ntd_ctdatazakik')->nullable();
            $table->string('ntd_svedreg', 500)->nullable();
            $table->string('ntd_zamennast', 500)->nullable();
            $table->string('ntd_razvitdok', 500)->nullable();
            $table->string('ntd_izmendop', 500)->nullable();
            $table->string('ntd_deistvuet', 50)->nullable();
            $table->string('ntd_zamennew', 500)->nullable();
            $table->string('ntd_orgnormdok', 500)->nullable();
            $table->string('ntd_mestoizdan', 500)->nullable();
            $table->string('ntd_kolstr', 50)->nullable();
            $table->string('ntd_dergatel', 500)->nullable();
            $table->string('ntd_adrdergat', 500)->nullable();
            $table->string('ntd_invnum', 500)->nullable();
            $table->string('ntd_primech', 1000)->nullable();
            $table->string('ntd_vpervie', 50)->nullable();
            $table->date('ntd_datavvoda')->nullable();
            $table->integer('export_id')->nullable();
            $table->integer('export_state')->nullable();
            $table->date('ntd_vvoddeistv')->nullable();
            $table->boolean('ntd_tolerance')->nullable();
            $table->date('ntd_dateapproval')->nullable();
        });
        Schema::create('bibl_mainntdi', function (Blueprint $table) {
            $table->unsignedInteger('ntdi_num')->autoIncrement();
            $table->string('ntdi_title', 300)->nullable();
            $table->smallInteger('ntdi_pubyear')->nullable();
            $table->integer('sindex_num')->nullable();
            $table->integer('sbelong_num')->nullable();
            $table->text('ntdi_spravka')->nullable();
            $table->smallInteger('ntdi_ext')->nullable();
            $table->integer('ntdi_sort')->nullable();
            $table->date('ntdi_lastedited')->nullable();
            $table->string('ntdi_tplanum', 20)->nullable();
            $table->string('ntdi_ctplanumik', 30)->nullable();
            $table->date('ntdi_ctdatazakik')->nullable();
            $table->string('ntdi_number', 40)->nullable();
            $table->string('ntdi_svedreg', 500)->nullable();
            $table->string('ntdi_vpervie', 50)->nullable();
            $table->string('ntdi_kod', 50)->nullable();
            $table->string('ntdi_zamennast', 500)->nullable();
            $table->string('ntdi_deistvuet', 50)->nullable();
            $table->string('ntdi_izdatel', 500)->nullable();
            $table->string('ntdi_mestoizdan', 500)->nullable();
            $table->string('ntdi_kolstr', 50)->nullable();
            $table->string('ntdi_dergatel', 500)->nullable();
            $table->string('ntdi_adrdergat', 500)->nullable();
            $table->string('ntdi_invnum', 500)->nullable();
            $table->string('ntdi_primech', 1000)->nullable();
            $table->date('ntdi_datavvoda')->nullable();
            $table->integer('export_id')->nullable();
            $table->integer('export_state')->nullable();
            $table->date('ntdi_vvoddeistv')->nullable();
            $table->boolean('ntdi_tolerance')->nullable();
            $table->date('ntdi_dateapproval')->nullable();
            $table->boolean('ntdi_history')->nullable();
            $table->text('ntdi_histf')->nullable();
            $table->string('ntdi_histfras', 5)->nullable();
        });
        Schema::create('bibl_mainpatent', function (Blueprint $table) {
            $table->unsignedInteger('mpnum')->autoIncrement();
            $table->integer('ptprnum')->nullable();
            $table->string('mpname', 1000)->nullable();
            $table->string('mpnumpat', 20)->nullable();
            $table->string('mpnumzaiavk', 20)->nullable();
            $table->date('mpdatazaiavk')->nullable();
            $table->string('mpmestopubl', 50)->nullable();
            $table->date('mpdatapubl')->nullable();
            $table->string('mpauthor', 1000)->nullable();
            $table->string('mpzaiavit', 1000)->nullable();
            $table->date('mpdatavvoda')->nullable();
            $table->string('mpprimech', 1000)->nullable();
            $table->text('mptext')->nullable();
            $table->string('mptextras', 10)->nullable();
            $table->date('mplastedited')->nullable();
            $table->integer('export_id')->nullable();
            $table->integer('export_state')->nullable();
            $table->boolean('mpizobreten')->nullable();
            $table->string('mpgroupowner', 30)->nullable();
        });
        Schema::create('bibl_primntd', function (Blueprint $table) {
            $table->unsignedInteger('ntd_num')->autoIncrement();
            $table->text('primntd_prim')->nullable();
        });
        Schema::create('bibl_primntdi', function (Blueprint $table) {
            $table->unsignedInteger('ntdi_num')->autoIncrement();
            $table->text('primntdi_prim')->nullable();
        });
        Schema::create('bibl_ptpodrazdel', function (Blueprint $table) {
            $table->unsignedInteger('ptprnum')->autoIncrement();
            $table->integer('ptrnum')->nullable();
            $table->string('ptprplanum', 20)->nullable();
            $table->string('ptprname', 1000)->nullable();
            $table->integer('ptprindeks')->nullable();
            $table->string('ptprtemp1', 20)->nullable();
            $table->string('ptprtemp2', 20)->nullable();
        });
        Schema::create('bibl_ptrazdel', function (Blueprint $table) {
            $table->unsignedInteger('ptrnum')->autoIncrement();
            $table->string('ptrplanum', 20)->nullable();
            $table->string('ptrname', 1000)->nullable();
        });
        Schema::create('bibl_rubrikator', function (Blueprint $table) {
            $table->unsignedInteger('rubnum')->autoIncrement();
            $table->string('rubname', 200)->nullable();
            $table->string('rubplanum', 20)->nullable();
        });
        Schema::create('bibl_rubrikbook', function (Blueprint $table) {
            $table->unsignedInteger('book_rubnum')->autoIncrement();
            $table->integer('rubnum')->nullable();
            $table->integer('book_num')->nullable();
        });
        Schema::create('bibl_rubrikinform', function (Blueprint $table) {
            $table->unsignedInteger('inform_rubnum')->autoIncrement();
            $table->integer('rubnum')->nullable();
            $table->integer('inform_num')->nullable();
        });
        Schema::create('bibl_rubrikntd', function (Blueprint $table) {
            $table->unsignedInteger('ntd_rubnum')->autoIncrement();
            $table->integer('rubnum')->nullable();
            $table->integer('ntd_num')->nullable();
        });
        Schema::create('bibl_rubrikntdi', function (Blueprint $table) {
            $table->unsignedInteger('ntdi_rubnum')->autoIncrement();
            $table->integer('rubnum')->nullable();
            $table->integer('ntdi_num')->nullable();
        });
        Schema::create('bibl_rubrikpatent', function (Blueprint $table) {
            $table->unsignedInteger('patent_rubnum')->autoIncrement();
            $table->integer('rubnum')->nullable();
            $table->integer('mpnum')->nullable();
        });
        Schema::create('bibl_sbelong', function (Blueprint $table) {
            $table->unsignedInteger('sbelong_num')->autoIncrement();
            $table->string('sbelong_belong', 50)->nullable();
        });
        Schema::create('bibl_sgroup', function (Blueprint $table) {
            $table->unsignedInteger('sgroup_num')->autoIncrement();
            $table->string('sgroup_group', 50)->nullable();
            $table->integer('sgroup_sorting')->nullable();
        });
        Schema::create('bibl_sindex', function (Blueprint $table) {
            $table->unsignedInteger('sindex_num')->autoIncrement();
            $table->string('sindex_index', 50)->nullable();
            $table->integer('sindex_sorting')->nullable();
        });
        Schema::create('bibl_stype', function (Blueprint $table) {
            $table->unsignedInteger('stype_num')->autoIncrement();
            $table->string('stype_type', 50)->nullable();
            $table->integer('stype_sorting')->nullable();
        });
        Schema::create('bibl_tematika', function (Blueprint $table) {
            $table->unsignedInteger('tnum')->autoIncrement();
            $table->string('tplanum', 20)->nullable();
            $table->string('tname', 250)->nullable();
            $table->smallInteger('ttip')->nullable();
        });
        Schema::create('bibl_tematikabook', function (Blueprint $table) {
            $table->unsignedInteger('book_tnum')->autoIncrement();
            $table->integer('tnum')->nullable();
            $table->integer('book_num')->nullable();
        });
        Schema::create('bibl_tematikainform', function (Blueprint $table) {
            $table->unsignedInteger('inform_tnum')->autoIncrement();
            $table->integer('tnum')->nullable();
            $table->integer('inform_num')->nullable();
        });
        Schema::create('bibl_tematikantd', function (Blueprint $table) {
            $table->unsignedInteger('ntd_tnum')->autoIncrement();
            $table->integer('tnum')->nullable();
            $table->integer('ntd_num')->nullable();
        });
        Schema::create('bibl_tematikantdi', function (Blueprint $table) {
            $table->unsignedInteger('ntdi_tnum')->autoIncrement();
            $table->integer('tnum')->nullable();
            $table->integer('ntdi_num')->nullable();
        });
        Schema::create('bibl_tempntd', function (Blueprint $table) {
            $table->unsignedInteger('n_zapisy')->autoIncrement();
            $table->string('number', 40)->nullable();
            $table->integer('ind')->nullable();
            $table->string('n_docum0', 40)->nullable();
            $table->string('n_docum1', 40)->nullable();
            $table->string('n_docum2', 40)->nullable();
            $table->string('n_docum3', 40)->nullable();
            $table->string('n_docum4', 40)->nullable();
        });
        Schema::create('bibl_textsbook', function (Blueprint $table) {
            $table->unsignedInteger('book_num')->autoIncrement();
            $table->text('textsbook_text')->nullable();
            $table->string('book_textras', 5)->nullable();
        });
        Schema::create('bibl_textsinform', function (Blueprint $table) {
            $table->unsignedInteger('inform_num')->autoIncrement();
            $table->text('textsinform_text')->nullable();
            $table->string('inform_textras', 5)->nullable();
        });
        Schema::create('bibl_textsntd', function (Blueprint $table) {
            $table->unsignedInteger('ntd_num')->autoIncrement();
            $table->text('textsntd_text')->nullable();
            $table->string('ntd_textras', 5)->nullable();
        });
        Schema::create('bibl_textsntdi', function (Blueprint $table) {
            $table->unsignedInteger('ntdi_num')->autoIncrement();
            $table->text('textsntdi_text')->nullable();
            $table->string('ntdi_textras', 5)->nullable();
        });
        Schema::create('bibl_textsoldntdi', function (Blueprint $table) {
            $table->unsignedInteger('ntdi_num')->autoIncrement();
            $table->text('textsntdi_textold')->nullable();
            $table->string('ntdi_textoldras', 5)->nullable();
        });
        Schema::create('bibl_textsptntdi', function (Blueprint $table) {
            $table->unsignedInteger('tsptntdinum')->autoIncrement();
            $table->integer('ntdi_num')->nullable();
            $table->text('tsptntditext')->nullable();
            $table->string('tsptntdiras', 5)->nullable();
            $table->string('tsptntdititle', 200)->nullable();
            $table->string('tsptntdiimpt', 3)->nullable();
            $table->date('tsptntdidata')->nullable();
            $table->date('tsptntdidataend')->nullable();
        });
        Schema::create('bibl_tpnntdi', function (Blueprint $table) {
            $table->unsignedInteger('tpnnum')->autoIncrement();
            $table->integer('ntdi_num')->nullable();
            $table->date('tpndata')->nullable();
            $table->string('tpnodobren', 3)->nullable();
            $table->text('tpntext')->nullable();
            $table->string('tpnras', 5)->nullable();
        });
        DB::statement("
        CREATE OR REPLACE VIEW public.eb_ntd_view
            AS ( SELECT 1 AS table_view,
                'ntdi'::character varying(20) AS table_name,
                bibl_mainntdi.ntdi_num AS id,
                (((bibl_sindex.sindex_index::text || ' '::text) || bibl_sbelong.sbelong_belong::text))::character varying(40) AS type_doc,
                bibl_mainntdi.ntdi_number AS number,
                bt.topic_id,
                bibl_mainntdi.ntdi_title AS title,
                bibl_mainntdi.ntdi_pubyear AS year_published,
                ba.authorntdi_author AS developer,
                bibl_mainntdi.ntdi_ext AS ftris,
                bibl_mainntdi.ntdi_ctplanumik AS contract_number,
                bibl_mainntdi.ntdi_ctdatazakik AS date,
                bibl_mainntdi.ntdi_vvoddeistv AS date_entry,
                bibl_mainntdi.ntdi_dateapproval AS date_approval,
                bibl_mainntdi.ntdi_history AS history,
                bibl_mainntdi.ntdi_deistvuet AS enable
               FROM bibl_mainntdi
                 LEFT JOIN bibl_sindex ON bibl_mainntdi.sindex_num = bibl_sindex.sindex_num
                 LEFT JOIN bibl_sbelong ON bibl_mainntdi.sbelong_num = bibl_sbelong.sbelong_num
                 LEFT JOIN LATERAL ( SELECT bibl_authorntdi.authorntdi_author
                       FROM bibl_authorntdi
                      WHERE bibl_mainntdi.ntdi_num = bibl_authorntdi.ntdi_num
                      ORDER BY bibl_authorntdi.authorntdi_pnum DESC
                     LIMIT 1) ba ON true
                 LEFT JOIN bibl_textsntdi ON bibl_mainntdi.ntdi_num = bibl_textsntdi.ntdi_num
                 LEFT JOIN ( SELECT bibl_tematikantdi.ntdi_num,
                        string_agg(bibl_tematikantdi.tnum::character varying(50)::text, ', '::text) AS topic_id
                       FROM bibl_tematikantdi
                      GROUP BY bibl_tematikantdi.ntdi_num
                      ORDER BY bibl_tematikantdi.ntdi_num) bt ON bibl_mainntdi.ntdi_num = bt.ntdi_num
              ORDER BY bibl_mainntdi.ntdi_pubyear DESC, bibl_mainntdi.ntdi_num)
            UNION
            ( SELECT DISTINCT 2 AS table_view,
                'ntd'::character varying(20) AS table_name,
                bibl_mainntd.ntd_num AS id,
                bibl_sgroup.sgroup_group AS type_doc,
                ''::character varying(40) AS number,
                bt.topic_id,
                bibl_mainntd.ntd_title AS title,
                bibl_mainntd.ntd_pubyear AS year_published,
                ba.authorntd_author AS developer,
                bibl_mainntd.ntd_ext AS ftris,
                bibl_mainntd.ntd_ctplanumik AS contract_number,
                bibl_mainntd.ntd_ctdatazakik AS date,
                NULL::date AS date_entry,
                NULL::date AS date_approval,
                false AS history,
                bibl_mainntd.ntd_deistvuet AS enable
               FROM bibl_mainntd
                 LEFT JOIN bibl_sgroup ON bibl_mainntd.sgroup_num = bibl_sgroup.sgroup_num
                 LEFT JOIN LATERAL ( SELECT bibl_authorntd.authorntd_author
                       FROM bibl_authorntd
                      WHERE bibl_mainntd.ntd_num = bibl_authorntd.ntd_num
                      ORDER BY bibl_authorntd.authorntd_pnum DESC
                     LIMIT 1) ba ON true
                 LEFT JOIN bibl_textsntd ON bibl_mainntd.ntd_num = bibl_textsntd.ntd_num
                 LEFT JOIN bibl_tematikantd ON bibl_mainntd.ntd_num = bibl_tematikantd.ntd_num
                 LEFT JOIN ( SELECT bibl_tematikantd_1.ntd_num,
                        string_agg(bibl_tematikantd_1.tnum::character varying(50)::text, ', '::text) AS topic_id
                       FROM bibl_tematikantd bibl_tematikantd_1
                      GROUP BY bibl_tematikantd_1.ntd_num
                      ORDER BY bibl_tematikantd_1.ntd_num) bt ON bibl_mainntd.ntd_num = bt.ntd_num
              ORDER BY bibl_mainntd.ntd_pubyear DESC, bibl_mainntd.ntd_num)
            UNION
            ( SELECT DISTINCT 3 AS table_view,
                'book'::character varying(20) AS table_name,
                bibl_mainbook.book_num AS id,
                bibl_mainbook.book_secondname AS type_doc,
                ''::character varying(40) AS number,
                bt.topic_id,
                bibl_mainbook.book_title AS title,
                bibl_mainbook.book_pubyear AS year_published,
                ba.authorbook_author AS developer,
                bibl_mainbook.book_ext AS ftris,
                bibl_mainbook.book_ctplanumik AS contract_number,
                bibl_mainbook.book_ctdatazakik AS date,
                NULL::date AS date_entry,
                NULL::date AS date_approval,
                false AS history,
                NULL::text AS enable
               FROM bibl_mainbook
                 LEFT JOIN LATERAL ( SELECT bibl_authorbook.authorbook_author
                       FROM bibl_authorbook
                      WHERE bibl_mainbook.book_num = bibl_authorbook.book_num
                      ORDER BY bibl_authorbook.authorbook_pnum DESC
                     LIMIT 1) ba ON true
                 LEFT JOIN bibl_tematikabook ON bibl_mainbook.book_num = bibl_tematikabook.book_num
                 LEFT JOIN ( SELECT bibl_tematikabook_1.book_num,
                        string_agg(bibl_tematikabook_1.tnum::character varying(50)::text, ', '::text) AS topic_id
                       FROM bibl_tematikabook bibl_tematikabook_1
                      GROUP BY bibl_tematikabook_1.book_num
                      ORDER BY bibl_tematikabook_1.book_num) bt ON bibl_tematikabook.book_num = bt.book_num
              ORDER BY bibl_mainbook.book_pubyear DESC, bibl_mainbook.book_num)
            UNION
            ( SELECT DISTINCT 4 AS table_view,
                'inform'::character varying(20) AS table_name,
                bibl_maininform.inform_num AS id,
                bibl_stype.stype_type AS type_doc,
                ''::character varying(40) AS number,
                bt.topic_id,
                bibl_maininform.inform_title AS title,
                bibl_maininform.inform_pubyear AS year_published,
                ba.authorinform_author AS developer,
                bibl_maininform.inform_ext AS ftris,
                bibl_maininform.inform_ctplanumik AS contract_number,
                bibl_maininform.inform_ctdatazakik AS date,
                NULL::date AS date_entry,
                NULL::date AS date_approval,
                false AS history,
                NULL::text AS enable
               FROM bibl_maininform
                 LEFT JOIN bibl_stype ON bibl_maininform.stype_num = bibl_stype.stype_num
                 LEFT JOIN LATERAL ( SELECT bibl_authorinform.authorinform_author
                       FROM bibl_authorinform
                      WHERE bibl_maininform.inform_num = bibl_authorinform.inform_num
                      ORDER BY bibl_authorinform.authorinform_pnum DESC
                     LIMIT 1) ba ON true
                 LEFT JOIN ( SELECT bibl_tematikainform.inform_num,
                        string_agg(bibl_tematikainform.tnum::character varying(50)::text, ', '::text) AS topic_id
                       FROM bibl_tematikainform
                      GROUP BY bibl_tematikainform.inform_num
                      ORDER BY bibl_tematikainform.inform_num) bt ON bibl_maininform.inform_num = bt.inform_num
              ORDER BY bibl_maininform.inform_pubyear DESC, bibl_maininform.inform_num)
              ORDER BY 8 DESC, 1, 3;
        ");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bibl_authorbook');
        Schema::dropIfExists('bibl_authorinform');
        Schema::dropIfExists('bibl_authorntd');
        Schema::dropIfExists('bibl_authorntdi');
        Schema::dropIfExists('bibl_keywordbook');
        Schema::dropIfExists('bibl_keywordinform');
        Schema::dropIfExists('bibl_keywordntd');
        Schema::dropIfExists('bibl_keywordntdi');
        Schema::dropIfExists('bibl_mainbook');
        Schema::dropIfExists('bibl_maininform');
        Schema::dropIfExists('bibl_mainntd');
        Schema::dropIfExists('bibl_mainntdi');
        Schema::dropIfExists('bibl_mainpatent');
        Schema::dropIfExists('bibl_primntd');
        Schema::dropIfExists('bibl_primntdi');
        Schema::dropIfExists('bibl_ptpodrazdel');
        Schema::dropIfExists('bibl_ptrazdel');
        Schema::dropIfExists('bibl_rubrikator');
        Schema::dropIfExists('bibl_rubrikbook');
        Schema::dropIfExists('bibl_rubrikinform');
        Schema::dropIfExists('bibl_rubrikntd');
        Schema::dropIfExists('bibl_rubrikntdi');
        Schema::dropIfExists('bibl_rubrikpatent');
        Schema::dropIfExists('bibl_sbelong');
        Schema::dropIfExists('bibl_sgroup');
        Schema::dropIfExists('bibl_sindex');
        Schema::dropIfExists('bibl_stype');
        Schema::dropIfExists('bibl_tematika');
        Schema::dropIfExists('bibl_tematikabook');
        Schema::dropIfExists('bibl_tematikainform');
        Schema::dropIfExists('bibl_tematikantd');
        Schema::dropIfExists('bibl_tematikantdi');
        Schema::dropIfExists('bibl_tempntd');
        Schema::dropIfExists('bibl_textsbook');
        Schema::dropIfExists('bibl_textsinform');
        Schema::dropIfExists('bibl_textsntd');
        Schema::dropIfExists('bibl_textsntdi');
        Schema::dropIfExists('bibl_textsoldntdi');
        Schema::dropIfExists('bibl_textsptntdi');
        Schema::dropIfExists('bibl_tpnntdi');
        DB::statement("DROP VIEW eb_ntd_view");
    }
};
