<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string("email", 250)->nullable();
            $table->string("first_name", 250)->nullable();
            $table->string("middle_name", 250)->nullable();
            $table->string("last_name", 250)->nullable();
            $table->string("position", 250)->nullable();
            $table->string("organization", 250)->nullable();
            $table->string("phone", 20)->nullable();
            $table->string("fax", 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("email");
            $table->dropColumn("first_name");
            $table->dropColumn("middle_name");
            $table->dropColumn("last_name");
            $table->dropColumn("position");
            $table->dropColumn("organization");
            $table->dropColumn("phone");
            $table->dropColumn("fax");
        });
    }
};
