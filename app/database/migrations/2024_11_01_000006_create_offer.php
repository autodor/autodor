<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('offer', function (Blueprint $table) {
            $table->uuid()->unique()->primary();
            $table->bigInteger('user_id')->unsigned();
            $table->string('type', 15);
            $table->foreign('user_id')->references('id')->on('users');
            $table->boolean('approved')->default('false');
            $table->string('organization')->nullable();
            $table->string('lastName')->nullable();
            $table->string('firstName')->nullable();
            $table->string('middleName')->nullable();
            $table->string('position')->nullable();
            $table->string('phoneNumber')->nullable();
            $table->string('leadLastName')->nullable();
            $table->string('leadFirstName')->nullable();
            $table->string('leadMiddleName')->nullable();
            $table->string('leadPosition')->nullable();
            $table->string('leadPhoneNumber')->nullable();
            $table->integer('year')->nullable();
            $table->string('title')->nullable();
            $table->string('customer')->nullable();
            $table->string('basis')->nullable();
            $table->string('workStatus')->nullable();
            $table->string('sourceFinance')->nullable();
            $table->uuid('initDataFile')->nullable();
            $table->uuid('conditionFile')->nullable();
            $table->string('targetWork')->nullable();
            $table->string('infoWork')->nullable();
            $table->uuid('contentWorkFile')->nullable();
            $table->string('finishWork')->nullable();
            $table->string('scopeApplication')->nullable();
            $table->string('integrationWork')->nullable();
            $table->string('rights')->nullable();
            $table->uuid('requirementFile')->nullable();
            $table->string('conditionWork')->nullable();
            $table->string('deadlineWork')->nullable();
            $table->string('subject')->nullable();
            $table->uuid('listReviewerFile')->nullable();
            $table->string('specialConditions')->nullable();
            $table->string('placeWork')->nullable();
            $table->uuid('planWorkFile')->nullable();
            $table->uuid('suggestionFile')->nullable();
            $table->uuid('offersFile')->nullable();
            $table->uuid('niokrDirection')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('offer');
    }
};
