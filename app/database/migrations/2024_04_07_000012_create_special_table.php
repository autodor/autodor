<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('special', function (Blueprint $table) {
            $table->id();
            $table->string("referal", 100);
            $table->integer('year_plan');
            $table->bigInteger('budget_item_id');
            $table->string('name', 500)->nullable();
            $table->unsignedBigInteger('organization_id')->nullable();
            $table->unsignedBigInteger('niokr_id')->nullable();
            $table->date('date_payment')->nullable();
            $table->integer('day_off')->nullable();
            $table->text('detail')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('special');
    }
};
