<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BudgetItemSeeder extends Seeder
{
    private function getArray()
    {
        return [
           ["id"=>"1", "name"=>"Информационное обеспечение"],
           ["id"=>"3", "name"=>"НИОКР"],
        ];
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->getArray() as $row) {
            DB::table('budget_item')->insert($row);
        }
    }
}
