<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegulationTopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('regulation_topic')->insert([
            'id' => 101,
            "name" => '1.1 Нормативный (методический) документ, утвержденный федеральными органами государственной власти (до 1991 года - органами власти Союза ССР)'
        ]);

        DB::table('regulation_topic')->insert([
            'id' => 102,
            "name" => '1.2 Нормативный (методический) документ, разработанный органами государственной власти субъектов Российской Федерации (до 1991 года - органами власти республик Союза ССР), включая региональные стандарты и региональные своды правил'
        ]);

        DB::table('regulation_topic')->insert([
            'id' => 103,
            "name" => '1.3 Прочие документы в сфере дорожного хозяйства'
        ]);

        DB::table('regulation_topic')->insert([
            'id' => 104,
            "name" => '1.4 Информационные и аналитические материалы'
        ]);

        DB::table('regulation_topic')->insert([
            'id' => 105,
            "name" => '1.5 Международные стандарты, региональные стандарты, региональные своды правил, стандарты иностранных государств и своды правил иностранных государств'
        ]);
    }
}
