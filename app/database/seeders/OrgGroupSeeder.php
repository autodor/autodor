<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrgGroupSeeder extends Seeder
{
    private function getArray()
    {
        return [
           ["id" => "1", "name" => "Вузы"],
           ["id" => "2", "name" => "Дорожные организации СНГ"],
           ["id" => "3", "name" => "Дорожные управления и подведомственные организации (по регионам)"],
           ["id" => "4", "name" => "Колледжи и техникумы"],
           ["id" => "5", "name" => "Научные и проектные организации"],
           ["id" => "6", "name" => "Общественные организации"],
           ["id" => "7", "name" => "Прочие организации"],
           ["id" => "8", "name" => "ФГАОУ, находящиеся в ведении ФДА"],
           ["id" => "9", "name" => "Федеральные бюджетные учреждения, подведомственные ФДА"],
           ["id" => "10", "name" => "Филиалы подведомственных организаций ФДА"],
           ["id" => "11", "name" => "ФКУ, подведомственные ФДА"],
           ["id" => "12", "name" => "Центральный аппарат Федерального дорожного агентства"]
        ];
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->getArray() as $row) {
            DB::table('group_org')->insert($row);
        }
    }
}
