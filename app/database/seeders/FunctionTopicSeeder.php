<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FunctionTopicSeeder extends Seeder
{

    private function getArray()
    {
        return [
            [
                'id' => 20100000000,
                'name' => "2.1 Правовое обеспечение деятельности в области дорожного хозяйства"
            ],
            [
                'id' => 20200000000,
                'name' => "2.2 Регламентация порядка разработки нормативных документов, в том числе в области дорожного хозяйства (вопросы технического регулирования)"
            ],
            [
                'id' => 20300000000,
                'name' => "2.3 Классификация, термины и определения, сокращения"
            ],
            [
                'id' => 20400000000,
                'name' => "2.4 Изыскания и проектирование"
            ],
            [
                'id' => 20401000000,
                'name' => "2.4.1 Изыскания автомобильных дорог и искусственных сооружений"
            ],
            [
                'id' => 20401010000,
                'name' => "2.4.1.1 Регламентация проведения изысканий"
            ],
            [
                'id' => 20401020000,
                'name' => "2.4.1.2 Технологии проведения изысканий"
            ],
            [
                'id' => 20401030000,
                'name' => "2.4.1.3 Оборудование для проведения изысканий"
            ],
            [
                'id' => 20402000000,
                'name' => "2.4.2 Проектирование автомобильных дорог и искусственных сооружений"
            ],
            [
                'id' => 20402010000,
                'name' => "2.4.2.1 Проектирование элементов автомобильных дорог"
            ],
            [
                'id' => 20402010100,
                'name' => "2.4.2.1.1 Проектирование земляного полотна автомобильных дорог"
            ],
            [
                'id' => 20402010200,
                'name' => "2.4.2.1.2 Проектирование дорожной одежды"
            ],
            [
                'id' => 20402020000,
                'name' => "2.4.2.2 Проектирование искусственных сооружений"
            ],
            [
                'id' => 20402030000,
                'name' => "2.4.2.3 Общие требования к проектированию"
            ],
            [
                'id' => 20402040000,
                'name' => "2.4.2.4 Технологии проектирования"
            ],
            [
                'id' => 20402050000,
                'name' => "2.4.2.5 Организация и ведение авторского надзора в строительстве"
            ],
            [
                'id' => 20402060000,
                'name' => "2.4.2.6 Требования к оформлению проектной документации"
            ],
            [
                'id' => 20500000000,
                'name' => "2.5 Строительство, реконструкция и капитальный ремонт автомобильных дорог и искусственных сооружений"
            ],
            [
                'id' => 20501000000,
                'name' => "2.5.1 Общие требования"
            ],
            [
                'id' => 20501010000,
                'name' => "2.5.1.1 Организация и технологии строительства"
            ],
            [
                'id' => 20501020000,
                'name' => "2.5.1.2 Организация и производство строительного контроля"
            ],
            [
                'id' => 20501030000,
                'name' => "2.5.1.3 Технологии работ по капитальному ремонту автомобильных дорог"
            ],
            [
                'id' => 20501040000,
                'name' => "2.5.1.4 Контроль качества при проведении капитального ремонта автомобильных дорог"
            ],
            [
                'id' => 20502000000,
                'name' => "2.5.2 Земляное полотно"
            ],
            [
                'id' => 20503000000,
                'name' => "2.5.3 Дорожная одежда"
            ],
            [
                'id' => 20504000000,
                'name' => "2.5.4 Покрытие"
            ],
            [
                'id' => 20505000000,
                'name' => "2.5.5 Дорожно-строительные материалы"
            ],
            [
                'id' => 20505010000,
                'name' => "2.5.5.1 Вяжущие и добавки"
            ],
            [
                'id' => 20505020000,
                'name' => "2.5.5.2 Геосинтетические и композитные материалы"
            ],
            [
                'id' => 20505030000,
                'name' => "2.5.5.3 Прочие материалы для строительства, реконструкции и капитального ремонта дорог и искусственных сооружений"
            ],
            [
                'id' => 20505030100,
                'name' => "2.5.5.3.1 Древесина"
            ],
            [
                'id' => 20505030200,
                'name' => "2.5.5.3.2 Защита от коррозии"
            ],
            [
                'id' => 20505030300,
                'name' => "2.5.5.3.3 Краски, эмали и иные лакокрасочные материалы"
            ],
            [
                'id' => 20505030400,
                'name' => "2.5.5.3.4 Нефтепродукты"
            ],
            [
                'id' => 20505030500,
                'name' => "2.5.5.3.5 Прочие материалы вне классификатора"
            ],
            [
                'id' => 20505040000,
                'name' => "2.5.5.4 Асфальтобетоны"
            ],
            [
                'id' => 20505050000,
                'name' => "2.5.5.5 Бетоны, железобетоны, добавки для бетонов"
            ],
            [
                'id' => 20505060000,
                'name' => "2.5.5.6 Цемент"
            ],
            [
                'id' => 20505070000,
                'name' => "2.5.5.7 Горные породы, песок, щебень, гравий"
            ],
            [
                'id' => 20505080000,
                'name' => "2.5.5.8 Грунты"
            ],
            [
                'id' => 20506000000,
                'name' => "2.5.6 Дорожно-строительная техника"
            ],
            [
                'id' => 20507000000,
                'name' => "2.5.7 Элементы обустройства автомобильных дорог"
            ],
            [
                'id' => 20508000000,
                'name' => "2.5.8 Автоматизированные системы управления дорожным движением"
            ],
            [
                'id' => 20509000000,
                'name' => "2.5.9 Прочее"
            ],
            [
                'id' => 20600000000,
                'name' => "2.6 Эксплуатация автомобильных дорог и искусственных сооружений"
            ],
            [
                'id' => 20601000000,
                'name' => "2.6.1 Ремонт автомобильных дорог и искусственных сооружений"
            ],
            [
                'id' => 20601010000,
                'name' => "2.6.1.1 Общие требования"
            ],
            [
                'id' => 20601020000,
                'name' => "2.6.1.2 Технологии ремонта автомобильных дорог и искусственных сооружений"
            ],
            [
                'id' => 20601030000,
                'name' => "2.6.1.3 Материалы для ремонта автомобильных дорог и искусственных сооружений"
            ],
            [
                'id' => 20601030100,
                'name' => "2.6.1.3.1 Вяжущие и добавки"
            ],
            [
                'id' => 20601030200,
                'name' => "2.6.1.3.2 Геосинтетические и композитные материалы"
            ],
            [
                'id' => 20601030300,
                'name' => "2.6.1.3.3 Прочие материалы для ремонта дорог и искусственных сооружений"
            ],
            [
                'id' => 20601030301,
                'name' => "2.6.1.3.3.1 Древесина"
            ],
            [
                'id' => 20601030302,
                'name' => "2.6.1.3.3.2 Защита от коррозии"
            ],
            [
                'id' => 20601030303,
                'name' => "2.6.1.3.3.3 Краски, эмали и иные лакокрасочные материалы"
            ],
            [
                'id' => 20601030304,
                'name' => "2.6.1.3.3.4 Нефтепродукты"
            ],
            [
                'id' => 20601030305,
                'name' => "2.6.1.3.3.5 Прочие материалы вне классификатора"
            ],
            [
                'id' => 20601030400,
                'name' => "2.6.1.3.4 Асфальтобетоны"
            ],
            [
                'id' => 20601030500,
                'name' => "2.6.1.3.5 Бетоны, железобетоны, добавки для бетонов"
            ],
            [
                'id' => 20601030600,
                'name' => "2.6.1.3.6 Цемент"
            ],
            [
                'id' => 20601030700,
                'name' => "2.6.1.3.7 Горные породы, песок, щебень, гравий"
            ],
            [
                'id' => 20601030800,
                'name' => "2.6.1.3.8 Грунты"
            ],
            [
                'id' => 20601040000,
                'name' => "2.6.1.4 Техника для ремонта автомобильных дорог и искусственных сооружений"
            ],
            [
                'id' => 20601050000,
                'name' => "2.6.1.5 Прочее"
            ],
            [
                'id' => 20602000000,
                'name' => "2.6.2 Содержание автомобильных дорог и искусственных сооружений"
            ],
            [
                'id' => 20602010000,
                'name' => "2.6.2.1 Общие требования"
            ],
            [
                'id' => 20602020000,
                'name' => "2.6.2.2 Технологии проведения работ по содержанию"
            ],
            [
                'id' => 20602030000,
                'name' => "2.6.2.3 Материалы для содержания автомобильных дорог и искусственных сооружений"
            ],
            [
                'id' => 20602030100,
                'name' => "2.6.2.3.1 Вяжущие и добавки"
            ],
            [
                'id' => 20602030200,
                'name' => "2.6.2.3.2 Прочие материалы для содержания дорог и искусственных сооружений"
            ],
            [
                'id' => 20602030201,
                'name' => "2.6.2.3.2.1 Защита от коррозии"
            ],
            [
                'id' => 20602030202,
                'name' => "2.6.2.3.2.2 Краски, эмали и иные лакокрасочные материалы"
            ],
            [
                'id' => 20602030203,
                'name' => "2.6.2.3.2.3 Прочие материалы вне классификатора"
            ],
            [
                'id' => 20602030300,
                'name' => "2.6.2.3.3 Асфальтобетоны"
            ],
            [
                'id' => 20602030400,
                'name' => "2.6.2.3.4 Горные породы, песок, щебень, гравий"
            ],
            [
                'id' => 20602040000,
                'name' => "2.6.2.4 Требования к транспортно-эксплуатационному состоянию автомобильных дорог"
            ],
            [
                'id' => 20602050000,
                'name' => "2.6.2.5 Техника для содержания автомобильных дорог и искусственных сооружений"
            ],
            [
                'id' => 20602060000,
                'name' => "2.6.2.6 Прочее"
            ],
            [
                'id' => 20700000000,
                'name' => "2.7 Лаборатории, приборы, оборудование"
            ],
            [
                'id' => 20701000000,
                'name' => "2.7.1 Методы испытаний"
            ],
            [
                'id' => 20702000000,
                'name' => "2.7.2 Методы контроля"
            ],
            [
                'id' => 20703000000,
                'name' => "2.7.3 Лабораторное и испытательное оборудование, оснащение лабораторий, расходные материалы"
            ],
            [
                'id' => 20800000000,
                'name' => "2.8 Оценка состояния (транспортно-эксплуатационные качества)"
            ],
            [
                'id' => 20900000000,
                'name' => "2.9 Безопасность дорожного движения, технологии и средства организации дорожного движения"
            ],
            [
                'id' => 21000000000,
                'name' => "2.10 Кадровое обеспечение, безопасность и охрана труда"
            ],
            [
                'id' => 21100000000,
                'name' => "2.11 Охрана окружающей среды"
            ],
            [
                'id' => 21200000000,
                'name' => "2.12 Управление, развитие дорожной сети, планирование, учет и анализ экономика дорожного хозяйства"
            ],
            [
                'id' => 21300000000,
                'name' => "2.13 Ценообразование и сметное нормирование в дорожном хозяйстве"
            ],
            [
                'id' => 21301000000,
                'name' => "2.13.1 Федеральные и отраслевые нормативы"
            ],
            [
                'id' => 21301010000,
                'name' => "2.13.1.1 Элементные сметные нормативы"
            ],
            [
                'id' => 21301020000,
                'name' => "2.13.1.2 Укрупненные сметные нормативы"
            ],
            [
                'id' => 21301030000,
                'name' => "2.13.1.3 Отраслевые элементные сметные нормативы"
            ],
            [
                'id' => 21301040000,
                'name' => "2.13.1.4 Отраслевые укрупненные сметные нормативы"
            ],
            [
                'id' => 21301050000,
                'name' => "2.13.1.5 Методические документы"
            ],
            [
                'id' => 21302000000,
                'name' => "2.13.2 Территориальные сметные нормативы"
            ],
            [
                'id' => 21302010000,
                'name' => "2.13.2.1 Территориальные элементные сметные нормативы"
            ],
            [
                'id' => 21302020000,
                'name' => "2.13.2.2 Территориальные укрупненные сметные нормативы"
            ],
            [
                'id' => 21303000000,
                'name' => "2.13.3 Прочие нормативы"
            ],
            [
                'id' => 21303010000,
                'name' => "2.13.3.1 Индивидуальные сметные нормативы"
            ],
            [
                'id' => 21303020000,
                'name' => "2.13.3.2 Информационные, разъяснительные документы"
            ],
            [
                'id' => 21400000000,
                'name' => "2.14 Диспетчеризация и связь в дорожном хозяйстве"
            ],
            [
                'id' => 21500000000,
                'name' => "2.15 Организация инновационной деятельности"
            ],
            [
                'id' => 21600000000,
                'name' => "2.16 Профилактика чрезвычайных ситуаций"
            ],
            [
                'id' => 21700000000,
                'name' => "2.17 Транспортная безопасность"
            ],
            [
                'id' => 21800000000,
                'name' => "2.18 Информационное обеспечение, создание и ведение баз данных, программное обеспечение баз данных, автоматизированные системы управления дорожным движением"
            ],
            [
                'id' => 21900000000,
                'name' => "2.19 Система менеджмента качества в дорожном хозяйстве, подтверждение соответствия"
            ],
            [
                'id' => 21901000000,
                'name' => "2.19.1 Показатели качества используемого оборудования"
            ],
            [
                'id' => 22000000000,
                'name' => "2.20 Объекты дорожного сервиса, производственные объекты"
            ],
            [
                'id' => 22100000000,
                'name' => "2.21 Документы смежных дорожному хозяйству областей"
            ]
        ];
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->getArray() as $row) {
            DB::table('function_topic')->insert($row);
        }
    }
}
