<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventTypeSeeder extends Seeder
{
    private function getArray()
    {
        return [
           ["id"=>"1", "name"=>"ПНСТ"],
           ["id"=>"2", "name"=>"ГОСТ"],
           ["id"=>"3", "name"=>"ГОСТ Р"],
           ["id"=>"4", "name"=>"ОДМ"],
        ];
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->getArray() as $row) {
            DB::table('events_type')->insert($row);
        }
    }
}
