const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
  devServer: {
    client: {
      webSocketURL: 'wss://avtodor.local:3000/wss',
    },
    host: '0.0.0.0',
    allowedHosts: 'all',
  },
  transpileDependencies: true
})

