/* Custom filtering function which will search data in column four between two values */
// $.fn.dataTable.ext.search.push(
//     function(settings, data, dataIndex) {
//         var min = parseInt($('#min').val(), 10);
//         var max = parseInt($('#max').val(), 10);
//         var age = parseFloat(data[0]) || 0; // use data for the age column

//         if ((isNaN(min) && isNaN(max)) ||
//             (isNaN(min) && age <= max) ||
//             (min <= age && isNaN(max)) ||
//             (min <= age && age <= max)) {
//             return true;
//         }
//         return false;
//     }
// );

// function activaTab(tab) {
//     $('.nav-tabs a[href="#' + tab + '"]').tab('show');
// };

$(document).ready(function() {
    // $("#DataTable_main").DataTable({
    //     "ajax": '../ajax.txt'
    // });
    // $(function() { // Узнаем текущий год
    //     $(".curr-year").text( (new Date).getFullYear() );
    // });

    // Меняет цветовую тему сайта
    $('#color-change').bind("click", function() {
        if($("#page-theme").attr("href") == "../css/blue-style.css"){
            $("#page-theme").attr("href","../css/green-style.css");
        } else if($("#page-theme").attr("href") == "../css/green-style.css"){
            $("#page-theme").attr("href","../css/gray-style.css");
        } else if($("#page-theme").attr("href") == "../css/gray-style.css"){
            $("#page-theme").attr("href","../css/blue-style.css");
        }
    });

    // Решает проблему при переключении вкладок с fixedheader и scroller
    $('a[data-bs-toggle="tab"]').on('hide.bs.tab', function(e) {
        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw();
        // $($.fn.dataTable.tables(true)).css('width', '100%');
        // $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
    });

    // Решает проблему при изменении размера окна с fixedheader и scroller
    $(window).on('resize', function() {
        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw();
    });

    // Решает проблему при переключении страниц в таблице с fixedheader и scroller
    // $('a[data-bs-toggle="tab"]').on('page.dt', function() {
    //     $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw();
    // });

    // Скрипт спойлера
    // $('.show_text').on('click', function() {
    //     if ($(this).prev().is(':visible')) {
    //         $(this).text('<...>');
    //         $(this).prev().hide('slow');
    //     } else {
    //         $(this).text('^');
    //         $(this).prev().show('slow');
    //     }
    // });

    // Спойлер
    // $('.spoiler-body').hide();
    // $('.spoiler-title').click(function() {
    //     $(this).bs-toggleClass('opened').bs-toggleClass('closed').prev().slidebs-toggle();
    //     if ($(this).hasClass('opened')) {
    //         $(this).html('&nbsp;скрыть');
    //     } else {
    //         $(this).html('&nbsp;подробнее...');
    //     }
    // });
    // Спойлер

    var hash = window.location.hash; // откроем нужную вкладку по внешней ссылке
    hash && $('ul.nav-pills a[href="' + hash + '"]').tab('show');

    // сделаем переключение вкладок ссылками с классом tab_changer
    $('.tab_changer').on('click', function() {
        var hash = $(this).attr("href");
        hash && $('ul.nav-tabs a[href="' + hash + '"]').tab('show');
    });

    // Активируем вкладки Bootstrap
    $('a[data-bs-toggle="tab"]').on('shown.bs.tab', function(e) {
        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    });

    // $('#searchInput').AddXbutton({});

    // hide #back-top first
    $("#back-top").hide();

    // fade in #back-top
    $(function() {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-top a').click(function() {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

    // Активируем всплывающие окна Bootstrap
    // $('[data-bs-toggle="popover"]').popover();
    var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
    var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
      return new bootstrap.Popover(popoverTriggerEl)
    })
    
    $(".loader").fadeOut("slow");

    // Активируем подсказки Bootstrap
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
      return new bootstrap.Tooltip(tooltipTriggerEl)
    })    
    // $('[data-bs-toggle="tooltip"]').tooltip({
    //     animated: 'fade',
    //     // placement: 'bottom',
    //     html: true
    // });
});

// FIX froozen multi modal for bootstrap
$(document).on('hidden.bs.modal', '.modal', function () {
    $('.modal:visible').length && $(document.body).addClass('modal-open');
});