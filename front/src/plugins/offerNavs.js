export default [
    {
        id: 'offer-ca-fda',
        component: '@/components/offer/OfferCdFda',
        img: '/img/newdesign/icons_nto-46.png',
        title: 'ПРЕДЛОЖЕНИЯ В ПЛАН НИОКР (ЦА ФДА)',
        guest: true
    },
    {
        id: 'offer-organization',
        component: '@/components/offer/OfferOrganization',
        img: '/img/newdesign/icons_nto-46.png',
        title: 'ПРЕДЛОЖЕНИЯ В ПЛАН НИОКР (от организаций)',
        guest: true
    },
    {
        id: 'offer-review',
        component: '@/components/offer/OfferReview',
        img: '/img/newdesign/icons_nto-50.png',
        title: 'РАССМОТРЕНИЕ ПРЕДЛОЖЕНИЙ В ПЛАН НИОКР',
        guest: true
    },
    {
        id: 'offer-list',
        component: '@/components/offer/OfferList',
        img: '/img/newdesign/icons_nto-50.png',
        title: 'ПЕРЕЧЕНЬ НИОКР',
        guest: true
    },
]
