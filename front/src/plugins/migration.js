import privateApi from "@/plugins/private";

const urlIBase = '/api/migration/proxy';
const urlPg = '/api/migration/read';
const urlPgInsert = '/api/migration/insert';
const urlPgTruncate = '/api/migration/truncate';
const urlPgCount = '/api/migration/count/';
export const migrationCount = 100

export const checkFields = (ib, pg) => {
    if (ib.length !== pg.length) return false
    for (let i = 0; i < ib.length; i++) {
        if (ib[i].name !== pg[i].name) return false
        if (!checkType(ib[i].type, ib[i].size, pg[i].type, pg[i].size)) return false
    }
    return true
}

export const checkType = (ibType, ibSize, pgType, pgSize) => {
    if (ibType === 'integer' && ibSize === 4 && pgType === 'int4') return true
    if (ibType === 'smallint' && ibSize === 2 && pgType === 'int2') return true
    if (ibType === 'varchar' && pgType === 'varchar' && ibSize === pgSize) return true
    if (ibType === 'char' && pgType === 'varchar' && ibSize === pgSize) return true
    if ((ibType === 'char' || ibType === 'varchar') && ibSize === 1 && pgType === 'bool') return true
    if (ibType === 'date' && pgType === 'date') return true
    if (ibType === 'blob' && pgType === 'text') return true
}

export const typeFieldToStr = (filedType) => {
    let typeField = [];
    typeField[261] = 'blob'
    typeField[14] = 'char'
    typeField[40] = 'cstring'
    typeField[11] = 'd_float'
    typeField[27] = 'double'
    typeField[10] = 'float'
    typeField[16] = 'int64'
    typeField[8] = 'integer'
    typeField[9] = 'quad'
    typeField[7] = 'smallint'
    typeField[12] = 'date'
    typeField[13] = 'time'
    typeField[35] = 'timestamp'
    typeField[37] = 'varchar'
    return typeField[filedType]
}

export const createItem = (title, id) => {
    return {
        id,
        title,
        isPresent: true,
        checkFields: false,
        fieldsIb: [],
        fieldsPg: [],
        sizeIb: -1,
        sizePg: -1,
        viewFields: false
    }
}

export const createField = (name, type, size) => {
    return {name, type, size}
}

export const sqlIbStructure = `
    select R.RDB$RELATION_NAME  as TABLE_NAME,
           R.RDB$FIELD_POSITION as POSITION_FIELD,
           R.RDB$FIELD_NAME     as FIELD_NAME,
           F.RDB$FIELD_LENGTH   as FIELD_LENGTH,
           F.RDB$FIELD_TYPE     as FIELD_TYPE,
           F.RDB$FIELD_SCALE,
           F.RDB$FIELD_SUB_TYPE
    from RDB$FIELDS F,
         RDB$RELATION_FIELDS R
    where F.RDB$FIELD_NAME = R.RDB$FIELD_SOURCE
      and R.RDB$SYSTEM_FLAG = 0
    order by R.RDB$RELATION_NAME, R.RDB$FIELD_POSITION;`
export const sqlPgStructure = (db) => {
    return `
        select table_name, column_name, udt_name, character_maximum_length
        from information_schema.columns
        where table_name like '${db.toLowerCase()}_%'
        order by table_name, ordinal_position;`
}

export const readIb = async (table, db, id, count = false) => {
    let request = {
        'table': table,
        'db': db,
        'count': count ? '1' : null,
        'id': id,
        'limit': migrationCount
    };
    return (await privateApi.post(urlIBase, request)).data
}

export const readPg = async (sqlString) => {
    const data = {
        sql: sqlString
    }
    return (await privateApi.post(urlPg, data)).data
}
export const insertPg = async (table, data) => {
    const insert = {
        table,
        data: JSON.stringify(data)
    }
    return (await privateApi.post(urlPgInsert, insert)).data
}

export const truncatePg = async (table) => {
    const data = {table}
    return (await (privateApi.post(urlPgTruncate, data))).data
}

export const countPg = async (table) => {
    return (await (privateApi.get(urlPgCount + table))).data
}
