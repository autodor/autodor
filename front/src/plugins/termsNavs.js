export default [
    {
        id: 'terms',
        component: '@/components/terms/TheTerms',
        img: '/img/newdesign/icons_nto-47.png',
        title: 'ПОИСК ПО ТЕРМИНАМ И ОПРЕДЕЛЕНИЯМ',
        guest: true
    },
    {
        id: 'docs',
        component: '@/components/search/nioktr/TheNioktrSearch',
        img: '/img/newdesign/icons_nto-48.png',
        title: 'ПОИСК ТЕРМИНОВ, ОПИСАННЫХ В ДОКУМЕНТАХ',
        guest: true
    }
]
