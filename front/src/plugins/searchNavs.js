export default [
    {
        id: 'global',
        component: '@/components/search/global/TheSearchGlobal',
        img: '/img/newdesign/icons_nto-45.png',
        title: 'ГЛОБАЛЬНЫЙ'
    },
    {
        id: 'nioktr',
        component: '@/components/search/nioktr/TheNioktrSearch',
        img: '/img/newdesign/icons_nto-46.png',
        title: 'НИОКТР'
    },
    {
        id: 'eb-ntd',
        component: '@/components/search/eb-ntd/TheEbNtdSearch',
        img: '/img/newdesign/icons_nto-47.png',
        title: 'ЭБ НТД'
    },
    {
        id: 'eb-patents',
        component: '@/components/search/eb-patents/TheEbPatentsSearch',
        img: '/img/newdesign/icons_nto-48.png',
        title: 'ЭБ ПАТЕНТЫ'
    },
    {
        id: 'ats-dh',
        component: '@/components/search/ats-dh/TheAtsDhSearch',
        img: '/img/newdesign/icons_nto-49.png',
        title: 'АТС ДХ',
        access: ['Admin', 'Operator']
    },
    {
        id: 'special',
        component: '@/components/search/special/TheSpecialSearch',
        img: '/img/newdesign/icons_nto-50.png',
        title: 'СПЕЦИАЛЬНЫЙ ПОИСК',
        access: ['Admin', 'Operator', 'UNTIiIT', 'Special']
    },
    {
        id: 'tender',
        component: '@/components/search/tender/TheTenderSearch',
        img: '/img/newdesign/icons_nto-51.png',
        title: 'КОНКУРС',
        access: ['Admin', 'Operator', 'UNTIiIT', 'Special', 'FKU']
    },
    {
        id: 'information',
        component: '@/components/search/information/TheInformationSearch',
        img: '/img/newdesign/icons_nto-52.png',
        title: 'СПРАВКИ',
        access: ['Admin', 'Operator', 'UNTIiIT']
    },
    {
        id: 'events',
        component: '@/components/search/events/TheEventsSearch',
        img: '/img/newdesign/icons_nto-53.png',
        title: 'ИНДИКАТОР СОБЫТИЙ',
        access: ['Admin', 'Operator', 'UNTIiIT'],
        count: true
    }
]
