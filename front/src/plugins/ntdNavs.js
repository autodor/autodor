export default [
    {
        id: 'input-ntd',
        component: '@/components/ntd/FindNtd',
        img: '/img/newdesign/icons_nto-46.png',
        title: 'ВВОД ДАННЫХ (ФКУ)',
        guest: true
    },
    {
        id: 'find-ntd',
        component: '@/components/ntd/InputNtd',
        img: '/img/newdesign/icons_nto-50.png',
        title: 'РЕЗУЛЬТАТ ПОИСКА ПО ВНЕДРЕНИЮ НТД',
        guest: true
    },
]
