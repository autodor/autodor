import axios from "axios";
import typesDoc from '../json/types-doc.json'
import customers from '../json/customers.json'
import directions from '../json/directions.json'
import directionsDh from '../json/directions-dh.json'
import technologies from '../json/technologies.json'
import criticalTechnologies from '../json/critical-technologies.json'
import priorityTechnologies from '../json/priority-technologies.json'
import niokrDirection from '../json/niokr-direction.json'

//
// const url = '/json/'
const urlDictionary = '/api/dictionary/'

const currentYear = new Date().getFullYear();
let currentListYear = [];
for (let i = 1999; i < currentYear; i++) {
    currentListYear.push(i + 1);
}

async function loadDictionary() {
    const response = await axios
        .post(urlDictionary)
        .then(
            res => {
                return res.data;
            }
        )
    return response;
}

const listDictionaries = await loadDictionary();
const topics = await listDictionaries.topics;
const mpkChapter = await listDictionaries.mpkChapter;
const mpkSubsection = await listDictionaries.mpkSubsection;
const budgetItem = await listDictionaries.budgetItem;

// function camelToUnderscore(key) {
//     var result = key.replace(/([A-Z])/g, " $1");
//     return result.split(' ').join('-').toLowerCase();
// }
//
// async function load(list) {
//     const response = await axios
//         .get(url + camelToUnderscore(list) + '.json')
//         .then(
//             res => {
//                 return res.data.items;
//             }
//         )
//     return response;
// }

//
// const topics = (await loadDictionary('topics')).map(item => {
//     return {
//         id: item.id,
//         name: item.code + ' ' + item.name,
//         type: item.type
//     }
// });

export default {
    currentListYear,
    // admUnits: await load('admUnits'),
    // organizationGroups: await loadDictionary('organization-groups'),
    // officePositions: await loadDictionary('positions'),
    typesDoc: typesDoc.items,
    // qualifierStandard: await load('qualifierStandard'),
    topics,
    budgetItem,
    // qualifiers: topics.filter(item => item.type === 2),
    // regulations: topics.filter(item => item.type === 1 && item.name.charAt(0) == 1),
    // functions: topics.filter(item => item.type === 1 && item.name.charAt(0) == 2),
    mpkChapter,
    mpkSubsection,
    // districts: await loadDictionary('district'),
    niokrDirections: niokrDirection.items,
    customers: customers.items,
    technologies: technologies.items,
    directions: directions.items,
    directionsDh: directionsDh.items,
    priorityTechnologies: priorityTechnologies.items,
    criticalTechnologies: criticalTechnologies.items
}
