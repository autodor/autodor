Для запуска приложения необходимо собрать контейнеры

``docker compose build``

Потом запустить приложение 

``docker compose up -d``

Установить миграции

``docker compose exec laravel php artisan migrate``

И загрузить первоначальные данные

``docker compose exec laravel php artisan db:seed --class=UserSeeder``

И загрузить информацию о файлах

``docker compose exec laravel php artisan app:read-file-dir``